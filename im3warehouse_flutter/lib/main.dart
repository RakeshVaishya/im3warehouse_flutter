import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/Lager.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/views/home/home.dart';
import 'package:im3_warehouse/views/landing_page/landing_page.dart';
import 'package:im3_warehouse/views/login/login_main.dart';
import 'package:path_provider/path_provider.dart';

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  var docsDir = await _getDocsDir();
  String canonFilename = '$docsDir/$_logFilename';
  await Lager.initializeLogging(canonFilename);
  await Lager.log('ENTERED main() ...');

  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  String comapnyCode = "";
  String userId = "";
  String url = "";

  await Lager.log('Line 1 ...');

  try {
    comapnyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
    print("Company code----->" + comapnyCode);
     } catch (e) {
    print(e);

     }

  await Lager.log('Line 2 ...');

  try {
    userId =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    print("userId ----->" + userId);
  } catch (e) {
    print(e);
    
  }

  await Lager.log('Line 3 ...');

  try {
    url = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.MOBILE_URL);
  } catch (e) {
    print(e);
  }


  await Lager.log('Line 4 ...');
  if (url != null && url.length > 0)
  {
    NetworkConfig.BASE_URL = url;
    print("Base Url--->" + NetworkConfig.BASE_URL);
  } else {
    await Lager.log('Line 5 ...');
  }
  if (comapnyCode != null && comapnyCode.length > 0)
  {
    await Lager.log('Line 6 ...');
    if( userId != null && userId.length > 0)
    {
      runApp(HomePage());
      return;
    }
    else {
      runApp(Login());
      return;
    }
  }
  else {
    runApp(LandingPage());
    return;
  }
}

 Future<String> _getDocsDir() async {
   final directory = await getApplicationDocumentsDirectory();
   return directory.path;
 }

 var _logFilename = 'back_to_now.txt';
