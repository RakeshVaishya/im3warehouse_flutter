

import 'package:im3_warehouse/models/reverse_by_po/po_item_detail.dart';

class ReverseReceiveItem  extends POItemDetails {
  String displayDateReceived;
  String iNC;
  String iPOReceiverNo;
  String iReleaseNo;
  String issueUOMQuantity;
  String itemRate;
  String pOReceiverNo;
  String quantityReceived;
  String quantityReturned;
  String releaseNo;
  String releaseQuantity;

  ReverseReceiveItem.fromJson(Map<String, dynamic> json) {
    companyCode = json['Company_Code'];
    displayDateReceived = json['Display_Date_Received'];
    iNC = json['INC'];
    iPartNo = json['IPart_No'];
    iPONo = json['IPO_No'];
    iPOReceiverNo = json['IPO_Receiver_No'];
    iReleaseNo = json['IRelease_No'];
    issueUOMOrderQuantity = json['Issue_UOM_Order_Quantity'];
    issueUOMQuantity = json['Issue_UOM_Quantity'];
    itemRate = json['Item_Rate'];
    lineItemSrNo = json['Line_Item_Sr_No'];
    pOReceiverNo = json['PO_Receiver_No'];
    quantityReceived = json['Quantity_Received'];
    quantityReturned = json['Quantity_Returned'];
    releaseNo = json['Release_No'];
    releaseQuantity = json['Release_Quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Company_Code'] = this.companyCode;
    data['Display_Date_Received'] = this.displayDateReceived;
    data['INC'] = this.iNC;
    data['IPart_No'] = this.iPartNo;
    data['IPO_No'] = this.iPONo;
    data['IPO_Receiver_No'] = this.iPOReceiverNo;
    data['IRelease_No'] = this.iReleaseNo;
    data['Issue_UOM_Order_Quantity'] = this.issueUOMOrderQuantity;
    data['Issue_UOM_Quantity'] = this.issueUOMQuantity;
    data['Item_Rate'] = this.itemRate;
    data['Line_Item_Sr_No'] = this.lineItemSrNo;
    data['PO_Receiver_No'] = this.pOReceiverNo;
    data['Quantity_Received'] = this.quantityReceived;
    data['Quantity_Returned'] = this.quantityReturned;
    data['Release_No'] = this.releaseNo;
    data['Release_Quantity'] = this.releaseQuantity;
    return data;
  }
}
