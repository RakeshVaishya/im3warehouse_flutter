import 'package:im3_warehouse/base/BaseModel.dart';
import 'package:im3_warehouse/models/part_details.dart';
class  IssueByPickListModel extends PartDetails{
  String allowShippement;
  String canBeIssued;
  String documentNo;
  String iInvPickingHeaderId;
  String iNVActionCode;
  String iSalesOrderLineItem;
  String iSalesOrderNo;
  String isConfirmPick;
  String isSerializedPart;
  String issuedQuantity;
  String itemStatus;
  String iTransferRequestID;
  String iWONo;
  String orderQuantity;
  String parentILineItem;
  String partSrNo ;
  String pickLocation;
  String pickTicketNo;
  String quantityLeftToIssue;
  String roomAreaCode;
  String serialNo;
  String sOPalletNo;
  String stockSrNo;
  String iRoomCode;

  String  iFacilityCode;

  IssueByPickListModel(
  {this.allowShippement,
  this.canBeIssued,
  this.documentNo,
  this.iInvPickingHeaderId,
  this.iNVActionCode,
  this.iSalesOrderLineItem,
  this.iSalesOrderNo,
  this.isConfirmPick,
  this.isSerializedPart,
  this.issuedQuantity,
  this.itemStatus,
  this.iTransferRequestID,
  this.iWONo,
  this.orderQuantity,
  this.parentILineItem,
  this.partSrNo,
  this.pickLocation,
  this.pickTicketNo,
  this.quantityLeftToIssue,
  this.roomAreaCode,
  this.serialNo,
  this.sOPalletNo,
  this.stockSrNo,
  this.iRoomCode});

  IssueByPickListModel.fromJson(Map<String, dynamic> json) {
  allowShippement = json['Allow_Shippement'];
  canBeIssued = json['canBeIssued'];
  companyCode= json['CompanyCode'];
  documentNo = json['documentNo'];
  iInvPickingHeaderId = json['IInvPickingHeaderId'];
  iInvTypeCode = json['IInvTypecode'];
  iNVActionCode = json['INV_Action_Code'];
  iPartNo = json['IPartNo'];
  iSalesOrderLineItem = json['ISalesOrderLineItem'];
  iSalesOrderNo = json['ISalesOrderNo'];
  isConfirmPick = json['IsConfirmPick'];
  isSerializedPart = json['IsSerializedPart'];
  issuedQuantity = json['IssuedQuantity'];
  itemStatus = json['ItemStatus'];
  iTransferRequestID = json['ITransferRequestID'];
  iWONo = json['IWONo'];
  orderQuantity = json['OrderQuantity'];
  parentILineItem = json['ParentILineItem'];
  partDescription = json['Part_Description'];
  PartNo = json['Part_No'];
  partSrNo = json['PartSrNo'];
  pickLocation = json['PickLocation'];
  pickTicketNo = json['PickTicketNo'];
  quantityLeftToIssue = json['QuantityLeftToIssue'];
  roomAreaCode = json['Room_Area_Code'];
  serialNo = json['SerialNo'];
  sOPalletNo = json['SOPalletNo'];
  stockSrNo = json['StockSrNo'];
  iRoomCode=  json["IRoomCode"];
  iFacilityCode=  json["IFacilityCode"];


  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['Allow_Shippement'] = this.allowShippement;
  data['canBeIssued'] = this.canBeIssued;
  data['CompanyCode'] = this.companyCode;
  data['documentNo'] = this.documentNo;
  data['IInvPickingHeaderId'] = this.iInvPickingHeaderId;
  data['IInvTypecode'] = this.iInvTypeCode;
  data['INV_Action_Code'] = this.iNVActionCode;
  data['IPartNo'] = this.iPartNo;
  data['ISalesOrderLineItem'] = this.iSalesOrderLineItem;
  data['ISalesOrderNo'] = this.iSalesOrderNo;
  data['IsConfirmPick'] = this.isConfirmPick;
  data['IsSerializedPart'] = this.isSerializedPart;
  data['IssuedQuantity'] = this.issuedQuantity;
  data['ItemStatus'] = this.itemStatus;
  data['ITransferRequestID'] = this.iTransferRequestID;
  data['IWONo'] = this.iWONo;
  data['OrderQuantity'] = this.orderQuantity;
  data['ParentILineItem'] = this.parentILineItem;
  data['Part_Description'] = this.partDescription;
  data['Part_No'] = this.PartNo;
  data['PartSrNo'] = this.partSrNo;
  data['PickLocation'] = this.pickLocation;
  data['PickTicketNo'] = this.pickTicketNo;
  data['QuantityLeftToIssue'] = this.quantityLeftToIssue;
  data['Room_Area_Code'] = this.roomAreaCode;
  data['SerialNo'] = this.serialNo;
  data['SOPalletNo'] = this.sOPalletNo;
  data['StockSrNo'] = this.stockSrNo;
  data['IRoomCode'] = this.iRoomCode;
  data['IFacilityCode'] = this.iFacilityCode;


  return data;
  }

}