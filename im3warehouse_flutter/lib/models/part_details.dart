import 'package:im3_warehouse/models/add_edit_inventory_model.dart';

class PartDetails extends Inventory{

  String active;
  String billingType;
  String billingTypeCode;
  String billingTypeDescription;
  String billingX0020TypeX0020Code;
  String billingX0020TypeX0020Description;
  String classCode;
  String companyCode;
  String costPerUnit;
  String expirationDate;
  String facilityCode;
  String facilityName;
  String iInvTypeCode;
  String invTypeDescription;
  String iPartNo;
  String manufacturerCode;
  String manufacturerName;
  String maximumQuantity;
  String minimumQuantity;
  String onHandQuantity;
  String partDescription;
  String primaryVendor;
  String primaryVendorName;
  String rate;
  String roomAreaCode;
  String roomAreaName;
  String roomCode;
  String roomDescription;
  String serialNumber;
  String status;
  String uOMCode;
  String uOMDescription;
  String vendorLotNumber;
  String isVendorLotNumberExist;
  String isExpireDateExist;
  int physicalCount;
  String binLocation;
  String caseSize;
  String actionCode;

  PartDetails(
  {this.active,
  this.billingType,
  this.billingTypeCode,
  this.billingTypeDescription,
  this.billingX0020TypeX0020Code,
  this.billingX0020TypeX0020Description,
  this.classCode,
  this.companyCode,
  this.costPerUnit,
  this.expirationDate,
  this.facilityCode,
  this.facilityName,
  this.iInvTypeCode,
  this.invTypeDescription,
  this.iPartNo,
  this.manufacturerCode,
  this.manufacturerName,
  this.maximumQuantity,
  this.minimumQuantity,
  this.onHandQuantity,
  this.partDescription,
  this.primaryVendor,
  this.primaryVendorName,
  this.rate,
  this.roomAreaCode,
  this.roomAreaName,
  this.roomCode,
  this.roomDescription,
  this.serialNumber,
  this.status,
  this.uOMCode,
  this.uOMDescription,
  this.vendorLotNumber});

  PartDetails.fromJson(Map<String, dynamic> json) {
  active = json['Active'];
  billingType = json['Billing_Type'];
  billingTypeCode = json['Billing_Type_Code'];
  billingTypeDescription = json['Billing_Type_Description'];
  billingX0020TypeX0020Code = json['Billing_x0020_Type_x0020_Code'];
  billingX0020TypeX0020Description =
  json['Billing_x0020_Type_x0020_Description'];
  classCode = json['Class_Code'];
  companyCode = json['Company_Code'];
  costPerUnit = json['Cost_Per_Unit'];
  DateAdded = json['Date_Added'];
  isExpireDateExist = json['Expiration_Date'];
  facilityCode = json['Facility_Code'];
  facilityName = json['Facility_Name'];
  IBillingTypeCode = json['IBilling_Type_Code'];
  iInvTypeCode = json['IInv_Type_Code'];
  invTypeDescription = json['Inv_Type_Description'];
  iPartNo = json['IPart_No'];
  IUOMCode = json['IUOM_Code'];
  manufacturerCode = json['Manufacturer_Code'];
  manufacturerName = json['Manufacturer_Name'];
  maximumQuantity = json['Maximum_Quantity'];
  minimumQuantity = json['Minimum_Quantity'];
  onHandQuantity = json['On_Hand_Quantity'];
  partDescription = json['Part_Description'];
  PartNo = json['Part_No'];
  primaryVendor = json['Primary_Vendor'];
  primaryVendorName = json['Primary_Vendor_Name'];
  rate = json['Rate'];
  roomAreaCode = json['Room_Area_Code'];
  roomAreaName = json['Room_Area_Name'];
  roomCode = json['Room_Code'];
  roomDescription = json['Room_Description'];
  isSerailNumber = json['Serial_Number'];
  status = json['Status'];
  uOMCode = json['UOM_Code'];
  uOMDescription = json['UOM_Description'];
  isVendorLotNumberExist = json['Vendor_Lot_Number'];
  Rate =json['Rate'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['Active'] = this.active;
  data['Billing_Type'] = this.billingType;
  data['Billing_Type_Code'] = this.billingTypeCode;
  data['Billing_Type_Description'] = this.billingTypeDescription;
  data['Billing_x0020_Type_x0020_Code'] = this.billingX0020TypeX0020Code;
  data['Billing_x0020_Type_x0020_Description'] =
  this.billingX0020TypeX0020Description;
  data['Class_Code'] = this.classCode;
  data['Company_Code'] = this.companyCode;
  data['Cost_Per_Unit'] = this.costPerUnit;
  data['Date_Added'] = this.DateAdded;
  data['Expiration_Date'] = this.expirationDate;
  data['Facility_Code'] = this.facilityCode;
  data['Facility_Name'] = this.facilityName;
  data['IBilling_Type_Code'] = this.IBillingTypeCode;
  data['IInv_Type_Code'] = this.iInvTypeCode;
  data['Inv_Type_Description'] = this.invTypeDescription;
  data['IPart_No'] = this.iPartNo;
  data['IUOM_Code'] = this.IUOMCode;
  data['Manufacturer_Code'] = this.manufacturerCode;
  data['Manufacturer_Name'] = this.manufacturerName;
  data['Maximum_Quantity'] = this.maximumQuantity;
  data['Minimum_Quantity'] = this.minimumQuantity;
  data['On_Hand_Quantity'] = this.onHandQuantity;
  data['Part_Description'] = this.partDescription;
  data['Part_No'] = this.PartNo;
  data['Primary_Vendor'] = this.primaryVendor;
  data['Primary_Vendor_Name'] = this.primaryVendorName;
  data['Rate'] = this.rate;
  data['Room_Area_Code'] = this.roomAreaCode;
  data['Room_Area_Name'] = this.roomAreaName;
  data['Room_Code'] = this.roomCode;
  data['Room_Description'] = this.roomDescription;
  data['Serial_Number'] = this.serialNumber;
  data['Status'] = this.status;
  data['UOM_Code'] = this.uOMCode;
  data['UOM_Description'] = this.uOMDescription;
  data['Vendor_Lot_Number'] = this.isVendorLotNumberExist;
  return data;
  }
}