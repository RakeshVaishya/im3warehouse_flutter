
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_part_modal.dart';

class IssueToEmployeeModel extends IssueByWOPartModal
{

  IssueToEmployeeModel.fromJson(Map<String, dynamic> json) {
    billingTypeCode = json['Billing_Type_Code'];
    companyCode = json['Company_Code'];
    iBillingTypeCode = json['IBilling_Type_Code'];
    iInvTypeCode = json['IInv_Type_Code'];
    iPartNo = json['IPart_No'];
    iUOMCode = json['IUOM_Code'];
    partDescription = json['Part_Description'];
    partNo = json['Part_No'];
    primaryRoomArea = json['PrimaryRoomArea'];
    rate = json['Rate'];
    serialNumber = json['Serial_Number'];
    uOMCode = json['UOM_Code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Billing_Type_Code'] = this.billingTypeCode;
    data['Company_Code'] = this.companyCode;
    data['IBilling_Type_Code'] = this.iBillingTypeCode;
    data['IInv_Type_Code'] = this.iInvTypeCode;
    data['IPart_No'] = this.iPartNo;
    data['IUOM_Code'] = this.iUOMCode;
    data['Part_Description'] = this.partDescription;
    data['Part_No'] = this.partNo;
    data['PrimaryRoomArea'] = this.primaryRoomArea;
    data['Rate'] = this.rate;
    data['Serial_Number'] = this.serialNumber;
    data['UOM_Code'] = this.uOMCode;
    return data;
  }

}