class EmployeeModal {
  String address1;
  String address2;
  String birthDate;
  String cellPhone;
  String cF1;
  String cF10;
  String cF2;
  String cF3;
  String cF4;
  String cF5;
  String cF6;
  String cF7;
  String cF8;
  String cF9;
  String city;
  String companyCode;
  String contractor;
  String countryCode;
  String createDateTime;
  String createUserID;
  String dateEmployed;
  String dateHired;
  String dateTerminated;
  String departmentCode;
  String departmentName;
  String emailID;
  String emergencyContact;
  String emergencyPhone;
  String employeeFirstName;
  String employeeID;
  String employeeLastName;
  String employeeName;
  String employeeStatusCode;
  String employeeStatusDescription;
  String facilityCode;
  String facilityName;
  String hEmployeeName;
  String homePhone;
  String iCraftCode;
  String iDepartmentCode;
  String iEmployeeStatusCode;
  String iFacilityCode;
  String iPositionCode;
  String iSalaryCode;
  String lastUpdateDateTime;
  String lastUpdateUserID;
  String notes;
  String positionCode;
  String positionDescription;
  String salaryCode;
  String salaryCodeDescription;
  String shiftCode;
  String shiftCode1;
  String shiftDescription;
  String sSNNo;
  String sSNNo1;
  String stateCode;
  String status;
  String titleID;
  String zip;

  EmployeeModal(
      {this.address1,
      this.address2,
      this.birthDate,
      this.cellPhone,
      this.cF1,
      this.cF10,
      this.cF2,
      this.cF3,
      this.cF4,
      this.cF5,
      this.cF6,
      this.cF7,
      this.cF8,
      this.cF9,
      this.city,
      this.companyCode,
      this.contractor,
      this.countryCode,
      this.createDateTime,
      this.createUserID,
      this.dateEmployed,
      this.dateHired,
      this.dateTerminated,
      this.departmentCode,
      this.departmentName,
      this.emailID,
      this.emergencyContact,
      this.emergencyPhone,
      this.employeeFirstName,
      this.employeeID,
      this.employeeLastName,
      this.employeeName,
      this.employeeStatusCode,
      this.employeeStatusDescription,
      this.facilityCode,
      this.facilityName,
      this.hEmployeeName,
      this.homePhone,
      this.iCraftCode,
      this.iDepartmentCode,
      this.iEmployeeStatusCode,
      this.iFacilityCode,
      this.iPositionCode,
      this.iSalaryCode,
      this.lastUpdateDateTime,
      this.lastUpdateUserID,
      this.notes,
      this.positionCode,
      this.positionDescription,
      this.salaryCode,
      this.salaryCodeDescription,
      this.shiftCode,
      this.shiftCode1,
      this.shiftDescription,
      this.sSNNo,
      this.sSNNo1,
      this.stateCode,
      this.status,
      this.titleID,
      this.zip});

  EmployeeModal.fromJson(Map<String, dynamic> json) {
    address1 = json['Address_1'];
    address2 = json['Address_2'];
    birthDate = json['Birth_Date'];
    cellPhone = json['Cell_Phone'];
    cF1 = json['CF_1'];
    cF10 = json['CF_10'];
    cF2 = json['CF_2'];
    cF3 = json['CF_3'];
    cF4 = json['CF_4'];
    cF5 = json['CF_5'];
    cF6 = json['CF_6'];
    cF7 = json['CF_7'];
    cF8 = json['CF_8'];
    cF9 = json['CF_9'];
    city = json['City'];
    companyCode = json['Company_Code'];
    contractor = json['Contractor'];
    countryCode = json['Country_Code'];
    createDateTime = json['Create_Date_Time'];
    createUserID = json['Create_User_ID'];
    dateEmployed = json['Date_Employed'];
    dateHired = json['Date_Hired'];
    dateTerminated = json['Date_Terminated'];
    departmentCode = json['Department_code'];
    departmentName = json['Department_Name'];
    emailID = json['Email_ID'];
    emergencyContact = json['Emergency_Contact'];
    emergencyPhone = json['Emergency_Phone'];
    employeeFirstName = json['Employee_First_Name'];
    employeeID = json['Employee_ID'];
    employeeLastName = json['Employee_Last_Name'];
    employeeName = json['Employee_Name'];
    employeeStatusCode = json['Employee_Status_Code'];
    employeeStatusDescription = json['Employee_Status_Description'];
    facilityCode = json['Facility_Code'];
    facilityName = json['Facility_Name'];
    hEmployeeName = json['HEmployee_Name'];
    homePhone = json['Home_Phone'];
    iCraftCode = json['ICraft_Code'];
    iDepartmentCode = json['IDepartment_Code'];
    iEmployeeStatusCode = json['IEmployee_Status_Code'];
    iFacilityCode = json['IFacility_Code'];
    iPositionCode = json['IPosition_Code'];
    iSalaryCode = json['ISalary_Code'];
    lastUpdateDateTime = json['Last_Update_Date_time'];
    lastUpdateUserID = json['Last_Update_User_ID'];
    notes = json['Notes'];
    positionCode = json['Position_Code'];
    positionDescription = json['Position_Description'];
    salaryCode = json['Salary_Code'];
    salaryCodeDescription = json['Salary_Code_Description'];
    shiftCode = json['Shift_Code'];
    shiftCode1 = json['Shift_Code1'];
    shiftDescription = json['Shift_Description'];
    sSNNo = json['SSN_No'];
    sSNNo1 = json['SSN_No1'];
    stateCode = json['State_Code'];
    status = json['Status'];
    titleID = json['Title_ID'];
    zip = json['Zip'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Address_1'] = this.address1;
    data['Address_2'] = this.address2;
    data['Birth_Date'] = this.birthDate;
    data['Cell_Phone'] = this.cellPhone;
    data['CF_1'] = this.cF1;
    data['CF_10'] = this.cF10;
    data['CF_2'] = this.cF2;
    data['CF_3'] = this.cF3;
    data['CF_4'] = this.cF4;
    data['CF_5'] = this.cF5;
    data['CF_6'] = this.cF6;
    data['CF_7'] = this.cF7;
    data['CF_8'] = this.cF8;
    data['CF_9'] = this.cF9;
    data['City'] = this.city;
    data['Company_Code'] = this.companyCode;
    data['Contractor'] = this.contractor;
    data['Country_Code'] = this.countryCode;
    data['Create_Date_Time'] = this.createDateTime;
    data['Create_User_ID'] = this.createUserID;
    data['Date_Employed'] = this.dateEmployed;
    data['Date_Hired'] = this.dateHired;
    data['Date_Terminated'] = this.dateTerminated;
    data['Department_code'] = this.departmentCode;
    data['Department_Name'] = this.departmentName;
    data['Email_ID'] = this.emailID;
    data['Emergency_Contact'] = this.emergencyContact;
    data['Emergency_Phone'] = this.emergencyPhone;
    data['Employee_First_Name'] = this.employeeFirstName;
    data['Employee_ID'] = this.employeeID;
    data['Employee_Last_Name'] = this.employeeLastName;
    data['Employee_Name'] = this.employeeName;
    data['Employee_Status_Code'] = this.employeeStatusCode;
    data['Employee_Status_Description'] = this.employeeStatusDescription;
    data['Facility_Code'] = this.facilityCode;
    data['Facility_Name'] = this.facilityName;
    data['HEmployee_Name'] = this.hEmployeeName;
    data['Home_Phone'] = this.homePhone;
    data['ICraft_Code'] = this.iCraftCode;
    data['IDepartment_Code'] = this.iDepartmentCode;
    data['IEmployee_Status_Code'] = this.iEmployeeStatusCode;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IPosition_Code'] = this.iPositionCode;
    data['ISalary_Code'] = this.iSalaryCode;
    data['Last_Update_Date_time'] = this.lastUpdateDateTime;
    data['Last_Update_User_ID'] = this.lastUpdateUserID;
    data['Notes'] = this.notes;
    data['Position_Code'] = this.positionCode;
    data['Position_Description'] = this.positionDescription;
    data['Salary_Code'] = this.salaryCode;
    data['Salary_Code_Description'] = this.salaryCodeDescription;
    data['Shift_Code'] = this.shiftCode;
    data['Shift_Code1'] = this.shiftCode1;
    data['Shift_Description'] = this.shiftDescription;
    data['SSN_No'] = this.sSNNo;
    data['SSN_No1'] = this.sSNNo1;
    data['State_Code'] = this.stateCode;
    data['Status'] = this.status;
    data['Title_ID'] = this.titleID;
    data['Zip'] = this.zip;
    return data;
  }
}
