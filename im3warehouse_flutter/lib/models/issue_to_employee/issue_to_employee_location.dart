class IssueToEmployeeLocation {
  String companyCode;
  String facilityCode;
  String iFacilityCode;
  String iRoomCode;
  String iRoomAreaCode;
  String onHandQuantity;
  String roomAreaCode;
  String roomCode;
  String stockRoom;
  String stockSrNo;

IssueToEmployeeLocation.fromJson(Map<String, dynamic> json) {
    companyCode = json['CompanyCode'];
    facilityCode = json['FacilityCode'];
    iFacilityCode = json['IFacilityCode'];
    iRoomCode = json['IRoom_Code'];
    iRoomAreaCode = json['IRoomAreaCode'];
    onHandQuantity = json['OnHandQuantity'];
    roomAreaCode = json['RoomAreaCode'];
    roomCode = json['RoomCode'];
    stockRoom = json['StockRoom'];
    stockSrNo = json['StockSrNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyCode'] = this.companyCode;
    data['FacilityCode'] = this.facilityCode;
    data['IFacilityCode'] = this.iFacilityCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['IRoomAreaCode'] = this.iRoomAreaCode;
    data['OnHandQuantity'] = this.onHandQuantity;
    data['RoomAreaCode'] = this.roomAreaCode;
    data['RoomCode'] = this.roomCode;
    data['StockRoom'] = this.stockRoom;
    data['StockSrNo'] = this.stockSrNo;
    return data;
  }
}