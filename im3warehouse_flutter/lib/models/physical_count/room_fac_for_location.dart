import 'package:im3_warehouse/models/dynamic_help.dart';

class RoomFacForLocation extends DynamicHelp{

  String facilityCode;
  String ifacilityCode;
  String iLocationRoomFacility="";
  String locationRoomFacility="";
  String stockRoom;

  RoomFacForLocation(
      {this.facilityCode,
        this.ifacilityCode,
        this.iLocationRoomFacility,
        this.locationRoomFacility,
        this.stockRoom});

  RoomFacForLocation.fromJson(Map<String, dynamic> json) {
    facilityCode = json['Facility_Code'];
    ifacilityCode = json['Ifacility_Code'];
    iLocationRoomFacility = json ['ILocationRoomFacility'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_code'];
    locationRoomFacility = json['LocationRoomFacility'];
    roomAreaCode = json['Room_Area_Code'];
    roomCode = json['Room_Code'];
    stockRoom = json['StockRoom'];

  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Facility_Code'] = this.facilityCode;
    data['Ifacility_Code'] = this.ifacilityCode;
    data['ILocationRoomFacility'] = this.iLocationRoomFacility;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_code'] = this.iRoomCode;
    data['LocationRoomFacility'] = this.locationRoomFacility;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Code'] = this.roomCode;
    data['StockRoom'] = this.stockRoom;
    return data;
  }

}