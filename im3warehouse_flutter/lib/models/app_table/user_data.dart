import 'dart:convert';

import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';

class UserData {
  static UserData userData;
  static UserData getInstance() {
    if (userData == null) {
      userData = new UserData();
    }
    return userData;
  }


/*

  108|ColdFront|test_coldfront|Super User|test_coldfront|MM/dd/yyyy|4|VAN|S5degYUi8TjUBgJw9X7Y4w==|BYhzraoREXGM9y74Buu4/w==
  <?xml version="1.0" encoding="utf-8"?>
  <string xmlns="http://m.mtrack.com/"> company id-> 108|  company name-->  ColdFront|  user Id- test_coldfront|
   user Name Super user
  | employee = test_coldfront|  date time format  ==MM/dd/yyyy|  default i facity code  2|  faciity code ==  CGY|
   uid for cookie S5degYUi8TjUBgJw9X7Y4w==| pwd for cookie BYhzraoREXGM9y74Buu4/w==
 </string>
*/

  String companyCode;
  String mobileURL;
  String siteURL;
  String verificationCode;
  String strCompanyCode;
  String companyName;
  String userId;
  String userName;
  String employeeName;
  String dateTypeFormat;
  String defaultFacilityCodeValue; /*Default facility code value set in facility dialog*/
  String defaultIFacilityCode ; /*Default I facility code*/
  String sessionID;
  String sessionPassword;

  UserData(
      {this.companyCode,
      this.mobileURL,
      this.siteURL,
      this.verificationCode,
      this.strCompanyCode,
      this.companyName,
      this.userId,
      this.userName,
      this.employeeName,
      this.dateTypeFormat,
      this.defaultFacilityCodeValue,
      this.defaultIFacilityCode,
      this.sessionID,
      this.sessionPassword});

  UserData.fromJson(Map<String, dynamic> json) {
    companyCode = json[JsonUtil.KEY_Company_Code];
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.KEY_Company_Code, companyCode);

    mobileURL = json[JsonUtil.MOBILE_URL];
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.MOBILE_URL, mobileURL);
    NetworkConfig.BASE_URL = mobileURL;

    siteURL = json[JsonUtil.SITE_URL];
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.SITE_URL, siteURL);

    verificationCode = json[JsonUtil.VERIFICATION_CODE];
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.VERIFICATION_CODE, verificationCode);
  }

  UserData.fromJsonwithList(List<String> jsonList) {
    strCompanyCode = jsonList.elementAt(0);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.STR_COMPANY_CODE, strCompanyCode);

    companyName = jsonList.elementAt(1);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.COMPANY_NAME, companyName);

    userId = jsonList.elementAt(2);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.USER_ID, userId);

    userName = jsonList.elementAt(3);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.USER_NAME, userName);

    employeeName = jsonList.elementAt(4);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.EMPLOYEE_NAME, employeeName);

    dateTypeFormat = jsonList.elementAt(5);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.DATE_FORMAT, dateTypeFormat);

    defaultIFacilityCode = jsonList.elementAt(6);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.DEFAULT_I_FACILITY_CODE, defaultIFacilityCode);

    defaultFacilityCodeValue = jsonList.elementAt(7);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.DEFAULT_FACILITY_CODE_VALUE, defaultFacilityCodeValue);

    sessionID = jsonList.elementAt(8);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.SESSION_ID, sessionID);

    sessionPassword = jsonList.elementAt(9);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .addStringToSF(JsonUtil.SESSION_PASSWORD, sessionPassword);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data[JsonUtil.KEY_Company_Code] = this.companyCode;
    data[JsonUtil.MOBILE_URL] = this.mobileURL;
    data[JsonUtil.SITE_URL] = this.siteURL;
    data[JsonUtil.VERIFICATION_CODE] = this.verificationCode;

    return data;
  }
}
