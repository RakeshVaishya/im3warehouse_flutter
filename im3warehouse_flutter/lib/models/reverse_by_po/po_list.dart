class   PoDetail {
  String btnCheck;
  String btnRceive;
  String companyCode;
  String dateReceived;
  String dateWanted;
  String defaultCASESize;
  String defaultPalletSize;
  String disabledFields;
  String employeeID;
  String exchangeRate;
  String facilityCode;
  String iAccountNo;
  String iBillingTypeCode;
  String iDepartmentCode;
  String iEQCode;
  String iFacilityCode;
  String iINVTypeCode;
  String iLineProductCode;
  String ipartNo;
  String iPONo;
  String iReleaseNo;
  String iRoomAreaCode;
  String iRoomCode;
  String isMagento;
  String isPrimaryStockRoom;
  String iSSupplementPart;
  String iSalesOrderLineItem;
  String iSalesOrderNo;
  String isCompulsoryExpirationDate;
  String isCompulsoryVendorLotNo;
  String issueIUOMCode;
  String issueUOMOrderQuantity;
  String issueUOMQuantity;
  String issueUOMUnitPrice;
  String itemQuantity;
  String iUOMCode;
  String ivendorCode;
  String iWoNo;
  String lastUpdateDateTime;
  String lineItemSrNo;
  String parentIPartNo;
  String partDescription;
  String partNo;
  String partSrNo;
  String pODateRequested;
  String pODateRequestedTime;
  String pOItemStatusCode;
  String pOItemStatusDescription;
  String poItemType;
  String poNo;
  String pOType;
  String pOTypeDisplay;
  String primaryIFacilityCode;
  String primaryIRoomCode;
  String putawayLocation;
  String putawayZone;
  String qtyReceivedTill;
  String quantityReceived;
  String quantityReturned;
  String receivingQty;
  String releaseNo;
  String releaseQuantity;
  String roomAreaCode;
  String roomCode;
  String salesOrderNumber;
  String serialNumber;
  String serialNumberTextBox;
  String stockSrNo;
  String supplierASNNo;
  String taxRate;
  String taxable;
  String uOMCode;
  String vendorCode;
  String vendorPartID;

  PoDetail(
      {this.btnCheck,
      this.btnRceive,
      this.companyCode,
      this.dateReceived,
      this.dateWanted,
      this.defaultCASESize,
      this.defaultPalletSize,
      this.disabledFields,
      this.employeeID,
      this.exchangeRate,
      this.facilityCode,
      this.iAccountNo,
      this.iBillingTypeCode,
      this.iDepartmentCode,
      this.iEQCode,
      this.iFacilityCode,
      this.iINVTypeCode,
      this.iLineProductCode,
      this.ipartNo,
      this.iPONo,
      this.iReleaseNo,
      this.iRoomAreaCode,
      this.iRoomCode,
      this.isMagento,
      this.isPrimaryStockRoom,
      this.iSSupplementPart,
      this.iSalesOrderLineItem,
      this.iSalesOrderNo,
      this.isCompulsoryExpirationDate,
      this.isCompulsoryVendorLotNo,
      this.issueIUOMCode,
      this.issueUOMOrderQuantity,
      this.issueUOMQuantity,
      this.issueUOMUnitPrice,
      this.itemQuantity,
      this.iUOMCode,
      this.ivendorCode,
      this.iWoNo,
      this.lastUpdateDateTime,
      this.lineItemSrNo,
      this.parentIPartNo,
      this.partDescription,
      this.partNo,
      this.partSrNo,
      this.pODateRequested,
      this.pODateRequestedTime,
      this.pOItemStatusCode,
      this.pOItemStatusDescription,
      this.poItemType,
      this.poNo,
      this.pOType,
      this.pOTypeDisplay,
      this.primaryIFacilityCode,
      this.primaryIRoomCode,
      this.putawayLocation,
      this.putawayZone,
      this.qtyReceivedTill,
      this.quantityReceived,
      this.quantityReturned,
      this.receivingQty,
      this.releaseNo,
      this.releaseQuantity,
      this.roomAreaCode,
      this.roomCode,
      this.salesOrderNumber,
      this.serialNumber,
      this.serialNumberTextBox,
      this.stockSrNo,
      this.supplierASNNo,
      this.taxRate,
      this.taxable,
      this.uOMCode,
      this.vendorCode,
      this.vendorPartID});

  PoDetail.fromJson(Map<String, dynamic> json) {
    btnCheck = json['BtnCheck'];
    btnRceive = json['BtnRceive'];
    companyCode = json['company_code'];
    dateReceived = json['Date_Received'];
    dateWanted = json['DateWanted'];
    defaultCASESize = json['Default_CASE_Size'];
    defaultPalletSize = json['Default_Pallet_Size'];
    disabledFields = json['Disabled_Fields'];
    employeeID = json['Employee_ID'];
    exchangeRate = json['ExchangeRate'];
    facilityCode = json['Facility_Code'];
    iAccountNo = json['IAccount_No'];
    iBillingTypeCode = json['IBilling_Type_Code'];
    iDepartmentCode = json['IDepartment_Code'];
    iEQCode = json['IEQ_Code'];
    iFacilityCode = json['IFacility_Code'];
    iINVTypeCode = json['IINV_type_Code'];
    iLineProductCode = json['ILine_Product_Code'];
    ipartNo = json['ipart_no'];
    iPONo = json['IPO_No'];
    iReleaseNo = json['IRelease_No'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    isMagento = json['Is_Magento'];
    isPrimaryStockRoom = json['Is_PrimaryStockRoom'];
    iSSupplementPart = json['IS_Supplement_Part'];
    iSalesOrderLineItem = json['ISalesOrderLineItem'];
    iSalesOrderNo = json['ISalesOrderNo'];
    isCompulsoryExpirationDate = json['IsCompulsory_Expiration_Date'];
    isCompulsoryVendorLotNo = json['IsCompulsory_Vendor_Lot_No'];
    issueIUOMCode = json['Issue_IUOM_Code'];
    issueUOMOrderQuantity = json['Issue_UOM_Order_Quantity'];
    issueUOMQuantity = json['Issue_UOM_Quantity'];
    issueUOMUnitPrice = json['Issue_UOM_Unit_Price'];
    itemQuantity = json['Item_Quantity'];
    iUOMCode = json['IUOM_Code'];
    ivendorCode = json['ivendor_code'];
    iWoNo = json['IWo_No'];
    lastUpdateDateTime = json['last_update_date_time'];
    lineItemSrNo = json['Line_Item_Sr_No'];
    parentIPartNo = json['Parent_IPart_No'];
    partDescription = json['Part_Description'];
    partNo = json['Part_No'];
    partSrNo = json['Part_Sr_No'];
    pODateRequested = json['PO_Date_Requested'];
    pODateRequestedTime = json['PO_Date_Requested_Time'];
    pOItemStatusCode = json['PO_Item_Status_Code'];
    pOItemStatusDescription = json['PO_Item_Status_Description'];
    poItemType = json['Po_Item_Type'];
    poNo = json['Po_No'];
    pOType = json['POType'];
    pOTypeDisplay = json['POTypeDisplay'];
    primaryIFacilityCode = json['Primary_IFacilityCode'];
    primaryIRoomCode = json['Primary_IRoomCode'];
    putawayLocation = json['Putaway_Location'];
    putawayZone = json['Putaway_Zone'];
    qtyReceivedTill = json['qty_received_till'];
    quantityReceived = json['Quantity_Received'];
    quantityReturned = json['Quantity_Returned'];
    receivingQty = json['ReceivingQty'];
    releaseNo = json['Release_No'];
    releaseQuantity = json['Release_Quantity'];
    roomAreaCode = json['Room_Area_Code'];
    roomCode = json['RoomCode'];
    salesOrderNumber = json['SalesOrderNumber'];
    serialNumber = json['Serial_Number'];
    serialNumberTextBox = json['Serial_NumberTextBox'];
    stockSrNo = json['Stock_Sr_No'];
    supplierASNNo = json['Supplier_ASN_No'];
    taxRate = json['Tax_Rate'];
    taxable = json['Taxable'];
    uOMCode = json['UOM_code'];
    vendorCode = json['vendor_code'];
    vendorPartID = json['Vendor_Part_ID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BtnCheck'] = this.btnCheck;
    data['BtnRceive'] = this.btnRceive;
    data['company_code'] = this.companyCode;
    data['Date_Received'] = this.dateReceived;
    data['DateWanted'] = this.dateWanted;
    data['Default_CASE_Size'] = this.defaultCASESize;
    data['Default_Pallet_Size'] = this.defaultPalletSize;
    data['Disabled_Fields'] = this.disabledFields;
    data['Employee_ID'] = this.employeeID;
    data['ExchangeRate'] = this.exchangeRate;
    data['Facility_Code'] = this.facilityCode;
    data['IAccount_No'] = this.iAccountNo;
    data['IBilling_Type_Code'] = this.iBillingTypeCode;
    data['IDepartment_Code'] = this.iDepartmentCode;
    data['IEQ_Code'] = this.iEQCode;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IINV_type_Code'] = this.iINVTypeCode;
    data['ILine_Product_Code'] = this.iLineProductCode;
    data['ipart_no'] = this.ipartNo;
    data['IPO_No'] = this.iPONo;
    data['IRelease_No'] = this.iReleaseNo;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['Is_Magento'] = this.isMagento;
    data['Is_PrimaryStockRoom'] = this.isPrimaryStockRoom;
    data['IS_Supplement_Part'] = this.iSSupplementPart;
    data['ISalesOrderLineItem'] = this.iSalesOrderLineItem;
    data['ISalesOrderNo'] = this.iSalesOrderNo;
    data['IsCompulsory_Expiration_Date'] = this.isCompulsoryExpirationDate;
    data['IsCompulsory_Vendor_Lot_No'] = this.isCompulsoryVendorLotNo;
    data['Issue_IUOM_Code'] = this.issueIUOMCode;
    data['Issue_UOM_Order_Quantity'] = this.issueUOMOrderQuantity;
    data['Issue_UOM_Quantity'] = this.issueUOMQuantity;
    data['Issue_UOM_Unit_Price'] = this.issueUOMUnitPrice;
    data['Item_Quantity'] = this.itemQuantity;
    data['IUOM_Code'] = this.iUOMCode;
    data['ivendor_code'] = this.ivendorCode;
    data['IWo_No'] = this.iWoNo;
    data['last_update_date_time'] = this.lastUpdateDateTime;
    data['Line_Item_Sr_No'] = this.lineItemSrNo;
    data['Parent_IPart_No'] = this.parentIPartNo;
    data['Part_Description'] = this.partDescription;
    data['Part_No'] = this.partNo;
    data['Part_Sr_No'] = this.partSrNo;
    data['PO_Date_Requested'] = this.pODateRequested;
    data['PO_Date_Requested_Time'] = this.pODateRequestedTime;
    data['PO_Item_Status_Code'] = this.pOItemStatusCode;
    data['PO_Item_Status_Description'] = this.pOItemStatusDescription;
    data['Po_Item_Type'] = this.poItemType;
    data['Po_No'] = this.poNo;
    data['POType'] = this.pOType;
    data['POTypeDisplay'] = this.pOTypeDisplay;
    data['Primary_IFacilityCode'] = this.primaryIFacilityCode;
    data['Primary_IRoomCode'] = this.primaryIRoomCode;
    data['Putaway_Location'] = this.putawayLocation;
    data['Putaway_Zone'] = this.putawayZone;
    data['qty_received_till'] = this.qtyReceivedTill;
    data['Quantity_Received'] = this.quantityReceived;
    data['Quantity_Returned'] = this.quantityReturned;
    data['ReceivingQty'] = this.receivingQty;
    data['Release_No'] = this.releaseNo;
    data['Release_Quantity'] = this.releaseQuantity;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['RoomCode'] = this.roomCode;
    data['SalesOrderNumber'] = this.salesOrderNumber;
    data['Serial_Number'] = this.serialNumber;
    data['Serial_NumberTextBox'] = this.serialNumberTextBox;
    data['Stock_Sr_No'] = this.stockSrNo;
    data['Supplier_ASN_No'] = this.supplierASNNo;
    data['Tax_Rate'] = this.taxRate;
    data['Taxable'] = this.taxable;
    data['UOM_code'] = this.uOMCode;
    data['vendor_code'] = this.vendorCode;
    data['Vendor_Part_ID'] = this.vendorPartID;
    return data;
  }
}
