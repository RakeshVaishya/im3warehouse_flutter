class  POItemDetails{

  String acclaimPONo;
  String accountCode;
  String additionalPONo;
  String adjustCharges;
  String adjustIAccountNo;
  String billingTypeCode;
  String billingTypeDescription;
  String categoryReferenceCode;
  String cF1;
  String cF10;
  String cF2;
  String cF3;
  String cF4;
  String cF5;
  String cF6;
  String cF7;
  String cF8;
  String cF9;
  String companyCode;
  String conversionFactor;
  String createDateTime;
  String createUserID;
  String currencyExchangeRate;
  String customerPONo;
  String dateWanted;
  String departmentCode;
  String eqCode;
  String freightIAccountNo;
  String frieghtCharges;
  String iAccountNo;
  String iDepartmentCode;
  String iEQCode;
  String iLineProductCode;
  String iManufacturerCode;
  String iPartNo;
  String iPONo;
  String iProjectCode;
  String iROOMCode;
  String isSupplementalPart;
  String issueIUOMCode;
  String issueUomCode;
  String issueUomDescription;
  String issueUOMOrderQuantity;
  String issueUOMUnitPrice;
  String iWONo;
  String lastUpdateDateTime;
  String lastUpdateUserID;
  String leadDays;
  String lineItemSequenceNo;
  String lineItemSrNo;
  String lineProductCode;
  String manufacturerCode;
  String manufacturerName;
  String noOfReceivers;
  String orderQuantity;
  String partDescription;
  String partNo;
  String pOItemCost;
  String pOItemNotes;
  String pOItemStatusCode;
  String pOItemStatusDescription;
  String poItemType;
  String poItemTypeDescription;
  String pONo;
  String projectCode;
  String purchaseIUomCode;
  String purchaseUomCode;
  String purchaseUomDescription;
  String purchaseUomUnitPrice;
  String roomAreaCode;
  String stockSrNo;
  String supplementCount;
  String taxRate;
  String taxable;
  String totalReceivedQty;
  String totalReturnedQty;
  String totalReversedQty;
  String wONo;

  POItemDetails(
  {this.acclaimPONo,
  this.accountCode,
  this.additionalPONo,
  this.adjustCharges,
  this.adjustIAccountNo,
  this.billingTypeCode,
  this.billingTypeDescription,
  this.categoryReferenceCode,
  this.cF1,
  this.cF10,
  this.cF2,
  this.cF3,
  this.cF4,
  this.cF5,
  this.cF6,
  this.cF7,
  this.cF8,
  this.cF9,
  this.companyCode,
  this.conversionFactor,
  this.createDateTime,
  this.createUserID,
  this.currencyExchangeRate,
  this.customerPONo,
  this.dateWanted,
  this.departmentCode,
  this.eqCode,
  this.freightIAccountNo,
  this.frieghtCharges,
  this.iAccountNo,
  this.iDepartmentCode,
  this.iEQCode,
  this.iLineProductCode,
  this.iManufacturerCode,
  this.iPartNo,
  this.iPONo,
  this.iProjectCode,
  this.iROOMCode,
  this.isSupplementalPart,
  this.issueIUOMCode,
  this.issueUomCode,
  this.issueUomDescription,
  this.issueUOMOrderQuantity,
  this.issueUOMUnitPrice,
  this.iWONo,
  this.lastUpdateDateTime,
  this.lastUpdateUserID,
  this.leadDays,
  this.lineItemSequenceNo,
  this.lineItemSrNo,
  this.lineProductCode,
  this.manufacturerCode,
  this.manufacturerName,
  this.noOfReceivers,
  this.orderQuantity,
  this.partDescription,
  this.partNo,
  this.pOItemCost,
  this.pOItemNotes,
  this.pOItemStatusCode,
  this.pOItemStatusDescription,
  this.poItemType,
  this.poItemTypeDescription,
  this.pONo,
  this.projectCode,
  this.purchaseIUomCode,
  this.purchaseUomCode,
  this.purchaseUomDescription,
  this.purchaseUomUnitPrice,
  this.roomAreaCode,
  this.stockSrNo,
  this.supplementCount,
  this.taxRate,
  this.taxable,
  this.totalReceivedQty,
  this.totalReturnedQty,
  this.totalReversedQty,
  this.wONo});

  POItemDetails.fromJson(Map<String, dynamic> json) {
  acclaimPONo = json['Acclaim_PO_No'];
  accountCode = json['Account_Code'];
  additionalPONo = json['Additional_PO_No'];
  adjustCharges = json['Adjust_Charges'];
  adjustIAccountNo = json['Adjust_IAccount_No'];
  billingTypeCode = json['Billing_Type_Code'];
  billingTypeDescription = json['Billing_Type_Description'];
  categoryReferenceCode = json['Category_Reference_Code'];
  cF1 = json['CF_1'];
  cF10 = json['CF_10'];
  cF2 = json['CF_2'];
  cF3 = json['CF_3'];
  cF4 = json['CF_4'];
  cF5 = json['CF_5'];
  cF6 = json['CF_6'];
  cF7 = json['CF_7'];
  cF8 = json['CF_8'];
  cF9 = json['CF_9'];
  companyCode = json['Company_Code'];
  conversionFactor = json['Conversion_Factor'];
  createDateTime = json['Create_Date_time'];
  createUserID = json['Create_User_ID'];
  currencyExchangeRate = json['CurrencyExchangeRate'];
  customerPONo = json['Customer_PO_No'];
  dateWanted = json['Date_Wanted'];
  departmentCode = json['Department_Code'];
  eqCode = json['Eq_Code'];
  freightIAccountNo = json['Freight_IAccount_No'];
  frieghtCharges = json['Frieght_Charges'];
  iAccountNo = json['IAccount_No'];
  iDepartmentCode = json['IDepartment_Code'];
  iEQCode = json['IEQ_Code'];
  iLineProductCode = json['ILine_Product_Code'];
  iManufacturerCode = json['IManufacturer_Code'];
  iPartNo = json['IPart_No'];
  iPONo = json['IPO_No'];
  iProjectCode = json['IProject_Code'];
  iROOMCode = json['IROOM_Code'];
  isSupplementalPart = json['Is_Supplemental_Part'];
  issueIUOMCode = json['Issue_IUOM_Code'];
  issueUomCode = json['Issue_Uom_Code'];
  issueUomDescription = json['Issue_Uom_Description'];
  issueUOMOrderQuantity = json['Issue_UOM_Order_Quantity'];
  issueUOMUnitPrice = json['Issue_UOM_Unit_Price'];
  iWONo = json['IWO_No'];
  lastUpdateDateTime = json['Last_Update_Date_time'];
  lastUpdateUserID = json['Last_Update_User_ID'];
  leadDays = json['Lead_Days'];
  lineItemSequenceNo = json['Line_Item_Sequence_No'];
  lineItemSrNo = json['Line_Item_Sr_No'];
  lineProductCode = json['Line_Product_Code'];
  manufacturerCode = json['Manufacturer_Code'];
  manufacturerName = json['Manufacturer_Name'];
  noOfReceivers = json['No_of_Receivers'];
  orderQuantity = json['Order_Quantity'];
  partDescription = json['Part_Description'];
  partNo = json['Part_No'];
  pOItemCost = json['PO_Item_Cost'];
  pOItemNotes = json['PO_Item_Notes'];
  pOItemStatusCode = json['PO_Item_Status_Code'];
  pOItemStatusDescription = json['PO_Item_Status_Description'];
  poItemType = json['Po_Item_Type'];
  poItemTypeDescription = json['Po_Item_Type_Description'];
  pONo = json['PO_No'];
  projectCode = json['Project_Code'];
  purchaseIUomCode = json['Purchase_IUom_Code'];
  purchaseUomCode = json['Purchase_Uom_Code'];
  purchaseUomDescription = json['Purchase_Uom_Description'];
  purchaseUomUnitPrice = json['Purchase_Uom_Unit_Price'];
  roomAreaCode = json['Room_Area_Code'];
  stockSrNo = json['Stock_Sr_No'];
  supplementCount = json['Supplement_Count'];
  taxRate = json['Tax_Rate'];
  taxable = json['Taxable'];
  totalReceivedQty = json['Total_Received_Qty'];
  totalReturnedQty = json['Total_Returned_Qty'];
  totalReversedQty = json['Total_Reversed_Qty'];
  wONo = json['WO_No'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['Acclaim_PO_No'] = this.acclaimPONo;
  data['Account_Code'] = this.accountCode;
  data['Additional_PO_No'] = this.additionalPONo;
  data['Adjust_Charges'] = this.adjustCharges;
  data['Adjust_IAccount_No'] = this.adjustIAccountNo;
  data['Billing_Type_Code'] = this.billingTypeCode;
  data['Billing_Type_Description'] = this.billingTypeDescription;
  data['Category_Reference_Code'] = this.categoryReferenceCode;
  data['CF_1'] = this.cF1;
  data['CF_10'] = this.cF10;
  data['CF_2'] = this.cF2;
  data['CF_3'] = this.cF3;
  data['CF_4'] = this.cF4;
  data['CF_5'] = this.cF5;
  data['CF_6'] = this.cF6;
  data['CF_7'] = this.cF7;
  data['CF_8'] = this.cF8;
  data['CF_9'] = this.cF9;
  data['Company_Code'] = this.companyCode;
  data['Conversion_Factor'] = this.conversionFactor;
  data['Create_Date_time'] = this.createDateTime;
  data['Create_User_ID'] = this.createUserID;
  data['CurrencyExchangeRate'] = this.currencyExchangeRate;
  data['Customer_PO_No'] = this.customerPONo;
  data['Date_Wanted'] = this.dateWanted;
  data['Department_Code'] = this.departmentCode;
  data['Eq_Code'] = this.eqCode;
  data['Freight_IAccount_No'] = this.freightIAccountNo;
  data['Frieght_Charges'] = this.frieghtCharges;
  data['IAccount_No'] = this.iAccountNo;
  data['IDepartment_Code'] = this.iDepartmentCode;
  data['IEQ_Code'] = this.iEQCode;
  data['ILine_Product_Code'] = this.iLineProductCode;
  data['IManufacturer_Code'] = this.iManufacturerCode;
  data['IPart_No'] = this.iPartNo;
  data['IPO_No'] = this.iPONo;
  data['IProject_Code'] = this.iProjectCode;
  data['IROOM_Code'] = this.iROOMCode;
  data['Is_Supplemental_Part'] = this.isSupplementalPart;
  data['Issue_IUOM_Code'] = this.issueIUOMCode;
  data['Issue_Uom_Code'] = this.issueUomCode;
  data['Issue_Uom_Description'] = this.issueUomDescription;
  data['Issue_UOM_Order_Quantity'] = this.issueUOMOrderQuantity;
  data['Issue_UOM_Unit_Price'] = this.issueUOMUnitPrice;
  data['IWO_No'] = this.iWONo;
  data['Last_Update_Date_time'] = this.lastUpdateDateTime;
  data['Last_Update_User_ID'] = this.lastUpdateUserID;
  data['Lead_Days'] = this.leadDays;
  data['Line_Item_Sequence_No'] = this.lineItemSequenceNo;
  data['Line_Item_Sr_No'] = this.lineItemSrNo;
  data['Line_Product_Code'] = this.lineProductCode;
  data['Manufacturer_Code'] = this.manufacturerCode;
  data['Manufacturer_Name'] = this.manufacturerName;
  data['No_of_Receivers'] = this.noOfReceivers;
  data['Order_Quantity'] = this.orderQuantity;
  data['Part_Description'] = this.partDescription;
  data['Part_No'] = this.partNo;
  data['PO_Item_Cost'] = this.pOItemCost;
  data['PO_Item_Notes'] = this.pOItemNotes;
  data['PO_Item_Status_Code'] = this.pOItemStatusCode;
  data['PO_Item_Status_Description'] = this.pOItemStatusDescription;
  data['Po_Item_Type'] = this.poItemType;
  data['Po_Item_Type_Description'] = this.poItemTypeDescription;
  data['PO_No'] = this.pONo;
  data['Project_Code'] = this.projectCode;
  data['Purchase_IUom_Code'] = this.purchaseIUomCode;
  data['Purchase_Uom_Code'] = this.purchaseUomCode;
  data['Purchase_Uom_Description'] = this.purchaseUomDescription;
  data['Purchase_Uom_Unit_Price'] = this.purchaseUomUnitPrice;
  data['Room_Area_Code'] = this.roomAreaCode;
  data['Stock_Sr_No'] = this.stockSrNo;
  data['Supplement_Count'] = this.supplementCount;
  data['Tax_Rate'] = this.taxRate;
  data['Taxable'] = this.taxable;
  data['Total_Received_Qty'] = this.totalReceivedQty;
  data['Total_Returned_Qty'] = this.totalReturnedQty;
  data['Total_Reversed_Qty'] = this.totalReversedQty;
  data['WO_No'] = this.wONo;
  return data;
  }
  }


