class ReceiveByPoModel{


  final String part;
  final String orderQ;
  final String releaseNo;
  final String line;
  final String receivedQ;
  final String releaseQ;

   ReceiveByPoModel(
  {this.part,
  this.orderQ,
  this.releaseNo,
  this.line,
  this.receivedQ,
  this.releaseQ});

}