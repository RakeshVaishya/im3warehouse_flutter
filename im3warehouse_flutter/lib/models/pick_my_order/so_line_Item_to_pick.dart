class SOLineItemToPick {
  String companyCode;
  String customer;
  String deliveryDate;
  String deliveryDateTime;
  String iCustomerID;
  String iFacilityCode;
  String iInvPickingHeaderId;
  String iInvTypecode;
  String iNVActionCode;
  String iPartNo;
  String iRoomCode;
  String iSalesOrderLineItem;
  String iSalesOrderNo;
  String isPickedByZone;
  String isSerializedPart;
  String issuedQuantity;
  String itemStatusDesc;
  String iUOMCode;
  String orderQuantity;
  String parentISalesOrderLineItem;
  String partDescription;
  String partNo;
  String pickLocation;
  String pickTicketNo;
  String quantityLeftToIssue;
  String salesOrderNo;
  String serialNo;
  String showCheckbox;
  String stockSrNo;
  String uOMCode;

  int totalPicked =0;
  int itemRemainToPick =0;
  String  itemStatus ="";  // "",  issued  , skip,
  SOLineItemToPick(
      {this.companyCode,
        this.customer,
        this.deliveryDate,
        this.deliveryDateTime,
        this.iCustomerID,
        this.iFacilityCode,
        this.iInvPickingHeaderId,
        this.iInvTypecode,
        this.iNVActionCode,
        this.iPartNo,
        this.iRoomCode,
        this.iSalesOrderLineItem,
        this.iSalesOrderNo,
        this.isPickedByZone,
        this.isSerializedPart,
        this.issuedQuantity,
        this.itemStatusDesc,
        this.iUOMCode,
        this.orderQuantity,
        this.parentISalesOrderLineItem,
        this.partDescription,
        this.partNo,
        this.pickLocation,
        this.pickTicketNo,
        this.quantityLeftToIssue,
        this.salesOrderNo,
        this.serialNo,
        this.showCheckbox,
        this.stockSrNo,
        this.uOMCode});

  SOLineItemToPick.fromJson(Map<String, dynamic> json) {
    companyCode = json['CompanyCode'];
    customer = json['Customer'];
    deliveryDate = json['Delivery_Date'];
    deliveryDateTime = json['DeliveryDateTime'];
    iCustomerID = json['ICustomer_ID'];
    iFacilityCode = json['IFacilityCode'];
    iInvPickingHeaderId = json['IInvPickingHeaderId'];
    iInvTypecode = json['IInvTypecode'];
    iNVActionCode = json['INV_Action_Code'];
    iPartNo = json['IPartNo'];
    iRoomCode = json['IRoomCode'];
    iSalesOrderLineItem = json['ISalesOrderLineItem'];
    iSalesOrderNo = json['ISalesOrderNo'];
    isPickedByZone = json['IsPickedByZone'];
    isSerializedPart = json['IsSerializedPart'];
    issuedQuantity = json['IssuedQuantity'];
    itemStatusDesc = json['ItemStatusDesc'];
    iUOMCode = json['IUOM_Code'];
    orderQuantity = json['OrderQuantity'];
    parentISalesOrderLineItem = json['ParentISalesOrderLineItem'];
    partDescription = json['Part_Description'];
    partNo = json['Part_No'];
    pickLocation = json['PickLocation'];
    pickTicketNo = json['PickTicketNo'];
    quantityLeftToIssue = json['QuantityLeftToIssue'];
    salesOrderNo = json['SalesOrderNo'];
    serialNo = json['SerialNo'];
    showCheckbox = json['showCheckbox'];
    stockSrNo = json['StockSrNo'];
    uOMCode = json['UOM_Code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CompanyCode'] = this.companyCode;
    data['Customer'] = this.customer;
    data['Delivery_Date'] = this.deliveryDate;
    data['DeliveryDateTime'] = this.deliveryDateTime;
    data['ICustomer_ID'] = this.iCustomerID;
    data['IFacilityCode'] = this.iFacilityCode;
    data['IInvPickingHeaderId'] = this.iInvPickingHeaderId;
    data['IInvTypecode'] = this.iInvTypecode;
    data['INV_Action_Code'] = this.iNVActionCode;
    data['IPartNo'] = this.iPartNo;
    data['IRoomCode'] = this.iRoomCode;
    data['ISalesOrderLineItem'] = this.iSalesOrderLineItem;
    data['ISalesOrderNo'] = this.iSalesOrderNo;
    data['IsPickedByZone'] = this.isPickedByZone;
    data['IsSerializedPart'] = this.isSerializedPart;
    data['IssuedQuantity'] = this.issuedQuantity;
    data['ItemStatusDesc'] = this.itemStatusDesc;
    data['IUOM_Code'] = this.iUOMCode;
    data['OrderQuantity'] = this.orderQuantity;
    data['ParentISalesOrderLineItem'] = this.parentISalesOrderLineItem;
    data['Part_Description'] = this.partDescription;
    data['Part_No'] = this.partNo;
    data['PickLocation'] = this.pickLocation;
    data['PickTicketNo'] = this.pickTicketNo;
    data['QuantityLeftToIssue'] = this.quantityLeftToIssue;
    data['SalesOrderNo'] = this.salesOrderNo;
    data['SerialNo'] = this.serialNo;
    data['showCheckbox'] = this.showCheckbox;
    data['StockSrNo'] = this.stockSrNo;
    data['UOM_Code'] = this.uOMCode;
    return data;
  }
}
