import 'pick_order.dart';

class OrderItem extends PickOrder {

  String allowShippement;
  String companyCode;
  String createDateTime;
  String customer;
  String customerType;
  String deliveryDate;
  String iPartNo;
  String iSalesOrderLineItem;
  String isSerializedPart;
  String lineItemSrNo;
  String orderQuantity;
  String parentISalesOrderLineItem;
  String partNo;
  String partDescription;
  String pickedQty;
  String pickedQuantity;
  String qtyLeftToPick;
  String serial;
  String uOMCode;
  String wareHouseIFacilityCode;
  String wareHouseIRoomCode;



  OrderItem.fromJson(Map<String, dynamic> json) {
    allowShippement = json['Allow_Shippement'];
    companyCode = json['CompanyCode'];
    createDateTime = json['CreateDateTime'];
    customer = json['Customer'];
    customerType = json['Customer_Type'];
    deliveryDate = json['Delivery_Date'];
    iPartNo = json['IPart_No'];
    iSalesOrderLineItem = json['ISalesOrderLineItem'];
    iSalesOrderNo = json['ISalesOrderNo'];
    isSerializedPart = json['IsSerializedPart'];
    lineItemSrNo = json['LineItemSrNo'];
    orderQuantity = json['Order_Quantity'];
    parentISalesOrderLineItem = json['ParentISalesOrderLineItem'];
    partNo = json['Part_No'];
    partDescription = json['PartDescription'];
    pickedQty = json['Picked_Qty'];
    pickedQuantity = json['Picked_Quantity'];
    qtyLeftToPick = json['QtyLeftToPick'];
    salesOrderNo = json['SalesOrderNo'];
    serial = json['Serial'];
    uOMCode = json['UOM_Code'];
    wareHouseIFacilityCode = json['WareHouse_IFacility_Code'];
    wareHouseIRoomCode = json['WareHouse_IRoom_Code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Allow_Shippement'] = this.allowShippement;
    data['CompanyCode'] = this.companyCode;
    data['CreateDateTime'] = this.createDateTime;
    data['Customer'] = this.customer;
    data['Customer_Type'] = this.customerType;
    data['Delivery_Date'] = this.deliveryDate;
    data['IPart_No'] = this.iPartNo;
    data['ISalesOrderLineItem'] = this.iSalesOrderLineItem;
    data['ISalesOrderNo'] = this.iSalesOrderNo;
    data['IsSerializedPart'] = this.isSerializedPart;
    data['LineItemSrNo'] = this.lineItemSrNo;
    data['Order_Quantity'] = this.orderQuantity;
    data['ParentISalesOrderLineItem'] = this.parentISalesOrderLineItem;
    data['Part_No'] = this.partNo;
    data['PartDescription'] = this.partDescription;
    data['Picked_Qty'] = this.pickedQty;
    data['Picked_Quantity'] = this.pickedQuantity;
    data['QtyLeftToPick'] = this.qtyLeftToPick;
    data['SalesOrderNo'] = this.salesOrderNo;
    data['Serial'] = this.serial;
    data['UOM_Code'] = this.uOMCode;
    data['WareHouse_IFacility_Code'] = this.wareHouseIFacilityCode;
    data['WareHouse_IRoom_Code'] = this.wareHouseIRoomCode;
    return data;
  }
}