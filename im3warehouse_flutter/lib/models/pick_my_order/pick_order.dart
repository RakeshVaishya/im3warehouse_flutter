class PickOrder{

  String companyCode;
  String createdDate;
  String facilityCode;
  String iSalesOrderNo;
  String notes;
  String saleOrderStatus;
  String salesOrderNo;
  String totalItemQty;
   String backOrderQty;
   String pickedQuantity;
   String qtyleft;

  PickOrder(
      {this.backOrderQty,
        this.companyCode,
        this.createdDate,
        this.facilityCode,
        this.iSalesOrderNo,
        this.notes,
        this.pickedQuantity,
        this.qtyleft,
        this.saleOrderStatus,
        this.salesOrderNo,
        this.totalItemQty});


  PickOrder.fromJson(Map<String, dynamic> json) {

  backOrderQty = json['BackOrderQty'];
  companyCode = json['CompanyCode'];
  createdDate = json['CreatedDate'];
  facilityCode = json['Facility_Code'];
  iSalesOrderNo = json['ISalesOrderNo'];
  notes = json['Notes'];
  pickedQuantity = json['PickedQuantity'];
  qtyleft = json['Qtyleft'];
  saleOrderStatus = json['SaleOrderStatus'];
  salesOrderNo = json['SalesOrderNo'];
  totalItemQty = json['TotalItemQty'];
  }

  Map<String, dynamic> toJson() {

  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['BackOrderQty'] = this.backOrderQty;
  data['CompanyCode'] = this.companyCode;
  data['CreatedDate'] = this.createdDate;
  data['Facility_Code'] = this.facilityCode;
  data['ISalesOrderNo'] = this.iSalesOrderNo;
  data['Notes'] = this.notes;
  data['PickedQuantity'] = this.pickedQuantity;
  data['Qtyleft'] = this.qtyleft;
  data['SaleOrderStatus'] = this.saleOrderStatus;
  data['SalesOrderNo'] = this.salesOrderNo;
  data['TotalItemQty'] = this.totalItemQty;
  return data;
  }
  }
