class ReservesPartLocation{

  String availableQuantity;
  String companyCode;
  String hardReserveQuantity;
  String iFacilityCode;
  String iPartNo;
  String iRoomAreaCode;
  String iRoomCode;
  String onHandQuantity;
  String openPOQty;
  String probableReserveQuantity;
  String softReserveQuantity;
  String stockSrNo="";

  ReservesPartLocation(
      {this.availableQuantity,
        this.companyCode,
        this.hardReserveQuantity,
        this.iFacilityCode,
        this.iPartNo,
        this.iRoomAreaCode,
        this.iRoomCode,
        this.onHandQuantity,
        this.openPOQty,
        this.probableReserveQuantity,
        this.softReserveQuantity,
        this.stockSrNo});

  ReservesPartLocation.fromJson(Map<String, dynamic> json) {
    availableQuantity = json['Available_Quantity'];
    companyCode = json['Company_Code'];
    hardReserveQuantity = json['HardReserveQuantity'];
    iFacilityCode = json['IFacility_Code'];
    iPartNo = json['IPart_No'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    onHandQuantity = json['On_Hand_Quantity'];
    openPOQty = json['OpenPOQty'];
    probableReserveQuantity = json['ProbableReserveQuantity'];
    softReserveQuantity = json['SoftReserveQuantity'];
    stockSrNo = json['Stock_Sr_No'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Available_Quantity'] = this.availableQuantity;
    data['Company_Code'] = this.companyCode;
    data['HardReserveQuantity'] = this.hardReserveQuantity;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IPart_No'] = this.iPartNo;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['On_Hand_Quantity'] = this.onHandQuantity;
    data['OpenPOQty'] = this.openPOQty;
    data['ProbableReserveQuantity'] = this.probableReserveQuantity;
    data['SoftReserveQuantity'] = this.softReserveQuantity;
    data['Stock_Sr_No'] = this.stockSrNo;
    return data;
  }
}

