import 'package:im3_warehouse/utils/utility.dart';

class IssuByWorkTaskLocationModal {
  String breakpackLocation;
  String companyCode;
  String facilityCode;
  String iFacilityCode;
  String iRoomAreaCode;
  String iRoomCode;
  String onHandQuantity;
  String pickableLocation;
  String primaryStockroom;
  String roomAreaCode;
  String roomCode;
  String stockSrNo;
  String stockRoom;
  List<String> stockRoomList = [];

  IssuByWorkTaskLocationModal(
      {this.breakpackLocation,
      this.companyCode,
      this.facilityCode,
      this.iFacilityCode,
      this.iRoomAreaCode,
      this.iRoomCode,
      this.onHandQuantity,
      this.pickableLocation,
      this.primaryStockroom,
      this.roomAreaCode,
      this.roomCode,
      this.stockSrNo,
      this.stockRoom});

  IssuByWorkTaskLocationModal.fromJson(Map<String, dynamic> json) {
    breakpackLocation = json['Breakpack_Location'];
    companyCode = json['Company_Code'];
    facilityCode = json['Facility_Code'];
    iFacilityCode = json['IFacility_Code'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    onHandQuantity = json['On_Hand_Quantity'];
    pickableLocation = json['Pickable_Location'];
    primaryStockroom = json['Primary_Stockroom'];
    roomAreaCode = json['Room_Area_Code'];
    roomCode = json['Room_Code'];
    stockSrNo = json['Stock_Sr_No'];
    stockRoom = json['StockRoom'];
    stockRoomList = Utility.setDataInList(stockRoom);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Breakpack_Location'] = this.breakpackLocation;
    data['Company_Code'] = this.companyCode;
    data['Facility_Code'] = this.facilityCode;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['On_Hand_Quantity'] = this.onHandQuantity;
    data['Pickable_Location'] = this.pickableLocation;
    data['Primary_Stockroom'] = this.primaryStockroom;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Code'] = this.roomCode;
    data['Stock_Sr_No'] = this.stockSrNo;
    data['StockRoom'] = this.stockRoom;
    return data;
  }
}
