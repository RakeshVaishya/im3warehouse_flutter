class IssueByGetAllTaskModal {
  String iWONo;
  String iWoTaskNo;
  String taskCode;
  String taskCode1;
  String taskDescription;

  IssueByGetAllTaskModal(
      {this.iWONo,
      this.iWoTaskNo,
      this.taskCode,
      this.taskCode1,
      this.taskDescription});

  IssueByGetAllTaskModal.fromJson(Map<String, dynamic> json) {
    iWONo = json['IWO_No'];
    iWoTaskNo = json['IWo_task_No'];
    taskCode = json['Task_Code'];
    taskCode1 = json['Task_Code1'];
    taskDescription = json['Task_Description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['IWO_No'] = this.iWONo;
    data['IWo_task_No'] = this.iWoTaskNo;
    data['Task_Code'] = this.taskCode;
    data['Task_Code1'] = this.taskCode1;
    data['Task_Description'] = this.taskDescription;
    return data;
  }
}
