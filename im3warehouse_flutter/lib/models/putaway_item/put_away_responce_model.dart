import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/utils/utility.dart';

class PutAwayResponseModel extends PartDetails{

  String confirmPickPartFlag;
  String iLocationRoomFacility;
  String iPONo;
  String iPutawayLabelID;
  String putawayLocationRequiredOrRecommended;
  String putawayLabelID;
  String putawayLocation;
  String putawayQty;
  String iRoomCode;
String iPartNo;

  PutAwayResponseModel(
  {this.confirmPickPartFlag,
  this.iLocationRoomFacility,
  this.iPONo,
  this.iPutawayLabelID,
  this.putawayLocationRequiredOrRecommended,
  this.putawayLabelID,
  this.putawayLocation,
  this.putawayQty,
  this.iRoomCode});

  PutAwayResponseModel.fromJson(Map<String, dynamic> json)
  {
    iLocationRoomFacility =json['ILocationRoomFacility'];
    confirmPickPartFlag = json['Confirm_Pick_Part_flag'];
    iPutawayLabelID = json['IPutawayLabelID'];
    iRoomCode = json['IRoomCode'];
    iPartNo =json['Ipart_no'] ;
    iPONo =json['IPO_No'] ;
    PartNo = json['Part_No'];
    putawayLocationRequiredOrRecommended =json['Putaway_Location_Required_Or_Recommended'];
    putawayLabelID = json['PutawayLabelID'];
    putawayLocation = json['PutawayLocation'];
    putawayQty = json['PutawayQty'];
    roomCode = json['Room_Code'];

  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['Confirm_Pick_Part_flag'] = this.confirmPickPartFlag;
  data['ILocationRoomFacility'] = this.iLocationRoomFacility;
  data['Ipart_no'] = this.iPartNo;
  data['IPO_No'] = this.iPONo;
  data['IPutawayLabelID'] = this.iPutawayLabelID;
  data['IRoomCode'] = this.iLocationRoomFacility;
  data['Part_No'] = this.PartNo;
  data['Putaway_Location_Required_Or_Recommended'] =
  this.putawayLocationRequiredOrRecommended;
  data['PutawayLabelID'] = this.putawayLabelID;
  data['PutawayLocation'] = this.putawayLocation;
  data['PutawayQty'] = this.putawayQty;
  data['Room_Code'] = this.roomCode;
  return data;
  }
  }




