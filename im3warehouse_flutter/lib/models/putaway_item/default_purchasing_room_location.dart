import 'package:im3_warehouse/models/facility.dart';


class DefaultPurchasingRoomLocation extends Facility{

  String companyCode;
  String iRoomAreaCode;
  String iRoomCode;
  String iZoneCode;
  String roomAreaCode;
  String roomAreaName;
  String roomCode;
  String roomName;
  String zoneCode;

  DefaultPurchasingRoomLocation.fromJson(Map<String, dynamic> json) {
    facilityCode = json['FacilityCode'];
    facilityName = json['FacilityName'];
    iFacilityCode = json['IFacilityCode'];
    iRoomAreaCode = json['IRoomAreaCode'];
    iRoomCode = json['IRoomCode'];
    iZoneCode = json['IZoneCode'];
    roomAreaCode = json['RoomAreaCode'];
    roomAreaName = json['RoomAreaName'];
    roomCode = json['RoomCode'];
    roomName = json['RoomName'];
    zoneCode = json['ZoneCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FacilityCode'] = this.facilityCode;
    data['FacilityName'] = this.facilityName;
    data['IFacilityCode'] = this.iFacilityCode;
    data['IRoomAreaCode'] = this.iRoomAreaCode;
    data['IRoomCode'] = this.iRoomCode;
    data['IZoneCode'] = this.iZoneCode;
    data['RoomAreaCode'] = this.roomAreaCode;
    data['RoomAreaName'] = this.roomAreaName;
    data['RoomCode'] = this.roomCode;
    data['RoomName'] = this.roomName;
    data['ZoneCode'] = this.zoneCode;
    return data;
  }
}

