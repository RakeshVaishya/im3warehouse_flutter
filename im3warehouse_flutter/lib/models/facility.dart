import 'package:im3_warehouse/databases/database_constant.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';

class Facility {
  static Facility facility;

  static Facility getFacilityInstance() {
    if (facility == null) {
      facility = Facility();
    }
    return facility;
  }

  String bROKERAGE;
  String companyCode;
  String cUSTOMDUTY;
  String defaultIFacilityCode;
  String facilityCode;
  String facilityName;
  String facilityCurrency;
  String fREIGHTCHARGES;
  String iFacilityCode;
  String iNSURANCE;
  String selectedFacilityCode;
  String sHIPPING;
  String tAX;

  Facility(
      {this.bROKERAGE,
      this.companyCode,
      this.cUSTOMDUTY,
      this.defaultIFacilityCode,
      this.facilityCode,
      this.facilityName,
      this.facilityCurrency,
      this.fREIGHTCHARGES,
      this.iFacilityCode,
      this.iNSURANCE,
      this.selectedFacilityCode,
      this.sHIPPING,
      this.tAX});

  Facility.fromJson(Map<String, dynamic> json) {
    bROKERAGE = json['BROKERAGE'];
    companyCode = json['Company_Code'];
    cUSTOMDUTY = json['CUSTOMDUTY'];
    defaultIFacilityCode = json['Default_IFacility_Code'];
    facilityCode = json['Facility_Code'];
    facilityName = json['Facility_Name'];
    facilityCurrency = json['FacilityCurrency'];
    fREIGHTCHARGES = json['FREIGHTCHARGES'];
    iFacilityCode = json['IFacility_Code'];
    iNSURANCE = json['INSURANCE'];
    selectedFacilityCode = json['SelectedFacility_Code'];
    sHIPPING = json['SHIPPING'];
    tAX = json['TAX'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BROKERAGE'] = this.bROKERAGE;
    data['Company_Code'] = this.companyCode;
    data['CUSTOMDUTY'] = this.cUSTOMDUTY;
    data['Default_IFacility_Code'] = this.defaultIFacilityCode;
    data['Facility_Code'] = this.facilityCode;
    data['Facility_Name'] = this.facilityName;
    data['FacilityCurrency'] = this.facilityCurrency;
    data['FREIGHTCHARGES'] = this.fREIGHTCHARGES;
    data['IFacility_Code'] = this.iFacilityCode;
    data['INSURANCE'] = this.iNSURANCE;
    data['SelectedFacility_Code'] = this.selectedFacilityCode;
    data['SHIPPING'] = this.sHIPPING;
    data['TAX'] = this.tAX;
    return data;
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map[DataBaseConstant.COLUMN_FAC_BROKERAGE] = bROKERAGE;
    map[DataBaseConstant.COLUMN_FAC_COMPANY_CODE] = companyCode;
    map[DataBaseConstant.COLUMN_FAC_CUSTOMDUTY] = cUSTOMDUTY;
    map[DataBaseConstant.COLUMN_FAC_DEFAULT_IFACILITY_CODE] =
        defaultIFacilityCode;
    map[DataBaseConstant.COLUMN_FAC_FACILIY_CODE] = facilityCode;
    map[DataBaseConstant.COLUMN_FAC_FACILITY_NAME] = facilityName;
    map[DataBaseConstant.COLUMN_FAC_FACILITY_CURRENCY] = facilityCurrency;
    map[DataBaseConstant.COLUMN_FAC_FREIGHTCHARGES] = fREIGHTCHARGES;
    map[DataBaseConstant.COLUMN_FAC_IFACILITY_CODE] = iFacilityCode;
    map[DataBaseConstant.COLUMN_FAC_INSURANCE] = iNSURANCE;
    map[DataBaseConstant.COLUMN_FAC_SELECTED_FACILITY_CODE] =
        selectedFacilityCode;
    map[DataBaseConstant.COLUMN_FAC_SHIPPING] = sHIPPING;
    map[DataBaseConstant.COLUMN_FAC_TAX] = tAX;
    return map;
  }

  Facility.fromMapObject(Map<String, dynamic> map) {
    bROKERAGE = map[DataBaseConstant.COLUMN_FAC_BROKERAGE];
    companyCode = map[DataBaseConstant.COLUMN_FAC_COMPANY_CODE];
    cUSTOMDUTY = map[DataBaseConstant.COLUMN_FAC_CUSTOMDUTY];
    defaultIFacilityCode =
        map[DataBaseConstant.COLUMN_FAC_DEFAULT_IFACILITY_CODE];
    facilityCode = map[DataBaseConstant.COLUMN_FAC_FACILIY_CODE];
    facilityName = map[DataBaseConstant.COLUMN_FAC_FACILITY_NAME];
    facilityCurrency = map[DataBaseConstant.COLUMN_FAC_FACILITY_CURRENCY];
    fREIGHTCHARGES = map[DataBaseConstant.COLUMN_FAC_FREIGHTCHARGES];
    iFacilityCode = map[DataBaseConstant.COLUMN_FAC_IFACILITY_CODE];
    iNSURANCE = map[DataBaseConstant.COLUMN_FAC_INSURANCE];
    selectedFacilityCode =
        map[DataBaseConstant.COLUMN_FAC_SELECTED_FACILITY_CODE];
    sHIPPING = map[DataBaseConstant.COLUMN_FAC_SHIPPING];
    tAX = map[DataBaseConstant.COLUMN_FAC_TAX];
  }
}
