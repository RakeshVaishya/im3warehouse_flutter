import 'package:im3_warehouse/base/BaseModel.dart';
import 'package:im3_warehouse/models/add_edit_inventory_model.dart';

class DynamicHelp extends BaseModel {
  String roomAreaCode;
  String roomAreaName;
  String iRoomAreaCode;
  String facilityCode;
  String facilityName;
  String roomCode;
  String roomDescription;
  String iCustomerID;
  String iSConsignmentRoom;
  String zoneCode;
  String zoneName;
  String iZoneCode;
  String iFacilityCode;
  String iRoomCode;

  DynamicHelp(
      {this.roomAreaCode,
      this.roomAreaName,
      this.iRoomAreaCode,
      this.facilityCode,
      this.facilityName,
      this.roomCode,
      this.roomDescription,
      this.iCustomerID,
      this.iSConsignmentRoom,
      this.zoneCode,
      this.zoneName,
      this.iZoneCode,
      this.iFacilityCode,
      this.iRoomCode
      });
  DynamicHelp.fromJson(Map<String, dynamic> json) {
    roomAreaCode = json['Room_Area_Code'];
    roomAreaName = json['Room_Area_Name'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    facilityCode = json['Facility_Code'];
    facilityName = json['Facility_Name'];
    roomCode = json['Room_Code'];
    roomDescription = json['Room_Description'];
    iCustomerID = json['ICustomer_ID'];
    iSConsignmentRoom = json['IS_Consignment_Room'];
    zoneCode = json['Zone_Code'];
    zoneName = json['Zone_Name'];
    iZoneCode = json['IZoneCode'];
    iFacilityCode = json['IFactilityCode'];
    iRoomCode = json['IRoomCode'];

    UOM_Code = json['UOM_Code'];
    IUOM_Code = json['IUOM_Code'];
    ucmDescription = json['UOM_Description'];

    IBilling_Type_Code = json['IBilling_Type_Code'];
    Billing_Type_Code = json['Billing_Type_Code'];
    billingTypeDescription = json['Billing_Type_Description'];

    //[{"IBilling_Type_Code":"1","Billing_Type_Code":"B","Billing_Type_Description":"Billable"}]
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Area_Name'] = this.roomAreaName;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['Facility_Code'] = this.facilityCode;
    data['Facility_Name'] = this.facilityName;
    data['Room_Code'] = this.roomCode;
    data['Room_Description'] = this.roomDescription;
    data['ICustomer_ID'] = this.iCustomerID;
    data['IS_Consignment_Room'] = this.iSConsignmentRoom;
    data['Zone_Code'] = this.zoneCode;
    data['Zone_Name'] = this.zoneName;
    data['IZoneCode'] = this.iZoneCode;
    data['IFactilityCode'] = this.iFacilityCode;
    data['IRoomCode'] = this.iRoomCode;
    data['UOM_Code'] = this.UOM_Code;
    data['IUOM_Code'] = this.IUOM_Code;
    data['UOM_Description'] = this.ucmDescription;
    data['IBilling_Type_Code'] = this.IBilling_Type_Code;
    data['Billing_Type_Code'] = this.Billing_Type_Code;
    data['Billing_Type_Description'] = this.billingTypeDescription;
    return data;
  }
}
