class InventoryDetail {
  String availableQuantity;
  String location;
  String onHandQuantity;
  String partNo;
  String stockRoom;


  InventoryDetail({
    this.availableQuantity,
    this.location,
    this.onHandQuantity,
    this.partNo,
    this.stockRoom,
  });

  InventoryDetail.fromJson(Map<String, dynamic> json) {
    availableQuantity = json['Available_quantity'];
    location = json['Location'];
    onHandQuantity = json['On_Hand_Quantity'];
    partNo = json['Part_No'];
    stockRoom = json['Stock_Room'];
  }
}
