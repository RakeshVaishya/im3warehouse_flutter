class Equipment{
  String eQCode;
  String eQShortDescription;
  String parentEQCode;
  String serialNo;
  String model;
  String iEQCode;
  String vIN;
  String accountCode;
  String departmentCode;
  String lineProductCode;

  Equipment(
  {this.eQCode,
  this.eQShortDescription,
  this.parentEQCode,
  this.serialNo,
  this.model,
  this.iEQCode,
  this.vIN,
  this.accountCode,
  this.departmentCode,
  this.lineProductCode});

  Equipment.fromJson(Map<String, dynamic> json) {
  eQCode = json['EQ_Code'];
  eQShortDescription = json['EQ_Short_Description'];
  parentEQCode = json['Parent_EQ_Code'];
  serialNo = json['Serial_No'];
  model = json['Model'];
  iEQCode = json['IEQ_Code'];
  vIN = json['VIN'];
  accountCode = json['Account_Code'];
  departmentCode = json['Department_code'];
  lineProductCode = json['Line_Product_Code'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['EQ_Code'] = this.eQCode;
  data['EQ_Short_Description'] = this.eQShortDescription;
  data['Parent_EQ_Code'] = this.parentEQCode;
  data['Serial_No'] = this.serialNo;
  data['Model'] = this.model;
  data['IEQ_Code'] = this.iEQCode;
  data['VIN'] = this.vIN;
  data['Account_Code'] = this.accountCode;
  data['Department_code'] = this.departmentCode;
  data['Line_Product_Code'] = this.lineProductCode;
  return data;
  }
  }

