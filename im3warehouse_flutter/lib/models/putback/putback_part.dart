import 'package:im3_warehouse/models/part_details.dart';

class PutBackPart extends PartDetails{

  String actionSrNo;
  String actualQuantity;
  String caseSizeFlag;
  String companyCode;
  String defaultCaseSize;
  String expDate;
  String expirationDate;
  String facilityCode;
  String facilityName;
  String iBillingTypeCode;
  String iEQCode;
  String iFacilityCode;
  String iPartNo;
  String iRoomAreaCode;
  String iRoomCode;
  String isProductionPart;
  String isPutBack;
  String isRebuildPart;
  String itemLedgerCost;
  String iUOMCode;
  String iWONo;
  String iWOTaskNo;
  String iWOTaskSOPSrNo;
  String lineItemSrNo;
  String onHandQuantity;
  String partDescription;
  String partNo;
  String partQuantity;
  String partSrNo;
  String putBackQuantity;
  String quantity;
  String roomAreaCode;
  String roomCode;
  String serialNumber;
  String serialNo;
  String stockSrNo;
  String taskCode;
  String transactionDate;
  String transactionType;
  String vendorLotID;
  String vendorLotNumber;
  String wOPartStatusCode;
  String wOID;
  String wOTskInst;
 bool selected =false;

  PutBackPart(
      {this.actionSrNo,
        this.actualQuantity,
        this.caseSizeFlag,
        this.companyCode,
        this.defaultCaseSize,
        this.expDate,
        this.expirationDate,
        this.facilityCode,
        this.facilityName,
        this.iBillingTypeCode,
        this.iEQCode,
        this.iFacilityCode,
        this.iPartNo,
        this.iRoomAreaCode,
        this.iRoomCode,
        this.isProductionPart,
        this.isPutBack,
        this.isRebuildPart,
        this.itemLedgerCost,
        this.iUOMCode,
        this.iWONo,
        this.iWOTaskNo,
        this.iWOTaskSOPSrNo,
        this.lineItemSrNo,
        this.onHandQuantity,
        this.partDescription,
        this.partNo,
        this.partQuantity,
        this.partSrNo,
        this.putBackQuantity,
        this.quantity,
        this.roomAreaCode,
        this.roomCode,
        this.serialNumber,
        this.serialNo,
        this.stockSrNo,
        this.taskCode,
        this.transactionDate,
        this.transactionType,
        this.vendorLotID,
        this.vendorLotNumber,
        this.wOPartStatusCode,
        this.wOID,
        this.wOTskInst});

  PutBackPart.fromJson(Map<String, dynamic> json) {
    actionSrNo = json['Action_Sr_No'];
    actualQuantity = json['Actual_Quantity'];
    caseSizeFlag = json['Case_Size_flag'];
    companyCode = json['Company_Code'];
    defaultCaseSize = json['Default_Case_Size'];
    expDate = json['ExpDate'];
    expirationDate = json['Expiration_Date'];
    facilityCode = json['Facility_Code'];
    facilityName = json['Facility_Name'];
    iBillingTypeCode = json['IBilling_Type_Code'];
    iEQCode = json['IEQ_Code'];
    iFacilityCode = json['IFacility_Code'];
    iPartNo = json['IPart_No'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    isProductionPart = json['Is_Production_Part'];
    isPutBack = json['Is_PutBack'];
    isRebuildPart = json['IsRebuildPart'];
    itemLedgerCost = json['Item_Ledger_Cost'];
    iUOMCode = json['IUOM_Code'];
    iWONo = json['IWO_No'];
    iWOTaskNo = json['IWO_Task_No'];
    iWOTaskSOPSrNo = json['IWO_Task_SOP_Sr_No'];
    lineItemSrNo = json['Line_Item_Sr_No'];
    onHandQuantity = json['On_Hand_Quantity'];
    partDescription = json['Part_Description'];
    partNo = json['Part_No'];
    partQuantity = json['Part_Quantity'];
    partSrNo = json['Part_Sr_No'];
    putBackQuantity = json['PutBack_Quantity'];
    quantity = json['Quantity'];
    Rate = json['Rate'];
    roomAreaCode = json['Room_Area_Code'];
    roomCode = json['Room_Code'];
    serialNumber = json['Serial_Number'];
    serialNo = json['SerialNo'];
    stockSrNo = json['Stock_Sr_No'];
    taskCode = json['TaskCode'];
    transactionDate = json['Transaction_Date'];
    transactionType = json['Transaction_Type'];
    vendorLotID = json['Vendor_Lot_ID'];
    print("vendorLotID--"+vendorLotID);
    vendorLotNumber = json['Vendor_Lot_Number'];
    wOPartStatusCode = json['WO_Part_Status_Code'];
    wOID = json['WOID'];
    wOTskInst = json['WOTskInst'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Action_Sr_No'] = this.actionSrNo;
    data['Actual_Quantity'] = this.actualQuantity;
    data['Case_Size_flag'] = this.caseSizeFlag;
    data['Company_Code'] = this.companyCode;
    data['Default_Case_Size'] = this.defaultCaseSize;
    data['ExpDate'] = this.expDate;
    data['Expiration_Date'] = this.expirationDate;
    data['Facility_Code'] = this.facilityCode;
    data['Facility_Name'] = this.facilityName;
    data['IBilling_Type_Code'] = this.iBillingTypeCode;
    data['IEQ_Code'] = this.iEQCode;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IPart_No'] = this.iPartNo;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['Is_Production_Part'] = this.isProductionPart;
    data['Is_PutBack'] = this.isPutBack;
    data['IsRebuildPart'] = this.isRebuildPart;
    data['Item_Ledger_Cost'] = this.itemLedgerCost;
    data['IUOM_Code'] = this.iUOMCode;
    data['IWO_No'] = this.iWONo;
    data['IWO_Task_No'] = this.iWOTaskNo;
    data['IWO_Task_SOP_Sr_No'] = this.iWOTaskSOPSrNo;
    data['Line_Item_Sr_No'] = this.lineItemSrNo;
    data['On_Hand_Quantity'] = this.onHandQuantity;
    data['Part_Description'] = this.partDescription;
    data['Part_No'] = this.partNo;
    data['Part_Quantity'] = this.partQuantity;
    data['Part_Sr_No'] = this.partSrNo;
    data['PutBack_Quantity'] = this.putBackQuantity;
    data['Quantity'] = this.quantity;
    data['Rate'] = this.Rate;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Code'] = this.roomCode;
    data['Serial_Number'] = this.serialNumber;
    data['SerialNo'] = this.serialNo;
    data['Stock_Sr_No'] = this.stockSrNo;
    data['TaskCode'] = this.taskCode;
    data['Transaction_Date'] = this.transactionDate;
    data['Transaction_Type'] = this.transactionType;
    data['Vendor_Lot_ID'] = this.vendorLotID;
    data['Vendor_Lot_Number'] = this.vendorLotNumber;
    data['WO_Part_Status_Code'] = this.wOPartStatusCode;
    data['WOID'] = this.wOID;
    data['WOTskInst'] = this.wOTskInst;
    return data;
  }
}