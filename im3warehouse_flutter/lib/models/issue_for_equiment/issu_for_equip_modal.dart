class IssuForEquipmentModal {
  String accountDescription;
  String accountNo;
  String assetNo;
  String cF1;
  String cF10;
  String cF2;
  String cF3;
  String cF4;
  String cF5;
  String cF6;
  String cF7;
  String cF8;
  String cF9;
  String companyCode;
  String createDateTime;
  String createUserID;
  String currencyCode;
  String currencySymbol;
  String dateInstalled;
  String departmentCode;
  String departmentName;
  String displayDateInstalled;
  String displayPMScheduledDate;
  String eQCode;
  String eQLongDescription;
  String eQRiskDescription;
  String eQShortDescription;
  String eQTypeCode;
  String facilityCode;
  String facilityName;
  String hDepartmentName;
  String hEquipmentDescription;
  String hRoomAreaName;
  String hRoomDescription;
  String iAccountNo;
  String iAccountNo1;
  String iDepartmentCode;
  String iEQCode;
  String iEQConditionCode;
  String iEQRiskCode;
  String iEQStatusCode;
  String iEQTypeCode;
  String iFacilityCode;
  String iLineProductCode;
  String iManufacturerCode;
  String iPMAccountNo;
  String iProjectCode;
  String iRoomAreaCode;
  String iRoomCode;
  String iUOMCode;
  String iVendorCode;
  String lastUpdateDateTime;
  String lastUpdateUserID;
  String level;
  String lineProductCode;
  String lineProductDescription;
  String location;
  String manufacturerCode;
  String manufacturerName;
  String model;
  String noPMTag;
  String noWO;
  String parentEQCode;
  String parentIEQCode;
  String pMAccountNo;
  String pMScheduledDate;
  String replacementCost;
  String responsibleEmployeeID;
  String responsibleEmployeeName;
  String riskCode;
  String roomAreaCode;
  String roomAreaName;
  String roomCode;
  String roomDescription;
  String serialNo;
  String vendorCode;
  String vendorName;

  IssuForEquipmentModal(
      {this.accountDescription,
      this.accountNo,
      this.assetNo,
      this.cF1,
      this.cF10,
      this.cF2,
      this.cF3,
      this.cF4,
      this.cF5,
      this.cF6,
      this.cF7,
      this.cF8,
      this.cF9,
      this.companyCode,
      this.createDateTime,
      this.createUserID,
      this.currencyCode,
      this.currencySymbol,
      this.dateInstalled,
      this.departmentCode,
      this.departmentName,
      this.displayDateInstalled,
      this.displayPMScheduledDate,
      this.eQCode,
      this.eQLongDescription,
      this.eQRiskDescription,
      this.eQShortDescription,
      this.eQTypeCode,
      this.facilityCode,
      this.facilityName,
      this.hDepartmentName,
      this.hEquipmentDescription,
      this.hRoomAreaName,
      this.hRoomDescription,
      this.iAccountNo,
      this.iAccountNo1,
      this.iDepartmentCode,
      this.iEQCode,
      this.iEQConditionCode,
      this.iEQRiskCode,
      this.iEQStatusCode,
      this.iEQTypeCode,
      this.iFacilityCode,
      this.iLineProductCode,
      this.iManufacturerCode,
      this.iPMAccountNo,
      this.iProjectCode,
      this.iRoomAreaCode,
      this.iRoomCode,
      this.iUOMCode,
      this.iVendorCode,
      this.lastUpdateDateTime,
      this.lastUpdateUserID,
      this.level,
      this.lineProductCode,
      this.lineProductDescription,
      this.location,
      this.manufacturerCode,
      this.manufacturerName,
      this.model,
      this.noPMTag,
      this.noWO,
      this.parentEQCode,
      this.parentIEQCode,
      this.pMAccountNo,
      this.pMScheduledDate,
      this.replacementCost,
      this.responsibleEmployeeID,
      this.responsibleEmployeeName,
      this.riskCode,
      this.roomAreaCode,
      this.roomAreaName,
      this.roomCode,
      this.roomDescription,
      this.serialNo,
      this.vendorCode,
      this.vendorName});

  IssuForEquipmentModal.fromJson(Map<String, dynamic> json) {
    accountDescription = json['Account_Description'];
    accountNo = json['Account_No'];
    assetNo = json['Asset_No'];
    cF1 = json['CF_1'];
    cF10 = json['CF_10'];
    cF2 = json['CF_2'];
    cF3 = json['CF_3'];
    cF4 = json['CF_4'];
    cF5 = json['CF_5'];
    cF6 = json['CF_6'];
    cF7 = json['CF_7'];
    cF8 = json['CF_8'];
    cF9 = json['CF_9'];
    companyCode = json['Company_Code'];
    createDateTime = json['Create_Date_Time'];
    createUserID = json['Create_User_ID'];
    currencyCode = json['Currency_Code'];
    currencySymbol = json['Currency_Symbol'];
    dateInstalled = json['Date_Installed'];
    departmentCode = json['Department_code'];
    departmentName = json['Department_Name'];
    displayDateInstalled = json['Display_Date_Installed'];
    displayPMScheduledDate = json['Display_PM_Scheduled_Date'];
    eQCode = json['EQ_Code'];
    eQLongDescription = json['EQ_Long_Description'];
    eQRiskDescription = json['EQ_Risk_Description'];
    eQShortDescription = json['EQ_Short_Description'];
    eQTypeCode = json['EQ_Type_Code'];
    facilityCode = json['Facility_Code'];
    facilityName = json['Facility_Name'];
    hDepartmentName = json['HDepartment_Name'];
    hEquipmentDescription = json['HEquipment_Description'];
    hRoomAreaName = json['HRoom_Area_Name'];
    hRoomDescription = json['HRoom_Description'];
    iAccountNo = json['IAccount_No'];
    iAccountNo1 = json['IAccount_No1'];
    iDepartmentCode = json['IDepartment_Code'];
    iEQCode = json['IEQ_Code'];
    iEQConditionCode = json['IEQ_Condition_Code'];
    iEQRiskCode = json['IEQ_Risk_Code'];
    iEQStatusCode = json['IEQ_Status_Code'];
    iEQTypeCode = json['IEQ_Type_Code'];
    iFacilityCode = json['IFacility_Code'];
    iLineProductCode = json['ILine_Product_Code'];
    iManufacturerCode = json['IManufacturer_Code'];
    iPMAccountNo = json['IPM_Account_No'];
    iProjectCode = json['IProject_Code'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    iUOMCode = json['IUOM_Code'];
    iVendorCode = json['IVendor_Code'];
    lastUpdateDateTime = json['Last_Update_Date_time'];
    lastUpdateUserID = json['Last_Update_User_ID'];
    level = json['Level'];
    lineProductCode = json['Line_Product_Code'];
    lineProductDescription = json['Line_Product_Description'];
    location = json['Location'];
    manufacturerCode = json['Manufacturer_Code'];
    manufacturerName = json['Manufacturer_Name'];
    model = json['Model'];
    noPMTag = json['No_PM_Tag'];
    noWO = json['No_WO'];
    parentEQCode = json['Parent_EQ_Code'];
    parentIEQCode = json['Parent_IEQ_Code'];
    pMAccountNo = json['PM_Account_No'];
    pMScheduledDate = json['PM_Scheduled_Date'];
    replacementCost = json['Replacement_Cost'];
    responsibleEmployeeID = json['Responsible_Employee_ID'];
    responsibleEmployeeName = json['Responsible_Employee_Name'];
    riskCode = json['Risk_Code'];
    roomAreaCode = json['Room_Area_Code'];
    roomAreaName = json['Room_Area_Name'];
    roomCode = json['Room_Code'];
    roomDescription = json['Room_Description'];
    serialNo = json['Serial_No'];
    vendorCode = json['Vendor_code'];
    vendorName = json['Vendor_Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Account_Description'] = this.accountDescription;
    data['Account_No'] = this.accountNo;
    data['Asset_No'] = this.assetNo;
    data['CF_1'] = this.cF1;
    data['CF_10'] = this.cF10;
    data['CF_2'] = this.cF2;
    data['CF_3'] = this.cF3;
    data['CF_4'] = this.cF4;
    data['CF_5'] = this.cF5;
    data['CF_6'] = this.cF6;
    data['CF_7'] = this.cF7;
    data['CF_8'] = this.cF8;
    data['CF_9'] = this.cF9;
    data['Company_Code'] = this.companyCode;
    data['Create_Date_Time'] = this.createDateTime;
    data['Create_User_ID'] = this.createUserID;
    data['Currency_Code'] = this.currencyCode;
    data['Currency_Symbol'] = this.currencySymbol;
    data['Date_Installed'] = this.dateInstalled;
    data['Department_code'] = this.departmentCode;
    data['Department_Name'] = this.departmentName;
    data['Display_Date_Installed'] = this.displayDateInstalled;
    data['Display_PM_Scheduled_Date'] = this.displayPMScheduledDate;
    data['EQ_Code'] = this.eQCode;
    data['EQ_Long_Description'] = this.eQLongDescription;
    data['EQ_Risk_Description'] = this.eQRiskDescription;
    data['EQ_Short_Description'] = this.eQShortDescription;
    data['EQ_Type_Code'] = this.eQTypeCode;
    data['Facility_Code'] = this.facilityCode;
    data['Facility_Name'] = this.facilityName;
    data['HDepartment_Name'] = this.hDepartmentName;
    data['HEquipment_Description'] = this.hEquipmentDescription;
    data['HRoom_Area_Name'] = this.hRoomAreaName;
    data['HRoom_Description'] = this.hRoomDescription;
    data['IAccount_No'] = this.iAccountNo;
    data['IAccount_No1'] = this.iAccountNo1;
    data['IDepartment_Code'] = this.iDepartmentCode;
    data['IEQ_Code'] = this.iEQCode;
    data['IEQ_Condition_Code'] = this.iEQConditionCode;
    data['IEQ_Risk_Code'] = this.iEQRiskCode;
    data['IEQ_Status_Code'] = this.iEQStatusCode;
    data['IEQ_Type_Code'] = this.iEQTypeCode;
    data['IFacility_Code'] = this.iFacilityCode;
    data['ILine_Product_Code'] = this.iLineProductCode;
    data['IManufacturer_Code'] = this.iManufacturerCode;
    data['IPM_Account_No'] = this.iPMAccountNo;
    data['IProject_Code'] = this.iProjectCode;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['IUOM_Code'] = this.iUOMCode;
    data['IVendor_Code'] = this.iVendorCode;
    data['Last_Update_Date_time'] = this.lastUpdateDateTime;
    data['Last_Update_User_ID'] = this.lastUpdateUserID;
    data['Level'] = this.level;
    data['Line_Product_Code'] = this.lineProductCode;
    data['Line_Product_Description'] = this.lineProductDescription;
    data['Location'] = this.location;
    data['Manufacturer_Code'] = this.manufacturerCode;
    data['Manufacturer_Name'] = this.manufacturerName;
    data['Model'] = this.model;
    data['No_PM_Tag'] = this.noPMTag;
    data['No_WO'] = this.noWO;
    data['Parent_EQ_Code'] = this.parentEQCode;
    data['Parent_IEQ_Code'] = this.parentIEQCode;
    data['PM_Account_No'] = this.pMAccountNo;
    data['PM_Scheduled_Date'] = this.pMScheduledDate;
    data['Replacement_Cost'] = this.replacementCost;
    data['Responsible_Employee_ID'] = this.responsibleEmployeeID;
    data['Responsible_Employee_Name'] = this.responsibleEmployeeName;
    data['Risk_Code'] = this.riskCode;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Area_Name'] = this.roomAreaName;
    data['Room_Code'] = this.roomCode;
    data['Room_Description'] = this.roomDescription;
    data['Serial_No'] = this.serialNo;
    data['Vendor_code'] = this.vendorCode;
    data['Vendor_Name'] = this.vendorName;
    return data;
  }
}
