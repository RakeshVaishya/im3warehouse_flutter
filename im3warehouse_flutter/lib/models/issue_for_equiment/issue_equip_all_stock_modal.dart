import 'package:im3_warehouse/utils/utility.dart';

class IssueEquipGetAllStockModal {
  String availableQuantity;
  String billingTypeCode;
  String breakpackLocation;
  String cF1;
  String cF10;
  String cF2;
  String cF3;
  String cF4;
  String cF5;
  String cF6;
  String cF7;
  String cF8;
  String cF9;
  String companyCode;
  String createDateTime;
  String createUserID;
  String facilityCode;
  String facilityName;
  String hardReserveQuantity;
  String iBillingTypeCode;
  String iFacilityCode;
  String iPartNo;
  String iRoomAreaCode;
  String iRoomCode;
  String isPrimaryStockroom;
  String iZoneCode;
  String lastUpdateDateTime;
  String lastUpdateUserID;
  String maximumQuantity;
  String minimumQuantity;
  String noOfStockOuts;
  String onHandQty;
  String onHandQuantity;
  String onPOQuantity;
  String openBalanceQuantity;
  String partDescription;
  String partNo;
  String pickableLocation;
  String primaryStockroom;
  String probableReserveQuantity;
  String reorderQuantity;
  String reserveQuantity;
  String roomAreaCode;
  String roomAreaName;
  String roomCode;
  String roomDescription;
  String softReserveQuantity;
  String sortAvailableQuantity;
  String sortHardReserveQuantity;
  String sortMaximumQuantity;
  String sortMinimumQuantity;
  String sortProbableReserveQuantity;
  String sortReorderQuantity;
  String sortSoftReserveQuantity;
  String stockRoomStatus;
  String stockSrNo;
  String stockRoom;
  String zoneCode;
  List<String> stockRoomList =[];

  IssueEquipGetAllStockModal(
      {this.availableQuantity,
      this.billingTypeCode,
      this.breakpackLocation,
      this.cF1,
      this.cF10,
      this.cF2,
      this.cF3,
      this.cF4,
      this.cF5,
      this.cF6,
      this.cF7,
      this.cF8,
      this.cF9,
      this.companyCode,
      this.createDateTime,
      this.createUserID,
      this.facilityCode,
      this.facilityName,
      this.hardReserveQuantity,
      this.iBillingTypeCode,
      this.iFacilityCode,
      this.iPartNo,
      this.iRoomAreaCode,
      this.iRoomCode,
      this.isPrimaryStockroom,
      this.iZoneCode,
      this.lastUpdateDateTime,
      this.lastUpdateUserID,
      this.maximumQuantity,
      this.minimumQuantity,
      this.noOfStockOuts,
      this.onHandQty,
      this.onHandQuantity,
      this.onPOQuantity,
      this.openBalanceQuantity,
      this.partDescription,
      this.partNo,
      this.pickableLocation,
      this.primaryStockroom,
      this.probableReserveQuantity,
      this.reorderQuantity,
      this.reserveQuantity,
      this.roomAreaCode,
      this.roomAreaName,
      this.roomCode,
      this.roomDescription,
      this.softReserveQuantity,
      this.sortAvailableQuantity,
      this.sortHardReserveQuantity,
      this.sortMaximumQuantity,
      this.sortMinimumQuantity,
      this.sortProbableReserveQuantity,
      this.sortReorderQuantity,
      this.sortSoftReserveQuantity,
      this.stockRoomStatus,
      this.stockSrNo,
      this.stockRoom,
      this.zoneCode});

  IssueEquipGetAllStockModal.fromJson(Map<String, dynamic> json) {
    availableQuantity = json['Available_Quantity'];
    billingTypeCode = json['Billing_Type_Code'];
    breakpackLocation = json['Breakpack_Location'];
    cF1 = json['CF_1'];
    cF10 = json['CF_10'];
    cF2 = json['CF_2'];
    cF3 = json['CF_3'];
    cF4 = json['CF_4'];
    cF5 = json['CF_5'];
    cF6 = json['CF_6'];
    cF7 = json['CF_7'];
    cF8 = json['CF_8'];
    cF9 = json['CF_9'];
    companyCode = json['Company_Code'];
    createDateTime = json['Create_Date_Time'];
    createUserID = json['Create_User_ID'];
    facilityCode = json['Facility_Code'];
    facilityName = json['Facility_Name'];
    hardReserveQuantity = json['HardReserveQuantity'];
    iBillingTypeCode = json['IBilling_Type_Code'];
    iFacilityCode = json['IFacility_Code'];
    iPartNo = json['IPart_No'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    isPrimaryStockroom = json['Is_Primary_Stockroom'];
    iZoneCode = json['IZone_Code'];
    lastUpdateDateTime = json['Last_Update_Date_time'];
    lastUpdateUserID = json['Last_Update_User_ID'];
    maximumQuantity = json['Maximum_Quantity'];
    minimumQuantity = json['Minimum_Quantity'];
    noOfStockOuts = json['No_of_Stock_Outs'];
    onHandQty = json['On_Hand_Qty'];
    onHandQuantity = json['On_Hand_Quantity'];
    onPOQuantity = json['OnPOQuantity'];
    openBalanceQuantity = json['Open_Balance_Quantity'];
    partDescription = json['Part_Description'];
    partNo = json['Part_No'];
    pickableLocation = json['Pickable_Location'];
    primaryStockroom = json['Primary_Stockroom'];
    probableReserveQuantity = json['ProbableReserveQuantity'];
    reorderQuantity = json['Reorder_quantity'];
    reserveQuantity = json['Reserve_Quantity'];
    roomAreaCode = json['Room_Area_Code'];
    roomAreaName = json['Room_Area_Name'];
    roomCode = json['Room_Code'];
    roomDescription = json['Room_Description'];
    softReserveQuantity = json['Soft_Reserve_Quantity'];
    sortAvailableQuantity = json['Sort_Available_Quantity'];
    sortHardReserveQuantity = json['Sort_HardReserveQuantity'];
    sortMaximumQuantity = json['Sort_Maximum_Quantity'];
    sortMinimumQuantity = json['Sort_Minimum_Quantity'];
    sortProbableReserveQuantity = json['Sort_ProbableReserveQuantity'];
    sortReorderQuantity = json['Sort_Reorder_quantity'];
    sortSoftReserveQuantity = json['Sort_Soft_Reserve_Quantity'];
    stockRoomStatus = json['Stock_Room_Status'];
    stockSrNo = json['Stock_Sr_No'];
    stockRoom = json['StockRoom'];
    zoneCode = json['Zone_Code'];
    stockRoomList= Utility.setDataInList(stockRoom);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Available_Quantity'] = this.availableQuantity;
    data['Billing_Type_Code'] = this.billingTypeCode;
    data['Breakpack_Location'] = this.breakpackLocation;
    data['CF_1'] = this.cF1;
    data['CF_10'] = this.cF10;
    data['CF_2'] = this.cF2;
    data['CF_3'] = this.cF3;
    data['CF_4'] = this.cF4;
    data['CF_5'] = this.cF5;
    data['CF_6'] = this.cF6;
    data['CF_7'] = this.cF7;
    data['CF_8'] = this.cF8;
    data['CF_9'] = this.cF9;
    data['Company_Code'] = this.companyCode;
    data['Create_Date_Time'] = this.createDateTime;
    data['Create_User_ID'] = this.createUserID;
    data['Facility_Code'] = this.facilityCode;
    data['Facility_Name'] = this.facilityName;
    data['HardReserveQuantity'] = this.hardReserveQuantity;
    data['IBilling_Type_Code'] = this.iBillingTypeCode;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IPart_No'] = this.iPartNo;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['Is_Primary_Stockroom'] = this.isPrimaryStockroom;
    data['IZone_Code'] = this.iZoneCode;
    data['Last_Update_Date_time'] = this.lastUpdateDateTime;
    data['Last_Update_User_ID'] = this.lastUpdateUserID;
    data['Maximum_Quantity'] = this.maximumQuantity;
    data['Minimum_Quantity'] = this.minimumQuantity;
    data['No_of_Stock_Outs'] = this.noOfStockOuts;
    data['On_Hand_Qty'] = this.onHandQty;
    data['On_Hand_Quantity'] = this.onHandQuantity;
    data['OnPOQuantity'] = this.onPOQuantity;
    data['Open_Balance_Quantity'] = this.openBalanceQuantity;
    data['Part_Description'] = this.partDescription;
    data['Part_No'] = this.partNo;
    data['Pickable_Location'] = this.pickableLocation;
    data['Primary_Stockroom'] = this.primaryStockroom;
    data['ProbableReserveQuantity'] = this.probableReserveQuantity;
    data['Reorder_quantity'] = this.reorderQuantity;
    data['Reserve_Quantity'] = this.reserveQuantity;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Area_Name'] = this.roomAreaName;
    data['Room_Code'] = this.roomCode;
    data['Room_Description'] = this.roomDescription;
    data['Soft_Reserve_Quantity'] = this.softReserveQuantity;
    data['Sort_Available_Quantity'] = this.sortAvailableQuantity;
    data['Sort_HardReserveQuantity'] = this.sortHardReserveQuantity;
    data['Sort_Maximum_Quantity'] = this.sortMaximumQuantity;
    data['Sort_Minimum_Quantity'] = this.sortMinimumQuantity;
    data['Sort_ProbableReserveQuantity'] = this.sortProbableReserveQuantity;
    data['Sort_Reorder_quantity'] = this.sortReorderQuantity;
    data['Sort_Soft_Reserve_Quantity'] = this.sortSoftReserveQuantity;
    data['Stock_Room_Status'] = this.stockRoomStatus;
    data['Stock_Sr_No'] = this.stockSrNo;
    data['StockRoom'] = this.stockRoom;
    data['Zone_Code'] = this.zoneCode;
    return data;
  }
}
