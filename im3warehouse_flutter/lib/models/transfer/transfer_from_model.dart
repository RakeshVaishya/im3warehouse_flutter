import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/utils/utility.dart';

class TransferFromModel extends DynamicHelp{

  String companyCode;
  String onHandQuantity;
  String primaryStockroom;
  String stockSrNo;
  String stockRoom;
  List<String> stockRoomList =[];
  TransferFromModel();

  TransferFromModel.fromJson(Map<String, dynamic> json) {
  companyCode = json['Company_Code'];
  facilityCode = json['Facility_Code'];
  iFacilityCode = json['IFacility_Code'];
  iRoomAreaCode = json['IRoom_Area_Code'];
  iRoomCode = json['IRoom_Code'];
  onHandQuantity = json['On_Hand_Quantity'];
  primaryStockroom = json['Primary_Stockroom'];
  roomAreaCode = json['Room_Area_Code'];
  roomCode = json['Room_Code'];
  stockSrNo = json['Stock_Sr_No'];
  stockRoom = json['StockRoom'];
  //stockRoomList= Utility.setDataInList(stockRoom);
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['Company_Code'] = this.companyCode;
  data['Facility_Code'] = this.facilityCode;
  data['IFacility_Code'] = this.iFacilityCode;
  data['IRoom_Area_Code'] = this.iRoomAreaCode;
  data['IRoom_Code'] = this.iRoomCode;
  data['On_Hand_Quantity'] = this.onHandQuantity;
  data['Primary_Stockroom'] = this.primaryStockroom;
  data['Room_Area_Code'] = this.roomAreaCode;
  data['Room_Code'] = this.roomCode;
  data['Stock_Sr_No'] = this.stockSrNo;
  data['StockRoom'] = this.stockRoom;
  return data;
    }

  }




