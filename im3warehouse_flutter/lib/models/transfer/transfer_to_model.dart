import 'package:im3_warehouse/base/BaseModel.dart';
import 'package:im3_warehouse/utils/utility.dart';

import 'transfer_from_model.dart';

class TransferToModel extends TransferFromModel{

  String createDateTime;
  String createUserID;
  String facility;
  String hRoomAreaName;
  String lastUpdateDateTime;
  String lastUpdateUserID;
  String room;
  List<String> roomList =[];


  TransferToModel.fromJson(Map<String, dynamic> json) {
    companyCode = json['Company_Code'];
    createDateTime = json['Create_Date_Time'];
    createUserID = json['Create_User_ID'];
    facility = json['Facility'];
    facilityCode = json['Facility_Code'];
    facilityName = json['Facility_Name'];
    hRoomAreaName = json['HRoom_Area_Name'];
    iFacilityCode = json['IFacility_Code'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    iZoneCode = json['IZoneCode'];
    lastUpdateDateTime = json['Last_Update_Date_time'];
    lastUpdateUserID = json['Last_Update_User_ID'];
    room = json['Room'];
    roomList= Utility.setDataInList(room);
    roomAreaCode = json['Room_Area_Code'];
    roomAreaName = json['Room_Area_Name'];
    roomCode = json['Room_Code'];
    roomDescription = json['Room_Description'];
    zoneCode = json['Zone_Code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Company_Code'] = this.companyCode;
    data['Create_Date_Time'] = this.createDateTime;
    data['Create_User_ID'] = this.createUserID;
    data['Facility'] = this.facility;
    data['Facility_Code'] = this.facilityCode;
    data['Facility_Name'] = this.facilityName;
    data['HRoom_Area_Name'] = this.hRoomAreaName;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['IZoneCode'] = this.iZoneCode;
    data['Last_Update_Date_time'] = this.lastUpdateDateTime;
    data['Last_Update_User_ID'] = this.lastUpdateUserID;
    data['Room'] = this.room;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Area_Name'] = this.roomAreaName;
    data['Room_Code'] = this.roomCode;
    data['Room_Description'] = this.roomDescription;
    data['Zone_Code'] = this.zoneCode;
    return data;
  }
}

