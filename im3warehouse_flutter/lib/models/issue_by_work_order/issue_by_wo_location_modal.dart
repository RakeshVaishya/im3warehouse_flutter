import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/utils/utility.dart';

class IssueByWoLocationModel extends DynamicHelp {
  String breakpackLocation;
  String companyCode;
  String onHandQuantity;
  String pickableLocation;
  String primaryStockroom;
  String stockSrNo;
  String stockRoom;

  IssueByWoLocationModel(
      {this.breakpackLocation,
      this.companyCode,
      this.onHandQuantity,
      this.pickableLocation,
      this.primaryStockroom,
      this.stockSrNo,
      this.stockRoom});

  IssueByWoLocationModel.fromJson(Map<String, dynamic> json) {
    breakpackLocation = json['Breakpack_Location'];
    companyCode = json['Company_Code'];
    facilityCode = json['Facility_Code'];
    iFacilityCode = json['IFacility_Code'];
    iRoomAreaCode = json['IRoom_Area_Code'];
    iRoomCode = json['IRoom_Code'];
    onHandQuantity = json['On_Hand_Quantity'];
    pickableLocation = json['Pickable_Location'];
    primaryStockroom = json['Primary_Stockroom'];
    roomAreaCode = json['Room_Area_Code'];
    roomCode = json['Room_Code'];
    stockSrNo = json['Stock_Sr_No'];
    stockRoom = json['StockRoom'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Breakpack_Location'] = this.breakpackLocation;
    data['Company_Code'] = this.companyCode;
    data['Facility_Code'] = this.facilityCode;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IRoom_Area_Code'] = this.iRoomAreaCode;
    data['IRoom_Code'] = this.iRoomCode;
    data['On_Hand_Quantity'] = this.onHandQuantity;
    data['Pickable_Location'] = this.pickableLocation;
    data['Primary_Stockroom'] = this.primaryStockroom;
    data['Room_Area_Code'] = this.roomAreaCode;
    data['Room_Code'] = this.roomCode;
    data['Stock_Sr_No'] = this.stockSrNo;
    data['StockRoom'] = this.stockRoom;
    return data;
  }
}
