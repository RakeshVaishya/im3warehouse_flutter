class IssueByWorkOrderModal {
  String iCustomerID;
  String iFacilityCode;
  String iWONo;
  String wONo;
  String workOrderDescription;

  IssueByWorkOrderModal(
      {this.iCustomerID,
      this.iFacilityCode,
      this.iWONo,
      this.wONo,
      this.workOrderDescription});

  IssueByWorkOrderModal.fromJson(Map<String, dynamic> json) {

    iCustomerID = json['ICustomer_ID'];
    iFacilityCode = json['IFacility_Code'];
    iWONo = json['IWO_No'];
    wONo = json['WO_No'];
    workOrderDescription = json['Work_Order_Description'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ICustomer_ID'] = this.iCustomerID;
    data['IFacility_Code'] = this.iFacilityCode;
    data['IWO_No'] = this.iWONo;

    data['WO_No'] = this.wONo;
    data['Work_Order_Description'] = this.workOrderDescription;

    return data;
  }
}
