import 'package:intl/intl.dart';

class DateTimeUtil{
static  String  DATE_FORMAT_MM_DD_YYYY="mm/dd/yyyy";
  static String getCurrentDate(String forMat){
    DateTime today = new DateTime.now();
      return "${today.month.toString()}"+forMat+"${today.day.toString().padLeft(2,'0')}"
          ""+forMat+"${today.year.toString().padLeft(2,'0')}";
  }

  static void getDateDiffernce(){

    DateTime dateTimeCreatedAt = DateTime.parse('2019-9-11');
    DateTime dateTimeNow = DateTime.now();
    final differenceInDays = dateTimeNow.difference(dateTimeCreatedAt).inDays;
    print('$differenceInDays');
  }
}