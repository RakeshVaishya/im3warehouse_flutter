import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';

class Utility {
  static ProgressDialog progressDialog;
  static startProgressDialog(BuildContext context) {
    progressDialog = new ProgressDialog(context);
    progressDialog.style(message: 'Please wait...');
    progressDialog.show();
  }

  static closeProgressDialog() {
    if (progressDialog != null) {
   //   progressDialog.dismiss();
     }
  }

  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  static Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  static showToastMsg(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        backgroundColor: Colors.white,
        fontSize: 16.0,
        textColor: Colors.red,
        gravity: ToastGravity.CENTER);
  }


  static showSuccessToastMsg(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        backgroundColor: Colors.white,
        fontSize: 16.0,
        textColor: Colors.green,
        gravity: ToastGravity.CENTER);
  }

  static Widget getText(String title, double fontSize) {
    return Text(
      title,
      style: TextStyle(
          fontFamily: AppStrings.SEGOEUI_FONT,
          fontSize: fontSize,
          fontWeight: FontWeight.w400),
    );
  }

  static bool isList(String data) {
    if (data.startsWith("[")) {
      return true;
    } else {
      return false;
    }
  }

  static List<String> setDataInList(String data) {
    List<String> dataList = [];
    if (isList(data))
    {
      data = data.replaceAll("[", "");
      print("remove [ " + data);
      data = data.replaceAll("]", "");
      print("remove ] " + data);
      print(data);
      dataList = data.split(",");
      print(dataList.length);
    } else {
      dataList.add(data);
    }
    return dataList;
  }

  static Future<String> selectDate(
    BuildContext context,
  ) async {
    DateTime dateTime = DateTime.now();
    String strDateValue;
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: dateTime,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2100));
    if (picked != null && picked != dateTime)
    {
      DateFormat formatter = DateFormat('MM-dd-yyyy');
      strDateValue = formatter.format(picked);
    }
    print("date -->"+strDateValue);
    return strDateValue;
  }
}
