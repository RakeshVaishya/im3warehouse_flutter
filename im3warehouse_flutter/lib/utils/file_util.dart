import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:io' as Io;

/* set path lib in the pub with latest version */

class FileUtil {
  String path;
  File file;
  String rootPath;
  String getPath() {
    return path;
  }

  void setPath(String path) {
    this.path = path;
  }

  Future<String> getRootPath() async {
    rootPath = (await getExternalStorageDirectory()).path;
    print(rootPath);
    return rootPath;
  }

  bool isFileExist(String fileName) {
    bool isExist;
    file = new Io.File(fileName);
    file.exists().then((isExist) {
      print("file is exist -->$isExist");

      if (isExist) {
        isExist = true;
        return;
      } else {
        isExist = false;
      }
    });
    print("file is status return -->$isExist");
    return isExist;
  }

  void createFile(String fileName) async {
    file = new Io.File(fileName);
    file.exists().then((isExist) {
      print("file is status return -->$isExist");
      if (isExist) {
        print("file is already created -->");

        return;
      } else {
        print("file  create  -->");
        file.createSync(recursive: true);
      }
    });
  }

  String readFile(String fileName) {
    file = new Io.File(fileName);
    bool exist = isFileExist(fileName);
    String data;

    if (exist) {
      Stream<List<int>> inputStream = file.openRead();
      inputStream
          .transform(utf8.decoder) // Decode bytes to UTF-8.
          .transform(new LineSplitter()) // Convert stream to individual lines.
          .listen((String line) {
        // Process results.
        print('$line: ${line.length} bytes');
        data += line;
      }, onDone: () {
        print('File is now closed.');
      }, onError: (e) {
        print(e.toString());
      });
    } else {
      data = "file not exist";
    }
    return data;
  }

  void writeFile(String data, String fileName) {
    file = new Io.File(fileName);
    bool exist = isFileExist(fileName);
    if (exist) {
      var sink = file.openWrite();
      sink.write('FILE ACCESSED ${new DateTime.now()}\n');
      sink.close();
    } else {
      createFile(fileName);
      var sink = file.openWrite();
      sink.write('FILE ACCESSED ${new DateTime.now()}\n');
      sink.close();
    }
  }

  void deleteFile(String fileName) async {
    file = new File(fileName);
    file.exists().then((isExist) {
      if (isExist) {
        file.delete(recursive: true);
      } else {
        return;
      }
    });
  }

  Future<Directory> createDir(String path) async {
    print("PAth for dir-->");
    Directory directory;
    try {
      directory = new Io.Directory(path);
      directory.exists().then((status) {
        if (status) {
          print("dir already created-->" + path);
        } else {
          directory.create(recursive: true);
        }
      });
      print("PAth for dir-->" + directory.path);
    } catch (e) {
      print("PAth for dir-->" + directory.path);
    }
    return directory;
  }

  Future<Directory> createMultiPleDir(String path) async {
    print("PAth for dir-->");
    Directory directory;
    try {
      directory = await new Io.Directory(path).create(recursive: true);
      print("PAth for dir-->" + directory.path);
    } catch (e) {
      print(" exception -->PAth for dir-->" + directory.path);
    }
    return directory;
  }

  void deleteDir(String path) {
    print("PAth for dir-->");
    Directory directory;
    try {
      directory = new Io.Directory(path);
      directory.exists().then((status) {
        if (status) {
          directory.delete(recursive: true);
        } else {
          print("Dir not deleted");
        }
      });
    } catch (e) {
      print("exeception for  delete   dir-->" + directory.path);
    }
  }
}
