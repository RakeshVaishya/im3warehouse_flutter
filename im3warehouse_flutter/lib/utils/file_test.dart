import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:im3_warehouse/utils/file_util.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io' as Io;
class readwritefile extends StatefulWidget {
  @override
  _readwritefileState createState() => _readwritefileState();
}

class _readwritefileState extends State<readwritefile> {
  final String title_readwritefile = "ReadWriteFile";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: title_readwritefile,
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: ReadWriteData(),
    );
  }
}

class ReadWriteData extends StatefulWidget {
  @override
  _ReadWriteDataState createState() => _ReadWriteDataState();
}

class _ReadWriteDataState extends State<ReadWriteData> {
  var testdir;
  var dir;
  var directory_test;

  FileUtil fileUtil;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Read Write Files",
          style: TextStyle(fontSize: 20.0),
          textAlign: TextAlign.center,
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 110, top: 25),
              child: RaisedButton(
                child: Text(
                  "Create Folder",
                  style: TextStyle(
                    fontSize: 20.0,
                    fontFamily: "Times New Roman",
                  ),
                ),
                onPressed: () {
                  // permissionCheck();
                  createFolder();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<Io.File> createFolder() async {

    dir = (await getExternalStorageDirectory()).path;

    if (testdir == null) {


      Directory directory = await new Io.Directory('$dir/FlutterDemo1/flutter_tuts').create(recursive: true);


      File file = new Io.File(
          '${testdir.path}/${DateTime.now().toUtc().toIso8601String()}.jpg')
        ..writeAsBytes(null);

      showToast(testdir.path);
      print("Directory created--->" + testdir.path);
      return file;
    } else if (testdir != directory_test) {
      testdir = await new Io.Directory('$dir/FlutterDemo1/flutter_tuts')
          .create(recursive: true);
      showToast(testdir.path);
      File file = new Io.File(
          '${testdir.path}/${DateTime.now().toUtc().toIso8601String()}.txt')
        ..writeAsString("contents");
      return file;
    } else {
      print("Directory exists....");
      showToast("Directory exists...");
    }
  }

  // Future<bool> permissionCheck() async {
  //   bool checkResult = await SimplePermissions.checkPermission(
  //       Permission.WriteExternalStorage);
  //   if (!checkResult) {
  //     var status = await SimplePermissions.requestPermission(
  //         Permission.WriteExternalStorage);
  //     if (status == PermissionStatus.authorized) {
  //       print("Permission granted");
  //       createFolder();
  //     }
  //     return true;
  //   } else {
  //     print("permission already granted");
  //     createFolder();
  //     return true;
  //   }
  // }

  void showToast(String message) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      textColor: Colors.white,
      backgroundColor: Colors.purple,
      fontSize: 20.0,
    );
  }
}