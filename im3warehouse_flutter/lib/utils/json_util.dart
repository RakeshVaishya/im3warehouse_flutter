class JsonUtil {
  static final String KEY_Company_Code = "Company_Code";
  static final String MOBILE_URL = "Mobile_Url";
  static final String SITE_URL = "Site_Url";
  static final String VERIFICATION_CODE = "Verification_Code";

  static final String STR_COMPANY_CODE = "strCompanyCode";
  static final String COMPANY_NAME = "companyName";
  static final String USER_ID = "userId";
  static final String USER_NAME = "userName";
  static final String EMPLOYEE_NAME = "employeeName";
  static final String DATE_FORMAT = "dateFormat";
  static final String DEFAULT_FACILITY_CODE_VALUE = "defaultFacilityCodeValue";
  static final String DEFAULT_I_FACILITY_CODE = "defaultIFacilityCode";
  static final String SESSION_ID = "sessionID";
  static final String SESSION_PASSWORD = "sessionPassword";
  static final String FACILITY_DATA = "keyFacility";

  // static final String KEY_Company_Name= "Company_Name";
  // static final String KEY_Cust_Auto_Generate= "Cust_Auto_Generate";
  // static final String KEY_Cust_over_Write= "Cust_over_Write";
  // static final String KEY_Employee_ID= "Employee_ID";
  // static final String KEY_EQ_Auto_Generate ="EQ_Auto_Generate";
  // static final String KEY_EQ_over_Write ="EQ_over_Write";
  // static final String KEY_GlobalCustomer ="GlobalCustomer";
  // static final String KEY_Inv_Auto_Generate="Inv_Auto_Generate";
  // static final String KEY_Inv_over_Write="Inv_over_Write";
  // static final String KEY_InvoicePrefix="InvoicePrefix";
  // static final String KEY_PO_Auto_Generate="PO_Auto_Generate";
  // static final String KEY_PO_over_Write="PO_over_Write";
  // static final String KEY_PWD="PWD";
  // static final String KEY_Token="Token";
  // static final String KEY_UID="UID";
  // static final String KEY_User_First_Name="User_First_Name";
  // static final String KEY_User_ID="User_ID";
  // static final String KEY_User_Last_Name="User_Last_Name";
  // static final String KEY_Ven_Auto_Generate="Ven_Auto_Generate";
  // static final String KEY_Ven_over_Write="Ven_over_Write";
  // static final String KEY_version="version";
  // static final String KEY_WO_Auto_Generate="WO_Auto_Generate";
  // static final String KEY_WO_over_Write="WO_over_Write";

/*Table 4 data--*/

  static final String KEY_Allow_schedule_date_on_wo =
      "Allow_schedule_date_on_wo";
  static final String KEY_Allow_Stock_For_PO_Item = "Allow_Stock_For_PO_Item";

  static final String KEY_Apply_FET = "Apply_FET";
  static final String KEY_Bench_Fee = "Bench_Fee";
  static final String KEY_Column1 = "Column1";

  static final String KEY_Facility_Department_Flag = "Facility_Department_Flag";
  static final String KEY_FET_Percentage = "FET_Percentage";
  static final String KEY_GEN_Currency_Code = "GEN_Currency_Code";

  static final String KEY_GEN_Date_Format = "GEN_Date_Format";
  static final String KEY_Generate_Invoice_for = "Generate_Invoice_for";
  static final String KEY_Is_Craft_Code_Required = "Is_Craft_Code_Required";

  static final String KEY_IsReq_VendorPartID_For_Spcl_Part_In_WO =
      "IsReq_VendorPartID_For_Spcl_Part_In_WO";
  static final String KEY_IsShowCarrierLogo = "IsShowCarrierLogo";
  static final String KEY_PO_Tax_Required = "PO_Tax_Required";

/*

  App().getAppPreferences().setUserData(AppPreferences.PREF_TYPE_STRING,  "Company_Code", companyCode);

  companyName = json['Company_Name'];
  App().getAppPreferences().setUserData(AppPreferences.PREF_TYPE_STRING,  "Company_Name", companyName);
  custAutoGenerate = json['Cust_Auto_Generate'];
  custOverWrite = json['Cust_over_Write'];
  employeeID = json['Employee_ID'];
  eQAutoGenerate = json['EQ_Auto_Generate'];
  eQOverWrite = json['EQ_over_Write'];
  globalCustomer = json['GlobalCustomer'];
  invAutoGenerate = json['Inv_Auto_Generate'];
  invOverWrite = json['Inv_over_Write'];
  invoicePrefix = json['InvoicePrefix'];
  pOAutoGenerate = json['PO_Auto_Generate'];
  pOOverWrite = json['PO_over_Write'];
  pWD = json['PWD'];
  token = json['Token'];
  uID = json['UID'];
  userFirstName = json['User_First_Name'];
  userID = json['User_ID'];
  userLastName = json['User_Last_Name'];
  venAutoGenerate = json['Ven_Auto_Generate'];
  venOverWrite = json['Ven_over_Write'];
  version = json['version'];
  wOAutoGenerate = json['WO_Auto_Generate'];
  wOOverWrite = json['WO_over_Write'];
*/

}
