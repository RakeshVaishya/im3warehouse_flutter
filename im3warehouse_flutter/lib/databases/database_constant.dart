class DataBaseConstant {
  static final String DB_NAME = "im_warehouse.db";
  static final int VERSION = 1;
  static final String COLUNM_ID = "id";
  static final String COLUNM_COMPANY_CODE = "companyCode";

  // Table for Facility response
  static final TABLE_FACILITY = "facilityData";
  static final String COLUMN_FAC_BROKERAGE = "brokerage";
  static final String COLUMN_FAC_COMPANY_CODE = "companyCode";
  static final String COLUMN_FAC_CUSTOMDUTY = "customDuty";
  static final String COLUMN_FAC_DEFAULT_IFACILITY_CODE =
      "defaultIfacilityCode";
  static final String COLUMN_FAC_FACILIY_CODE = "facilityCode";
  static final String COLUMN_FAC_FACILITY_NAME = "facilityName";
  static final String COLUMN_FAC_FACILITY_CURRENCY = "facilityCurrency";
  static final String COLUMN_FAC_FREIGHTCHARGES = "freightCharges";
  static final String COLUMN_FAC_IFACILITY_CODE = "iFacilityCode";
  static final String COLUMN_FAC_INSURANCE = "insurance";
  static final String COLUMN_FAC_SELECTED_FACILITY_CODE =
      "selectedFacilityCode";
  static final String COLUMN_FAC_SHIPPING = "shipping";
  static final String COLUMN_FAC_TAX = "tax";

  static String CREATE_TABLE_FACILITY = "CREATE TABLE " +
      TABLE_FACILITY +
      "( " +
      COLUMN_FAC_BROKERAGE +
      " TEXT, " +
      COLUMN_FAC_COMPANY_CODE +
      " TEXT, " +
      COLUMN_FAC_CUSTOMDUTY +
      " TEXT, " +
      COLUMN_FAC_DEFAULT_IFACILITY_CODE +
      " TEXT, " +
      COLUMN_FAC_FACILIY_CODE +
      " TEXT, " +
      COLUMN_FAC_FACILITY_NAME +
      " TEXT, " +
      COLUMN_FAC_FACILITY_CURRENCY +
      " TEXT, " +
      COLUMN_FAC_FREIGHTCHARGES +
      " TEXT, " +
      COLUMN_FAC_IFACILITY_CODE +
      " TEXT, " +
      COLUMN_FAC_INSURANCE +
      " TEXT, " +
      COLUMN_FAC_SELECTED_FACILITY_CODE +
      " TEXT, " +
      COLUMN_FAC_SHIPPING +
      " TEXT, " +
      COLUMN_FAC_TAX +
      " TEXT );";

  /*Table 1 in json responce from the login */

  static final TABLE_CD_INV_ACTION = "Cd_Inv_Action";
  static final String COLUNM_INV_ACTION_CODE = "iNVActionCode";
  static final String COLUNM_INV_ACTION_NAME = "iNVActionName";
  static final String COLUNM_SHOW_ON_PART = "showOnPart";

  static String CREATE_TABLE_CD_INV_ACTION = "CREATE TABLE " +
      TABLE_CD_INV_ACTION +
      "( " +
      COLUNM_ID +
      " INTEGER NOT NULL PRIMARY KEY "
          "AUTOINCREMENT, "
          "" +
      COLUNM_COMPANY_CODE +
      "	TEXT, " +
      COLUNM_INV_ACTION_CODE +
      "	TEXT,"
          "  " +
      COLUNM_INV_ACTION_NAME +
      "	TEXT ,  " +
      COLUNM_SHOW_ON_PART +
      "  TEXT );";

  /*Table 2 in json responce from the login */
  static final TABLE_WO_CD_INV_TYPE = "Wo_CD_INV_Type";
  static final String COLUNM_I_INV_TYPE_CODE = "iInvTypeCode";
  static final String COLUNM_INV_TYPE_DESCRIPTION = "invTypeDescription";
  static final String COLUNM_IS_STOCK_REQUIRED = "isStockroomRequired";

  static String CREATE_TABLE_WO_CD_INV_TYPE = "CREATE TABLE " +
      TABLE_WO_CD_INV_TYPE +
      " "
          "( " +
      COLUNM_ID +
      " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
      COLUNM_COMPANY_CODE +
      "	TEXT, "
          " " +
      COLUNM_I_INV_TYPE_CODE +
      "	TEXT,"
          "   " +
      COLUNM_INV_TYPE_DESCRIPTION +
      "	TEXT ," +
      COLUNM_IS_STOCK_REQUIRED +
      " TEXT );";

  /*Table 3 in json responce from the login */
  static final TABLE_CD_INVOICE_TERMS = "Cd_Invoice_Terms";
  static final String COLUNM_ITERM_ID = "iTermID";
  static final String COLUNM_TERM_DESCRIPTION = "termDescription";

  static String CREATE_TABLE_CD_INVOICE_TERMS = "CREATE TABLE " +
      TABLE_CD_INVOICE_TERMS +
      "  "
          "( " +
      COLUNM_ID +
      " 	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
      COLUNM_COMPANY_CODE +
      "	TEXT,"
          "  " +
      COLUNM_ITERM_ID +
      "	TEXT,  " +
      COLUNM_TERM_DESCRIPTION +
      "	TEXT );";

  /*Table 5 in json responce from the login */
  static final TABLE_CD_COUNTRY = "Cd_Country";
  static final String COLUNM_COUNTRY_CODE = "countryCode";
  static final String COLUNM_COUNTRY_NAME = "countryName";

  static String CREATE_TABLE_CD_COUNTRY = "CREATE TABLE  " +
      TABLE_CD_COUNTRY +
      " ( " +
      COLUNM_ID +
      "	INTEGER NOT NULL "
          "PRIMARY KEY AUTOINCREMENT ," +
      COLUNM_COUNTRY_CODE +
      " 	TEXT, " +
      COLUNM_COUNTRY_NAME +
      " 	TEXT );";

  /*Table 6 in json responce from the login */
  static final TABLE_CD_STATE = "Cd_State";
  static final String COLUNM_STATE_CODE = "stateCode";
  static final String COLUNM_STATE_NAME = "stateName";

  static String CREATE_TABLE_CD_STATE = "CREATE TABLE " +
      TABLE_CD_STATE +
      " "
          "( " +
      COLUNM_ID +
      "	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,  " +
      COLUNM_COUNTRY_CODE +
      ""
          "	TEXT,  " +
      COLUNM_STATE_CODE +
      "	TEXT,  " +
      COLUNM_STATE_NAME +
      "	TEXT );";

  /*Table 7 in json responce */
  static final TABLE_CD_BILLABLE_TYPE = "Cd_Billable_Type";
  static final String COLUNM_BILLING_TYPE_CODE = "billingTypeCode";
  static final String COLUNM_BILLING_TYPE_DESC = "billingTypeDescription";
  static final String COLUNM_I_BILLING_TYPE_CODE = "iBillingTypeCode";

  static String CREATE_TABLE_CD_BILLABLE_TYPE = "CREATE TABLE " +
      TABLE_CD_BILLABLE_TYPE +
      ""
          " ( " +
      COLUNM_ID +
      "	INTEGER NOT NULL PRIMARY "
          "KEY AUTOINCREMENT, " +
      COLUNM_BILLING_TYPE_CODE +
      "	TEXT, " +
      COLUNM_BILLING_TYPE_DESC +
      ""
          "	TEXT," +
      COLUNM_I_BILLING_TYPE_CODE +
      "	TEXT );";

  /*Table 8 in json responce from the login */

  static final TABLE_CD_CUST_GROUP = "Cd_CustGroup";
  static final String COLUNM_CUST_GROUP_NAME = "custGroupName";
  static final String COLUNM_I_CUST_GROUP_ID = "iCustGroupID";

  static String CREATE_TABLE_CD_CUSTGROUP = "CREATE TABLE " +
      TABLE_CD_CUST_GROUP +
      " ("
          " " +
      COLUNM_ID +
      "	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
          " " +
      COLUNM_CUST_GROUP_NAME +
      "	TEXT,  " +
      COLUNM_I_CUST_GROUP_ID +
      "	TEXT  );";

/*Table 9 in json responce */

  static final TABLE_CD_CLASS_CODE = "Cd_ClassCode";
  static final String COLUNM_I_CLASS_CODE = "iClassCode";
  static final String COLUNM_I_CLASS_CODE_DESC = "classCodeDesc";

  static String CREATE_TABLE_CD_CLASSCODE = "CREATE TABLE " +
      TABLE_CD_CLASS_CODE +
      " "
          " (" +
      COLUNM_ID +
      "	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
          "" +
      COLUNM_I_CLASS_CODE +
      " TEXT, " +
      COLUNM_I_CLASS_CODE_DESC +
      " "
          "	TEXT );";

  static String SELECT_FROM = "Select * From ";
}

/*  Query for get all list data */
