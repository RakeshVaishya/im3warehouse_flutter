import 'dart:async';

import 'package:im3_warehouse/databases/entity/task.dart';
import 'package:im3_warehouse/databases/dao/task_dao.dart';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:path/path.dart';
part 'database.g.dart';
@Database(version: 1, entities: [Task])
abstract class FlutterDatabase extends FloorDatabase {
  TaskDao get taskDao;
}
