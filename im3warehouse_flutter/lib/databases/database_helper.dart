import 'dart:async';
import 'dart:io' as io;
import 'package:im3_warehouse/databases/database_constant.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
class DatabaseHelper implements DataBaseConstant {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();
  Future<Database> initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DataBaseConstant.DB_NAME);
    var theDb = await openDatabase(path,
        version: DataBaseConstant.VERSION, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    print("start table creation--");
    print("Query for facility table creation -->" +
        DataBaseConstant.CREATE_TABLE_FACILITY);
    await db.execute(DataBaseConstant.CREATE_TABLE_FACILITY);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_CD_BILLABLE_TYPE);
    // await db.execute(DataBaseConstant.CREATE_TABLE_CD_BILLABLE_TYPE);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_CD_CLASSCODE);
    // await db.execute(DataBaseConstant.CREATE_TABLE_CD_CLASSCODE);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_CD_COUNTRY);
    // await db.execute(DataBaseConstant.CREATE_TABLE_CD_COUNTRY);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_CD_CUSTGROUP);
    // await db.execute(DataBaseConstant.CREATE_TABLE_CD_CUSTGROUP);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_CD_INV_ACTION);
    // await db.execute(DataBaseConstant.CREATE_TABLE_CD_INV_ACTION);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_CD_INVOICE_TERMS);
    // await db.execute(DataBaseConstant.CREATE_TABLE_CD_INVOICE_TERMS);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_WO_CD_INV_TYPE);
    // await db.execute(DataBaseConstant.CREATE_TABLE_WO_CD_INV_TYPE);

    // print("Query for table creation -->" +
    //     DataBaseConstant.CREATE_TABLE_CD_STATE);
    // await db.execute(DataBaseConstant.CREATE_TABLE_CD_STATE);

    // print(" table creation- done-");
  }
  createFacilty(String table, Facility facility) async {
    final dbclient = await db;
    print("adding data in table-->" + table);
    final res = await dbclient.insert(
        DataBaseConstant.TABLE_FACILITY, facility.toMap());
    return res;
  }

  //  insertData(
  //     String table, List<Facility> facility1) async {
  //   final dbclient = await db;
  //   var res;
  //   print("inserted into method--->");
  //   for (int i = 0; i < facility1.length; i++) {
  //     Facility fac2 = facility1.elementAt(i);

  //     print("adding data in table-->" + table);
  //     res =
  //         await dbclient.insert(DataBaseConstant.TABLE_FACILITY, fac2.toMap());

  //     print("Data inserted successfully into table---->");
  //   }
  //   return res;
  // }
 Future<List<int>> insertFacilityData(String table, List<dynamic> objects) async {
    print("adding data in table-->" + table);
    List<dynamic> listRes = new List();
    var dbClient = await db;
    List<int> res;
    try {
      await dbClient.transaction((db) async {
        objects.forEach((obj) async {
          try {
            Facility fac6 = Facility.fromJson(obj);
            var iRes = await db.insert(table, fac6.toMap());
            print(iRes);
            listRes.add(iRes);
            print("Data inserted into the Facility table---->");
          } catch (ex) {
            print(ex);
          }
        });
      });
      res = listRes;
    } catch (er) {
      print(er);
    }
    return res;
  }

  // Future<List<Map<String, dynamic>>> getFacMapData() async {
  //   var dbClient = await db;
  //   var result = await dbClient.rawQuery(
  //       DataBaseConstant.SELECT_FROM + DataBaseConstant.TABLE_FACILITY);

  //   return result;
  // }

  // Future<List<Facility>> getFacilityD() async {
  //   var facMapData = await getFacMapData();
  //   int count = facMapData.length;

  //   List<Facility> facList = List<Facility>();
  //   for (int i = 0; i < count; i++) {
  //     facList.add(Facility.fromMapObject(facMapData[i]));
  //   }
  //   return facList;
  // }

  Future<List<Facility>> getFacilityData() async {

   print( "get data from ");
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery(
        DataBaseConstant.SELECT_FROM + DataBaseConstant.TABLE_FACILITY);
    print(" facility Data  --DATA SIZE---->  ");
    print(list.length);
    List<Facility> facilityListTable = new List();
    for (int i = 0; i < list.length; i++) {
      var fac = new Facility();
      fac.companyCode = list[i][DataBaseConstant.COLUMN_FAC_COMPANY_CODE];
      fac.facilityCode = list[i][DataBaseConstant.COLUMN_FAC_FACILIY_CODE];
      fac.iFacilityCode = list[i][DataBaseConstant.COLUMN_FAC_IFACILITY_CODE];
      fac.bROKERAGE = list[i][DataBaseConstant.COLUMN_FAC_BROKERAGE];
      fac.defaultIFacilityCode =
          list[i][DataBaseConstant.COLUMN_FAC_DEFAULT_IFACILITY_CODE];
      fac.sHIPPING = list[i][DataBaseConstant.COLUMN_FAC_SHIPPING];
      fac.tAX = list[i][DataBaseConstant.COLUMN_FAC_TAX];
      fac.cUSTOMDUTY = list[i][DataBaseConstant.COLUMN_FAC_CUSTOMDUTY];
      fac.facilityCurrency =
          list[i][DataBaseConstant.COLUMN_FAC_FACILITY_CURRENCY];
      fac.iNSURANCE = list[i][DataBaseConstant.COLUMN_FAC_INSURANCE];
      fac.fREIGHTCHARGES = list[i][DataBaseConstant.COLUMN_FAC_FREIGHTCHARGES];

      print("Ifacility code from table------>");
      print(fac.iFacilityCode);

      print("facility code from table------>");
      print(fac.facilityCode);

      print("company code from table------>");
      print(fac.companyCode);

      // print("facility brokerage from table------>");
      // print(fac.bROKERAGE);

      // print("facility customduty from table------>");
      // print(fac.cUSTOMDUTY);

      // print("facility defaultfacilitycode from table------>");
      // print(fac.defaultIFacilityCode);

      // print("facility currency from table------>");
      // print(fac.facilityCurrency);

      // print("facility freightcharges from table------>");
      // print(fac.fREIGHTCHARGES);

      // print("facility insurance from table------>");
      // print(fac.iNSURANCE);

      // print("facility shipping from table------>");
      // print(fac.sHIPPING);

      // print("facility tax from table------>");
      // print(fac.tAX);

      facilityListTable.add(fac);
    }
    print(facilityListTable.length);
    return facilityListTable;
  }




}
