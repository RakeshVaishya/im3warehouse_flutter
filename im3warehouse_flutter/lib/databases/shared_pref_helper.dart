import 'dart:convert';

import 'package:im3_warehouse/models/facility.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  SharedPreferencesHelper._internal() {
    print("SharedPreferencesHelper intialize--");
  }
  static SharedPreferencesHelper _sharedPreferencesHelper;

  static SharedPreferencesHelper getSharedPreferencesHelperInstance() {
    if (_sharedPreferencesHelper == null) {
      _sharedPreferencesHelper = SharedPreferencesHelper._internal();
    }
    return _sharedPreferencesHelper;
  }


  Future <bool> clearPref() async{

    SharedPreferences prefs =await SharedPreferences.getInstance();
    prefs.clear().then((value)
    {
      print(value
          );

    }).catchError((e){
      print(""+e.toString()

      );

    });

  }

  addStringToSF(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  addIntToSF(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, value);
  }

  addDoubleToSF(String key, double value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setDouble(key, value);
  }

  addBoolToSF(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
  }

  addListStringToSF(String key, List<String> value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(key, value);
  }

  Future<List<String>> getListString(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> listString = prefs.getStringList(key);
    return listString;
  }

  Future<String> getStringValuesSF(String key)
  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key);
    return stringValue;
  }

  Future<bool> getBoolValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return bool
    bool boolValue = prefs.getBool(key);
    return boolValue;
  }

  Future<int> getIntValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    int intValue = prefs.getInt(key) ?? 0;
    return intValue;
  }

  Future<double> getDoubleValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return double
    double doubleValue = prefs.getDouble(key) ?? 0;
    return doubleValue;
  }

  Future<bool> isDataExistToSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDataExist = prefs.containsKey(key);
    return isDataExist;
  }
}
