import 'package:floor/floor.dart';

@entity
class RepairRequest {
  @PrimaryKey(autoGenerate: true)
  final int id;
  final String message;
  RepairRequest(this.id, this.message);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is RepairRequest &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              message == other.message;

  @override
  int get hashCode => id.hashCode ^ message.hashCode;

  @override
  String toString() {
    return 'Task{id: $id, message: $message}';
  }
}
