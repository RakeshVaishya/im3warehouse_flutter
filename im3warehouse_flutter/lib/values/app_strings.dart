import 'dart:convert';
import 'dart:io' show Platform;
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/utils/json_util.dart';

class AppStrings {
/*

 company code =okaybeta
username = test_okay
password=  beta@121

*/

  /* Hardcode data for testing local */


/*

  static String API_GET_COMPANY_URL ="http://m.im3.com/web/service/MobileServiceForWO.asmx/GetCompanyUrl";
  static String BASE_URL = "https://local.im3cloud.com";
  static const String VERIFICATION_CODE = "local";
  static const String LOGIN = "ankitasawant";
  static const String PASSWORD = "password@123";

*/


  // "http://im3mobile.hamarisuraksha.com/";
  /* input data for  beta testing beta */



   static String API_GET_COMPANY_URL ="http://beta.reefermobile.im3cloud.com/web/service/MobileServiceForWO.asmx/GetCompanyUrl";
   static String BASE_URL = "http://beta.reefermobile.im3cloud.com/";



   static const String VERIFICATION_CODE = "coldfrontbeta";
   static const String LOGIN = "test_coldfront";
   static const String PASSWORD = "beta@121";
   static Facility facility;


/*


   static const String VERIFICATION_CODE = "";
   static const String LOGIN = "";
   static const String PASSWORD = "";
*/



  static String getDevicePlateForm(){
    if (Platform.isAndroid)
      return  "Android Native (Flutter)";
     else if (Platform.isIOS)
      return  "Ios Native (Flutter)";
  }


/*

  static const String VERIFICATION_CODE = "chargerbeta";
  static const String LOGIN = "test_charger";
  static const String PASSWORD = "im3@beta@122";

*/


  /* editingControllerUser.text = "sagarshinde63";
  // editingControllerPass.text = "password@123";
*/


//--------------------------------------------------- APP level  -----------------------------------------------------------------------------------------

  static  int NETWORK_TIMEOUT_DURATION = 30;
  static  int DIALOG_TIMEOUT_DURATION = 30;

  static const String SEGOEUI_FONT = "SegoeUI";
  static String SESSION_ID = "";
  static const String APP_NAME = "AH";
  static const String SAVE = "Save";
  static double PADDING_VALUE_10 = 10.0;
  static double PADDING_VALUE_5 = 5.0;
  static double PADDING_VALUE_3 = 3.0;
  static double LABEL_FONT_SIZE = 15.0;
  static double BUTTON_FONT_SIZE = 20.0;
  static const double FONT_SIZE_18 = 17.0;
  static const double FONT_SIZE_16 = 16.0;
  static const double FONT_SIZE_15 = 15.0;
  static const double FONT_SIZE_13 = 13.0;
  static const double FONT_SIZE_12 = 12.0;

  // <------------------------Landing Page----------------------->

  static const String COMPANY_NAME = "Powered By PeoplePlus Software";
  static const String IMAGE_ROUND_INFO = "images/round_info.png";
  static const String IMAGE_BARCODE = "images/barcode.png";
  static const String IMAGE_IM3_LOGO = "images/im3_big.png";
  static const String LABEL_VERIFICATION_CODE =
      "Enter mobile app verification code";
  static const String LABEL_ALERT_DIALOG =
      "You can get your verification code on the home screen of iM3 website";
  static const String LABEL_EXIT_DIALOG = "Do you really want to exit an app?";
  static const String LABEL_NO = "No";
  static const String LABEL_YES = "Yes";
  static const String LABEL_OK = "OK";

  /// App Strings Class -> Resource class for storing app level strings constants
// App Strings Class -> Resource class for storing app level strings constants

  //--------------------------------------------------- Login -----------------------------------------------------------------------------------------

  static const String LOGIN_USER_NAME_LABEL = "User Name";
  static const String LOGIN_USER_PASSWORD_LABEL = "Password";
  static const String ASTERIK_SYMBOL = "*";

  static const String LOGIN_USER_NAME_HINT = "Enter user name";
  static const String LOGIN_USER_PASSWORD_HINT = "Enter password";

  static const String LOGIN_USER_EMAIL_ID = "User ID / Email";
  static const String LOGIN_FORGOT_PASSWORD = "Forgot Password ?";

  static const String LOGIN_USER_NAME_ERROR_MSG =
      "User name must be at least 3 characters";
  static const String LOGIN_USER_PASSWORD_ERROR_MSG =
      "Password must be at least 8 characters";
  static const String LOGIN_USER_NAME_INVALID_MSG = "Invalid user name";
  static const String LOGIN_USER_PASSWORD_INVALID_MSG = "Invalid user password";

  static const String LOGIN_RESET_BUTTON_LABEL = "Reset";
  static const String LOGIN_LOGIN_BUTTON_LABEL = "Login";

  static const String LOGIN_SUCCESSFUL_LOGIN_MSG = "Successful login";
  static const String LOGIN_UNSUCCESSFUL_LOGIN_MSG = "Invalid credentials";

  static const String IM3_STATUS_MSG = "Powered By Peopleplus Software";

  static const String DASHBOARD = "Dashboard";
  static const String INVENTORY = "Inventory";
  static const String REPAIR = "Repair";
  static const String SHIPPING = "Shipping";
  static const String ORDERS = "Orders";

  static const String IMAGE_DASHBOARD_ACTIVE = "images/Dashboard-active.png";
  static const String IMAGE_DASHBOARD = "images/Dashboard.png";

  static const String IMAGE_INVENTORY_ACTIVE = "images/Inventory-active.png";
  static const String IMAGE_INVENTORY = "images/Inventory.png";

  static const String IMAGE_REPAIR_ACTIVE = "images/Repair-active.png";
  static const String IMAGE_REPAIR = "images/Repair.png";

  static const String IMAGE_SHIPPING_ACTIVE = "images/Shipping-active.png";
  static const String IMAGE_SHIPPING = "images/Shipping.png";

  static const String IMAGE_ORDERS_ACTIVE = "images/Orders-active.png";
  static const String IMAGE_ORDERS = "images/Orders.png";

//------------------------------------------ Add Edit Inventory ----------------------------------------------//
  static const String ADD_EDIT_INVENTORY = "Add / Edit Inventory";

  static const String LABEL_INVENTORY_TYPE = "Inventory Type";
  static const String LABEL_MANUFACTURE = "Manufacture";
  static const String LABEL_BILLING_TYPE = "Billing Type";
  static const String LABEL_TAX_TYPE = "Tax Type";
  static const String LABEL_IS_SERIALIZED = "Is serialized";
  static const String LABEL_UPLOAD_PICTURE = "Upload Picture";
  static const String IMAGE_UPLOAD = "images/Upload.png";
  static const String IMAGE_CAMERA = "images/camera.png";
  static const String IMAGE_GALLARY = "images/gallary.png";
  static const String ERROR_PART_NO = "please Enter Part No";
  static const String ERROR_DESCRIPTION = "please Enter Description";
  static const String ERROR_UOM = "please Enter UOM";
  static const String ERROR_INVENTORY_TYPE = "please Enter Inventory Type";
  static const String ERROR_MANUFACTURE = "please Enter Manufacture";
  static const String ERROR_BILLING_TYPE = "please Enter Billing Type";
  static const String ERROR_TAX_TYPE = "please Enter Tax Type";
  static const String SAVE_AND_DO_ADJUSTMENTS = "Save & Do Adjustments";
  static const String CAMERA = "Camera";
  static const String GALLARY = "Gallary";
  /*---------------------------------  Add and Edit  Repair Request ----------------------------*/

  static const String LABEL_REPAIR_REQUEST = "Repair Request *";
  static String DROP_DOWN_DEFAULT_SELECTED = "Open";
  static String ERROR_VALUE_CAN_NOT_EMPTY = "Value Can\'t Be Empty";
  static const String LABEL_DESCRIPTION = "Description";
  static const String LABEL_STATUS = "Status";
  static const String STR_OPEN = "Open";
  static const String STR_CLOSE = "Close";
  static const String LABEL_WARRANTY_RECOVERY = "Warranty Recovery";
  static const String LABEL_RUSH_REPAIR = "Rush Repair";
  static const String LABEL_CUSTOMER = "Customer";
  static const String LABEL_CUSTOMER_BILL_TO = LABEL_CUSTOMER + " Bill to";
  static const String LABEL_CUSTOMER_SHIP_TO = LABEL_CUSTOMER + " Ship to";
  static const String LABEL_CUSTOMER_REFERENCE_NO =
      LABEL_CUSTOMER + " Reference No.";
  static const String LABEL_CUSTOMER_REFERENCE_NO_2 =
      LABEL_CUSTOMER + " Reference No.2";
  static const String LABEL_CUSTOMER_REFERENCE_NO_3 =
      LABEL_CUSTOMER + " Reference No.3";
  static const String LABEL_CUSTOMER_REFERENCE_NO_4 =
      LABEL_CUSTOMER + " Reference No.4";
  static const String LABEL_CUSTOMER_REFERENCE_NO_5 =
      LABEL_CUSTOMER + " Reference No.5";
  static const String LABEL_CUSTOMER_REFERENCE_NO_6 =
      LABEL_CUSTOMER + " Reference No.6";
  static const String LABEL_CUSTOMER_STATED_ISSUE =
      LABEL_CUSTOMER + "Customer Stated Issue";

  /* All Screen Names */
  static const String LABEL_HOME = "Home";

  static String USER_ID = "";
  static String COMPANY_CODE = "";
  static String I_FACILITY_CODE = "";



// reference
//----------------------------------- Part Details----------------------------------------------------
  static const String LABEL_ON_HAND_QUANTITY = "On Hand Quantity";
  static const String ERROR_ON_HAND_QUANTITY = "please Enter On Hand Quantity";

// <---------------------------------Physical Count--------------------------------------------->

  static const String IMAGE_QR_CODE = "images/QRCode.png";
  static const String strOne = "1";
  static const String strRoomNameFacilityName = "Room Name - Facility Name";
  static const String LABEL_PART_NO = "Part No";
  static const String LABEL_PART_DESC = "Part desc";
  static const String LABEL_BIN = "Bin #";
  static const String LABEL_DATA = "Data";
  static const String LABEL_SERIAL_NO = "Serial No. #";
  static const String LABEL_ON_HAND_QNTY = "On Hand Qty.";

  static const String LABEL_CASE_SIZE = "Case size";
  static const String LABEL_COST_PER_UNIT = "Cost Per Unit";
  static const String LABEL_UOM = "UOM";
  static const String LABEL_FACILITY_HASHTAG = "Facility #";
  static const String LABEL_PRIMARY_WAREHOUSE = "Primary WareHouse";
  static const String LABEL_UPDATE = "Update";
  static String LABEL_PART_NO_ERROR = "No Item Selected !";
  static const String LABEL_BIN_ERROR = "Putaway Location Code can not blank !";
  static const String LABEL_PART_DESC_ERROR = "Part description cannot be blank";
  static const String LABEL_PART_PRICE_ERROR = "Please Enter Price !";

  static const String LABEL_ACTION_TYPE = ' Action #';
  static const String ERROR_ADD_PART =
      'Part does not exist! Would you like to add it to iM3 ?';

  // <---------------------------Issue By PickList--------------------------------------->

  static const String LABEL_PICK_TICKET_LINE_NO = "Pick Ticket Line No";

  // <------------------------------Issue By PO---------------------------------------->
  static const String LABEL_PO_NUMBER = "PO#";

  /*<----------------------Put away Item----------------------------------------->          */

  static const String LABEL_ID_OR_PALLET_NO = "Label ID/Pallet No";
  static const String LABEL_PUT_AWAY_LOCATION = "Putaway Location";
  static const String LABEL_CONFIRM_PUT_AWAY_LOCATION =
      "Confirm Putaway Location";
  static const String BTN_ADD_LOCATION = "Add Location";
  static const String LABEL_PUT_AWAY_QTY_LEFT = "Putaway Quantity Left";
  static const String LABEL_PUT_AWAY_QTY = "Putaway Quantity";
  static const String ERROR_PUT_AWAY =
      "Putaway Label ID/Pallet No cannot be Blank";
  static const String LABEL_LOCATION_CODE = "Location Code";
  static const String LABEL_LOCATION_DESC = "Location Description";

  // <--------------------------DashBoard---------------------------------------------->

  static const String IMAGE_FACILITY = "images/hotelnew.png";
  static const String IMAGE_TRANSFER = "images/buildings.png";

  // Facility
  static const String LABEL_FACILITY = "Facility";

  //  Physical Count
  static const String LABEL_PHYSICAL_COUNT = "Physical Count";
  static const String LABEL_ADD_PART = "Add Part";

  // Transfer
  static const String LABEL_TRANSFER = 'Transfer';

  // Issue By Picklist
  static const String LABEL_ISSUE_BY_PICKLIST = 'Issue By Picklist';

  // Issue By Work Order
  static const String LABEL_ISSUE_BY_WORK_ORDER = 'Issue By Work Order';

  // Issue By WO / Task
  static const String LABEL_ISSUE_BY_WO_OR_TASK = 'Issue By WO / Task';

  // Issue for Equipement
  static const String LABEL_ISSUE_FOR_EQUIPEMENT = 'Issue for Equipment';

  // Putback
//  static const String LABEL_PICKLIST = 'Putback From WorkOrder';

  // Pick My Order
  static const String LABEL_PICK_MY_ORDER = 'Pick My Order';

  // Receive by PO
  static const String LABEL_RECEIVE_BY_PO = 'Receive by PO';

  static const String LABEL_LINE_NUMBER = "Line #";
  static const String LABEL_PART_NUMBER = "Part #";
  static const String LABEL_ORDER_Q = "Order Q";
  static const String LABEL_RELEASE_NO = "Release No";
  static const String LABEL_STOCK_ROOM = "Stock Room";

  static const String LABEL_RECEIVE_Q = "Release No";
  static const String LABEL_ST = "Receive Q";
  //static const String LABEL_SERIAL_NO= "Serial No";
  static const String LABEL_VENDOR_LOT_NO = "Vendor Lot No";
  static const String LABEL_EXPIRATION_DATE = "Expiration Date";
  static const String LABEL_EQUIPMENT = "Equipment";

  static const String LABEL_EQUIPMENT_DESC = "EQ Desc";
  static const String LABEL_NOTES = "Notes";

  // Receive by Part
  static const String LABEL_RECEIVE_BY_PART = 'Receive by Part';
  static const String LABEL_RECEIVE_BY_PART_ACTIVITY = 'Receive by Part Activity';

  // Reverse Receive
  static const String LABEL_REVERSE_RECEIVE = 'Reverse Receive';

  // Putaway Items
  static const String LABEL_PUTAWAY_ITEMS = "Putaway Items";

  /*Issue for Equipement*/
  // static const String LABEL_ISSUE_FOR_EQUIPEMENT = "Issue for Equipment";

  /* put back from work order */
  static const String LABEL_PUTBACK = "Putback From WorkOrder";

  /*Receive by PO*/
  // static const String LABEL_RECEIVE_BY_PO = "Receive by PO";

  // -----------------Receive By PO Activity----------------------------------------->
  static const String LABEL_RECEIVE_BY_PO_ACTIVITY = "Receive By PO ACTIVITY";

  // -----------------Receive By PO Activity----------------------------------------->
  static const String LABEL_SERIAL_NO_ERROR = "Please Enter Serial no.";

  //<------------------Reverse Receive-------------------------------------------->

  static const String LABEL_SEARCH = "Search";
  static const String LABEL_CANCEL = "Cancel";
  static const String LABEL_REVERSE_RECEIVE_ACTIVITY = "Reverse Receive ACTIVITY";

  static const String LABEL_ISSUE_TO_EMPLOYEE = "Issue To Employee";
  static const String LABEL_TRANSFER_PALLET = "Transfer Pallet";

  static  String newlyAddedPartNo="";
  static const String LABEL_PUT_BACK_LINE_ITEM_SELECT = "Please select Line Item Which you want to putback ? ";
  static int NUMBER_OF_RECORD_TO_FETCH  = 12 ;


  static const String LABEL_VIEW_INVENTORY = "View Inventory";

  
}
