import 'package:flutter/material.dart';

/// App Colors Class - Resource class for storing app level color constants
class AppColors {

  static const Color PRIMARY_COLOR = Color(0xFF35B4C5);
  static const Color PRIMARY_COLOR_LIGHT = Color(0xFFA5CFF1);
  static const Color PRIMARY_COLOR_DARK = Color(0xFF0D3656);
  static const Color ACCENT_COLOR = Color(0xFFF2DA04);

  static const Color CYAN_BLUE_COLOR  = Color(0xff0D68C4);


  static const String BUTTON_COLOR="#0D68C4";
   static Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }


}
