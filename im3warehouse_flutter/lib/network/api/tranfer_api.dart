import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/network/network_config.dart';

class TransferApi extends NetworkConfig {
  static const String API_GET_STOCK_FOR_TRANSFER_PART =   "getStockforTransferPart";
  static const String API_GET_ROOM_TO_TRANSFER_PART = "getRoomToTransferPart";
  static const String API_TRANSFER_QUANTITY = "Transferquantity";

  static String getStockForTransferPart(
      String userId, Facility facility, PartDetails partDetails ,String location) {
/*
  http://m.beta.coldfront.im3cloud.com/Web/Service/Mobile.asmx/getStockforTransferPart?
  CompanyCode=108&p int IPartNo=885&pintIFacilityCode=2&pstrUserid=test_coldfront&location=X43

*/

    String param = NetworkConfig.API_PARAM_COMPANY_CODE +
        NetworkConfig.EQUAL_SYMBOL +
        partDetails.companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrUserid" +
        NetworkConfig.EQUAL_SYMBOL +
        userId +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pintIFacilityCode" +
        NetworkConfig.EQUAL_SYMBOL +
        facility.iFacilityCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pintIPartNo" +
        NetworkConfig.EQUAL_SYMBOL +
        partDetails.iPartNo +
        NetworkConfig.AMPERSAND_SYMBOL +
        "location" +
        NetworkConfig.EQUAL_SYMBOL +
        location;

    return param;
  }

  static String getRoomToTransferPart(
      String toBin, Facility facility, PartDetails partDetails) {
/*

   http://m.beta.coldfront.im3cloud.com/Web/Service/Mobile.asmx/getRoomToTransferPart?
   CompanyCode=108
   &pstrFacilityCode=CGY
   &pstrRoomCode=
   &pstrRoomAreaCode=cccc
   &pstrRoomAreaName=
   &pintRoomAreaStatus=1
*/

    String param = NetworkConfig.API_PARAM_COMPANY_CODE +
        NetworkConfig.EQUAL_SYMBOL +
        partDetails.companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrFacilityCode" +
        NetworkConfig.EQUAL_SYMBOL +
        facility.facilityCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrRoomCode" +
        NetworkConfig.EQUAL_SYMBOL +
        "" +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrRoomAreaCode" +
        NetworkConfig.EQUAL_SYMBOL +
        toBin.trim() +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrRoomAreaName" +
        NetworkConfig.EQUAL_SYMBOL +
        "" +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pintRoomAreaStatus" +
        NetworkConfig.EQUAL_SYMBOL +
        "1";
    return param;
  }

/*
  Transfer save --Public Function Transferquantity(pstrPart As String, pstrStockFrom As String, pstrStockTo As String, ByVal partQuantity As Integer) As String
*/

  static String setTransferQuantityToLocation(
      String part,
      String stockFrom,
      String stockTransferToLocation,
      String partQuantity,
      String CompanyCode,
      String UserId,
      String EmployeeId) {
    String param = "pstrPart" +
        NetworkConfig.EQUAL_SYMBOL +
        part +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrStockFrom" +
        NetworkConfig.EQUAL_SYMBOL +
        stockFrom +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrStockTo" +
        NetworkConfig.EQUAL_SYMBOL +
        stockTransferToLocation +
        NetworkConfig.AMPERSAND_SYMBOL +
        "partQuantity" +
        NetworkConfig.EQUAL_SYMBOL +
        partQuantity.toString() +
        NetworkConfig.AMPERSAND_SYMBOL +
        "CompanyCode" +
        NetworkConfig.EQUAL_SYMBOL +
        CompanyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "UserId" +
        NetworkConfig.EQUAL_SYMBOL +
        UserId +
        NetworkConfig.AMPERSAND_SYMBOL +
        "EmployeeId" +
        NetworkConfig.EQUAL_SYMBOL +
        EmployeeId;
    return param;
  }

/*

  Transferquantity(ByVal pstrPart As String,
      ByVal pstrStockFrom As String,
  ByVal pstrStockTo As String,
      ByVal partQuantity As Integer,
  ByVal CompanyCode As Integer,
      ByVal UserId As String,
  ByVal EmployeeId As String)
*/

}
