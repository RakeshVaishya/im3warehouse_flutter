
import 'dart:convert';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_part_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_work_order_modal.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/putback/putback_part.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/values/app_strings.dart';


class PubBackApi extends NetworkConfig{

  /*
  *     beta cold front testing data
  *     wrok order number --> W012060
  *     Part no   X4 7300 -CHROME
  *     Location      --->   VAN-SALT-LAKE CITY
  *     Serail Number - TAM 1597966
  *
  * */

  static const String API_GET_WO_MASTER_FOR_HELP =   "GetWOMasterForHelp";
  static const String API_GET_PART_DATA_FOR_ISSUE_MOBILE = "getPartDataForIssueMobile";
  static const String API_CHECK_SERIAL_NO = "checkSerialNo";
  static const String API_GET_PART_DATA_FOR_PUT_BACK_MOBILE = "getPartDataForPutbackWoMobile";
  static const String API_SAVE_PUT_BACK_FROM_WORK_ORDER = "savePutbackFromWO";



  static String getPartDataForIssueMobile(
      String companyCode, String pstrPartNo, String iFacilityCode) {

//CompanyCode As Integer,
//    ByVal pstrPartNo As String,
//    ByVal IFacilityCode As Integer


    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "pstrPartNo" + NetworkConfig.EQUAL_SYMBOL + pstrPartNo +NetworkConfig.AMPERSAND_SYMBOL+
        "IFacilityCode" + NetworkConfig.EQUAL_SYMBOL + iFacilityCode;

    return param;
  }

  static String   getWOMasterForHelp (
      String companyCode, String woNo ,String mode) {
    /*
    ByVal CompanyCode As Integer,
        ByVal WoNo As String
    */

    print("mode---"+mode);
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL +      companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +  "WoNo" +  NetworkConfig.EQUAL_SYMBOL +woNo+NetworkConfig.AMPERSAND_SYMBOL+
        "Mode" +NetworkConfig.EQUAL_SYMBOL+mode;
    return param;
  }


  static String   getPartDataForPutBackWoMobile (
  String companyCode,IssueByWOPartModal partDetails,   IssueByWorkOrderModal workOrder) {
    /*
    ByVal CompanyCode As Integer,
        ByVal WoNo As String
    */

    ;
    /*CompanyCode: 107
    IWONo: 52837
    IPartNo: 79607
    IWOTaskNo: 0
    IWOTaskSOPSrNo: 0
    InvTransactionQuantity: 0
    InvTransactionflag: false
    */
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL +  companyCode + NetworkConfig.AMPERSAND_SYMBOL +
        "IWONo" +  NetworkConfig.EQUAL_SYMBOL +workOrder.iWONo+NetworkConfig.AMPERSAND_SYMBOL+
        "IPartNo" +  NetworkConfig.EQUAL_SYMBOL +partDetails.iPartNo+NetworkConfig.AMPERSAND_SYMBOL+
        "IWOTaskNo" +  NetworkConfig.EQUAL_SYMBOL +"0" +NetworkConfig.AMPERSAND_SYMBOL+
        "IWOTaskSOPSrNo" +  NetworkConfig.EQUAL_SYMBOL +"0" +NetworkConfig.AMPERSAND_SYMBOL+
        "InvTransactionQuantity" +  NetworkConfig.EQUAL_SYMBOL +"0" +NetworkConfig.AMPERSAND_SYMBOL+
        "InvTransactionflag" +  NetworkConfig.EQUAL_SYMBOL +"false";
    return param;
  }
  static String getAllTask(
      String companyCode, String iWoNo, String taskCode) {

    /*
    Public Function GetAllTask(ByVal CompanyCode As Integer,
    ByVal IWoNo As Integer,
    ByVal TaskCode As String) As String
    Dim ds As DataSet = Netxert.APP.WorkOrder.WorkOrderLogic.GetWOTaskForHelp(CompanyCode, IWoNo, TaskCode)
*/

    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "IWoNo" + NetworkConfig.EQUAL_SYMBOL + iWoNo +NetworkConfig.AMPERSAND_SYMBOL+
        "TaskCode" + NetworkConfig.EQUAL_SYMBOL + taskCode;
    return param;
  }




  static String getStockforMobilePart(
      String companyCode, String pintIPartNo, String pintIFacilityCode, String pstrUserId,String location) {

    /*art(ByVal CompanyCode As Integer,
    ByVal pintIPartNo As String,
    ByVal pintIFacilityCode As Integer,
    ByVal pstrUserid As String,
    ByVal location As String) As String
    */

    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode + NetworkConfig.AMPERSAND_SYMBOL
        +  "pintIPartNo"  +    NetworkConfig.EQUAL_SYMBOL + pintIPartNo+NetworkConfig.AMPERSAND_SYMBOL+
        "pintIFacilityCode"  +    NetworkConfig.EQUAL_SYMBOL + pintIFacilityCode+NetworkConfig.AMPERSAND_SYMBOL+
        "pstrUserId"  +    NetworkConfig.EQUAL_SYMBOL + pstrUserId+NetworkConfig.AMPERSAND_SYMBOL+
        "location"  +    NetworkConfig.EQUAL_SYMBOL + location;

    return param;
  }

  static String checkSerialNo(
      String companyCode, String iPartNo, String serialNo, String actionCode,String actionSrNo,
      String stockSrNo, String quantity,
      String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId
      ) {

/*
    companyCode: "108"
    iPartNo: "17809"
    serialNo: "test"
    actionCode: 1
    actionSrNo: 0
    stockSrNo: "53906"
    quantity: 1
    iWoNo: 0
    iSalesOrderNo: 0
    lineItemSrNo: 0
    controlId: "txtSerialNo"
*/

    Map data = new Map <String, dynamic>();
    data["companyCode"] = companyCode;
    data["iPartNo"] = iPartNo;
    data["serialNo"] = serialNo;
    data["actionCode"] = actionCode;
    data["actionSrNo"] = actionSrNo;
    data["stockSrNo"] = stockSrNo;
    data["quantity"] = quantity;
    data["iWoNo"] = iWoNo;
    data["iSalesOrderNo"] = iSalesOrderNo;
    data["lineItemSrNo"] = lineItemSrNo;
    data["controlId"] = controlId;
    String param  =jsonEncode(data);


    /* String param = "companyCode" +  NetworkConfig.EQUAL_SYMBOL + companyCode + NetworkConfig.AMPERSAND_SYMBOL
        +  "iPartNo"  +    NetworkConfig.EQUAL_SYMBOL + iPartNo+NetworkConfig.AMPERSAND_SYMBOL+
        "serialNo"  +    NetworkConfig.EQUAL_SYMBOL + serialNo+NetworkConfig.AMPERSAND_SYMBOL+
        "actionCode"  +    NetworkConfig.EQUAL_SYMBOL + actionCode+NetworkConfig.AMPERSAND_SYMBOL+
        "actionSrNo"  +    NetworkConfig.EQUAL_SYMBOL + actionSrNo+NetworkConfig.AMPERSAND_SYMBOL+
        "quantity"  +    NetworkConfig.EQUAL_SYMBOL + quantity+NetworkConfig.AMPERSAND_SYMBOL+
        "iWoNo"  +    NetworkConfig.EQUAL_SYMBOL + iWoNo+NetworkConfig.AMPERSAND_SYMBOL+
        "iSalesOrderNo"  +    NetworkConfig.EQUAL_SYMBOL + iSalesOrderNo+NetworkConfig.AMPERSAND_SYMBOL+
        "lineItemSrNo"  +    NetworkConfig.EQUAL_SYMBOL + lineItemSrNo+NetworkConfig.AMPERSAND_SYMBOL+
        "controlId"  +    NetworkConfig.EQUAL_SYMBOL + controlId;
    return param;
 */
    return param;
  }




/*
  IssueByWorkTaskModal workOder;
  IssueByGetAllTaskModal workOrderTask;
  IssueByWorkTaskPartModal partDetail;
  IssuByWorkTaskLocationModal locationDetail;
*/


  static String savePutBackFromWo(
      String companyCode,   IssueByWOPartModal partDetails,
  IssueByWorkOrderModal workOrder,
  PutBackPart _putBackPart ,
      RoomFacForLocation roomFacForLocation, String putBackQty,String userId,String location
  )
  {
/*


    Public Function savePutbackFromWO(ByVal CompanyCode As Integer,
        ByVal IWONo As Integer,
        ByVal SerialNo As Integer,
        ByVal IssueQuantity As Integer,
        ByVal WorkorderNo As Integer,
        ByVal PutbackLoc As Integer,
        ByVal PutbackQuantity As Integer,
        ByVal PartNo As String,
        ByVal strPartDetail As String,
        ByVal Room As String,
        ByVal InvTransactionflag As Boolean) As String
*/


  //  hdnPart').val(tbl.IPart_No + '~' + tbl.Part_Description + '~' + tbl.UOM_Code + '~' + tbl.Part_No + '~' + tbl.IUOM_Code + '~' + tbl.Billing_Type_Code);

  //  val(tbl.Vendor_Lot_ID + '~' + tbl.Action_Sr_No + '~' + tbl.Default_Case_Size + '~' +
    //  tbl.IWO_Task_SOP_Sr_No + '~' + tbl.IWO_Task_No + '~' + tbl.Part_Sr_No + '~'
    //  + tbl.Item_Ledger_Cost + '~' + tbl.ExpDate + '~' + tbl.Rate + '~' + tbl.IBilling_Type_Code);



    print("iPartNo -->"+partDetails.iPartNo);
    print("partDescription-->"+partDetails.partDescription);
    print("uOMCode -->"+partDetails.uOMCode);
    print("partNo -->"+partDetails.partNo);
    print("iUOMCode -->"+partDetails.iUOMCode);
    print("billingTypeCode -->"+partDetails.billingTypeCode);


    String partNo =partDetails.iPartNo +"~"+partDetails.partDescription+"~" +partDetails.uOMCode+"~"
          +partDetails.partNo+"~"+ partDetails.iUOMCode+"~" +partDetails.billingTypeCode;

      print(" this is complete String with tild for the paramenter  - partNo ->");
      print("partNo--->"+partNo);


      print("vendor lot before id -->"+_putBackPart.vendorLotID);

      if(_putBackPart.vendorLotID == null || _putBackPart.vendorLotID.length==0)
      {
        _putBackPart.vendorLotID ="";
      }
      if(_putBackPart.serialNumber =="0")
      {
        _putBackPart.serialNo ="";
      }
      print("vendor lot after id -->"+_putBackPart.vendorLotID);
      print("actionSrNo -->"+_putBackPart.actionSrNo);
      print("defaultCaseSize-->"+_putBackPart.defaultCaseSize);
      print("iWOTaskSOPSrNo-->"+_putBackPart.iWOTaskSOPSrNo);
      print("iWOTaskNo-->"+_putBackPart.iWOTaskNo);
      print("partSrNo-->"+_putBackPart.partSrNo);
      print("itemLedgerCost-->"+_putBackPart.itemLedgerCost);
      print("expDate -->"+_putBackPart.expDate);
      print("Rate-->"+_putBackPart.Rate);
      print("iBillingTypeCode-->"+_putBackPart.iBillingTypeCode);
      String strPartDetail =_putBackPart.vendorLotID+"~"+_putBackPart.actionSrNo+
          "~"+_putBackPart.defaultCaseSize+"~"+
          _putBackPart.iWOTaskSOPSrNo+"~"+_putBackPart.iWOTaskNo+"~"+
          _putBackPart.partSrNo+"~"  +_putBackPart.itemLedgerCost+"~"+
          _putBackPart.expDate+"~" +_putBackPart.Rate+"~"+_putBackPart.iBillingTypeCode;

      print(" this is complete String with tild for the - strPartDetail-->");
      print("location--->"+location);
      print("strPartDetail--->"+strPartDetail);


      int partQuantity;
    if (StringUtil.isInt(_putBackPart.actualQuantity))
    {
      partQuantity =
          StringUtil.tryParse(_putBackPart.actualQuantity);
    }
    else{
      if (StringUtil.isDouble(_putBackPart.actualQuantity))
      {
        double d =
        StringUtil.tryParse(_putBackPart.actualQuantity);
        partQuantity =d.round();
      }
    }


    print("IssueQuantity--->"+partQuantity.toString());



    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL +
        companyCode + NetworkConfig.AMPERSAND_SYMBOL +
        "IWONo" +  NetworkConfig.EQUAL_SYMBOL +workOrder.iWONo+NetworkConfig.AMPERSAND_SYMBOL+
        "SerialNo" +  NetworkConfig.EQUAL_SYMBOL +_putBackPart.serialNo+NetworkConfig.AMPERSAND_SYMBOL+
        "IssueQuantity" +  NetworkConfig.EQUAL_SYMBOL +partQuantity.toString() +NetworkConfig.AMPERSAND_SYMBOL+
        "WorkorderNo" +  NetworkConfig.EQUAL_SYMBOL +workOrder.wONo +NetworkConfig.AMPERSAND_SYMBOL+
        "PutbackLoc" +  NetworkConfig.EQUAL_SYMBOL +location +NetworkConfig.AMPERSAND_SYMBOL+
        "PutbackQuantity" +  NetworkConfig.EQUAL_SYMBOL +putBackQty+NetworkConfig.AMPERSAND_SYMBOL+
        "PartNo" +  NetworkConfig.EQUAL_SYMBOL +partNo +NetworkConfig.AMPERSAND_SYMBOL+
        "strPartDetail" +  NetworkConfig.EQUAL_SYMBOL +strPartDetail+NetworkConfig.AMPERSAND_SYMBOL+
        "Room" +  NetworkConfig.EQUAL_SYMBOL +roomFacForLocation.iLocationRoomFacility +NetworkConfig.AMPERSAND_SYMBOL+
        "UserID" +  NetworkConfig.EQUAL_SYMBOL +userId ;


return param;

  }

}