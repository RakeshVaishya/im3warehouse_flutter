import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/models/putaway_item/default_purchasing_room_location.dart';
import 'package:im3_warehouse/network/network_config.dart';

class PutAwayApi extends NetworkConfig {

  static const String API_VALIDATE_PUT_AWAY_LABEl_ID =   "ValidatePutAwayLabelID";
  static const String API_GET_DEFAULT_PURCHASING_ROOM_LOCATION = "getDefaultPurchasingRoomLocation";
  static const String API_CHECK_DUPLICATE_ROOM_AREA_CODE = "IsDuplicateRoomAreaCode";
  static const String API_ADD_NEW_LOCATION = "addNewLocation";
  static const String API_ADD_PUT_AWAY = "AddPutaway";




  static String validatePutAwayLabelID(
      String companyCode,  Facility facility ,String PutawayLabelID) {
 /*
   http://m.im3.com/Web/Service/Mobile.asmx/ValidatePutAwayLabelID?CompanyCode=1&strPutawayLabelID=ZZ018905&IFacilityCode=10
*/
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "strPutawayLabelID" +  NetworkConfig.EQUAL_SYMBOL + PutawayLabelID + NetworkConfig.AMPERSAND_SYMBOL + "IFacilityCode"+
        NetworkConfig.EQUAL_SYMBOL +
        facility.iFacilityCode;
    return param;
  }

  static String getDefaultPurchasingRoomLocation(
      String companyCode,  String  iFacilityCode ,String iRoomCode)
  {

    /*
    Request URL: http://m.im3.com/Web/Service/Mobile.asmx/getDefaultPurchasingRoomLocation
*/
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "iFacilityCode" +  NetworkConfig.EQUAL_SYMBOL + iFacilityCode + NetworkConfig.AMPERSAND_SYMBOL + "iRoomCode"+
        NetworkConfig.EQUAL_SYMBOL +
        iRoomCode;
    return param;
  }

 // isDuplicateRoomAreaCode

  static String isDuplicateRoomAreaCode(
      String companyCode,  String  iFacilityCode ,String iRoomCode ,String roomAreaCode )
  {

  /*   Map<String, dynamic> data = new Map<String, dynamic>();
     data["companyCode"]=companyCode;
     data["iFacilityCode"]=iFacilityCode;
     data["iRoomCode"]=iRoomCode;
     data["roomAreaCode"]=roomAreaCode;
      String param =jsonEncode(data);
*/

     String param ="companyCode" +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "iFacilityCode" +  NetworkConfig.EQUAL_SYMBOL + iFacilityCode + NetworkConfig.AMPERSAND_SYMBOL + "iRoomCode"+
        NetworkConfig.EQUAL_SYMBOL +
        iRoomCode + NetworkConfig.AMPERSAND_SYMBOL +"roomAreaCode" + NetworkConfig.EQUAL_SYMBOL +roomAreaCode;

    return param;


  }

  static String addNewLocation(
                               DefaultPurchasingRoomLocation defaultPurchasingRoomLocation,String createUserId  )
  {


    /*CompanyCode: "108"
    IFacilityCode: "2"
    IRoomCode: "8"
    RoomAreaCode: "test"
    RoomAreaName: "test"
    Status: true
    Pickable_Location: true
    Breakpack_Location: true
    Quarantine_Location: false
    CF1: ""
    CF2: ""
    CF3: ""
    CF4: ""
    CF5: ""
    CF6: ""
    CF7: ""
    CF8: ""
    CF9: ""
    CF10: ""
    CreateUserId: "test_coldfront"
    sequenceno: "-1"
    IZonecode: "15"
    DockingLocation: false
    DefaultDockingLocation: false
    OverwriteDefaultDockLoc: false
    IsDefaultPurchasingLocation: false
    changeDefaultPurchasingLocation: false
    IsStagingLocation: false

*/
    Map<String, dynamic> data = new Map<String, dynamic>();

    data["CompanyCode"]=defaultPurchasingRoomLocation.companyCode;
    data["IFacilityCode"]=defaultPurchasingRoomLocation.iFacilityCode;
    data["IRoomCode"]=defaultPurchasingRoomLocation.iRoomCode;
    data["RoomAreaCode"]=defaultPurchasingRoomLocation.roomAreaCode;
    data["RoomAreaName"]=defaultPurchasingRoomLocation.roomAreaName;
    data["Status"]=true;
    data["Pickable_Location"]=true;
    data["Breakpack_Location"]=true;
    data["Quarantine_Location"]=false;
    data["CF1"]="";
    data["CF2"]="";
    data["CF3"]="";
    data["CF4"]="";
    data["CF5"]="";
    data["CF6"]="";
    data["CF7"]="";
    data["CF8"]="";
    data["CF9"]="";
    data["CF10"]="";
    data["CreateUserId"]=createUserId;
    data["sequenceno"]="-1";
    data["IZonecode"]=defaultPurchasingRoomLocation.iZoneCode;
    data["DockingLocation"]=false;
    data["DefaultDockingLocation"]=false;
    data["OverwriteDefaultDockLoc"]=false;
    data["IsDefaultPurchasingLocation"]=false;
    data["changeDefaultPurchasingLocation"]=false;
    data["IsStagingLocation"]=false;
    Map<String, dynamic> data2 = new Map<String, dynamic>();
    data2 ["addNewloc"] =data;
    String param =jsonEncode(data2);


    /*
        String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "iFacilityCode" +  NetworkConfig.EQUAL_SYMBOL + iFacilityCode + NetworkConfig.AMPERSAND_SYMBOL + "iRoomCode"+
        NetworkConfig.EQUAL_SYMBOL +
        iRoomCode ;*/

        return param;
  }



  static String addPutaway(
      String putawayLabelID,  String  iPutawayLabelID ,String putAwayqty ,
      String putawayLocation, String confirmPutawayLocation, String iLocationRoomFacility,String companyCode,String userId )
  {



/*
    Public Function AddPutaway(ByVal PutawayLabelID As String,
        ByVal IPutawayLabelID As Integer,
        ByVal PutAwayqty As Integer,
        ByVal PutawayLocation As String,
        ByVal ConfirmPutawayLocation As String,
        ByVal ILocationRoomFacility As String) As String
        ByVal CompanyCode As Integer,
                               ByVal UserID As String

*/

    String param = "PutawayLabelID" +  NetworkConfig.EQUAL_SYMBOL + putawayLabelID +  NetworkConfig.AMPERSAND_SYMBOL +
                   "IPutawayLabelID" +  NetworkConfig.EQUAL_SYMBOL + iPutawayLabelID + NetworkConfig.AMPERSAND_SYMBOL +
                    "PutAwayqty"+NetworkConfig.EQUAL_SYMBOL+putAwayqty+ NetworkConfig.AMPERSAND_SYMBOL +
                     "PutawayLocation"+NetworkConfig.EQUAL_SYMBOL+putawayLocation+ NetworkConfig.AMPERSAND_SYMBOL +
                      "ConfirmPutawayLocation"+NetworkConfig.EQUAL_SYMBOL+confirmPutawayLocation+ NetworkConfig.AMPERSAND_SYMBOL +
                      "ILocationRoomFacility"+NetworkConfig.EQUAL_SYMBOL+iLocationRoomFacility+NetworkConfig.AMPERSAND_SYMBOL+
                      "CompanyCode"+NetworkConfig.EQUAL_SYMBOL+companyCode+ NetworkConfig.AMPERSAND_SYMBOL +
                       "UserID"+NetworkConfig.EQUAL_SYMBOL+userId;


    ;

    return param;
  }

 }



