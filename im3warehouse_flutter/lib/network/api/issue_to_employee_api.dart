
import 'dart:convert';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/values/app_strings.dart';


class IssueToEmployeeApi extends NetworkConfig {

  static const String API_GET_EMPLOYEE = "GetEmployee";
  static const String API_GET_PART_DATA_FOR_ISSUE_MOBILE = "getPartDataForIssueMobile";
  static const String API_GET_ALL_STOCK_FROM_PART = "getAllStockforPart";


  static String   getEmployee (
      String companyCode, String empID) {
    /*
    ByVal CompanyCode As Integer,
        ByVal WoNo As String
    */
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL +      companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +  "EmpID" +    NetworkConfig.EQUAL_SYMBOL + empID;
    return param;
  }


  static String getPartDataForIssueMobile(
      String companyCode, String pstrPartNo, String iFacilityCode) {

//CompanyCode As Integer,
//    ByVal pstrPartNo As String,
//    ByVal IFacilityCode As Integer


    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "pstrPartNo" + NetworkConfig.EQUAL_SYMBOL + pstrPartNo +NetworkConfig.AMPERSAND_SYMBOL+
        "IFacilityCode" + NetworkConfig.EQUAL_SYMBOL + iFacilityCode;

    return param;
  }


  static String getAllStockForPart(
      String companyCode, String iPartNo, String iFacilityCode ,String  stockSrNo, String roomAreaCode) {


    /*
    companyCode: 107
    iPartNo: 80815
    iFacilityCode: 27
    stockSrNo: 0
    roomAreaCode: SL1
    */


    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "iPartNo" + NetworkConfig.EQUAL_SYMBOL + iPartNo +NetworkConfig.AMPERSAND_SYMBOL+
        "iPartNo" + NetworkConfig.EQUAL_SYMBOL + iFacilityCode+NetworkConfig.AMPERSAND_SYMBOL+
        "iFacilityCode" + NetworkConfig.EQUAL_SYMBOL + iFacilityCode +NetworkConfig.AMPERSAND_SYMBOL+
        "roomAreaCode" + NetworkConfig.EQUAL_SYMBOL + roomAreaCode;

    return param;
  }



}