import 'dart:convert';
import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/models/equipment/equipment.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_item_detail.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_list.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/values/app_strings.dart';

class ReceiveByPoApi extends NetworkConfig{

  /*
  *     beta cold front testing data
  *     wrok order number --> W012060
  *     Part no   X4 7300 -CHROME
  *     Location      --->   VAN-SALT-LAKE CITY
  *     Serail Number - TAM 1597966
  *
  * */

  static const String API_GET_ALL_RECIEVE_AGAINST_PO =   "GetAllReceiveAgainstPO";
  static const String API_GET_RECIEVE_PO_ITEM_DETAILS =   "GetReceivePOItemDetail";
  static const String API_IS_PART_SERIALIZED = "isPartSerialized";
  static const String API_CHECK_DUPLICATE_SERIAL_NO = "chkDuplicateSerialNo";
  static const String API_SAVE_RECEIVE_BY_PO = "saveReceiveByPO";
  static const String API_CHECK_SERIAL_NO = "checkSerialNo";



  static String  getAllReceiveAgainstPo ( String userId,
      String companyCode, String poNo  ,String partNo,Facility facility) {


/*
jsonBody---CompanyCode=107&PONo=PO02334&PartNo=&VendorCode=&itemStatusCode=1&iFacilityCode=29&offset=0&fetch=20&sortBy=

    Public Function GetAllReceiveAgainstPO(ByVal CompanyCode As Integer,
        ByVal PONo As String,
        ByVal PartNo As String,	blank
        ByVal VendorCode As String,	blank
        ByVal itemStatusCode As Integer,	DefaultValue="1"
        ByVal iFacilityCode As Integer,
        ByVal offset As Integer,	DefaultValue="0"
        ByVal fetch As Integer,	DefaultValue="20"
        ByVal sortBy As String)	blank
    blank

*/

     String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL +companyCode +NetworkConfig.AMPERSAND_SYMBOL +
         "PONo"  + NetworkConfig.EQUAL_SYMBOL + poNo  +NetworkConfig.AMPERSAND_SYMBOL +
         "PartNo" +NetworkConfig.EQUAL_SYMBOL+partNo+NetworkConfig.AMPERSAND_SYMBOL +
         "VendorCode" +NetworkConfig.EQUAL_SYMBOL+""+NetworkConfig.AMPERSAND_SYMBOL +
         "itemStatusCode" +NetworkConfig.EQUAL_SYMBOL+"1"+NetworkConfig.AMPERSAND_SYMBOL +
         "iFacilityCode" +NetworkConfig.EQUAL_SYMBOL+facility.iFacilityCode+NetworkConfig.AMPERSAND_SYMBOL +
         "UserId" +NetworkConfig.EQUAL_SYMBOL+userId+NetworkConfig.AMPERSAND_SYMBOL +
         "offset" +NetworkConfig.EQUAL_SYMBOL+"0"+NetworkConfig.AMPERSAND_SYMBOL +
         "fetch" +NetworkConfig.EQUAL_SYMBOL+"20"+NetworkConfig.AMPERSAND_SYMBOL +
         "sortBy" +NetworkConfig.EQUAL_SYMBOL+"";
    return param;
  }


  static String getReceivePOItemDetail(
      String companyCode, String iPONO, String lineItemSrNo) {
    /*Public Function GetReceivePOItemDetail(ByVal CompanyCode As Integer,
        ByVal IPONO As Integer,
        ByVal LineItemSrNo As Integer)
    */
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "iPONO" + NetworkConfig.EQUAL_SYMBOL + iPONO +NetworkConfig.AMPERSAND_SYMBOL+
        "lineItemSrNo" + NetworkConfig.EQUAL_SYMBOL + lineItemSrNo;
    return param;
  }


  static String isPartSerialized(
      String companyCode, String iPartNo) {
/*
    Public Function isPartSerialized(ByVal CompanyCode As Integer,
        ByVal IPartNo As Integer) As Boolean
*/
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "IPartNo" + NetworkConfig.EQUAL_SYMBOL + iPartNo;

    return param;
  }




  static String chkDuplicateSerialNo(
      String companyCode, String iPartNo,String serialNumber,String controlId, String actionCode) {

/*
  Public Function chkDuplicateSerialNo(ByVal companyCode As Integer,
      ByVal iPartNo As Integer,
      ByVal serialNumber As String,
      ByVal controlId As String,
      ByVal actionCode As Integer) As String
  */
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "iPartNo" + NetworkConfig.EQUAL_SYMBOL + iPartNo +NetworkConfig.AMPERSAND_SYMBOL +
        "serialNumber" + NetworkConfig.EQUAL_SYMBOL + serialNumber +NetworkConfig.AMPERSAND_SYMBOL +
        "controlId" + NetworkConfig.EQUAL_SYMBOL + controlId +NetworkConfig.AMPERSAND_SYMBOL +
        "actionCode" + NetworkConfig.EQUAL_SYMBOL + actionCode;

    return param;
  }

 static String  saveReceiveByPO ( String userId,
                String companyCode,Facility facility,PoDetail poDetail ,
                 POItemDetails poItemDetails,RoomFacForLocation
       roomFacForLocation,Equipment equipment ,String receiveQty,String
       serialNumber,String vendorLotNumber,String expDate ,String notes )
 {

/*

    Public Function saveReceiveByPO(
        ByVal CompanyCode As Integer,
        ByVal IPONO As Integer,
        ByVal LineItemSrNo As Integer,

        ByVal ReceiveQuantity As String,
        ByVal QtyReceiveTillDate As String,

        ByVal IReleaseNo As Integer,
        ByVal ReleaseNo As Integer,

        ByVal SerialNo As String,
        ByVal PartNo As String,

        ByVal IPartNo As Integer,
        ByVal PartDesc As String,

        ByVal ExchangeRate As Decimal,
        ByVal PartBillingType As String,

        ByVal ItemRate As String,
        ByVal OrderQuantity As String,
        ByVal IssueUOMQuantity As String,

        ByVal IssueUOMOrderQty As String,
        ByVal IssueUOMUnitPrice As String,
        ByVal IWONO As Integer,
        ByVal VendorCode As String,

        ByVal VenLNo As String,
        ByVal ExpDate As String,
        ByVal UserID As String,

        ByVal StockingLocation As String,
        ByVal Equipment As String) As String
*/

   String eQCode ="";
   if(equipment==null )
   {
     eQCode ="";
    }
      else{
      eQCode =equipment.eQCode + '~' +equipment.departmentCode + '~' +  equipment.accountCode+ '~' +equipment.lineProductCode;
      }

    String param = NetworkConfig.API_PARAM_COMPANY_CODE + NetworkConfig.EQUAL_SYMBOL +companyCode +NetworkConfig.AMPERSAND_SYMBOL +

        "IPONO"  + NetworkConfig.EQUAL_SYMBOL + poItemDetails.iPONo  +NetworkConfig.AMPERSAND_SYMBOL +
        "LineItemSrNo" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.lineItemSrNo+NetworkConfig.AMPERSAND_SYMBOL +

        "ReceiveQuantity" +NetworkConfig.EQUAL_SYMBOL+receiveQty+NetworkConfig.AMPERSAND_SYMBOL +
        "QtyReceiveTillDate" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.totalReceivedQty+NetworkConfig.AMPERSAND_SYMBOL +

        "IReleaseNo" +NetworkConfig.EQUAL_SYMBOL+poDetail.iReleaseNo+NetworkConfig.AMPERSAND_SYMBOL +
        "ReleaseNo" +NetworkConfig.EQUAL_SYMBOL+poDetail.releaseNo+NetworkConfig.AMPERSAND_SYMBOL +

        "SerialNo" +NetworkConfig.EQUAL_SYMBOL+serialNumber+NetworkConfig.AMPERSAND_SYMBOL +
        "PartNo" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.partNo+NetworkConfig.AMPERSAND_SYMBOL +

        "IPartNo" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.iPartNo+NetworkConfig.AMPERSAND_SYMBOL +
        "PartDesc" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.partDescription+NetworkConfig.AMPERSAND_SYMBOL +

     //        currencyExchangeRate = json['CurrencyExchangeRate'];
        "ExchangeRate" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.currencyExchangeRate+NetworkConfig.AMPERSAND_SYMBOL +

        "PartBillingType" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.billingTypeCode+NetworkConfig.AMPERSAND_SYMBOL +

        "ItemRate" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.issueUOMUnitPrice+NetworkConfig.AMPERSAND_SYMBOL +

        // orderQuantity = json['Order_Quantity'];
         "OrderQuantity" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.orderQuantity+NetworkConfig.AMPERSAND_SYMBOL +

        "IssueUOMQuantity" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.conversionFactor+NetworkConfig.AMPERSAND_SYMBOL +
        "IssueUOMOrderQty" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.issueUOMOrderQuantity+NetworkConfig.AMPERSAND_SYMBOL +

        "IssueUOMUnitPrice" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.issueUOMUnitPrice+NetworkConfig.AMPERSAND_SYMBOL +
        "IWONO" +NetworkConfig.EQUAL_SYMBOL+poItemDetails.iWONo+NetworkConfig.AMPERSAND_SYMBOL +

        "VendorCode" +NetworkConfig.EQUAL_SYMBOL+""+NetworkConfig.AMPERSAND_SYMBOL +
        "VenLNo" +NetworkConfig.EQUAL_SYMBOL+vendorLotNumber+NetworkConfig.AMPERSAND_SYMBOL +

        "ExpDate" +NetworkConfig.EQUAL_SYMBOL+expDate+NetworkConfig.AMPERSAND_SYMBOL +
        "UserID" +NetworkConfig.EQUAL_SYMBOL+userId+NetworkConfig.AMPERSAND_SYMBOL +

        "StockingLocation" +NetworkConfig.EQUAL_SYMBOL+roomFacForLocation.locationRoomFacility
        +NetworkConfig.AMPERSAND_SYMBOL +
        "Equipment" +NetworkConfig.EQUAL_SYMBOL+eQCode;

   return param;
  }

}