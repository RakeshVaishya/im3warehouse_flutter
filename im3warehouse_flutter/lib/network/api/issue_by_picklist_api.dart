import 'package:im3_warehouse/models/issue_pick_list/Issue_by_pick_list_model.dart';
import 'package:im3_warehouse/network/network_config.dart';

class IssueByPickListApi extends NetworkConfig{


//  CompanyCode=108&IPickListNo=27771&IFacilityCode=4
  static const String API_GET_PICK_TICKET_HELP =   "getPickTickethelp";
  static const String API_SAVE_ISSUE_BY_PICK_TICKET = "SaveIssueByPickTicket";


  static String getPickTickethelp(
      String CompanyCode, String IPickListNo, String  IFacilityCode) {
/*
  http://m.beta.coldfront.im3cloud.com/Web/Service/Mobile.asmx/getStockforTransferPart?
  CompanyCode=108&p int IPartNo=885&pintIFacilityCode=2&pstrUserid=test_coldfront&location=X43
*/

    String param = NetworkConfig.API_PARAM_COMPANY_CODE +   NetworkConfig.EQUAL_SYMBOL +   CompanyCode +   NetworkConfig.AMPERSAND_SYMBOL +
                  "IInvPickingHeaderId" + NetworkConfig.EQUAL_SYMBOL + IPickListNo +  NetworkConfig.AMPERSAND_SYMBOL +
                  "IFacilityCode" + NetworkConfig.EQUAL_SYMBOL +   IFacilityCode;
    return param;
  }


  static String saveIssueByPickTicket(IssueByPickListModel issueByPickListModel,
     String userId ,String empld,String iWorkOrderTask,String txtQuantity ) {

/*

  Public Function SaveIssueByPickTicket(ByVal IPartNo As Integer, ByVal PartNo As String, ByVal PickTicketNo As String,
      ByVal ISalesOrderNo As Integer, ByVal ISalesOrderLineItem As Integer,
      ByVal IWONo As Integer, ByVal Part_Sr_No As Integer,
      ByVal ITransfer_Request_ID As Integer, ByVal IWO_Task_No As Integer,

      ByVal IWO_Task_SOP_SR_No As Integer, ByVal OrderQuantity As Decimal,
      ByVal Serial_Number As Boolean, ByVal SerialNo As String,
      ByVal VendorLotNo As String, ByVal ExpirationDate As String,
      ByVal Quantity As Decimal, ByVal PartDesc As String,
      ByVal QtyLeftToIssue As Decimal, ByVal Stock_Sr_No As Integer,
      ByVal IPick_List_No As Integer, ByVal ItemStatus As Integer,
      ByVal Price As Decimal) As String
  Shahbaz says
*/
/*

    Public Function SaveIssueByPickTicket(
        ByVal IPartNo As Integer,
        ByVal CompanyCode As Integer,

        ByVal PartSrNo As Integer,
        ByVal ParentILineItem As Integer,


        ByVal PickTicketNo As String,
        ByVal TxtQuantity As String,

        ByVal UserId As String,
        ByVal ISalesOrderNo As Integer,

        ByVal hdnIPickListNo As Integer,
        ByVal ISalesOrderLineItem As Integer,

        ByVal IWONo As Integer,
        ByVal Part_Sr_No As Integer,

        ByVal ITransfer_Request_ID As Integer,
        ByVal IWO_Task_No As Integer,

        ByVal Serial_Number As Boolean,
        ByVal hdnTriggerCycleCount As Boolean,

        ByVal SerialNo As String,
        ByVal hdnNextCycleCountDate As String,
        ByVal txtEmpId As String,

        ByVal SOPalletNo As String,
        ByVal QuantityLeftToIssue As Decimal,

        ByVal PartDesc As String,
        ByVal Stock_Sr_No As Integer,
        ByVal IInvTypeCode As Integer) As String
       */



    String param =  "IPartNo" +   NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.iPartNo +  NetworkConfig.AMPERSAND_SYMBOL +
                     "CompanyCode" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.companyCode + NetworkConfig.AMPERSAND_SYMBOL +
                      "ParentILineItem" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.parentILineItem +  NetworkConfig.AMPERSAND_SYMBOL +
                      "PickTicketNo" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.pickTicketNo + NetworkConfig.AMPERSAND_SYMBOL +
                      "TxtQuantity" + NetworkConfig.EQUAL_SYMBOL +txtQuantity+ NetworkConfig.AMPERSAND_SYMBOL +

                    "UserId" + NetworkConfig.EQUAL_SYMBOL +userId + NetworkConfig.AMPERSAND_SYMBOL +

        "ISalesOrderNo" + NetworkConfig.EQUAL_SYMBOL +issueByPickListModel.iSalesOrderNo + NetworkConfig.AMPERSAND_SYMBOL +
        "hdnIPickListNo" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.iInvPickingHeaderId + NetworkConfig.AMPERSAND_SYMBOL +
                    "ISalesOrderLineItem" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.iSalesOrderLineItem+ NetworkConfig.AMPERSAND_SYMBOL +

                    "IWONo" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.iWONo + NetworkConfig.AMPERSAND_SYMBOL +
                    "Part_Sr_No" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.partSrNo + NetworkConfig.AMPERSAND_SYMBOL +
                    "ITransfer_Request_ID" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.iTransferRequestID + NetworkConfig.AMPERSAND_SYMBOL +

                   "IWO_Task_No" + NetworkConfig.EQUAL_SYMBOL + iWorkOrderTask + NetworkConfig.AMPERSAND_SYMBOL +
                   "Serial_Number" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.isSerializedPart+ NetworkConfig.AMPERSAND_SYMBOL +
        "hdnTriggerCycleCount" + NetworkConfig.EQUAL_SYMBOL + "FALSE" + NetworkConfig.AMPERSAND_SYMBOL +
        "SerialNo" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.serialNo + NetworkConfig.AMPERSAND_SYMBOL +
        "hdnNextCycleCountDate" + NetworkConfig.EQUAL_SYMBOL + "" + NetworkConfig.AMPERSAND_SYMBOL +
        "txtEmpId" + NetworkConfig.EQUAL_SYMBOL +userId +NetworkConfig.AMPERSAND_SYMBOL +
        "SOPalletNo" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.sOPalletNo + NetworkConfig.AMPERSAND_SYMBOL +
        "QuantityLeftToIssue" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.quantityLeftToIssue + NetworkConfig.AMPERSAND_SYMBOL +
        "PartDesc" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.partDescription + NetworkConfig.AMPERSAND_SYMBOL +
        "Stock_Sr_No" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.stockSrNo + NetworkConfig.AMPERSAND_SYMBOL +
        "IInvTypeCode" + NetworkConfig.EQUAL_SYMBOL + issueByPickListModel.stockSrNo
    ;

    return param;
  }



}