import 'dart:convert';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_location_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_part_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_work_order_modal.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/values/app_strings.dart';

class IssueByWorkOrderApi extends NetworkConfig{

  /*
  *     beta cold front testing data
  *     wrok order number --> W012060
  *     Part no   X4 7300 -CHROME
  *     Location      --->   VAN-SALT-LAKE CITY
  *     Serail Number - TAM 1597966
  *
  * */

  static const String API_GET_WO_MASTER_FOR_HELP =   "GetWOMasterForHelp";
  static const String API_GET_PART_DATA_FOR_ISSUE_MOBILE = "getPartDataForIssueMobile";
  static const String API_GET_STOCK_FOR_MOBILE_PART = "getStockforMobilePart";
  static const String API_ADD_ISSUE_BY_WO = "addIssueByWO";
  static const String API_CHECK_SERIAL_NO = "checkSerialNo";

  static String   getWOMasterForHelp (
      String companyCode, String woNo ,String mode) {
   /*
    ByVal CompanyCode As Integer,
        ByVal WoNo As String
    */
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL +      companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +  "WoNo" +    NetworkConfig.EQUAL_SYMBOL + woNo
        +NetworkConfig.AMPERSAND_SYMBOL + "Mode"+ NetworkConfig.EQUAL_SYMBOL +mode;
    return param;
  }


  static String getPartDataForIssueMobile(
      String companyCode, String pstrPartNo, String iFacilityCode) {

//CompanyCode As Integer,
//    ByVal pstrPartNo As String,
//    ByVal IFacilityCode As Integer


    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "pstrPartNo" + NetworkConfig.EQUAL_SYMBOL + pstrPartNo +NetworkConfig.AMPERSAND_SYMBOL+
        "IFacilityCode" + NetworkConfig.EQUAL_SYMBOL + iFacilityCode;

    return param;
  }



  static String getStockforMobilePart(
      String companyCode, String pintIPartNo, String pintIFacilityCode, String pstrUserId,String location) {

    /*art(ByVal CompanyCode As Integer,
    ByVal pintIPartNo As String,
    ByVal pintIFacilityCode As Integer,
    ByVal pstrUserid As String,
    ByVal location As String) As String
    */

 String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode + NetworkConfig.AMPERSAND_SYMBOL
                  +  "pintIPartNo"  +    NetworkConfig.EQUAL_SYMBOL + pintIPartNo+NetworkConfig.AMPERSAND_SYMBOL+
                     "pintIFacilityCode"  +    NetworkConfig.EQUAL_SYMBOL + pintIFacilityCode+NetworkConfig.AMPERSAND_SYMBOL+
                     "pstrUserId"  +    NetworkConfig.EQUAL_SYMBOL + pstrUserId+NetworkConfig.AMPERSAND_SYMBOL+
                      "location"  +    NetworkConfig.EQUAL_SYMBOL + location;

    return param;
  }

  static String checkSerialNo(
      String companyCode, String iPartNo, String serialNo, String actionCode,String actionSrNo,
      String stockSrNo, String quantity,
      String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId
      ) {

/*
    companyCode: "108"
    iPartNo: "17809"
    serialNo: "test"
    actionCode: 1
    actionSrNo: 0
    stockSrNo: "53906"
    quantity: 1
    iWoNo: 0
    iSalesOrderNo: 0
    lineItemSrNo: 0
    controlId: "txtSerialNo"
*/

    Map data = new Map <String, dynamic>();
    data["companyCode"] = companyCode;
    data["iPartNo"] = iPartNo;
    data["serialNo"] = serialNo;
    data["actionCode"] = actionCode;
    data["actionSrNo"] = actionSrNo;
    data["stockSrNo"] = stockSrNo;
    data["quantity"] = quantity;
    data["iWoNo"] = iWoNo;
    data["iSalesOrderNo"] = iSalesOrderNo;
    data["lineItemSrNo"] = lineItemSrNo;
    data["controlId"] = controlId;
    String param  =jsonEncode(data);


   /* String param = "companyCode" +  NetworkConfig.EQUAL_SYMBOL + companyCode + NetworkConfig.AMPERSAND_SYMBOL
        +  "iPartNo"  +    NetworkConfig.EQUAL_SYMBOL + iPartNo+NetworkConfig.AMPERSAND_SYMBOL+
        "serialNo"  +    NetworkConfig.EQUAL_SYMBOL + serialNo+NetworkConfig.AMPERSAND_SYMBOL+
        "actionCode"  +    NetworkConfig.EQUAL_SYMBOL + actionCode+NetworkConfig.AMPERSAND_SYMBOL+
        "actionSrNo"  +    NetworkConfig.EQUAL_SYMBOL + actionSrNo+NetworkConfig.AMPERSAND_SYMBOL+
        "quantity"  +    NetworkConfig.EQUAL_SYMBOL + quantity+NetworkConfig.AMPERSAND_SYMBOL+
        "iWoNo"  +    NetworkConfig.EQUAL_SYMBOL + iWoNo+NetworkConfig.AMPERSAND_SYMBOL+
        "iSalesOrderNo"  +    NetworkConfig.EQUAL_SYMBOL + iSalesOrderNo+NetworkConfig.AMPERSAND_SYMBOL+
        "lineItemSrNo"  +    NetworkConfig.EQUAL_SYMBOL + lineItemSrNo+NetworkConfig.AMPERSAND_SYMBOL+
        "controlId"  +    NetworkConfig.EQUAL_SYMBOL + controlId;
    return param;
 */
    return param;
  }



  static String addIssueByWO(
      String companyCode, String userId,String empId, String serialNumber,String quantity,
  IssueByWorkOrderModal issueForWorkOrderModal,
  IssueByWOPartModal issueByWOPartModal,
  IssueByWoLocationModel _issueByWoLocationModal ,
  )
  {
    /*
    issueByWO = {
      CompanyCode: im3.IssueByWO.companyCode,
      InvActionCode: 1,
      IWoNo: $('#' + this.ctl + 'HdnIWONo').val(),
      IPartNo: $('#' + this.ctl + 'HdnIPartNo').val(),
      PartNo: p.val().trim(),
      PartDescription: $('#spnPartDesc').html(),

      IInvTypeCode: this.IInvTypeCode,
      SerialNumber: $('#txtSerialNo').val().trim(),
      IUOMCode: p.attr('data-IUOMCode'),
      IBillingTypeCode: p.attr('data-IBillingTypeCode'),
      IBuildingCode: l.attr('data-IFacilityCode'),
      IRoomCode: l.attr('data-IRoomCode'),


      IRoomAreaCode: l.attr('data-IRoomAreaCode'),
      StockSrNo: l.attr('data-StockSrNo'),
      Quantity: $('#txtQuantity').val().trim(),
      CreateUserId: this.UserId,
      IssuedBy: empId,

      IsParentPart: true,
      IWOTaskNo: $('#' + this.ctl + 'HdnIWOTaskNo').val(),
      IsMobilePartAdded: true,
      transactionPage: im3.MobileEnums.transactionPage.MobileScannerIssuebyWO
    }
 */

    Map data = new Map <String, dynamic>();
    data["CompanyCode"] = companyCode;
    data["InvActionCode"] = 1;
    data["IWoNo"] = issueForWorkOrderModal.iWONo;
    data["IPartNo"] = issueByWOPartModal.iPartNo;
    data["PartNo"] = issueByWOPartModal.partNo;
    data["PartDescription"] = issueByWOPartModal.partDescription;
    data["IInvTypeCode"] = issueByWOPartModal.iInvTypeCode;
    data["SerialNumber"] = serialNumber;
    data["IUOMCode"] = issueByWOPartModal.iUOMCode;
    data["IBillingTypeCode"] = issueByWOPartModal.iBillingTypeCode;
    data["IBuildingCode"] =_issueByWoLocationModal.iFacilityCode;
    data["IRoomCode"] =_issueByWoLocationModal.iRoomCode;
    data["IRoomAreaCode"] = _issueByWoLocationModal.iRoomAreaCode;
    data["StockSrNo"] = _issueByWoLocationModal.stockSrNo;
    data["Quantity"] = quantity;
    data["CreateUserId"] = userId;
    data["IssuedBy"] = empId ;
    data["IsParentPart"] = true;
    data["IWOTaskNo"] = "0";
    data["IsMobilePartAdded"] = true;
    data["transactionPage"] = AppStrings.getDevicePlateForm();
    List<dynamic> list =[];
    list.add(data);
    Map data2 = new Map <String, dynamic>();
    data2["issueByWO"]=list;
    String param  =jsonEncode(data2);
    return param;
  }
}