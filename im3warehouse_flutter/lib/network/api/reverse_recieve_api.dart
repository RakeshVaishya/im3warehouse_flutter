import 'dart:convert';
import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/models/equipment/equipment.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_item_detail.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_list.dart';
import 'package:im3_warehouse/models/reverse_receive/reverse_recieve_item.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:xml/xml.dart';



class ReverseReceiveApi extends NetworkConfig{

  /*
  *     beta cold front testing data
  *     wrok order number --> W012060
  *     Part no   X4 7300 -CHROME
  *     Location      --->   VAN-SALT-LAKE CITY
  *     Serail Number - TAM 1597966
  *
  * */

  static const String API_GET_PO_REV_RECEIVE_BY_PART =   "GetPORevReceiveByPart";
  static const String API_GET_RECIEVE_PO_ITEM_DETAILS =   "GetReceivePOItemDetail";
  static const String API_IS_PART_SERIALIZED = "isPartSerialized";
  static const String API_CHECK_DUPLICATE_SERIAL_NO = "chkDuplicateSerialNo";
  static const String API_SAVE_REVERSE_RECEIVE = "saveReverseReceive";
  static const String API_CHECK_SERIAL_NO = "checkSerialNo";

  static const String API_GET_PO_REV_RECEIVE_BY_PART_DETAILS =   "GetPORevReceiveByPartDetails";




  static String  getPORevReceiveByPart (
      String companyCode, String poNo  ,String partNo) {


/*
jsonBody---CompanyCode=107&PONo=PO02334&PartNo=&VendorCode=&itemStatusCode=1&iFacilityCode=29&offset=0&fetch=20&sortBy=


Public Function GetPORevReceiveByPart(ByVal CompanyCode As Integer,
ByVal PONo As String,
ByVal PartNo As String) As String



*/

     String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL +companyCode +NetworkConfig.AMPERSAND_SYMBOL +
         "PONo"  + NetworkConfig.EQUAL_SYMBOL + poNo  +NetworkConfig.AMPERSAND_SYMBOL +
         "PartNo" +NetworkConfig.EQUAL_SYMBOL+partNo;
         return param;
  }



  static String getReceivePOItemDetail(
      String companyCode, String iPONO, String lineItemSrNo) {
    /*Public Function GetReceivePOItemDetail(ByVal CompanyCode As Integer,
        ByVal IPONO As Integer,
        ByVal LineItemSrNo As Integer)
    */
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "iPONO" + NetworkConfig.EQUAL_SYMBOL + iPONO +NetworkConfig.AMPERSAND_SYMBOL+
        "lineItemSrNo" + NetworkConfig.EQUAL_SYMBOL + lineItemSrNo;
    return param;
  }


  static String getPORevReceiveByPartDetails(
      String companyCode, String iPOReceiverNo ,String iPONO, String lineItemSrNo) {

 /*   Public Function GetPORevReceiveByPartDetails(
       ByVal CompanyCode As Integer,
        ByVal IPOReceiverNo As Integer,
        ByVal IPONO As Integer,
        ByVal LineItemSrNo As Integer)
        As POReceiver*/


    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "IPOReceiverNo" + NetworkConfig.EQUAL_SYMBOL + iPOReceiverNo +NetworkConfig.AMPERSAND_SYMBOL+
        "IPONO" + NetworkConfig.EQUAL_SYMBOL + iPONO +NetworkConfig.AMPERSAND_SYMBOL+
        "LineItemSrNo" + NetworkConfig.EQUAL_SYMBOL + lineItemSrNo ;


    return param;
  }


  static String isPartSerialized(
      String companyCode, String iPartNo) {
/*
    Public Function isPartSerialized(ByVal CompanyCode As Integer,
        ByVal IPartNo As Integer) As Boolean
*/
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +  NetworkConfig.AMPERSAND_SYMBOL +
        "IPartNo" + NetworkConfig.EQUAL_SYMBOL + iPartNo;

    return param;
  }




  static String chkDuplicateSerialNo(
      String companyCode, String iPartNo,String serialNumber,String controlId, String actionCode) {

/*
  Public Function chkDuplicateSerialNo(ByVal companyCode As Integer,
      ByVal iPartNo As Integer,
      ByVal serialNumber As String,
      ByVal controlId As String,
      ByVal actionCode As Integer) As String
  */
    String param = NetworkConfig.API_PARAM_COMPANY_CODE +  NetworkConfig.EQUAL_SYMBOL + companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "iPartNo" + NetworkConfig.EQUAL_SYMBOL + iPartNo +NetworkConfig.AMPERSAND_SYMBOL +
        "serialNumber" + NetworkConfig.EQUAL_SYMBOL + serialNumber +NetworkConfig.AMPERSAND_SYMBOL +
        "controlId" + NetworkConfig.EQUAL_SYMBOL + controlId +NetworkConfig.AMPERSAND_SYMBOL +
        "actionCode" + NetworkConfig.EQUAL_SYMBOL + actionCode;

    return param;
  }

 static String  saveReverseReceive (String userId,
                String companyCode,Facility facility,ReverseReceiveItem receiveItem
     ,RoomFacForLocation
       roomFacForLocation,Equipment equipment ,String iPOReceiverNo ,String
       serialNumber,String notes ,String receiveQty , XmlElement xmlElement,POItemDetails  poItemDetail ,String employeeId )
 {

/*

  ByVal CompanyCode As Integer,
ByVal IPONO As Integer,
ByVal LineItemSrNo As Integer,
ByVal IPOReceiverNo As Integer,

ByVal UserID As String,
ByVal IWONO As Integer,
ByVal Notes As String,
ByVal IssueUOMQuantity As String,
ByVal PartBillingType As String,
ByVal SerialNo As String,
ByVal IPartNo As Integer,
ByVal PartDesc As String,
ByVal StockingLocation As String,
ByVal ReceiveQuantity As String,
ByVal Equipment As String) As String

/
 */
   print("c   receiveQty ......"+receiveQty.toString());
   print("c   receiveItem.conversionFactor ......"+poItemDetail.conversionFactor);

   double recQty =double.parse(receiveQty.toString());
   double conversionFactor = double.parse(poItemDetail.conversionFactor);


 recQty =  conversionFactor *recQty;
print("c-----> ......"+xmlElement.getElement("Order_Quantity").text.toString());


   String eQCode ="";
   if(equipment==null )
   {
     eQCode ="";
    }
      else{
      eQCode =equipment.eQCode + '~' +equipment.departmentCode + '~' +  equipment.accountCode+ '~' +equipment.lineProductCode;
      }


        String param = NetworkConfig.API_PARAM_COMPANY_CODE + NetworkConfig.EQUAL_SYMBOL +companyCode +NetworkConfig.AMPERSAND_SYMBOL +
        "IPONO"  + NetworkConfig.EQUAL_SYMBOL + xmlElement.getElement("IPONo").text  +NetworkConfig.AMPERSAND_SYMBOL +
        "LineItemSrNo" +NetworkConfig.EQUAL_SYMBOL+xmlElement.getElement("LineItemSrNo").text +NetworkConfig.AMPERSAND_SYMBOL +

        "IPOReceiverNo" +NetworkConfig.EQUAL_SYMBOL+xmlElement.getElement("IPOReceiverNo").text+NetworkConfig.AMPERSAND_SYMBOL +
        "UserID" +NetworkConfig.EQUAL_SYMBOL+userId+NetworkConfig.AMPERSAND_SYMBOL +

        "IWONO" +NetworkConfig.EQUAL_SYMBOL+xmlElement.getElement("IWONo").text+NetworkConfig.AMPERSAND_SYMBOL +
        "Notes" +NetworkConfig.EQUAL_SYMBOL+notes+NetworkConfig.AMPERSAND_SYMBOL +

        "IssueUOMQuantity" +NetworkConfig.EQUAL_SYMBOL+poItemDetail.conversionFactor+NetworkConfig.AMPERSAND_SYMBOL +
            "EmployeeId" +NetworkConfig.EQUAL_SYMBOL+employeeId+NetworkConfig.AMPERSAND_SYMBOL +
        "PartBillingType" +NetworkConfig.EQUAL_SYMBOL+poItemDetail.billingTypeCode+NetworkConfig.AMPERSAND_SYMBOL +




        "SerialNo" +NetworkConfig.EQUAL_SYMBOL+serialNumber+NetworkConfig.AMPERSAND_SYMBOL +
        "IPartNo" +NetworkConfig.EQUAL_SYMBOL+xmlElement.getElement("IPartNo").text+NetworkConfig.AMPERSAND_SYMBOL +
        "PartDesc" +NetworkConfig.EQUAL_SYMBOL+xmlElement.getElement("PartDescription").text+NetworkConfig.AMPERSAND_SYMBOL +
        "StockingLocation" +NetworkConfig.EQUAL_SYMBOL+roomFacForLocation.locationRoomFacility
        +NetworkConfig.AMPERSAND_SYMBOL +
        "ReceiveQuantity" +NetworkConfig.EQUAL_SYMBOL+recQty.toString()
        +NetworkConfig.AMPERSAND_SYMBOL +
        "Equipment" +NetworkConfig.EQUAL_SYMBOL+eQCode;

   return param;
  }
}