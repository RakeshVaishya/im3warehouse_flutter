import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/network/network_config.dart';

class ViewInventoryApi extends NetworkConfig {
  static const String API_GET_PART_DATA_FOR_MOBILE=   "getPartDataForIssueMobile";
  static const String API_GET_ROOM_TO_TRANSFER_PART = "getRoomToTransferPart";
  static const String API_TRANSFER_QUANTITY = "Transferquantity";
  static const String API_CHK_Inventory_Detail = "chkInventoryDetail";




  static String getPartDataForIssueMobile(
      String pstrPartNo, Facility facility, String companyCode) {
/*
 Request URL: http://m.test.im3cloud.com/Web/Service/Mobile.asmx/getPartDataForIssueMobile?CompanyCode=107&pstrPartNo=rk0019&IFacilityCode=27


*/

    String param = NetworkConfig.API_PARAM_COMPANY_CODE +
        NetworkConfig.EQUAL_SYMBOL +
        companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrPartNo" +
        NetworkConfig.EQUAL_SYMBOL +
        pstrPartNo +
        NetworkConfig.AMPERSAND_SYMBOL +
        "IFacilityCode" +
        NetworkConfig.EQUAL_SYMBOL +
        facility.iFacilityCode;

    return param;
  }

  static String getRoomToTransferPart(
      String toBin, Facility facility, PartDetails partDetails) {
/*

   http://m.beta.coldfront.im3cloud.com/Web/Service/Mobile.asmx/getRoomToTransferPart?
   CompanyCode=108
   &pstrFacilityCode=CGY
   &pstrRoomCode=
   &pstrRoomAreaCode=cccc
   &pstrRoomAreaName=
   &pintRoomAreaStatus=1
*/

    String param = NetworkConfig.API_PARAM_COMPANY_CODE +
        NetworkConfig.EQUAL_SYMBOL +
        partDetails.companyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrFacilityCode" +
        NetworkConfig.EQUAL_SYMBOL +
        facility.facilityCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrRoomCode" +
        NetworkConfig.EQUAL_SYMBOL +
        "" +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrRoomAreaCode" +
        NetworkConfig.EQUAL_SYMBOL +
        toBin.trim() +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrRoomAreaName" +
        NetworkConfig.EQUAL_SYMBOL +
        "" +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pintRoomAreaStatus" +
        NetworkConfig.EQUAL_SYMBOL +
        "1";
    return param;
  }

/*
  Transfer save --Public Function Transferquantity(pstrPart As String, pstrStockFrom As String, pstrStockTo As String, ByVal partQuantity As Integer) As String
*/

  static String setTransferQuantityToLocation(
      String part,
      String stockFrom,
      String stockTransferToLocation,
      String partQuantity,
      String CompanyCode,
      String UserId,
      String EmployeeId) {
    String param = "pstrPart" +
        NetworkConfig.EQUAL_SYMBOL +
        part +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrStockFrom" +
        NetworkConfig.EQUAL_SYMBOL +
        stockFrom +
        NetworkConfig.AMPERSAND_SYMBOL +
        "pstrStockTo" +
        NetworkConfig.EQUAL_SYMBOL +
        stockTransferToLocation +
        NetworkConfig.AMPERSAND_SYMBOL +
        "partQuantity" +
        NetworkConfig.EQUAL_SYMBOL +
        partQuantity.toString() +
        NetworkConfig.AMPERSAND_SYMBOL +
        "CompanyCode" +
        NetworkConfig.EQUAL_SYMBOL +
        CompanyCode +
        NetworkConfig.AMPERSAND_SYMBOL +
        "UserId" +
        NetworkConfig.EQUAL_SYMBOL +
        UserId +
        NetworkConfig.AMPERSAND_SYMBOL +
        "EmployeeId" +
        NetworkConfig.EQUAL_SYMBOL +
        EmployeeId;
    return param;
  }

   static String chkInventoryDetail(String  companyCode ,
      String partNo ,
  String location) {



     String param = NetworkConfig.API_PARAM_COMPANY_CODE +
         NetworkConfig.EQUAL_SYMBOL +
         companyCode +
         NetworkConfig.AMPERSAND_SYMBOL +
         "PartNo" +
         NetworkConfig.EQUAL_SYMBOL +
         partNo +
         NetworkConfig.AMPERSAND_SYMBOL +
         "Location" +
         NetworkConfig.EQUAL_SYMBOL +
         location;

     return param;
   }



/*

  Transferquantity(ByVal pstrPart As String,
      ByVal pstrStockFrom As String,
  ByVal pstrStockTo As String,
      ByVal partQuantity As Integer,
  ByVal CompanyCode As Integer,
      ByVal UserId As String,
  ByVal EmployeeId As String)
*/

}
