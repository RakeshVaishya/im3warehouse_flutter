import 'dart:convert';
import 'dart:core';

import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/reserves_part_location.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/values/app_strings.dart';

class NetworkConfig {
  /*

   local  ip
   local.im3cloud.com


  Sagar says
  pintCompanyCode integer
  Sagar says
  UserId string
  */
/*

108|ColdFront|test_coldfront|Super User|test_coldfront|MM/dd/yyyy|4|VAN|S5degYUi8TjUBgJw9X7Y4w==|BYhzraoREXGM9y74Buu4/w==

  <?xml version="1.0" encoding="utf-8"?>
  <string xmlns="http://m.mtrack.com/"> company id-> 108|  company name-->  ColdFront|  user Id- test_coldfront|
   user Name = Super user
  | employee = test_coldfront|  date time format  ==MM/dd/yyyy|  default i facity code  2|  faciity code ==  CGY|
   uid for cookie S5degYUi8TjUBgJw9X7Y4w==| pwd for cookie BYhzraoREXGM9y74Buu4/w==
 </string>
*/
/*
*
*
testing data for serilize --
 R90, SB, SB 210, S-600M, SPECTRUM
  69UG15-068S, U X4 7300, SOLARA

* */

/*
  var part = {
  CompanyCode: im3.AdjustInventory.companyCode,
  CreateUserID: im3.AdjustInventory.userId,
  LastUpdateUserId: im3.AdjustInventory.userId,
  PartNo: $('#ctl00_ContentPlaceHolder1_txtAddPartNo').val().trim(),
  PartDescription: $('#txtPartDescription').val().trim(),
  SerailNumber: $('#chkSerializePart').is(':checked'),
  Rate: $('#txtPrice').val().trim(),
  IUOMCode: $('#ctl00_ContentPlaceHolder1_hdnDefaultIUOMCode').val(),
  IBillingTypeCode: $('#ctl00_ContentPlaceHolder1_hdnDefaultIBillingTypeCode').val(),
  DateAdded: d.mmddyyyy(),
  IInvTypeCode: 1,
  IPartStatusCode: 1,
  PartCriticalityCode: 1,
  Manufactured: 0,
  AutoPO: true,
  Tax_Required: $('#ctl00_ContentPlaceHolder1_hdnDefaultTaxType').val(),
  SmartFIFO: ((expiryDate == true) ? 4 : 1), // 4-byExpiryDate,1-FIFO
  VendorLotNumber: $('#chkVendorLotNo').is(':checked'),
  ExpirationDate: expiryDate,
  NotToBeSoldToCustomerGroup: im3.AdjustInventory.NotToBeSoldToCustomerGroup
}
Sagar says
/Web/Service/Mobile.asmx/addPart
*/
   static String API_GET_COMPANY_URL =AppStrings.API_GET_COMPANY_URL;
  static String BASE_URL = AppStrings.BASE_URL;

  static final headers = {'Content-Type': 'application/x-www-form-urlencoded'};
  static final jsonHeaders = {'Content-Type': 'application/json'};

  static String WEB_URL_PATH = "/web/service/Mobile.asmx/";
  static const String Login = "ValidateMobileAppUser";
  static const String GET_FACILITY_LIST_FOR_USER = "GetFacilityListForUser";

  /* API FOR PHYSICAL COUNT*/
/* RETRIVE PART DETAILS*/
  static const String API_GET_PART = "GetPart";
  static const String API_GET_ALL_STOCK = "GetAllStock";
  static const String API_GET_ROOM_FAC_FOR_LOCATION = "GetRoomFacForLocation";
  static const String API_GET_DYNAMIC_HELP = "getDynamicHelp";
  static const String API_GET_RESERVES_FOR_PART_LOCATION ="getReservesForPartLocation";
  static const String API_GET_ALL_CUSTOMER_GROUP_WITH_SEPERATOR ="getAllCustomerGroupWithSeperator";
  static const String API_ADD_PART = "addPart";
  static const String API_ADD_CYCLE_COUNT = "Addcycleccount";
  static const String API_CHECK_SERIAL_NO = "checkSerialNo";
  static const String API_CHECK_DUPLICATE_SERIAL_NO = "chkDuplicateSerialNo";

  static const String API_CHECK_EXIST_SERIAL_NO_ROOM_LEVEL  = "chkExistsSerialNoRoomLevel";




  //Issue for Work Order
 // static const String API_GET_WORK_ORDER = "GetWOMasterForHelp";
  //static const String API_GET_PART_DATA_ISSUEMOBILE =   "getPartDataForIssueMobile";
  //static const String API_GET_PART_LOCATION_ISSUEMOBILE = "getStockforMobilePart";

  // Issue for work Task
 // static const String API_GET_ALL_TASK = "GetAllTask";
//

  static const String Company_Code = "108";
  static const String Site_Url = "https://betacoldfront.im3cloud.com/";
  static const String Verification_Code = "coldfrontbeta";
  static List<String> loginData;

//  CompanyCode=1&PartNo=sagar&PartDesc=&status=-1&IFacilityCode=10&RoomAreaCode=&LUserId=sagarshinde63

  //CompanyCode=1&Mode=roomarea&CheckCountOnly=0&Min=1&Max=2&SearchTerm=location_sep&
  // Condition=AND%20CD_FC_Facility.IFacility_Code=10&IWONo=0&userid=sagarshinde63&IFacilityCode=

  /* */

  static String EQUAL_SYMBOL = "=";
  static String AMPERSAND_SYMBOL = "&";

  static String API_PARAM_COMPANY_CODE = "CompanyCode";
  static String API_PARAM_PART_NO = "PartNo";
  static String API_PARAM_PART_DESC = "PartDesc";
  static String API_PARAM_STATUS = "status";
  static String API_PARAM_IFACILITY_CODE = "IFacilityCode";
  static String API_PARAM_ROOM_AREA_CODE = "RoomAreaCode";
  static String API_PARAM_L_USER_ID = "LUserId";

  static String API_PARAM_MODE = "Mode";
  static String API_PARAM_CHECK_COUNT_ONLY = "CheckCountOnly";
  static String API_PARAM_MAX = "Max";
  static String API_PARAM_MIN = "Min";
  static String API_PARAM_SEARCH_TERM = "SearchTerm";
  static String API_PARAM_CONDITION = "Condition";
  static String API_PARAM_I_WORK_ORDER_NO = "IWONo";
  static String API_PARAM_USER_ID = "userid";

  static String API_PARAM_I_PART_NO = "iPartNo";
  static String API_PARAM_STOCK_NO = "stockSrNo";
  static String API_PARAM_I_ROOM_CODE = "iRoomCode";
  static String API_PARAM_I_ROOM_AREA_CODE = "iRoomAreaCode";

//  static String API_PARAM_CREATE_USER_ID="CreateUserID";
//  static String API_PARAM_LAST_UPDATE_USER_ID="LastUpdateUserId";
//  static String API_PARAM_SERAIL_NUMBER="SerailNumber";
//  static String API_PARAM_RATE="Rate";
//  static String API_PARAM_IUOM_CODE="IUOMCode";
//  static String API_PARAM_I_BILLING_TYPE_CODE="IBillingTypeCode";
//  static String API_PARAM_DATA_ADDED="DateAdded";
//  static String API_PARAM_I_INV_TYPE_CODE="IInvTypeCode";
//  static String API_PARAM_I_PART_STATUS_CODE="IPartStatusCode";
//  static String API_PARAM_PART_CRITICALITY_CODE="PartCriticalityCode";
//  static String API_PARAM_MANUFACTURED="Manufactured";
//  static String API_PARAM_AUTO_PO="AutoPO";
//  static String API_PARAM_TAX_REQUIRED="Tax_Required";
//  static String API_PARAM_SMART_FIFO="SmartFIFO";
//  static String API_PARAM_VENDOR_LOT_NUMBER="VendorLotNumber";
//  static String API_PARAM_EXPIRATION_DATE="ExpirationDate";
//  static String API_PARAM_NOT_TO_BE_SOLD_TO_CUSTOMER_GROUP="NotToBeSoldToCustomerGroup";

  static String getPartDetails(   Facility facility,  String userId,   String partNo,    String status,
  ) {
//   CompanyCode=1&PartNo=sagar&PartDesc=&status=-1&IFacilityCode=10&RoomAreaCode=&LUserId=sagarshinde63


    // uri = "http://m.im3.com/Web/Service/Mobile.asmx/GetPart";
    //    String  jsonBody =NetworkConfig.getPartDetails(facility,"sagarshinde63",partNo, "-1");



    String param = API_PARAM_COMPANY_CODE +     EQUAL_SYMBOL +        facility.companyCode +        AMPERSAND_SYMBOL +
        API_PARAM_PART_NO +        EQUAL_SYMBOL +        partNo +        AMPERSAND_SYMBOL +
        API_PARAM_PART_DESC +        EQUAL_SYMBOL +        AMPERSAND_SYMBOL +        API_PARAM_STATUS +      EQUAL_SYMBOL +        status +        AMPERSAND_SYMBOL +        API_PARAM_IFACILITY_CODE +        EQUAL_SYMBOL +
        facility.iFacilityCode +        AMPERSAND_SYMBOL +
        API_PARAM_ROOM_AREA_CODE +
        EQUAL_SYMBOL +
        AMPERSAND_SYMBOL +
        API_PARAM_L_USER_ID +
        EQUAL_SYMBOL +
        userId;
    return param;
  }

  static String getDynamicHelp(String companyCode, String mode,
      String checkCountOnly, String min,String max, String searchTerm, String condition, String iWorkOrderNo,
      String userId, String iFacilityCode) {
    //CompanyCode=1&Mode=roomarea&CheckCountOnly=0&Min=1&Max=10&SearchTerm=location_
    // sep&Condition=and CD_FC_Facility.IFacility_Code=10&IWONo=0&userid=sagarshinde63&IFacilityCode=10

    //    uri = "http://m.im3.com/Web/Service/Mobile.asmx/getDynamicHelp";

    /*( PartDetails partDetails ,Facility facility ,
    String checkCountOnly, String status, String min ,String max ,String userId,String searchBin )
    */

    //CompanyCode=1&Mode=roomarea&CheckCountOnly=0&Min=1&Max=10&SearchTerm=location_
    // sep&Condition=and CD_FC_Facility.IFacility_Code=10&IWONo=0&userid=sagarshinde63&IFacilityCode=10


    String param = API_PARAM_COMPANY_CODE +
        EQUAL_SYMBOL +
        companyCode +
        AMPERSAND_SYMBOL +
        API_PARAM_MODE +
        EQUAL_SYMBOL +
        mode +
        AMPERSAND_SYMBOL +
        API_PARAM_CHECK_COUNT_ONLY +
        EQUAL_SYMBOL +
        checkCountOnly +
        AMPERSAND_SYMBOL +
        API_PARAM_MIN +
        EQUAL_SYMBOL +
        min +
        AMPERSAND_SYMBOL +
        API_PARAM_MAX +
        EQUAL_SYMBOL +
        max +
        AMPERSAND_SYMBOL +
        API_PARAM_SEARCH_TERM +
        EQUAL_SYMBOL +
        searchTerm +
        AMPERSAND_SYMBOL +
        API_PARAM_CONDITION +
        EQUAL_SYMBOL +
        condition+
        AMPERSAND_SYMBOL +
        API_PARAM_I_WORK_ORDER_NO +
        EQUAL_SYMBOL +
        iWorkOrderNo +
        AMPERSAND_SYMBOL +
        API_PARAM_USER_ID +
        EQUAL_SYMBOL +
        userId +
        AMPERSAND_SYMBOL +
        API_PARAM_IFACILITY_CODE +
        EQUAL_SYMBOL +
        iFacilityCode;
     return param;
  }

  static String getReservesForPartLocation(RoomFacForLocation roomLocation,
      String companyCode, String iPartNo, String stockSrNo) {


    /* getReservesForPartLocation( PartDetails partDetails ,F acility facility ,
    String iPartNo,   String stockSrNo,String iRoomCode,String iRoomAreaCode,   )
    */
    //"companyCode=1&iPartNo=24922&stockSrNo=0&iFacilityCode=10&iRoomCode=2&iRoomAreaCode=13";
    /*DynamicHelp dynamicHelp ,
        String companyCode, String iPartNo,   String stockSrNo   )
    */
    //String  jsonBody = NetworkConfig. getReservesForPartLocation (dynamicHelp,companyCode ,partDetails.iPartNo,"0");

    //companyCode=1&iPartNo=24922&stockSrNo=0&iFacilityCode=10&iRoomCode=2&iRoomAreaCode=13
    String param = API_PARAM_COMPANY_CODE +
        EQUAL_SYMBOL +
        companyCode +
        AMPERSAND_SYMBOL +
        API_PARAM_I_PART_NO +
        EQUAL_SYMBOL +
        iPartNo +
        AMPERSAND_SYMBOL +
        API_PARAM_STOCK_NO +
        EQUAL_SYMBOL +
        stockSrNo +
        AMPERSAND_SYMBOL +
        API_PARAM_IFACILITY_CODE +
        EQUAL_SYMBOL +
        roomLocation.ifacilityCode+
        AMPERSAND_SYMBOL +
        API_PARAM_I_ROOM_CODE +
        EQUAL_SYMBOL +
        roomLocation.iRoomCode +
        AMPERSAND_SYMBOL +
        API_PARAM_I_ROOM_AREA_CODE +
        EQUAL_SYMBOL +
        roomLocation.iRoomAreaCode;
    return param;
  }

  static String API_PARAM_CREATE_USER_ID = "CreateUserID";
  static String API_PARAM_LAST_UPDATE_USER_ID = "LastUpdateUserId";
  static String API_PARAM_SERAIL_NUMBER = "SerailNumber";
  static String API_PARAM_RATE = "Rate";
  static String API_PARAM_IUOM_CODE = "IUOMCode";
  static String API_PARAM_I_BILLING_TYPE_CODE = "IBillingTypeCode";
  static String API_PARAM_DATA_ADDED = "DateAdded";
  static String API_PARAM_I_INV_TYPE_CODE = "IInvTypeCode";
  static String API_PARAM_I_PART_STATUS_CODE = "IPartStatusCode";
  static String API_PARAM_PART_CRITICALITY_CODE = "PartCriticalityCode";
  static String API_PARAM_MANUFACTURED = "Manufactured";
  static String API_PARAM_AUTO_PO = "AutoPO";
  static String API_PARAM_TAX_REQUIRED = "Tax_Required";
  static String API_PARAM_SMART_FIFO = "SmartFIFO";
  static String API_PARAM_VENDOR_LOT_NUMBER = "VendorLotNumber";
  static String API_PARAM_EXPIRATION_DATE = "ExpirationDate";
  static String API_PARAM_NOT_TO_BE_SOLD_TO_CUSTOMER_GROUP ="NotToBeSoldToCustomerGroup";
  static String API_PARAM_PART_DESCRIPTION = "PartDescription";



  static String getAllCustomerGroupWithSeparator(String companyCode ,String separator){

    String param = API_PARAM_COMPANY_CODE + EQUAL_SYMBOL +companyCode+AMPERSAND_SYMBOL
        + "seperator" + EQUAL_SYMBOL + separator;
    return param;

  }

  static String addPart(String userId, PartDetails partDetails)
  {
/*

  CompanyCode: im3.AdjustInventory.companyCode, 	Int
  CreateUserID: im3.AdjustInventory.userId,	String
  LastUpdateUserId: im3.AdjustInventory.userId,	String
  PartNo: $('#ctl00_ContentPlaceHolder1_txtAddPartNo').val().trim(),	string
  PartDescription: $('#txtPartDescription').val().trim(),	string
  SerailNumber: $('#chkSerializePart').is(':checked'),	string
  Rate: $('#txtPrice').val().trim(),	Decimal
  IUOMCode: $('#ctl00_ContentPlaceHolder1_hdnDefaultIUOMCode').val(),	int
  IBillingTypeCode: $('#ctl00_ContentPlaceHolder1_hdnDefaultIBillingTypeCode').val(),	int
  DateAdded: d.mmddyyyy(),	mmddyyyy()
  IInvTypeCode: 1,	int
  IPartStatusCode: 1,	int
  PartCriticalityCode: 1,	int
  Manufactured: 0,	int
  AutoPO: true,	Boolean
  Tax_Required: $('#ctl00_ContentPlaceHolder1_hdnDefaultTaxType').val(),	int
  SmartFIFO: ((expiryDate == true) ? 4 : 1), // 4-byExpiryDate,1-FIFO	int
  VendorLotNumber: $('#chkVendorLotNo').is(':checked'),	Boolean
  ExpirationDate: expiryDate,	Boolean
  NotToBeSoldToCustomerGroup: im3.AdjustInventory.NotToBeSoldToCustomerGroup	string
*/

  /*
    CompanyCode=&CreateUserID=&LastUpdateUserId=&PartNo=&PartDescription=
    &SerailNumber=&Rate=&IUOMCode=&IBillingTypeCode=&DateAdded=&IInvTypeCode=
    &IPartStatusCode=&PartCriticalityCode=&Manufactured=&AutoPO=&Tax_Required=&SmartFIFO=
    &VendorLotNumber=&ExpirationDate=&NotToBeSoldToCustomerGroup=
*/


  //String partCricaliry ="$partDetails.PartCriticalityCode";

    print("companyCode--"+partDetails.companyCode);
    print("userId  --"+userId);

    print("PartNo--"+partDetails.PartNo);
    print("partDescription--"+partDetails.partDescription);
    print("serialNumber--"+partDetails.isSerailNumber.toString());
    print(partDetails.rate);
    print(partDetails.rate);

    print(""+partDetails.IUOMCode);
    print(partDetails.IBillingTypeCode);
    print("DATE-"+partDetails.DateAdded);


    print("DATE-"+partDetails.DateAdded);
   // partDetails.DateAdded ="02/12/2020";
    print("IPartStatusCode-"+partDetails.IPartStatusCode.toString());


    /*
    partDetails.companyCode  =companyCode;
    partDetails.PartNo =_partNoController.text;
    partDetails.partDescription =_partDescController.text;
    partDetails.DateAdded=DateTimeUtil.getCurrentDate("/");
    partDetails.LastUpdateUserId =userId;
    partDetails.CreateUserID =userId;
    partDetails.Rate =_priceController.text;
    partDetails.IBillingTypeCode =selectedDynamicHelp.IBilling_Type_Code;
    partDetails.IUOMCode =selectedDynamicHelp.IUOM_Code;
    partDetails.iInvTypeCode ="1";
    partDetails.PartCriticalityCode =1;
    partDetails.AutoPO =true;
    partDetails.Tax_Required =1;*/




    Map data = new Map <String, dynamic>();
    data[API_PARAM_COMPANY_CODE] = partDetails.companyCode;
    data[API_PARAM_CREATE_USER_ID] = partDetails.CreateUserID;

    data[API_PARAM_LAST_UPDATE_USER_ID] = partDetails.LastUpdateUserId;
    data[API_PARAM_PART_NO] = partDetails.PartNo;

    data[API_PARAM_PART_DESCRIPTION] = partDetails.partDescription;
    data[API_PARAM_SERAIL_NUMBER] = partDetails.isSerailNumber;

    data[API_PARAM_SERAIL_NUMBER] = partDetails.isSerailNumber;
    data[API_PARAM_RATE] = partDetails.Rate;
    data[API_PARAM_IUOM_CODE] = partDetails.IUOMCode;
    data[API_PARAM_I_BILLING_TYPE_CODE] = partDetails.IBillingTypeCode;
    data[API_PARAM_DATA_ADDED] = partDetails.DateAdded;
    data[API_PARAM_I_INV_TYPE_CODE] = partDetails.iInvTypeCode;
    data[API_PARAM_I_PART_STATUS_CODE] = partDetails.IPartStatusCode;
    data[API_PARAM_PART_CRITICALITY_CODE] = partDetails.PartCriticalityCode;
    data[API_PARAM_MANUFACTURED] = partDetails.manufacturerCode;
    data[API_PARAM_AUTO_PO] = partDetails.AutoPO;
    data[API_PARAM_TAX_REQUIRED] = partDetails.Tax_Required;
    data[API_PARAM_SMART_FIFO] = partDetails.SmartFIFOValue;
    data[API_PARAM_VENDOR_LOT_NUMBER] = partDetails.isVendorLotNumber;
    data[API_PARAM_EXPIRATION_DATE] = partDetails.isExpirationDateExist;
    data[API_PARAM_NOT_TO_BE_SOLD_TO_CUSTOMER_GROUP] = partDetails.notToBeSoldToCustomerGroup;

    Map data2 = new Map <String, dynamic>();
    data2["part"]=data;
    String param  =jsonEncode(data2);
    print("data in json --->"+param);
   /* String param = API_PARAM_COMPANY_CODE +
        EQUAL_SYMBOL +
        partDetails.companyCode +
        AMPERSAND_SYMBOL +
        API_PARAM_CREATE_USER_ID +
        EQUAL_SYMBOL +
        partDetails.CreateUserID +
        AMPERSAND_SYMBOL +
        API_PARAM_LAST_UPDATE_USER_ID +
        EQUAL_SYMBOL +
        partDetails.LastUpdateUserId +
        AMPERSAND_SYMBOL +
        API_PARAM_PART_NO +
        EQUAL_SYMBOL +
        partDetails.PartNo +
        AMPERSAND_SYMBOL +
        API_PARAM_PART_DESCRIPTION +
        EQUAL_SYMBOL +
        partDetails.partDescription +
        AMPERSAND_SYMBOL +
        API_PARAM_SERAIL_NUMBER +
        EQUAL_SYMBOL +
        partDetails.isSerailNumber.toString() +
        AMPERSAND_SYMBOL +
        API_PARAM_RATE +
        EQUAL_SYMBOL +
        partDetails.Rate.toString() +
        AMPERSAND_SYMBOL +


        API_PARAM_IUOM_CODE +
        EQUAL_SYMBOL +
        partDetails.IUOMCode +
        AMPERSAND_SYMBOL +
        API_PARAM_I_BILLING_TYPE_CODE +
        EQUAL_SYMBOL +
        partDetails.IBillingTypeCode.toString() +
        AMPERSAND_SYMBOL +

        API_PARAM_DATA_ADDED +
        EQUAL_SYMBOL +
        partDetails.DateAdded +

        AMPERSAND_SYMBOL +

        API_PARAM_I_INV_TYPE_CODE +
        EQUAL_SYMBOL +
        partDetails.iInvTypeCode +

        AMPERSAND_SYMBOL +

        API_PARAM_I_PART_STATUS_CODE +
        EQUAL_SYMBOL +
        partDetails.IPartStatusCode.toString() +

        AMPERSAND_SYMBOL +

            API_PARAM_PART_CRITICALITY_CODE +
        EQUAL_SYMBOL +
        partDetails.PartCriticalityCode.toString()+


        AMPERSAND_SYMBOL +

        API_PARAM_MANUFACTURED +
        EQUAL_SYMBOL +
        partDetails.manufacturerCode.toString() +

        AMPERSAND_SYMBOL +

        API_PARAM_AUTO_PO +
        EQUAL_SYMBOL +
        partDetails.AutoPO.toString()+

        AMPERSAND_SYMBOL +
        API_PARAM_TAX_REQUIRED +
        EQUAL_SYMBOL +
        partDetails.Tax_Required.toString() +
        AMPERSAND_SYMBOL +
            API_PARAM_SMART_FIFO +
        EQUAL_SYMBOL +
        partDetails.SmartFIFOValue.toString() +
        AMPERSAND_SYMBOL +
        API_PARAM_VENDOR_LOT_NUMBER +
        EQUAL_SYMBOL +
        partDetails.isVendorLotNumber.toString() +
        AMPERSAND_SYMBOL +
        API_PARAM_EXPIRATION_DATE +
        EQUAL_SYMBOL +
        partDetails.isExpirationDateExist.toString() +
        AMPERSAND_SYMBOL +
        API_PARAM_NOT_TO_BE_SOLD_TO_CUSTOMER_GROUP +
        EQUAL_SYMBOL +partDetails.notToBeSoldToCustomerGroup

        ;
*/
    return param;
  }



 // static String
  static String addCycleCount(String userId, PartDetails partDetails, RoomFacForLocation roomFacForLocation,
      String employeeId,String dateFormat )
  {
/*

        Addcycleccount(ByVal pintCompanyCode As Integer,
        ByVal pstrUserID As String,
        ByVal pstrAddPartNo As String,
        ByVal pstrAddLocation As String,
        ByVal pintAddPhysicalCount As Integer,
        ByVal pintOnHandQty As Double,
        ByVal pstrRoom As String,
        ByVal pstrRate As String,
        ByVal pintIUOMCode As Integer,
        ByVal pintIBillTypeCode As Integer,
        ByVal pstrPartDesc As String,
        ByVal PboolIsSerialNumberMandatory As Boolean,
        ByVal pstrSerialNumber As String)
        ByVal pstritemCost As String,
        ByVal pstrcaseSize As String,
        ByVal pstrinvActionCode As Integer,
        ByVal pstrVendorLotNo As String,
        ByVal pstrExpirationDate As String


*/

    String param = "pintCompanyCode" +    EQUAL_SYMBOL +   partDetails.companyCode.trim() +    AMPERSAND_SYMBOL +
        "pstrUserID" +   EQUAL_SYMBOL +  userId.trim() +     AMPERSAND_SYMBOL +
        "pstrAddPartNo" +   EQUAL_SYMBOL +   partDetails.PartNo.trim() +  AMPERSAND_SYMBOL +
        "pstrAddLocation" +    EQUAL_SYMBOL +   partDetails.binLocation.trim() +     AMPERSAND_SYMBOL +
        "pintAddPhysicalCount" +     EQUAL_SYMBOL +  partDetails.physicalCount.toString().trim()+    AMPERSAND_SYMBOL +
        "pintOnHandQty" +   EQUAL_SYMBOL +   partDetails.onHandQuantity.toString().trim()+   AMPERSAND_SYMBOL +
        "pstrRoom" +   EQUAL_SYMBOL + roomFacForLocation.iLocationRoomFacility.toString()+  AMPERSAND_SYMBOL +
        "pstrRate" +   EQUAL_SYMBOL +  partDetails.Rate.trim() +  AMPERSAND_SYMBOL +
        "pintIUOMCode" +  EQUAL_SYMBOL +  partDetails.IUOMCode.trim() +   AMPERSAND_SYMBOL +
        "pintIBillTypeCode" +  EQUAL_SYMBOL +    partDetails.IBillingTypeCode.toString().trim() + AMPERSAND_SYMBOL +
        "pstrPartDesc" +  EQUAL_SYMBOL +  partDetails.partDescription.trim() +    AMPERSAND_SYMBOL +
        "PboolIsSerialNumberMandatory" +   EQUAL_SYMBOL +        partDetails.isSerailNumber.toString().trim()+        AMPERSAND_SYMBOL +
        "pstrSerialNumber" +        EQUAL_SYMBOL +        partDetails.serialNumber.toString().trim()+        AMPERSAND_SYMBOL +
        "pstritemCost" +        EQUAL_SYMBOL +        partDetails.costPerUnit.toString().trim()+        AMPERSAND_SYMBOL +
        "pstrcaseSize" +        EQUAL_SYMBOL +        partDetails.caseSize.toString().trim()+        AMPERSAND_SYMBOL +
        "pstrinvActionCode" +        EQUAL_SYMBOL +        partDetails.actionCode.toString().trim()+        AMPERSAND_SYMBOL +
        "pstrVendorLotNo" +        EQUAL_SYMBOL +        partDetails.vendorLotNumber.toString().trim()+        AMPERSAND_SYMBOL +
        "pstrExpirationDate" +        EQUAL_SYMBOL +        partDetails.expirationDate.toString().trim()  +AMPERSAND_SYMBOL +
        "IPartNo" +        EQUAL_SYMBOL +        partDetails.iPartNo.toString().trim()+        AMPERSAND_SYMBOL +
        "EmployeeID" +        EQUAL_SYMBOL +        employeeId.trim()+        AMPERSAND_SYMBOL +
        "UOMCode" +        EQUAL_SYMBOL +        partDetails.uOMCode.toString().trim()  +AMPERSAND_SYMBOL +
        "DateFormat" +        EQUAL_SYMBOL +        dateFormat.toString().trim();


    return param;
  }

  static String getRoomFacForLocation(String companyCode, String userId,
      String roomAreaCode,String ifacilitycode) {
    String param = "CompanyCode" +
        EQUAL_SYMBOL +
        companyCode.trim() +
        AMPERSAND_SYMBOL +
        "pstrRoomAreaCode" +
        EQUAL_SYMBOL +
        roomAreaCode.trim() +
        AMPERSAND_SYMBOL +
        "strUserID" +
        EQUAL_SYMBOL +
        userId +
        AMPERSAND_SYMBOL +
        "ifacilitycode" +
        EQUAL_SYMBOL + ifacilitycode;
    return param;
  }








  static String  chkDuplicateSerialNo(String companyCode ,
      String iPartNo ,
  String serialNumber ,
      String controlId ,
  String actionCode ) {

    /*ByVal iPartNo As Integer,
        ByVal serialNumber As String,
    ByVal controlId As String,
    ByVal actionCode As Integer
    */
       String param = "companyCode" +       EQUAL_SYMBOL +    companyCode.trim() +    AMPERSAND_SYMBOL +
        "iPartNo" +     EQUAL_SYMBOL +    iPartNo.trim() +   AMPERSAND_SYMBOL +
        "serialNumber" +     EQUAL_SYMBOL +   serialNumber.trim() +   AMPERSAND_SYMBOL +
        "controlId" +    EQUAL_SYMBOL + controlId.trim() +AMPERSAND_SYMBOL +
       "actionCode" +     EQUAL_SYMBOL +   actionCode.trim();
    return param;

  }



  static String checkSerialNo(String companyCode, String iPartNo, String serialNo,
      String actionCode,String actionSrNo,
      String stockSrNo,String quantity,String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId) {


/*  checkSerialNo(ByVal companyCode As Integer,
      ByVal iPartNo As Integer,
  ByVal serialNo As String,
      ByVal actionCode As Integer,
  ByVal actionSrNo As String,
      ByVal stockSrNo As String,
  ByVal quantity As Decimal,
      ByVal iWoNo As Integer,
  ByVal iSalesOrderNo As Integer,
      ByVal lineItemSrNo As Integer,
  ByVal controlId As String) As String*/

    String param = "companyCode" +
        EQUAL_SYMBOL +
        companyCode.trim() +
        AMPERSAND_SYMBOL +
        "iPartNo" +
        EQUAL_SYMBOL +
        iPartNo.trim() +
        AMPERSAND_SYMBOL +
        "serialNo" +
        EQUAL_SYMBOL +
        serialNo.trim() +

        AMPERSAND_SYMBOL +
        "actionCode" +
        EQUAL_SYMBOL +
        actionCode.trim() +
        AMPERSAND_SYMBOL +
        "actionSrNo" +
        EQUAL_SYMBOL +
        actionSrNo.trim() +
        AMPERSAND_SYMBOL +
        "stockSrNo" +
        EQUAL_SYMBOL + stockSrNo.trim()+
        AMPERSAND_SYMBOL +
        "quantity" +
        EQUAL_SYMBOL + quantity.trim()+
        AMPERSAND_SYMBOL +
        "iWoNo" +
        EQUAL_SYMBOL + iWoNo.trim()+
        AMPERSAND_SYMBOL +
        "iSalesOrderNo" +
        EQUAL_SYMBOL + iSalesOrderNo.trim()+
        AMPERSAND_SYMBOL +
        "lineItemSrNo" +
        EQUAL_SYMBOL + lineItemSrNo.trim()+
        AMPERSAND_SYMBOL + "controlId" +
         EQUAL_SYMBOL + controlId.trim();
        return param;
  }






  static String  chkExistsSerialNoRoomLevel(String companyCode,
      String actionCode,
      String controlId,
      String iPartNo ,
      String serialNo ,
      String lineItemSrNo,
      String iWoNo,
      String iSalesOrderNo,
      String iRoomCode,
      String iFacilityCode ,
      )


        {
//
//    {companyCode: "107", iPartNo: "80880", serialNo: "SRK0033,SRK0034", iFacilityCode: "27",…}
//    actionCode: 1
//    companyCode: "107"
//    controlId: "txtSerialNo_1,txtSerialNo_2"
//    iFacilityCode: "27"
//    iPartNo: "80880"
//    iRoomCode: "1"
//    iSalesOrderNo: "44558"
//    iWoNo: "0"
//    lineItemSrNo: "87347"
//    serialNo: "SRK0033,SRK0034"
//
    /*ByVal iPartNo As Integer,
        ByVal serialNumber As String,
    ByVal controlId As String,
    ByVal actionCode As Integer
    */



    Map data = new Map <String, dynamic>();
    data["companyCode"] = companyCode;
    data["actionCode"] = actionCode;
    data["iPartNo"] = iPartNo;
    data["controlId"] = controlId;
    data["iWoNo"] = iWoNo;
    data["iSalesOrderNo"] = iSalesOrderNo;
    data["lineItemSrNo"] = lineItemSrNo;
    data["iFacilityCode"] = iFacilityCode;
    data["iRoomCode"] = iRoomCode;
    data["serialNo"] = serialNo;
    String param  =jsonEncode(data);

    return param;

  }

}
