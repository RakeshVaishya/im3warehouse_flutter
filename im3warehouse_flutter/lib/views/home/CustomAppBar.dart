import 'package:flutter/material.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/CustomDrawer.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/landing_page/landing_page.dart';
import 'package:im3_warehouse/views/receive_by_po/receive_by_po.dart';
import 'package:im3_warehouse/views/receivebypart/receivebypart.dart';
import 'package:im3_warehouse/views/reversereceive/reverse_receive.dart';

class CustomAppBar extends AppBar implements ViewTypeCallBack {

  CustomAppBar({
    Key key,
    BuildContext context,
    Widget title,
    CustomDrawer customDrawer,
    bool isHomeIconVisible
  }) : super(
          key: key,
          title: title,
          titleSpacing: 0.0,
          backgroundColor: AppColors.CYAN_BLUE_COLOR,
          elevation: 5.0,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.all(10.0),
              child: Center(
              child:Visibility(
                visible: isHomeIconVisible ,
                child: new InkWell(
                  // this is the one you are looking for..........
                  child: new Container(
                    padding: const EdgeInsets.all(
                        05.0), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child:
                    Image(image: AssetImage("images/imlogo.png")),
                  ),
                  onTap: () {
                    print("im3 logo clicked..");
                    Text txt = title;
                    String strTitle = txt.data;
                    String homeTitle = AppStrings.LABEL_HOME;
                    if (AppStrings.newlyAddedPartNo.length > 0)
                    {
                      AppStrings.newlyAddedPartNo ="";
                    }
                    if(ReceiveByPo.poList !=null)
                    {
                      ReceiveByPo.poList =null;
                    }
                    if(ReceiveByPart.partList !=null)
                    {
                      ReceiveByPart.partList =null;
                    }
                    if(ReverseReceive.poList !=null)
                    {
                      ReverseReceive.poList =null;
                    }
                     if (strTitle.toLowerCase() != homeTitle.toLowerCase())
                    {
                      print("other than home class");
                      customDrawer.callback
                          .viewType(DashboardView(customDrawer.callback), AppStrings.LABEL_HOME);
                    } else {
                      print("Home class");
                    }
                  },
                ),
                )
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.all(13.0),
            //   child: Center(
            //     child: new InkWell(
            //       // this is the one you are looking for..........
            //       child: new Container(
            //         padding: const EdgeInsets.all(
            //             05.0), //I used some padding without fixed width and height
            //         decoration: new BoxDecoration(
            //           shape: BoxShape
            //               .circle, // You can use like this way or like the below line
            //           //borderRadius: new BorderRadius.circular(30.0),
            //           color: Colors.white,
            //         ),
            //         child: Image(image: AssetImage("images/hotelnew.png")),
            //       ),
            //       onTap: () {},
            //     ),
            //   ),
            // ),
            InkWell(
              child: Image(
                image: AssetImage("images/signout.png"),
                color: Colors.white,
              ),
              onTap: () {
                CustomDrawer(new ViewTypeCallBack());
                print("logout click..");
                showDialog(
                    context: context,
                    builder: (context) => logoutDialog(context));
                },
            ),
          ],
        );



 static  Widget logoutDialog(BuildContext context) {
    return AlertDialog(
      title: Text("Do you really want to Logout?"),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text("No"),
        ),
        FlatButton(
          onPressed: () {
            SharedPreferencesHelper.getSharedPreferencesHelperInstance().clearPref().then((value){
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => LandingPage()),
                    (Route<dynamic> route) => false,
              );
            });
          },
          child: Text("Yes"),
        ),
      ],
    );
  }

  @override
  void viewType(Widget widget, String viewType) {


  }
}
