import 'package:floor/floor.dart';
import 'package:flutter/material.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/facility/facility_dialog.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/issue_by_pick_list/issue_by_pick_list.dart';
import 'package:im3_warehouse/views/issue_by_wo_or_task/issue_by_wo_or_task.dart';
import 'package:im3_warehouse/views/issue_by_work_order/issue_by_work_order.dart';
import 'package:im3_warehouse/views/issue_for_equipement/issue_for_equipement.dart';
import 'package:im3_warehouse/views/issue_to_employee/issue_to_employee.dart';
import 'package:im3_warehouse/views/landing_page/landing_page.dart';
import 'package:im3_warehouse/views/login/login_main.dart';
import 'package:im3_warehouse/views/physical_count/physicalCount.dart';
import 'package:im3_warehouse/views/pick_my_order/pick_my_order.dart';
import 'package:im3_warehouse/views/putaway/putaway_items.dart';
import 'package:im3_warehouse/views/putback/putback.dart';
import 'package:im3_warehouse/views/receive_by_po/receive_by_po.dart';
import 'package:im3_warehouse/views/receivebypart/receivebypart.dart';
import 'package:im3_warehouse/views/reversereceive/reverse_receive.dart';
import 'package:im3_warehouse/views/transfer/tranfer.dart';
import 'package:im3_warehouse/views/transfer_pallet/tranfer_pallet.dart';
import 'package:im3_warehouse/views/view_inventory/view_inventory.dart';

class CustomDrawer extends StatefulWidget {

  ViewTypeCallBack callback;
  CustomDrawer(this.callback);
  @override
  CustomDrawerState createState() => CustomDrawerState();
}

class CustomDrawerState extends State<CustomDrawer> {
  String titleName = '';
  String userEmailId = '';
  String settingsTitle = '';
  String abouttitle = "About";
  String logoutTitle = "Logout";

  @override
  Widget build(BuildContext context)
  {

    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: UserAccountsDrawerHeader(
                accountName:
                    Utility.getText(titleName, AppStrings.FONT_SIZE_15),
                accountEmail:
                    Utility.getText(userEmailId, AppStrings.FONT_SIZE_15),
                currentAccountPicture: GestureDetector(
                    child: CircleAvatar(
                        backgroundColor: Colors.grey,
                        child: Icon(Icons.person, color: Colors.white))),
                decoration: BoxDecoration(color: AppColors.CYAN_BLUE_COLOR)),
          ),
          Expanded(
            flex: 8,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.person, color: Colors.redAccent),
                      title: Utility.getText(
                          AppStrings.LABEL_HOME, AppStrings.FONT_SIZE_15),
                      onTap: () {
                        Navigator.pop(context);
                        this.widget.callback.viewType(
                            DashboardView(this.widget.callback),
                            AppStrings.LABEL_HOME);
                      }),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/hotelnew.png"),
                      height: 20,
                      width: 20,
                      color: Colors.teal,
                    ),
                    title: Utility.getText(
                        AppStrings.LABEL_FACILITY, AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.of(context).pop();
                      Future.delayed(const Duration(milliseconds: 5), () {
                        setState(() {
                          showDialog(
                              context: context,
                              builder: (_) => FacilityDialog(viewTypeCallBack:this.widget.callback
                              ));
                        });
                      });
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/cloud_computing.png"),
                      height: 20,
                      width: 20,
                      color: Colors.purple,
                    ),
                    title: Utility.getText(AppStrings.LABEL_PHYSICAL_COUNT,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          PhysicalCountApp(this.widget.callback), AppStrings.LABEL_PHYSICAL_COUNT);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/buildings.png"),
                      height: 25,
                      width: 25,
                      color: Colors.teal,
                    ),
                    title: Utility.getText(
                        AppStrings.LABEL_TRANSFER, AppStrings.FONT_SIZE_15),
                    onTap: () {
                      print("transfer clicked..");
                      Navigator.pop(context);
                      this
                          .widget
                          .callback
                          .viewType(Transfer(this.widget.callback), AppStrings.LABEL_TRANSFER);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/settingsnote.png"),
                      height: 20,
                      width: 20,
                      color: Colors.amberAccent,
                    ),
                    title: Utility.getText(AppStrings.LABEL_ISSUE_BY_PICKLIST,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          IssueByPickListPage(this.widget.callback),
                          AppStrings.LABEL_ISSUE_BY_PICKLIST);
                    },
                  ),

                  ListTile(
                    leading: Image(
                      image: AssetImage("images/settingsnote.png"),
                      height: 20,
                      width: 20,
                      color: Colors.amberAccent,
                    ),
                    title: Utility.getText(AppStrings.LABEL_PICK_MY_ORDER,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          PickMyOrder(this.widget.callback),
                          AppStrings.LABEL_PICK_MY_ORDER);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/settingsnote.png"),
                      height: 20,
                      width: 20,
                      color: Colors.brown,
                    ),
                    title: Utility.getText(AppStrings.LABEL_ISSUE_BY_WORK_ORDER,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      print("issu by work order clicked");
                      Navigator.pop(context);
                      this.widget.callback.viewType(IssueByWorkOrder(this.widget.callback),
                          AppStrings.LABEL_ISSUE_BY_WORK_ORDER);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/settingsnote.png"),
                      height: 20,
                      width: 20,
                      color: Colors.grey,
                    ),
                    title: Utility.getText(AppStrings.LABEL_ISSUE_BY_WO_OR_TASK,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      print("issue by wo / task clicked..");
                      Navigator.pop(context);
                      this.widget.callback.viewType(IssueByWoOrTask(this.widget.callback),
                          AppStrings.LABEL_ISSUE_BY_WO_OR_TASK);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/settingsnote.png"),
                      height: 20,
                      width: 20,
                      color: Colors.green,
                    ),
                    title: Utility.getText(
                        AppStrings.LABEL_ISSUE_FOR_EQUIPEMENT,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(IssueForEquipment(this.widget.callback),
                          AppStrings.LABEL_ISSUE_FOR_EQUIPEMENT);

                      print("issue Equipement clicked..");
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/network.png"),
                      height: 20,
                      width: 20,
                      color: Colors.cyan,
                    ),
                    title: Utility.getText(
                        AppStrings.LABEL_PUTBACK, AppStrings.FONT_SIZE_15),
                    onTap: () {
                      print("Putback clicked..");
                      Navigator.pop(context);
                      this
                          .widget
                          .callback
                          .viewType( PutBack(this.widget.callback), AppStrings.LABEL_PUTBACK);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/document.png"),
                      height: 20,
                      width: 20,
                      color: Colors.red,
                    ),
                    title: Utility.getText(AppStrings.LABEL_RECEIVE_BY_PO,
                        AppStrings.FONT_SIZE_15),
                    onTap: ()
                    {
                      if(ReceiveByPo.poList !=null)
                      {
                        ReceiveByPo.poList =null;
                      }
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          ReceiveByPo(callBack:this.widget.callback),
                          AppStrings.LABEL_RECEIVE_BY_PO);
                      print("Receiv by PO clicked..");
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/document.png"),
                      height: 20,
                      width: 20,
                      color: Colors.orange,
                    ),
                    title: Utility.getText(AppStrings.LABEL_RECEIVE_BY_PART,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      print("Receive by part clicked....");
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          ReceiveByPart(callBack:this.widget.callback), AppStrings.LABEL_RECEIVE_BY_PART);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/transfer.png"),
                      height: 20,
                      width: 20,
                      color: Colors.blue,
                    ),
                    title: Utility.getText(AppStrings.LABEL_REVERSE_RECEIVE,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      print("Reverse receive clicked..");
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          ReverseReceive(callBack:this.widget.callback), AppStrings.LABEL_REVERSE_RECEIVE);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/network.png"),
                      height: 20,
                      width: 20,
                      color: Colors.indigo,
                    ),
                    title: Utility.getText(AppStrings.LABEL_PUTAWAY_ITEMS,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          PutAwayItems(this.widget.callback),
                          AppStrings.LABEL_PUTAWAY_ITEMS);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage("images/settingsnote.png"),
                      height: 20,
                      width: 20,
                      color: AppColors.CYAN_BLUE_COLOR,

                    ),
                    title: Utility.getText(AppStrings.LABEL_ISSUE_TO_EMPLOYEE,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          IssueToEmployee(this.widget.callback),
                          AppStrings.LABEL_ISSUE_TO_EMPLOYEE);
                    },
                  ),
                  ListTile(
                    leading: Image(
                      image: AssetImage( AppStrings.IMAGE_TRANSFER,
                      ),
                      height: 20,
                      width: 20,
                      color: Colors.indigo,
                    ),
                    title: Utility.getText(AppStrings.LABEL_TRANSFER_PALLET,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          TransferPallet(this.widget.callback),
                          AppStrings.LABEL_TRANSFER_PALLET);
                    },
                  ),

                  ListTile(
                    leading: Image(
                      image: AssetImage("images/settingsnote.png"),
                      height: 20,
                      width: 20,
                      color: Colors.green,
                    ),
                    title: Utility.getText(AppStrings.LABEL_VIEW_INVENTORY,
                        AppStrings.FONT_SIZE_15),
                    onTap: () {
                      Navigator.pop(context);
                      this.widget.callback.viewType(
                          ViewInventory(callBack:this.widget.callback),
                          AppStrings.LABEL_VIEW_INVENTORY);
                    },
                  ),
                  Divider(color: Colors.red, indent: 20.0),
                  ListTile(
                    leading: Icon(Icons.settings, color: Colors.blue),
                    title:
                        Utility.getText(settingsTitle, AppStrings.FONT_SIZE_15),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Icon(Icons.help, color: Colors.green),
                    title: Utility.getText(abouttitle, AppStrings.FONT_SIZE_15),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: Image(
                        image: AssetImage("images/signout.png"),
                        height: 20,
                        width: 20),
                    title:
                        Utility.getText(logoutTitle, AppStrings.FONT_SIZE_15),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) => logoutDialog(context));
                      },
                  ),

                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  static Widget logoutDialog(BuildContext context) {
    return AlertDialog(
      title: Text("Do you really want to Logout?"),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text("No"),
        ),
        FlatButton(
          onPressed: () {
            SharedPreferencesHelper.getSharedPreferencesHelperInstance().clearPref().then((value){
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => LandingPage()),
                    (Route<dynamic> route) => false,
              );
            });
          },
          child: Text("Yes"),
        ),
      ],
    );
  }


}
