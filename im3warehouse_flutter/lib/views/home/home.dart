import 'dart:convert';

import 'package:http/http.dart';
import 'package:im3_warehouse/databases/database_constant.dart';
import 'package:im3_warehouse/databases/database_helper.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/CustomAppBar.dart';
import 'package:im3_warehouse/views/home/CustomDrawer.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/landing_page/landing_page.dart';
import 'package:flutter/material.dart';
import 'package:im3_warehouse/views/physical_count/physicalCount.dart';
import 'package:im3_warehouse/views/receive_by_po/receive_by_po.dart';
import 'package:im3_warehouse/views/receivebypart/receivebypart.dart';
import 'package:im3_warehouse/views/reversereceive/reverse_receive.dart';
import 'package:xml/xml.dart' as xml;

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Home(),
    );
  }
}

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> implements ViewTypeCallBack {
  // implements DialogCallback
  List<Facility> facilityListDemo = [];

  var db;
  int selectedDrawerIndex = 0;
  bool isIcon = true;
  bool iconActive = false;
  int _page = 0;
  TextEditingController controller = new TextEditingController();
  String data;
  Widget appBarTitle = new Text(AppStrings.LABEL_HOME.toUpperCase(),
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontFamily: AppStrings.SEGOEUI_FONT,
      ));

  // Widget appBarTitle = new Padding(
  //   padding: EdgeInsets.all(10.0),
  //   child: new Container(
  //     child: Row(
  //       children: <Widget>[
  //         Padding(
  //           padding: EdgeInsets.all(0.0),
  //           child: new Container(
  //             decoration: new BoxDecoration(
  //               shape: BoxShape.circle,
  //               color: Colors.white,
  //             ),
  //             child: Image(
  //                 height: 10,
  //                 width: 10,
  //                 image: AssetImage(
  //                   "images/imlogo.png",
  //                 )),
  //           ),
  //         ),
  //         Text(AppStrings.LABEL_HOME.toUpperCase())
  //       ],
  //     ),
  //   ),
  // );

  Icon actionIcon = new Icon(Icons.search);
  Facility newDefaultValue;
  Facility facilityValue;
  List<Facility> faciltiyList;
  String strHomefacilityCode = "";
  String loginFacilityCode = "";
  String homeDefaultValue = "";
  String viewTypehome = "";
  Widget view;

  List<dynamic> facilityTableData;
  List<Facility> facilityListTable;
  CustomDrawer customDrawer;
  DashboardView dashboardView;

  @override
  void initState()
  {
    db = new DatabaseHelper();
    // facilityListDemo = widget.facilityListData;
    // getFaciltyData();
    viewTypehome = AppStrings.LABEL_HOME;
    // view = PhysicalCountApp();
    view = DashboardView(this);
    customDrawer = CustomDrawer(this);
    dashboardView = DashboardView(this);
    getData();
    getFacility();
  }

  Future<void> getFaciltyData() async {
    final uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.GET_FACILITY_LIST_FOR_USER;
    final headers = {'Content-Type': 'application/x-www-form-urlencoded'};
    String strcompnayCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
    String userId =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.USER_ID);

    String jsonBody = "pintCompanyCode=" + strcompnayCode + "&UserId=" + userId;
    print(jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);

    //<--------------------------------------------------------------->
    // String responseBody = TestData.getFacilityData();
    // print("Facility response---------->" + responseBody);

    // xml.XmlDocument parsedXml = xml.parse(responseBody);
    // print(parsedXml);
    // responseBody = parsedXml.findElements("string").first.text;

    // Map data1 = jsonDecode(responseBody);

    // Map data2 = data1['NewDataSet'];

    // Facility fac;
    // facilityTableData = data2['Table'];

    // if (data2['Table'] != null) {
    //   faciltiyList = new List<Facility>();
    //   data2['Table'].forEach((v) {
    //     faciltiyList.add(new Facility.fromJson(v));
    //     // fac = Facility.fromJson(v);
    //     // print("Data inserting into the table----->");
    //     // db.createFacilty(DataBaseConstant.TABLE_FACILITY, fac);
    //   });
    // }

    // db.createFacilty(DataBaseConstant.TABLE_FACILITY, fac);
    // db.insertData(DataBaseConstant.TABLE_FACILITY, faciltiyList);

    // Future<List<dynamic>> data = db.getFacilityData();
    // if (data != null) {
    //   print("data exist in the Facility table");
    // } else {
    //   print("data--------->inserting into the facility table");
    //   db.insertFacilityData(DataBaseConstant.TABLE_FACILITY, facilityTableData);
    // }

// <-------------------------------------------------------------------->

    Response response = await post(
      uri,
      headers: headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    if (statusCode == 200) {
      String responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      Map data1 = jsonDecode(responseBody);
      Map data2 = data1['NewDataSet'];
      facilityTableData = data2['Table'];
      if (data2['Table'] != null)
      {
        faciltiyList = new List<Facility>();
        data2['Table'].forEach((v) {
          faciltiyList.add(new Facility.fromJson(v));
        });
      }
      print("facility size----->");
      print(faciltiyList.length);
      // List<Facility> data = db.getFacilityData();
      // print("daTA INT TABLE-----");
      // print(data);
      // if (data != null) {
      //   print("data exist in the Facility table");
      //   db.getFacilityData();
      // } else {
      //   print("data--------->inserting into the facility table");
      //   db.insertFacilityData(
      //       DataBaseConstant.TABLE_FACILITY, facilityTableData);
      // }

      // db.getFacilityData();
      facilityListTable = new List();
      Future<List<Facility>> facLi = db.getFacilityData();
      facLi.then((facListData)
      {
        this.facilityListTable = facListData;
        print("table fac lenght---->");
        print(facilityListTable.length);

        if (facilityListTable.length > 0)
        {
          print("data exist in the Facility table");
          for (int i = 0; i < facilityListTable.length; i++) {
            Facility facd = facilityListTable.elementAt(i);
            print(facd.facilityCode);
          }
        } else {
          print("data--------->inserting into the facility table");
          db.insertFacilityData(
              DataBaseConstant.TABLE_FACILITY, facilityTableData);
        }
      });

      // db.insertFacilityData(DataBaseConstant.TABLE_FACILITY, facilityTableData);
      // db.getFacilityData();

      SharedPreferencesHelper.getSharedPreferencesHelperInstance()
          .getStringValuesSF(JsonUtil.DEFAULT_FACILITY_CODE_VALUE)
          .then((code) {
        print("defaultFacilityCode---->" + code);
        for (int i = 0; i < faciltiyList.length; i++) {
          if (identical(code.toString(),
              faciltiyList.elementAt(i).facilityCode.toString())) {
            Facility facility = faciltiyList.elementAt(i);
            print("JsonValue----------->" + jsonEncode(facility.toJson()));

            AppStrings.facility =facility;
            SharedPreferencesHelper.getSharedPreferencesHelperInstance()
                .addStringToSF(
                    JsonUtil.FACILITY_DATA, jsonEncode(facility.toJson()));
          }
        }
      });
    } else {}
  }

  Future<void> getFacility() async {
    String data =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.FACILITY_DATA);
       if(data !=null) {
      print("json data" + data);

      Map<String, dynamic> map = jsonDecode(data);
      Facility facility = Facility.fromJson(map);
      AppStrings.facility = facility;
      print("retrived from SH file--->" + facility.iFacilityCode);
    }
    else{

    }
  }
  void Tappbar(int index) {
    setState(() {
      _page = index;
    });
  }

  Future<bool> _onWillPop()
  {
    isHomeIconVisible =false;
    print("Back press called---home class "+viewTypehome);
    print("Back press called---home class "+viewTypehome);

    if (viewTypehome.contains(AppStrings.LABEL_HOME))
    {
      print("Back press called---home class --- dashboard ");
      return showDialog(
            context: context,
            builder: (context) => new AlertDialog(
              title: new Text('Are you sure?'),
              content: new Text('Do you want to exit an from app ?'),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: new Text('No'),
                ),
                new FlatButton(
                  onPressed: () {
                    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
                        .clearPref()
                        .then((value) {
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => LandingPage()),
                        (Route<dynamic> route) => false,
                      );
                    });
                  },
                  child: new Text('Yes'),
                ),
              ],
            ),
          ) ??
          false;
    } else if (viewTypehome.contains(AppStrings.LABEL_ADD_PART))
    {
      setState(() {
        view = PhysicalCountApp(this);
        appBarTitle = Utility.getText(
            AppStrings.LABEL_PHYSICAL_COUNT.toUpperCase(),
            AppStrings.FONT_SIZE_18);
        viewTypehome = AppStrings.LABEL_PHYSICAL_COUNT;
      });
    }

    else if (viewTypehome ==AppStrings.LABEL_RECEIVE_BY_PO)
    {
      if(ReceiveByPo.poList !=null)
      {
        ReceiveByPo.poList =null;
      }
      setState(() {
        view = DashboardView(this);
        appBarTitle = Utility.getText(
            AppStrings.LABEL_HOME.toUpperCase(), AppStrings.FONT_SIZE_18);
        viewTypehome = AppStrings.LABEL_HOME;
      });
    }
    else if (viewTypehome ==AppStrings.LABEL_RECEIVE_BY_PO_ACTIVITY)
    {
      setState(() {
        isHomeIconVisible =true;
        view = ReceiveByPo(callBack:this);
        appBarTitle = Utility.getText(
            AppStrings.LABEL_RECEIVE_BY_PO.toUpperCase(), AppStrings.FONT_SIZE_18);
        viewTypehome = AppStrings.LABEL_RECEIVE_BY_PO;
      });
    }

    else if (viewTypehome ==AppStrings.LABEL_RECEIVE_BY_PART)
     {
       if(ReceiveByPart.partList !=null)
       {
         ReceiveByPart.partList =null;
       }
       setState(() {
        isHomeIconVisible =true;
        setState(() {
          view = DashboardView(this);
          appBarTitle = Utility.getText(
              AppStrings.LABEL_HOME.toUpperCase(), AppStrings.FONT_SIZE_18);
          viewTypehome = AppStrings.LABEL_HOME;
        });
      });
    }

    else if (viewTypehome ==AppStrings.LABEL_RECEIVE_BY_PART_ACTIVITY)
    {
      print("Show Screen "+AppStrings.LABEL_RECEIVE_BY_PART_ACTIVITY);
      setState(() {
        isHomeIconVisible =true;
        view = ReceiveByPart(callBack:this);
        appBarTitle = Utility.getText(
            AppStrings.LABEL_RECEIVE_BY_PART.toUpperCase(),
            AppStrings.FONT_SIZE_18);
        viewTypehome = AppStrings.LABEL_RECEIVE_BY_PART;
      });
    }
    else if (viewTypehome
         ==AppStrings.LABEL_REVERSE_RECEIVE_ACTIVITY) {
      setState(() {
        view = ReverseReceive(callBack:this);
        appBarTitle = Utility.getText(
            AppStrings.LABEL_REVERSE_RECEIVE.toUpperCase(),
            AppStrings.FONT_SIZE_18);
        viewTypehome = AppStrings.LABEL_HOME;
      });
    }
    else if (viewTypehome.contains(AppStrings.LABEL_REVERSE_RECEIVE))
    {
        setState(() {
        view = DashboardView(this);
        appBarTitle = Utility.getText(
            AppStrings.LABEL_HOME.toUpperCase(), AppStrings.FONT_SIZE_18);
        viewTypehome = AppStrings.LABEL_HOME;
      });
    }

    else {
      print("other view type called from home class---->");
      setState(() {
        view = DashboardView(this);
        appBarTitle = Utility.getText(
            AppStrings.LABEL_HOME.toUpperCase(), AppStrings.FONT_SIZE_18);
        viewTypehome = AppStrings.LABEL_HOME;
      });
    }
  }
  bool isHomeIconVisible = false;
  @override
  Widget build(BuildContext context) {
    print("AppStrings.SESSION_ID---" + AppStrings.SESSION_ID);
    return MaterialApp(
      title: 'Fetch Data Example',
      home: WillPopScope(
        onWillPop: _onWillPop,
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            resizeToAvoidBottomPadding: true,
            appBar: CustomAppBar(
              context: context,
              title: appBarTitle,
              customDrawer: customDrawer,
              isHomeIconVisible: isHomeIconVisible,
            ),
            body: SafeArea(
              child: view,
              /*  child: Column(
                children: <Widget>[
                  Expanded(
                    c,hild: PhysicalCount(),
                  ),
                  // FlatButton(
                  //   onPressed: () {
                  //     if (faciltiyList != null && faciltiyList.length > 0) {
                  //       // showDialog(
                  //       //     context: context,
                  //       //     builder: (_) => FacilityDialog(
                  //       //           facilityListData: faciltiyList,
                  //       //           callback: this,
                  //       //         ));
                  //     } else {
                  //       Utility.showToastMsg(
                  //           "Data Not Please Check Internet Or Contact to Admin");
                  //     }
                  //   },
                  //   child: Text("Open facility dialog"),
                  //   color: Colors.blue,
                  // )
                ],
              ),
            ),
            */
            ),
            drawer: customDrawer,
          ),
        ),
      ),
    );
  }

  // @override
  // void getData(String data) {
  //   print("facility data in the callback--" + data);
  // }

  @override
  void getUserCustomerProfileData(String data) {}

  @override
  void viewType(Widget widget, String viewType) {
    print("viewType called ---"+viewType);

    if (viewType  ==AppStrings.LABEL_HOME)
    {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_HOME.toUpperCase(), AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = false;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    } else if (viewType == AppStrings.LABEL_TRANSFER) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_TRANSFER.toUpperCase(), AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    } else if (viewType == AppStrings.LABEL_PHYSICAL_COUNT)
    {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_PHYSICAL_COUNT.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }
    if (viewType == AppStrings.LABEL_ADD_PART) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_PHYSICAL_COUNT.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }
    else if (viewType ==AppStrings.LABEL_ISSUE_BY_PICKLIST) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_ISSUE_BY_PICKLIST.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType == AppStrings.LABEL_PICK_MY_ORDER) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_PICK_MY_ORDER.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType==AppStrings.LABEL_ISSUE_BY_WORK_ORDER) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_ISSUE_BY_WORK_ORDER.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    } else if (viewType==AppStrings.LABEL_ISSUE_BY_WO_OR_TASK) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_ISSUE_BY_WO_OR_TASK.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    } else if (viewType==AppStrings.LABEL_ISSUE_FOR_EQUIPEMENT) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_ISSUE_FOR_EQUIPEMENT.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    } else if (viewType==AppStrings.LABEL_PUTBACK) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_PUTBACK.toUpperCase(), AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    } else if (viewType==AppStrings.LABEL_RECEIVE_BY_PO) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_RECEIVE_BY_PO.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }
    else if (viewType==AppStrings.LABEL_RECEIVE_BY_PO_ACTIVITY) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_RECEIVE_BY_PO.toUpperCase(),
          AppStrings.FONT_SIZE_18);
        setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }
    else if (viewType == AppStrings.LABEL_RECEIVE_BY_PART)
    {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_RECEIVE_BY_PART.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType ==AppStrings.LABEL_RECEIVE_BY_PART_ACTIVITY) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_RECEIVE_BY_PART.toUpperCase(),
          AppStrings.FONT_SIZE_18);
        setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }


    else if (viewType==AppStrings.LABEL_REVERSE_RECEIVE) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_REVERSE_RECEIVE.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType==AppStrings.LABEL_REVERSE_RECEIVE_ACTIVITY) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_REVERSE_RECEIVE.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType==AppStrings.LABEL_PUTAWAY_ITEMS) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_PUTAWAY_ITEMS.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType==AppStrings.LABEL_ISSUE_TO_EMPLOYEE) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_ISSUE_TO_EMPLOYEE.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType==AppStrings.LABEL_TRANSFER_PALLET) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_TRANSFER_PALLET.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }

    else if (viewType==AppStrings.LABEL_VIEW_INVENTORY) {
      appBarTitle = Utility.getText(
          AppStrings.LABEL_VIEW_INVENTORY.toUpperCase(),
          AppStrings.FONT_SIZE_18);
      setState(() {
        isHomeIconVisible = true;
        viewTypehome = viewType;
        this.view = widget;
      });
      return;
    }
  }

  Future<void> getData() async {
    String data =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    AppStrings.facility = Facility.fromJson(map);
    print("retrived from SH file--->" + AppStrings.facility.iFacilityCode);

    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    AppStrings.USER_ID =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.USER_ID);
    AppStrings.COMPANY_CODE =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
    AppStrings.I_FACILITY_CODE =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.DEFAULT_I_FACILITY_CODE);

    print("AppStrings.USER_ID---"+AppStrings.USER_ID);
    print("AppStrings.COMPANY_CODE---"+AppStrings.COMPANY_CODE);
    print("AppStrings.I_FACILITY_CODE---"+AppStrings.I_FACILITY_CODE);
    print("retrived from SH file--->" + AppStrings.facility.iFacilityCode);

  }

// Widget getTextData(String strTitle) {
//   return Text(
//     strTitle,
//     style: TextStyle(
//         fontFamily: AppStrings.SEGOEUI_FONT,
//         fontSize: AppStrings.FONT_SIZE_18),
//   );
// }

/*@override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Widget>('widget', widget));
  }
*/
/* @override
  void getUserCustomerProfileData(Facility data) {
    print("facility data  facility Code--" + data.facilityCode);
  }*/
}
