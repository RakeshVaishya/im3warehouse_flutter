import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:im3_warehouse/databases/database_helper.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/facility/dialogcallback.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';

class FacilityDialog extends StatefulWidget {
  List<Facility> facilityListData;
  DialogCallback callback;
  ViewTypeCallBack viewTypeCallBack;
  String str;
  FacilityDialog({this.facilityListData, this.str, this.callback,this.viewTypeCallBack});
  @override
  _FacilityDialogState createState() => _FacilityDialogState();
}

class _FacilityDialogState extends State<FacilityDialog> {
  Facility newDefaultValue;
  List<Facility> facilityListDemo = [];
  String jsonValue;
  DatabaseHelper db;
  List<Facility> facilityListTable;

  @override
  void initState() {
    super.initState();

    db = DatabaseHelper();
    facilityListTable = new List();
     SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.DEFAULT_FACILITY_CODE_VALUE).then((defaultCode)
     {
       db.getFacilityData().then((list)
       {
         this.facilityListTable = list;
         setState(() {
         });

         for (int i = 0; i < facilityListTable.length; i++)
         {
           Facility facility = facilityListTable.elementAt(i);
           String facilityCode = facility.facilityCode;
           print("shardPrefValue Ifacility----->" + defaultCode);
           print("facilityCode code value---->" + facilityCode);
           if (defaultCode.contains(facilityCode))
           {
             setState(()
             {
               newDefaultValue = facilityListTable.elementAt(i);
               jsonValue = jsonEncode(newDefaultValue.toJson());
               SharedPreferencesHelper
                   .getSharedPreferencesHelperInstance()
                   .addStringToSF(JsonUtil.FACILITY_DATA, jsonValue);
                  AppStrings.facility =newDefaultValue;
             });
           }
         }
       });
     });
}


  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).unfocus();
    return Dialog(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: 40,
            color: Colors.blue,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Text(
                  "Facility",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontFamily: AppStrings.SEGOEUI_FONT),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 12.0,
              top: 20,
            ),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Select Facility",
                style: TextStyle(
                    fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8, top: 8),
            child: DropdownButton<Facility>(
              isExpanded: true,
              isDense: true,
              value: newDefaultValue,
              icon: Icon(
                Icons.arrow_drop_down,
                size: 30,
              ),
              items: facilityListTable.map((Facility facility)
              {
                return DropdownMenuItem<Facility>
                  (
                  value: facility,
                  child: getText(facility)
                );
              }).toList(),
                  onChanged: (val) async
                  {
                  setState(()
                  {
                  newDefaultValue = val;
                  });
                  // widget.callback.getUserCustomerProfileData(newDefaultValue);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 14.0),
            child: Container(
              height: 45,
              color: Colors.blue,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      onPressed: ()
                      {
                        jsonValue = jsonEncode(newDefaultValue.toJson());
                        SharedPreferencesHelper.getSharedPreferencesHelperInstance()
                            .addStringToSF(JsonUtil.FACILITY_DATA, jsonValue);
                        String stringData = newDefaultValue.facilityCode;
                        SharedPreferencesHelper.getSharedPreferencesHelperInstance()
                            .addStringToSF(JsonUtil.DEFAULT_FACILITY_CODE_VALUE, stringData);

                        AppStrings.facility =newDefaultValue;
                        print(" facility code----->" + newDefaultValue.iFacilityCode);
                        print("default facility code----->" + stringData);
                        this
                      .widget
                      .viewTypeCallBack.viewType(DashboardView(this.widget.viewTypeCallBack), AppStrings.LABEL_HOME);
                  Navigator.of(context).pop();
                  },
                      child: Text("Submit"),
                      color: Colors.blue,
                      textColor: Colors.white,
                    ),
                  ),
                  Expanded(
                    child: FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text("Cancel"),
                      color: Colors.blue,
                      textColor: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  List<Facility> getFacData(String data)
  {
      return facilityListTable;
  }

  // getData() async {
  //   print(facilityListDemo.length);

  //   String str =
  //       await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
  //           .getStringValuesSF(JsonUtil.FACILITY_CODE);

  //   print("str--->" + str);

  //   for (int i = 0; i < facilityListDemo.length; i++) {
  //     Facility facility = facilityListDemo.elementAt(i);
  //     jsonValue = jsonEncode(facility.toJson());
  //     print("JsonValue----------->" + jsonValue);
  //     String s = facilityListDemo.elementAt(i).facilityCode;

  //     print("shardPrefValue Ifacility----->" + s);

  //     if (str.contains(s)) {
  //       print("IFACILITY code value---->" + str);

  //       setState(() {
  //         newDefaultValue = facilityListDemo.elementAt(i);

  //         jsonValue = jsonEncode(newDefaultValue.toJson());

  //         SharedPreferencesHelper.getSharedPreferencesHelperInstance()
  //             .addStringToSF(JsonUtil.FACILITY_DATA, jsonValue);

  //         print("----- start ---");

  //         getFacility();
  //         print("----- end ---");
  //       });
  //     }
  //   }
  // }

  Future<void> getFacility() async {
    String data =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    Facility facility = Facility.fromJson(map);

    print("retrived from SH file--->" + facility.iFacilityCode);
  }

  Widget getText(Facility facility){
    print(facility.facilityCode);
    return Text(facility.facilityCode,
      overflow: TextOverflow.ellipsis,
    );
  }
}
