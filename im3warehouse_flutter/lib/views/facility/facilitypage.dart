import 'package:flutter/material.dart';
import 'package:im3_warehouse/views/facility/facility_dialog.dart';

class FacilityPage extends StatefulWidget {
  @override
  _FacilityPageState createState() => _FacilityPageState();
}

class _FacilityPageState extends State<FacilityPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "FacilityPage",
      home: Scaffold(
        appBar: AppBar(
          title: Text("FacilityPage"),
        ),
        body: Center(
          child: FlatButton(
            onPressed: () {
              //showDialog(context: context, builder: (_) => FacilityDialog());
            },
            child: Text("Open facility dialog"),
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}
