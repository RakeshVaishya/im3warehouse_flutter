import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/TestData.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/login/login_main.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;
import 'dart:async';
import 'package:im3_warehouse/databases/database_helper.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/app_table/user_data.dart';
import 'package:im3_warehouse/utils/json_util.dart';

import 'package:flutter/widgets.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Landing Page",
      home: LandingPageArea(),
    );
  }
}

class LandingPageArea extends StatefulWidget {
  @override
  _LandingPageAreaState createState() => _LandingPageAreaState();
}

class _LandingPageAreaState extends State<LandingPageArea> {
  Color darkbluecolor = Color(0xff0D68C4);
  Color greyColor = Color(0xff646464);
  String verficationCode = "";
  String textfieldValue;
  TextEditingController verificationEditController =
      new TextEditingController();
  FocusNode verficationFocus = new FocusNode();
  String oldValue = "";
  String newValue = "";
  String strVerficationCode = "";
  var db;

  var colorVar;
  ProgressDialog pr;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    db = new DatabaseHelper();
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);

   //testingData();


  }

  @override
  void dispose() {
    verificationEditController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context)
  {
    //FocusScope.of(context).requestFocus(verficationFocus);
    return SafeArea(
      child: WillPopScope(
        onWillPop: _onBackPressed,
        child: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Scaffold(
              backgroundColor: Colors.white,
              key: _scaffoldKey,
              body:Container(
               child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 0.0),
                      child: Image(
                        image: AssetImage(AppStrings.IMAGE_IM3_LOGO),
                        height: MediaQuery.of(context).size.height / 4,
                        width: MediaQuery.of(context).size.height / 6,
                      ),
                    ),
                  ),
                   new Positioned(child:
                   Align(
                     alignment: FractionalOffset.center,
                       child: SingleChildScrollView(
                      padding: const EdgeInsets.all(8.0),
                     child: Padding(
                       padding: const EdgeInsets.only(bottom:100.0),
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: <Widget>[
                           Padding(
                             padding: const EdgeInsets.only(top:0,left: 20.0, right: 20),
                             child: Row(
                               children: <Widget>[
                                 Expanded(
                                   flex: 6,
                                   child: TextFormField(
                                     controller: verificationEditController,
                                     style: TextStyle(
                                       fontFamily: AppStrings.SEGOEUI_FONT,
                                     ),
                                     focusNode: verficationFocus,
                                     keyboardType: TextInputType.text,
                                     textInputAction: TextInputAction.send,
                                     onFieldSubmitted: (text)
                                     {
                                       if(verificationEditController.text.length==0)
                                       {
                                         Utility.showToastMsg(
                                             "Please Enter Mobile App Code ");
                                         FocusScope.of(context).requestFocus(verficationFocus);

                                         return;
                                       }
                                       else{
                                         checkInternetConnection(context);
                                       }
                                     },
                                     decoration: InputDecoration(
                                       labelText:
                                       AppStrings.LABEL_VERIFICATION_CODE,
                                       labelStyle: TextStyle(
                                           fontFamily: AppStrings.SEGOEUI_FONT,
                                           fontSize: 15,
                                           color: getColorsData()),
                                       hintText:
                                       AppStrings.LABEL_VERIFICATION_CODE,
                                       hintStyle: TextStyle(
                                           fontSize: 13,
                                           fontFamily: AppStrings.SEGOEUI_FONT),
                                       contentPadding:
                                       EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 0),
                                     ),
                                     inputFormatters: [
                                       BlacklistingTextInputFormatter(
                                           RegExp('[ ]'))
                                     ],
                                   ),
                                 ),
                                 Expanded(
                                   child: InkWell(
                                     child: Image(
                                       image: AssetImage(AppStrings.IMAGE_BARCODE),
                                       height: 34,
                                       width: 32,
                                     ),
                                     onTap: () {
                                       scanCode();
                                     },
                                   ),
                                 ),
                                 Expanded(
                                     child: InkWell(
                                       child: Image(
                                         image:
                                         AssetImage(AppStrings.IMAGE_ROUND_INFO),
                                         height: 22,
                                         width: 21,
                                       ),
                                       onTap: () {
                                         verificationDialog();
                                       },
                                     )),
                               ],
                             ),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(
                                 left: 45.0, right: 45, top: 20),
                             child: MaterialButton(
                               onPressed: () {
                                 if(verificationEditController.text.length==0)
                                 {
                                   Utility.showToastMsg(
                                       "Please Enter Mobile App Code ");
                                   FocusScope.of(context).requestFocus(verficationFocus);

                                   return;
                                 }
                                 else{
                                   checkInternetConnection(context);
                                 }
                                 // pr.show();
                                 // getCompanyData();
                               },
                               child: Text(AppStrings.LABEL_OK),
                               color: darkbluecolor,
                               minWidth: double.infinity,
                               textColor: Colors.white,
                               shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.only(
                                       topLeft: Radius.circular(5),
                                       topRight: Radius.circular(5),
                                       bottomLeft: Radius.circular(5),
                                       bottomRight: Radius.circular(5))),
                             ),
                           )
                         ],
                       ),
                     ),
                  ),)
                     ,

                 ),
                    Align(

                      alignment: Alignment.bottomCenter,
child: Padding(
  padding: const EdgeInsets.only(top:0),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Text(
        AppStrings.IM3_STATUS_MSG,
        textAlign: TextAlign.end,
        style: TextStyle(
            fontSize: 16,
            fontFamily: "fonts/SegoeUI",
            color: Colors.blue[800]),
      ),
      Image.asset(
        "images/Registered.png",
        height: 40,
        width: 40,
      )
    ],
  ),


                    ),
                  )

                ],

              ),

            ),      resizeToAvoidBottomInset: false




          ),
        ),
      ),
    );
  }

  void checkInternetConnection(BuildContext context) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('check connection-------------->connected');
        pr.show();
        getCompanyData();
      }
    } on SocketException catch (_) {
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  Future<void> getCompanyData() async {
    // pr.show();
    if (verificationEditController.text.length != 0) {
      setState(()
      {
        oldValue = (verificationEditController.text);
      });

      print("value before replaced---------->");
      print(oldValue);
      print("value after replaced---------->");
      newValue = "${oldValue.replaceAll('&', '#/#')}";
      print(newValue);
    }

    strVerficationCode = "pstrVerificationCode=" + newValue;
    print("verification code-------> " + strVerficationCode);

    final uri = NetworkConfig.API_GET_COMPANY_URL;
    final headers = {'Content-Type': 'application/x-www-form-urlencoded'};
    String jsonBody = strVerficationCode;
    final encoding = Encoding.getByName('utf-8');

    // String respBody = TestData.getLandingdata();
    // setDataInTable(respBody);

    Response response =
        await post(uri, headers: headers, body: jsonBody, encoding: encoding);

    int statusCode = response.statusCode;
    print("StatusCode---->");
    print(statusCode);

    if (statusCode == 200) {
      //pr.dismiss();
      pr.hide();

      String responseBody = response.body;
      print("Response body------->" + responseBody);
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      setDataInTable(responseBody);
    } else if (statusCode == 500) {
//      pr.dismiss();
      pr.hide();


      String str = "Internal server error";
      showResponseErrorDialog(statusCode, str);
      print("repsonse msg--->Internal server error");
    } else if (statusCode == 404) {
//      pr.dismiss();
      pr.hide();


      String str1 = "Server Not found";
      showResponseErrorDialog(statusCode, str1);
      print("repsonse msg--->Server Not Found");
    } else {
     // pr.dismiss();
      pr.hide();


      String respBody = TestData.getLandingdata();

      String str3 = "Error from Server";
      showResponseErrorDialog(statusCode, str3);
    }
  }

  Widget showResponseErrorDialog(int statusCode, String strMsg) {
    showDialog(
      context: context,
      builder: (_) => AlertDialog(
        title: Text("Status code : $statusCode"),
        content: Text("$strMsg"),
        contentPadding: EdgeInsets.fromLTRB(25, 20, 5, 5),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("OK"),
          )
        ],
      ),
    );
  }

  Future<Void> verificationDialog() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
              AppStrings.LABEL_ALERT_DIALOG,
              style: TextStyle(
                fontFamily: AppStrings.SEGOEUI_FONT,
                fontSize: 16,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  AppStrings.LABEL_OK,
                  style: TextStyle(
                      fontFamily: AppStrings.SEGOEUI_FONT,
                      fontSize: 15,
                      color: Colors.blue),
                ),
              )
            ],
            contentPadding: EdgeInsets.fromLTRB(15, 20, 5, 5),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
          );
        });
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text(
              AppStrings.LABEL_EXIT_DIALOG,
              style: TextStyle(fontFamily: AppStrings.SEGOEUI_FONT),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  AppStrings.LABEL_NO,
                  style: TextStyle(
                      fontFamily: AppStrings.SEGOEUI_FONT, fontSize: 15),
                ),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              FlatButton(
                child: Text(
                  AppStrings.LABEL_YES,
                  style: TextStyle(
                      fontFamily: AppStrings.SEGOEUI_FONT, fontSize: 15),
                ),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              )
            ],
            // contentPadding: EdgeInsets.all(6),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5))),
          );
        });
  }

  Future scanCode() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() {
        this.verficationCode = barcode.rawContent;
        verificationEditController.text = verficationCode;
        print("verif code----------------->");
        print(verficationCode);
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        showToastMsg("Camera permission not granted");
      } else {
        showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      showToastMsg(
          'null (user returned using the "back-button" before scanning)');
    } catch (e) {
      showToastMsg("Unknown error: $e");
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
        SnackBar(content: Text("Please check your Internet connection"));
    _scaffoldKey.currentState.showSnackBar(snackbar);
  }

  void connectionDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text("Please check your Internet connection"),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(AppStrings.LABEL_OK),
              )
            ],
          );
        });
  }

  static showToastMsg(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        backgroundColor: Colors.blueAccent,
        fontSize: 16.0,
        textColor: Colors.white,
        gravity: ToastGravity.BOTTOM);
  }

  void setDataInTable(String responseBody) async {
    Map data1 = jsonDecode(responseBody);

    Map data2;
    try {
       data2 = data1['NewDataSet'];
    }catch(e){
      print("wertuiuytrewwertytre");
      setState(()
      {
        Utility.showToastMsg("Invalid Verication Code !");
      });
      return;
    }

    if(data2.length==0)
    {
      setState(()
      {
        Utility.showToastMsg("Invalid Verication Code !");
      });
return;
    }
    else{
      UserData.userData = UserData.fromJson(data2["Table"]);
      String strcompnayCode =
      await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
          .getStringValuesSF(JsonUtil.KEY_Company_Code);
      String mobileUrl =
      await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
          .getStringValuesSF(JsonUtil.MOBILE_URL);
      print("Comapany Code----->");
      print(strcompnayCode);
      print("Mobile URL----->");
      print(mobileUrl);
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => Login()),
            (Route<dynamic> route) => false,
      );
    }
  }
/*

  checkInternetConnection(
  value, IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP);
*/

  Color getColorsData() {
    if (verificationEditController.text.length > 0 ||
        verficationFocus.hasFocus) {
      colorVar = Colors.blue;
    } else {
      colorVar = Colors.black;
    }
    return colorVar;
  }

 void testingData(){
  verificationEditController.text = AppStrings.VERIFICATION_CODE;
     }
}
