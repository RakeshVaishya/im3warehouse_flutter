import 'dart:convert';
import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/issue_for_equiment/issu_for_equip_modal.dart';
import 'package:im3_warehouse/models/issue_for_equiment/issue_equip_all_stock_modal.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/network/api/issue_for_equipment_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;

class IssueForEquipment extends StatefulWidget {
  ViewTypeCallBack callBack;
  IssueForEquipment(this.callBack);
  @override
  State<StatefulWidget> createState() {
    return IssueForEquipmentState();
  }
}

class IssueForEquipmentState extends State<IssueForEquipment> {
  bool _isEquipmentExist = false;
  bool _isPartNoExist = false;
  bool _isSerialNo = false;
  var _formKey = GlobalKey<FormState>();
  bool _validate = false;
  String userId = "";
  String companyCode = "";
  String iFacilityCode = "";
  String strEquipmentDesc = "";
  String strPartDesc = "";
  String strOnHandQuantity = "";
  String strStockRoom = "";
  String selectedWareHouse = "";
  List<String> elementExistInWareHouseList = [];
  //ProgressDialog pr;
  IssuForEquipmentModal equipmentDetail;
  PartDetails _partDetails;
  IssueEquipGetAllStockModal stockDetail;

  FocusNode _equipmentFocus = FocusNode();
  FocusNode _partNoFocus = FocusNode();
  FocusNode _serialNoFocus = FocusNode();
  FocusNode _locationFocus = FocusNode();
  FocusNode _quantityFocus = FocusNode();

  Color equipmentColor;
  Color partColor;
  Color locationColor;
  Color serialColor;
  Color quantityColor;

  TextEditingController _scanEquipmentController = TextEditingController();
  TextEditingController _scanPartController = TextEditingController();
  TextEditingController _scanSerialNoController = TextEditingController();
  TextEditingController _quantityController = TextEditingController();
  var _dialogKey = GlobalKey<FormState>();
  bool isLoaderRunning =false;

  @override
  void initState() {
    super.initState();
    _scanEquipmentController.text = "EQ03725";
 //   _scanPartController.text = "ULTRA";
    _equipmentFocus.addListener(()
    {
      setState(() {
        equipmentColor = Colors.grey[600];

      });
    });
    _partNoFocus.addListener(() {


        setState(()
        {
        partColor = Colors.grey[600];
      });
    });
    _serialNoFocus.addListener(()
    {
      setState(() {
        serialColor = Colors.grey[600];
      });
    });
    _locationFocus.addListener(() {
      setState(() {
        locationColor = Colors.grey[600];
      });
    });
    _quantityFocus.addListener(() {
      setState(() {
        quantityColor = Colors.grey[600];
      });
    });
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Form(
          key: _formKey,
          autovalidate: _validate,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, right: 8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        textBaseline: TextBaseline.ideographic,
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: TextFormField(
                              focusNode: _equipmentFocus,
                              style: TextStyle(
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                              controller: _scanEquipmentController,
                              decoration: InputDecoration(
                                  labelText: "Equipment",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _equipmentFocus.hasFocus
                                          ? equipmentColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                              textInputAction: TextInputAction.send,
                              onFieldSubmitted: (value)
                              {
                                if(value.trim().length>0)
                                {
                                  _isEquipmentExist =false;
                                  checkInternetConnection(
                                      context, _scanEquipmentController.text, IssueForEquipmentApi.API_GET_EQUIPMENT);
                                  }
                                else{
                                }
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Equipment Code cannot be blank";
                                }
                              },
                            ),
                          ),
                          Expanded(

                            child: Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: InkWell(
                                child: Image.asset("images/QRCode.png"),
                                onTap: () {
                                  print("barcode clicked");
                                  scanItems(IssueForEquipmentApi.API_GET_EQUIPMENT);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, top: 10),
                      child: Text(
                        "EQ Desc:",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: AppStrings.SEGOEUI_FONT,color: Colors.grey[600] ,
                            fontSize: 15

                        ),
                      ),
                    ),
                    Visibility(
                      visible: _isEquipmentExist,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 5.0, top: 10),
                            child: Text(
                              strEquipmentDesc,
                              style: TextStyle(
                                fontFamily: AppStrings.SEGOEUI_FONT,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, right: 8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        textBaseline: TextBaseline.ideographic,
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: TextFormField(
                              focusNode: _partNoFocus,
                              style: TextStyle(
                                fontFamily: AppStrings.SEGOEUI_FONT,
                              ),
                              controller: _scanPartController,
                              decoration: InputDecoration(
                                  labelText: "Part No",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _partNoFocus.hasFocus
                                          ? partColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (value)
                              {
                                if(value.length>0)
                                {
                                  _isPartNoExist = false;
                                  checkInternetConnection(
                                      context, value, NetworkConfig.API_GET_PART);
                                  FocusScope.of(context).requestFocus(_serialNoFocus);
                                }
                                else{
                                }
                              },
                              onChanged:(value){
                                if(value.length ==0)
                                {
                                _isPartNoExist =false;

                                }
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please enter Part No";
                                }
                              },
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: InkWell(
                                child: Image.asset("images/QRCode.png"),
                                onTap: () {
                                  print("barcode clicked");
                                  scanItems(NetworkConfig.API_GET_PART);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 12.0, top: 10),
                      child: Text(
                        "Part Desc:",
                        style: TextStyle(
                            fontFamily: AppStrings.SEGOEUI_FONT,
                            fontSize: 15),
                      ),
                    ),

                    Visibility(
                      visible: _isPartNoExist,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 5.0, top: 10),
                                  child: Text(
                                    strPartDesc,
                                    style: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 12.0, right: 12, top: 5),
                              child: Divider(
                                height: 5,
                                thickness: 0.5,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                      const EdgeInsets.only(left: 12.0, top: 10),
                      child: Text(
                        "Stockroom",
                        style: TextStyle(
                            fontFamily: AppStrings.SEGOEUI_FONT,
                            fontSize: 15),
                      ),
                    ),
                    Visibility(
                        visible: _isPartNoExist,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 0.0, left: 12, right: 12),
                                child: DropdownButton<String>(
                                  value: selectedWareHouse,
                                  icon: Icon(Icons.arrow_drop_down),
                                  iconSize: 24,
                                  isExpanded: true,
                                  elevation: 10,
                                  hint: Text("text"),
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                    underline: Container(
                                    height: 1,
                                    color: Colors.grey,
                                  ),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      print("selectedCountTye--" + newValue);
                                      selectedWareHouse = newValue;
                                    });
                                  },
                                  items: elementExistInWareHouseList
                                      .map((dropdownValue) {
                                       return DropdownMenuItem<String>(
                                        value: dropdownValue,
                                        child: Text(
                                          dropdownValue,
                                          style: TextStyle(color: Colors.black),
                                        ));
                                  }).toList(),
                                ),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 12.0, top: 10),
                                  child: Text(
                                    "On Hand:",
                                    style: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontSize: 15),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 5.0, top: 10),
                                  child: Text(
                                    strOnHandQuantity,
                                    style: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 12.0, right: 12, top: 5),
                                  child: Divider(
                                  thickness: 0.5,
                                  height: 5,
                                  color: Colors.grey),
                            ),
                          ],
                        )),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0, right: 8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        textBaseline: TextBaseline.ideographic,
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: TextFormField(
                              focusNode: _serialNoFocus,
                              style: TextStyle(
                                fontFamily: AppStrings.SEGOEUI_FONT,
                              ),
                              controller: _scanSerialNoController,
                              decoration: InputDecoration(
                                  labelText: "Serial No",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _serialNoFocus.hasFocus
                                          ? serialColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                              onChanged: (value) {
                                if (value.length > 4) {
                                  Future.delayed(Duration(seconds: 2), () {
                                    setState(() {
                                      _isSerialNo = true;
                                    });
                                  });
                                }
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please enter Serial No";
                                }
                              },
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: InkWell(
                                child: Image.asset("images/QRCode.png"),
                                onTap: () {
                                  print("barcode clicked");
                                  String title = "SerialNo";
                                  scanItems(title);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 10.0, left: 12, right: 8, bottom: 8),
                         child: Row(
                           children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: TextFormField(
                              focusNode: _quantityFocus,
                              controller: _quantityController,
                              style: TextStyle(
                                fontFamily: AppStrings.SEGOEUI_FONT,
                              ),
                              decoration: InputDecoration(
                                  labelText: "Quantity",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _quantityFocus.hasFocus
                                          ? quantityColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Please enter Quantity";
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 12.0, right: 2),
                              child: FlatButton(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    "Issue".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontSize: 17),
                                  ),
                                ),
                                color: AppColors.CYAN_BLUE_COLOR,
                                onPressed: () {
                                  _onIssue();
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 12.0, left: 2),
                              child: FlatButton(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    "Cancel".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontSize: 17),
                                  ),
                                ),
                                color: AppColors.CYAN_BLUE_COLOR,
                                onPressed: () {
                                  onCancel();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future scanItems(String title) async {
    try {
      var barcode = await BarcodeScanner.scan();
      if (title == IssueForEquipmentApi.API_GET_EQUIPMENT) {
        setState(() {
          _scanEquipmentController.text = barcode.rawContent;
          getDetails(barcode.rawContent, IssueForEquipmentApi.API_GET_EQUIPMENT);
        });
      }
      else if (title == NetworkConfig.API_GET_PART)
      {
        setState(() {
          _scanPartController.text = barcode.rawContent;
          getDetails(barcode.rawContent, title);
        });
      } else if (title == "SerialNo") {
        setState(() {
          _scanSerialNoController.text = barcode.rawContent;
          getDetails(barcode.rawContent, "PartNo");
        });
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      Utility.showToastMsg(
          'null (user returned using the "back-button" before scanning)');
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  void checkInternetConnection(
      BuildContext context, String value, String type) async {
      try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('check connection-------------->connected');
        starLoader();
        Future.delayed(Duration(seconds: 90), () {
          dismissLoader();
          Utility.showToastMsg("TimeOut Please try again");
        });
        getData();
        if ( type ==IssueForEquipmentApi.API_GET_EQUIPMENT)
        {
          getDetails(value, type);
        }
        else if (type ==NetworkConfig.API_GET_PART)

        {
          getDetails(value, type);

        }

      }
    } on SocketException catch (_) {
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
        SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  Future<void> getData() async {
    String data =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.FACILITY_DATA);

    iFacilityCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.DEFAULT_I_FACILITY_CODE);

    print("IFacility code--->" + iFacilityCode);
    print("json data" + data);
    // Map<String, dynamic> map = jsonDecode(data);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
  }

  Future getDetails(String data, String type) async {
    print("data -->" + data);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";

    print(NetworkConfig.BASE_URL);

    if (type==IssueForEquipmentApi.API_GET_EQUIPMENT) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueForEquipmentApi.API_GET_EQUIPMENT;
      jsonBody = IssueForEquipmentApi.getEquipment(companyCode, data);

      print(jsonBody);
    }
    if (type== NetworkConfig.API_GET_PART) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_PART;
      Facility  facility  = new Facility();
      facility.companyCode =companyCode;
      facility.iFacilityCode  =iFacilityCode;
      jsonBody =IssueForEquipmentApi.getPartDetails(facility, userId, data, "1");
      print("jsonBody--->" + jsonBody);
    }
    print("data---" + jsonBody);
    print("URI---" + uri);

    final encoding = Encoding.getByName("utf-8");
    Response response = await post(uri,
        headers: NetworkConfig.headers, body: jsonBody, encoding: encoding);
    int statusCode = response.statusCode;
    print("Status Code");
    print(statusCode);
    String responseBody;
    Map map2;
    if (response.statusCode == 200) {
      responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      print("responseBody--->" + responseBody);
      if (responseBody !="0")
      {
        Map map = jsonDecode(responseBody);
        Map map1 = map["NewDataSet"];
        map2 = map1["Table"];
        onSuccessResponse(type, map2);
      }

      else{

      }
    } else {
    //  (type, map2);
      print("Wrong input");
      Utility.showToastMsg(
          "Please Select Facility and  Enter Correct Part No.");
    }
  }

  void onSuccessResponse(String type, Map map2) {
    if (type == IssueForEquipmentApi.API_GET_EQUIPMENT) {
      equipmentDetail = IssuForEquipmentModal.fromJson(map2);
      print("Equipment description---->" +
          equipmentDetail.hEquipmentDescription);
      dismissLoader();
      setState(() {
        _isEquipmentExist = true;
        strEquipmentDesc = equipmentDetail.hEquipmentDescription;
      });
    }
    if (type== NetworkConfig.API_GET_PART)
    {
      _partDetails = PartDetails.fromJson(map2);
      print("Part description-->" + _partDetails.partDescription);
      setState(() {
        _isPartNoExist = true;
        strPartDesc = _partDetails.partDescription;
        strOnHandQuantity = _partDetails.onHandQuantity;
      });
      String strIPartNo = _partDetails.iPartNo;
      getAllStock(strIPartNo);
    }
    if (type== IssueForEquipmentApi.API_GET_ALL_STOCK)
    {
      _partDetails = PartDetails.fromJson(map2);
      print("Part description-->" + _partDetails.partDescription);
      setState(() {
        _isPartNoExist = true;
        strPartDesc = _partDetails.partDescription;
        strOnHandQuantity = _partDetails.onHandQuantity;
      });
      String strIPartNo = _partDetails.iPartNo;
      getAllStock(strIPartNo);
    }


  }

  Future getAllStock(String iPartNo) async {
    String uri = "";
    String jsonBody;
    uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        IssueForEquipmentApi.API_GET_ALL_STOCK;
    jsonBody =IssueForEquipmentApi.getAllStock(companyCode, iPartNo, iFacilityCode);
       // "CompanyCode=$companyCode&IPartNo=$iPartNo&IFacilityCode=$iFacilityCode";
    print("jsonBody--->" + jsonBody);
    print("URI---" + uri);

    final encoding = Encoding.getByName("utf-8");
    Response response = await post(uri,
        headers: NetworkConfig.headers, body: jsonBody, encoding: encoding);

    int statusCode = response.statusCode;
    print("Status Code");
    print(statusCode);
    String responseBody;

    if (response.statusCode == 200)
    {
      responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      print("responseBody--->" + responseBody);
      if (!identical(responseBody, "0")) {
        Map map = jsonDecode(responseBody);
        Map map1 = map["NewDataSet"];
        List<IssueEquipGetAllStockModal> list = [];
        if (map1['Table'] != null)
        {
          map1['Table'].forEach((v)
          {
            list.add(IssueEquipGetAllStockModal.fromJson(v));
          });
        }
        for (var i in list)
        {
          setState(() {
            _isPartNoExist = true;
          });
          elementExistInWareHouseList = i.stockRoomList;
        }
        selectedWareHouse = elementExistInWareHouseList.elementAt(0);
        dismissLoader();
      } else {
        dismissLoader();
        Utility.showToastMsg("GeAllStock responseBody is empty");
        print("ResponseBody---->GeAllStock isEmpty");
      }
    } else {
      Utility.showToastMsg(
          "Please Select Facility and  Enter Correct Part No.");
    }
  }

  _onIssue() {
    if (_formKey.currentState.validate())
    {
      _formKey.currentState.save();
    } else {
      setState(() {
        _validate = true;
      });
    }

    doIssueForEquipement();
  }
 void doIssueForEquipement(){

    if(equipmentDetail==null)
    {
      Utility.showToastMsg("Sync for Equipement Description ");
      return;
    }
    if(_partDetails.iPartNo==null)
    {
      Utility.showToastMsg("Sync for Part Description ");
      return;
    }

    Utility.showToastMsg("Sending data to server");


 }

  onCancel() {
    String strEquipent = _scanEquipmentController.text;
    String strPart = _scanPartController.text;
    String strSerial = _scanSerialNoController.text;
    String strQuantity = _quantityController.text;

    if (strEquipent.length == 0 &&
        strPart.length == 0 &&
        strSerial.length == 0 &&
        strQuantity.length == 0) {
      this
          .widget
          .callBack
          .viewType(DashboardView(this.widget.callBack), AppStrings.LABEL_HOME);
    } else {
      getClearFields();
    }
  }

  void starLoader()
  {
    print("dialog started");
    if(!isLoaderRunning) {
      isLoaderRunning = true;
      Dialogs.showLoadingDialog(context, _dialogKey);
    }
  }
  void dismissLoader()
  {
    if(isLoaderRunning )
    {
      isLoaderRunning =false;
      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }

  getClearFields()
  {
    setState(() {
      _scanEquipmentController.text = "";
      _scanPartController.text = "";
      _scanSerialNoController.text = "";
      _quantityController.text = "";
      _partDetails =null;
      equipmentDetail =null;
      stockDetail =null;
      setState(() {
        _isEquipmentExist = false;
        _isPartNoExist = false;
      });
    });
  }

  void hideLocation()
 {
 }
void hidePartDetail(){

}
void hideEquipmentDetail()
{

}

}
