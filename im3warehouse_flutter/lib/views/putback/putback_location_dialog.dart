import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/putback/putback_part.dart';
import 'package:im3_warehouse/network/api/put_away_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/putback/dialogCallback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;
import 'package:flutter/widgets.dart';

class PutBackByWorkLocationDialog extends StatefulWidget {
  DialogCallback callback;
  List<PutBackPart> putBackPartList = [];
  PutBackPart putBackPart;
  String partNo;
  String workOrderNo;
  PutBackByWorkLocationDialog({this.callback, this.putBackPartList, this.partNo,this.workOrderNo});
  @override
  PutBackByWorkLocationDialogState createState() => PutBackByWorkLocationDialogState();
}

/*

class PutBackByWorkLocationDialogState extends State<PutBackByWorkLocationDialog>
    with WidgetsBindingObserver {
  var _dialogKey = GlobalKey<FormState>();

  ProgressDialog pr;
  DialogCallback _callback;
  Color serverResponeColor;
  String serverResponse = "";
  bool serverRespone = false;
  String createdUserId = "";
  List<PutBackPart> putBackPartList = [];

  String partNo;
  String workOrderNo;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    putBackPartList = widget.putBackPartList;
    partNo = widget.partNo;
    workOrderNo = widget.workOrderNo;
    _callback = widget.callback;


    print(  "size"+putBackPartList.length.toString());
    //getTest();
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID)
        .then((value) {
      createdUserId = value;
    });
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
          "Part Number Details ".toUpperCase(), textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
          )),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text("Part # $partNo is Added More Than Once in WO # $workOrderNo ",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
              )),
          Text(AppStrings.LABEL_PUT_BACK_LINE_ITEM_SELECT,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14,
              )),

           Container(

             child :new Column(
               children: <Widget>[makeRadioTiles()],
                             ),),

          Padding(
            padding: const EdgeInsets.only(top: 25.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 4.0),
                    child: MaterialButton(
                      onPressed: () {
                        //onSave();
                      },
                      color: AppColors.CYAN_BLUE_COLOR,
                      minWidth: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Save".toUpperCase(),
                          style:
                          TextStyle(fontSize: 14.0, color: Colors.white),
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0.0))),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 4.0),
                    child: MaterialButton(
                      onPressed: () {
                        //onCancel();
                      },
                      color: AppColors.CYAN_BLUE_COLOR,
                      minWidth: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Cancel".toUpperCase(),
                          style:
                          TextStyle(fontSize: 14.0, color: Colors.white),
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.all(Radius.circular(0.0))),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }


*/
/*
  getTitleText(PutBackPart data) {
    return data.lineItemSrNo + " " + data.partNo + "" + data.partDescription +
        " " + data.taskCode;
  }
*//*


  String _value2 ="";
  Widget  makeRadioTiles() {
    List<Widget> list = new List<Widget>();
    for (int i = 0; i < putBackPartList.length; i++)
    {

      list.add(new RadioListTile(
        value: putBackPartList[i].lineItemSrNo,
        groupValue: _value2,
        selected: putBackPartList[i].selected,
        onChanged: (val) {
          setState(() {
            for (int i = 0; i < putBackPartList.length; i++)
            {
              putBackPartList[i].selected = false;
            }
            _value2 = val;
            putBackPartList[i].selected = true;
          });
        },
        activeColor: Colors.purple,
        controlAffinity: ListTileControlAffinity.leading,
        title: new Text(
          ' ${putBackPartList[i].lineItemSrNo}',
          style: TextStyle(
              color: putBackPartList[i].selected ? Colors.black : Colors.grey,
              fontWeight:
              putBackPartList[i].selected ? FontWeight.bold : FontWeight
                  .normal),
        ),
      ));
      Column column = new Column(
        children: list,
      );
      return column;
    }

    void onSave() {
    }
    void reset() {
    }

    void onCancel() {
      //dismissDialog();
      Navigator.pop(context);
    }

    @override
    void deactivate() {
      super.deactivate();
      //this method not called when user press android back button or quit
      print('deactivate');
    }

    @override
    void dispose() {
      super.dispose();
      WidgetsBinding.instance.removeObserver(this);
      //this method not called when user press android back button or quit
      print('dispose');
    }

    @override
    void didChangeAppLifecycleState(AppLifecycleState state) {
      //print inactive and paused when quit
      print(state);
    }
  }
}
*/


class GroupModel {
  String text;
  int index;
  bool selected;

  GroupModel({this.text, this.index, this.selected});
}

class PutBackByWorkLocationDialogState extends State<PutBackByWorkLocationDialog>  with WidgetsBindingObserver {
  String _value2 = "";

/*


  [9:55 PM, 7/30/2020] Sagar Megasys Dot Net: Ok
  [10:01 PM, 7/30/2020] Sagar Megasys Dot Net: Test_okay
  [10:01 PM, 7/30/2020] Sagar Megasys Dot Net: beta@121
  [10:04 PM, 7/30/2020] Sagar Megasys Dot Net: Okay-llng
  [10:04 PM, 7/30/2020] Sagar Megasys Dot Net: Wo00003
*/



  ProgressDialog pr;
  DialogCallback _callback;
  Color serverResponeColor;
  String serverResponse = "";
  bool serverRespone = false;
  String createdUserId = "";
  List<PutBackPart> putBackPartList = [];
  PutBackPart  putBackPart;
  String partNo;
  String workOrderNo;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    putBackPartList = widget.putBackPartList;
    partNo = widget.partNo;
    workOrderNo = widget.workOrderNo;
    _callback = widget.callback;
    print(  "size"+putBackPartList.length.toString());
    //getTest();
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID)
        .then((value) {
      createdUserId = value;
    });
  }
  Widget makeRadioTiles() {
    List<Widget> list = new List<Widget>();
    for (int i = 0; i < putBackPartList.length; i++) {
      list.add(new RadioListTile(
        value: putBackPartList[i].lineItemSrNo,
        groupValue: _value2,
        selected: putBackPartList[i].selected,
        onChanged: (val) {
          setState(() {
            for (int i = 0; i < putBackPartList.length; i++) {
              putBackPartList[i].selected = false;
            }
             _value2 =val;
            putBackPartList[i].selected = true;
            putBackPart=  putBackPartList.elementAt(i);
            print("putBackPart-lineItemSrNo-"+putBackPart.lineItemSrNo);

          });
        },
        activeColor: Colors.black,
        controlAffinity: ListTileControlAffinity.leading,
        title: new Text(
          getTitleText( putBackPartList[i]),
          style: TextStyle(
              color: putBackPartList[i].selected ? Colors.black : Colors.black87,
              fontWeight:
              putBackPartList[i].selected ? FontWeight.bold : FontWeight.normal),
        ),
      ));
    }
    Column column = new Column(
      children: list,
    );
    return column;
  }

  getTitleText(PutBackPart data) {
    return data.lineItemSrNo + " " + data.partNo + "" + data.partDescription +
        " " + data.taskCode;
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: AppColors.CYAN_BLUE_COLOR,
        title: new Text(AppStrings.LABEL_PUTBACK.toUpperCase()),
      ),
      //hit Ctrl+space in intellij to know what are the options you can use in flutter widgets
      body: new Container(
        padding: new EdgeInsets.all(0.0),
        child: new Column (children: <Widget>[
          Padding(
     padding: new EdgeInsets.all(10.0),
     child:Text(
              "Part Number Details ".toUpperCase(), textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
              )),
        ),
         Padding(
        padding: const EdgeInsets.only(left: 5,right: 5),

        child:
          Text("Part # $partNo is Added More Than Once in WO # $workOrderNo ",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
              )),
      ),
          Padding(
        padding: new EdgeInsets.all(10.0),
        child:
          Text(AppStrings.LABEL_PUT_BACK_LINE_ITEM_SELECT,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14,
              )),),
          const Divider(
            color: Colors.blue,
            height: 1,
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),


          Expanded(child:
          SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child:
          Column(
            children:<Widget> [
              Column(
                children: <Widget>[makeRadioTiles()],
              ),
               ],
          ) , ),

          )
        ],
        ),

      ),
  bottomNavigationBar: Container(
    child:    Padding(
      padding: const EdgeInsets.only(top: 25.0 ,left: 10,right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 4.0),
              child: MaterialButton(
                onPressed: ()
                {
                  if(putBackPart ==null)
                  {

                    Utility.showToastMsg(
                        " No lineitem selected.!");
                    return;
                  }
                  else{
                    _callback.onItemSelect(putBackPart);
                    Navigator.pop(context);
                  }
                },
                color: AppColors.CYAN_BLUE_COLOR,
                minWidth: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Save".toUpperCase(),
                    style:
                    TextStyle(fontSize: 14.0, color: Colors.white),
                  ),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius:
                    BorderRadius.all(Radius.circular(0.0))),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 4.0),
              child: MaterialButton(
                onPressed: ()
                {
                  onCancel();
                },
                color: AppColors.CYAN_BLUE_COLOR,
                minWidth: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "Cancel".toUpperCase(),
                    style:
                    TextStyle(fontSize: 14.0, color: Colors.white),
                  ),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius:
                    BorderRadius.all(Radius.circular(0.0))),
              ),
            ),
          )
        ],
      ),
    ),
     ),
    );


  }

  void onCancel() {
    Navigator.pop(context);
  }
}
