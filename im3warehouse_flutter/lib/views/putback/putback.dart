import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_location_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_part_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_work_order_modal.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/putback/putback_part.dart';
import 'package:im3_warehouse/models/reserves_part_location.dart';
import 'package:im3_warehouse/network/api/PutBackApi.dart';
import 'package:im3_warehouse/network/api/issue_by_work_order_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/putback/putback_location_dialog.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;
import 'package:im3_warehouse/views/putback/dialogCallback.dart';

class PutBack extends StatefulWidget {
  ViewTypeCallBack callBack;
  PutBack(this.callBack);
  @override
  State<StatefulWidget> createState() {
    return PutBackState();
  }
}

class PutBackState extends State<PutBack> implements DialogCallback  {
  var _formKey = GlobalKey<FormState>();
  bool _validate = false;

  bool _isWorkOrder = false;
  bool _isPartNo = false;
  bool _isLocation = false;
  var _formLocationKey = GlobalKey<FormState>();
  String selectedCountTye = "";
  String userId = "";
  String companyCode = "";
  String iFacilityCode = "";
  String strWorkOrderDesc = "";
  String strPartDesc = "part Desc";
  String strIPartNo = "";
  String strOnHandQuantity = "";
  String strStockRoom = "";
  ProgressDialog pr;
  String employeeName = "";
  FocusNode _workOrderFocus = FocusNode();
  FocusNode _partNoFocus = FocusNode();
  FocusNode _locationFocus = FocusNode();
  FocusNode _putbackQuantityFocus = FocusNode();
  FocusNode _serialNumberFocus = FocusNode();
  Color wordOrderColor;
  Color partColor;
  Color locationColor;
  Color quantityColor;
  bool isLoaderRunning = false;
  bool isSerialPart = false;
  bool isQuantityEnable = true;
  bool colorStatus = true;
  SerialNumberStatus status;
  List<SerialNumberStatus> barcodeItem = [];
  IssueByWorkOrderModal workOrder;
  IssueByWOPartModal partDetails;
  IssueByWoLocationModel locationDetail;
  PutBackPart _putBackPart;
  List<PutBackPart> putBackPartList = [];
  List<RoomFacForLocation> roomFacForLocationList = [];
  String partBarcodeValue = "";
  RoomFacForLocation roomFacForLocation;
  ReservesPartLocation reservePartLocation;
  TextEditingController _workOrderController = TextEditingController();
  TextEditingController _partNoController = TextEditingController();
  TextEditingController _lineItemController = TextEditingController();
  TextEditingController _issueQuantityController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _putBackQuantityController = TextEditingController();
  TextEditingController _serialNoController = TextEditingController();

  BuildContext context;
  bool isLoading = true;
  bool serverRespone = false;
  Color serverResponeColor;
  String serverResponse = "";
  String serialNumber = "";
  bool isSerialNumberExist = false;

  @override
  void initState() {
    super.initState();
    facility = AppStrings.facility;
    iFacilityCode = facility.iFacilityCode;
    // Utility.showToastMsg( "Facii  "+iFacilityCode);
    print(iFacilityCode);
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    _workOrderFocus.addListener(() {
      setState(() {
        wordOrderColor = Colors.grey[600];
      });
    });
    _partNoFocus.addListener(() {
      setState(() {
        partColor = Colors.grey[600];
      });
    });
    _locationFocus.addListener(() {
      setState(() {
        locationColor = Colors.grey[600];
      });
    });
    _putbackQuantityFocus.addListener(() {
      setState(() {
        quantityColor = Colors.grey[600];
      });
    });
    testData(0);
    getData();
  }

  @override
  Widget build(BuildContext context) {
    if (_partNoController.text.length == 0) {
      FocusScope.of(context).requestFocus(_partNoFocus);
    }
    this.context = context;
    return issueByWOWidget();
  }

  Widget issueByWOWidget() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              child: Form(
                key: _formKey,
                autovalidate: _validate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Visibility(
                      visible: serverRespone,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          serverResponse,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: serverResponeColor),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 0.0, left: 12, right: 8, bottom: 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        textBaseline: TextBaseline.ideographic,
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: TextFormField(
                              focusNode: _partNoFocus,
                              controller: _partNoController,
                              textInputAction: TextInputAction.send,
                              onFieldSubmitted: (value) {
                                if (value.trim().length > 0) {
                                  if (isLoading) {
                                    isLoading = false;
                                    checkInternetConnection(
                                        value,
                                        PubBackApi
                                            .API_GET_PART_DATA_FOR_ISSUE_MOBILE);
                                       }
                                } else {

                                }
                              },
                              decoration: InputDecoration(
                                  labelText: "Part No",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _partNoFocus.hasFocus
                                          ? partColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                              onChanged: (value) {
                                if (value.length == 0) {
                                  reset();
                                }
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Part No cannot be blank";
                                }
                              },
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: InkWell(
                                child: Image.asset("images/QRCode.png"),
                                onTap: () {
                                  scanItems(PubBackApi
                                      .API_GET_PART_DATA_FOR_ISSUE_MOBILE);
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Visibility(
                      visible: _isPartNo,
                      maintainState: true,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 10),
                            child: Text(
                              strPartDesc,
                              style: TextStyle(
                                  fontFamily: AppStrings.SEGOEUI_FONT,
                                  fontSize: 16),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 0.0, left: 12, right: 8, bottom: 8),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.baseline,
                              textBaseline: TextBaseline.ideographic,
                              children: <Widget>[
                                Expanded(
                                  flex: 9,
                                  child: TextFormField(
                                    focusNode: _workOrderFocus,
                                    controller: _workOrderController,
                                    textInputAction: TextInputAction.send,
                                    onFieldSubmitted: (value) {
                                      if (value.trim().length > 0) {
                                        if (isLoading) {
                                          isLoading = false;
                                          checkInternetConnection(
                                              value,
                                              PubBackApi
                                                  .API_GET_WO_MASTER_FOR_HELP);
                                        }
                                      } else
                                        {
                                          FocusScope.of(context).requestFocus(_workOrderFocus);
                                        Utility.showToastMsg("WorkOrder No cannot be blank");
                                        return;

                                      }
                                    },
                                    decoration: InputDecoration(
                                        labelText: "Work Order No",
                                        labelStyle: TextStyle(
                                            fontFamily: AppStrings.SEGOEUI_FONT,
                                            color: _workOrderFocus.hasFocus
                                                ? wordOrderColor
                                                : null),
                                        contentPadding:
                                            EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                    onChanged: (value) {
                                      if (value.length == 0) {
                                        reset();
                                      }
                                    },
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "WorkOrder No cannot be blank";
                                      }
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 15.0),
                                    child: InkWell(
                                      child: Image.asset("images/QRCode.png"),
                                      onTap: () {
                                        print("barcode clicked");
                                        scanItems(PubBackApi
                                            .API_GET_WO_MASTER_FOR_HELP);
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Visibility(
                            visible: _isWorkOrder,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                strWorkOrderDesc,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                          ),
                          Visibility(
                            visible: _isLocation,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8.0, left: 10.0, right: 5.0),
                                    child: TextFormField(
                                      readOnly: true,
                                      style: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      controller: _lineItemController,
                                      decoration: InputDecoration(
                                          labelText: "LineItem #:",
                                          hintStyle: TextStyle(
                                              color: Colors.grey[600],
                                              fontFamily:
                                                  AppStrings.SEGOEUI_FONT),
                                          labelStyle: TextStyle(
                                              color: Colors.grey[600],
                                              fontFamily:
                                                  AppStrings.SEGOEUI_FONT),
                                          contentPadding:
                                              EdgeInsets.only(top: 3)),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      top: 10.0,
                                      left: 5.0,
                                      right: 10.0,
                                    ),
                                    child: TextFormField(
                                      readOnly: true,
                                      style: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      controller: _issueQuantityController,
                                      decoration: InputDecoration(
                                          labelText: "Issued QTY:",
                                          labelStyle: TextStyle(
                                              color: Colors.grey[600],
                                              fontFamily:
                                                  AppStrings.SEGOEUI_FONT),
                                          contentPadding:
                                              EdgeInsets.only(top: 3)),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Visibility(
                            visible: isSerialNumberExist &&
                                _isWorkOrder &&
                                _isPartNo,
                            child: Column(children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Serial #",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 10,
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 3.0, left: 8.0),
                                child: Text(
                                  serialNumber,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            ]),
                          ),
                          Visibility(
                            visible: _isWorkOrder && _isPartNo && _isLocation,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 10.0, left: 12, right: 8, bottom: 8),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: TextFormField(
                                      enabled: isQuantityEnable,
                                      focusNode: _putbackQuantityFocus,
                                      controller: _putBackQuantityController,
                                      keyboardType: TextInputType.number,
                                      onChanged: (value) {
                                        if (value.length > 0) {
                                          if (partDetails.serialNumber ==
                                                  "false" &&
                                              locationDetail.stockRoom.length >
                                                  0) {
                                            colorStatus = true;
                                            setState(() {});
                                          } else {
                                            colorStatus = false;
                                            setState(() {});
                                          }
                                        }
                                        if (value.length == 0) {
                                          setState(() {
                                            colorStatus = false;
                                          });
                                        }
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Putback QTY:",
                                          labelStyle: TextStyle(
                                              fontFamily:
                                                  AppStrings.SEGOEUI_FONT,
                                              color:
                                                  _putbackQuantityFocus.hasFocus
                                                      ? quantityColor
                                                      : null),
                                          contentPadding:
                                              EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Quantity cannot be blank or less than equal to zero";
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible: _isWorkOrder && _isPartNo  && _isLocation,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 0.0, left: 12, right: 8, bottom: 8),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.baseline,
                                textBaseline: TextBaseline.ideographic,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: TextFormField(
                                      focusNode: _locationFocus,
                                      controller: _locationController,
                                      textInputAction: TextInputAction.send,
                                      onFieldSubmitted: (value) {
                                        if (value.length > 0) {
                                          if (isLoading) {
                                            isLoading = false;
                                            starLoader();
                                            getRoomFacForLocation(companyCode, userId,
                                                _locationController.text, iFacilityCode);
                                          }
                                        } else {}
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Putback Location",
                                          labelStyle: TextStyle(
                                              fontFamily:
                                                  AppStrings.SEGOEUI_FONT,
                                              color: _locationFocus.hasFocus
                                                  ? locationColor
                                                  : null),
                                          contentPadding:
                                              EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                      onChanged: (value) {
                                        if (value.length == 0) {
                                          hideLocation();
                                        }
                                      },
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Please enter Location";
                                        }
                                      },
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 15.0),
                                      child: InkWell(
                                        child: Image(
                                          image:
                                              AssetImage("images/QRCode.png"),
                                        ),
                                        onTap: () {
                                          scanItems(NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION);
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                            visible: _isWorkOrder && _isPartNo  && _isLocation,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 12.0),
                                  child: Text(
                                    "WareHouse :",
                                    style: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: Colors.black,
                                        fontSize: 15),
                                  ),
                                ),
                                Container(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 0.0, left: 12.0, right: 12.0),
                                    child: DropdownButton<RoomFacForLocation>(
                                      value: roomFacForLocation,
                                      icon: Icon(Icons.arrow_drop_down),
                                      iconSize: 24,
                                      isExpanded: true,
                                      elevation: 10,
                                      hint: Text(""),
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      underline: Container(
                                        height: 1,
                                        color: Colors.grey,
                                      ),
                                      onChanged: (RoomFacForLocation newValue) {
                                        setState(() {
                                          roomFacForLocation = newValue;
                                        });
                                      },
                                      items: roomFacForLocationList
                                          .map((dropdownValue) {
                                        return DropdownMenuItem<
                                                RoomFacForLocation>(
                                            value: dropdownValue,
                                            child: Text(
                                              dropdownValue.roomCode,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13.5),
                                            ));
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 12.0, right: 2),
                              child: FlatButton(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    "PUTBACK".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontSize: 17),
                                  ),
                                ),
                                color: getColorButton(true),
                                onPressed: () {
                                  _onSave();
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 12.0, left: 2),
                              child: FlatButton(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    "Cancel".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontSize: 17),
                                  ),
                                ),
                                color: AppColors.CYAN_BLUE_COLOR,
                                onPressed: () {
                                  onCancel();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        // ),
      ),
    );
  }

  Future scanItems(String title) async {
    try {
      var barcode = await BarcodeScanner.scan();
      if (title == PubBackApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
        setState(() {
          _partNoController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, title);
        });
      }
      if (title == PubBackApi.API_GET_WO_MASTER_FOR_HELP) {
        setState(() {
          _workOrderController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, title);
        });
      } else if (title == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
      {
        _locationController.text = barcode.rawContent;
        setState(() {
          if (isLoading) {
            isLoading = false;
            starLoader();
            getRoomFacForLocation(companyCode, userId,
                _locationController.text, iFacilityCode);
          }
        });
      }
      else if (title == IssueByWorkOrderApi.API_CHECK_SERIAL_NO) {
        setState(() {
          _serialNoController.text = barcode.rawContent;
          checkInternetConnection(
              barcode.rawContent, IssueByWorkOrderApi.API_CHECK_SERIAL_NO);
        });
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      Utility.showToastMsg(
          'null (user returned using the "back-button" before scanning)');
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  void checkInternetConnection(String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
            () {
          isLoading = true;
          dismissLoader();
        });
        print('check connection-------------->connected');
        if (type == PubBackApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
        if (type == PubBackApi.API_GET_WO_MASTER_FOR_HELP) {
          print("value-----------" + value);
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
        else if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO) {
          starLoader();
          getData().then((va) {
            getDetails(value, IssueByWorkOrderApi.API_CHECK_SERIAL_NO);
          });
        }
         else if (type == PubBackApi.API_SAVE_PUT_BACK_FROM_WORK_ORDER)
         {
          starLoader();
          getDetails(value, PubBackApi.API_SAVE_PUT_BACK_FROM_WORK_ORDER);
        }
      }
    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
        SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  Facility facility;

  Future<void> getData() async {
    print("iFacilityCode=---" + iFacilityCode);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
    employeeName =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
  }

  Future getDetails(String value, String type) async {
    print("data -->" + value);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";
    Map headers = new Map();
    print(NetworkConfig.BASE_URL);
    if (type == PubBackApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PubBackApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE;
      jsonBody = IssueByWorkOrderApi.getPartDataForIssueMobile(
          companyCode, value, iFacilityCode);
    }

    if (type == PubBackApi.API_GET_WO_MASTER_FOR_HELP) {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PubBackApi.API_GET_WO_MASTER_FOR_HELP;
      jsonBody = PubBackApi.getWOMasterForHelp(companyCode, value, "putback");

    }

    if (type == PubBackApi.API_GET_PART_DATA_FOR_PUT_BACK_MOBILE) {
      /*   static String   getPartDataForPutBackWoMobile (
          String companyCode,IssueByWOPartModal partDetails,   IssueByWorkOrderModal workOrder) {
*/

      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PubBackApi.API_GET_PART_DATA_FOR_PUT_BACK_MOBILE;
      jsonBody = PubBackApi.getPartDataForPutBackWoMobile(
          companyCode, partDetails, workOrder);
    }
    else if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO)
    {
      /*
        checkSerialNo
        companyCode: "108"   iPartNo: "17809"    serialNo: "test"    actionCode: 1
        actionSrNo: 0   stockSrNo: "53906"    quantity: 1     iWoNo: 0    iSalesOrderNo: 0      lineItemSrNo: 0
        controlId: "txtSerialNo"

         static String checkSerialNo(
          String companyCode, String iPartNo, String serialNo, String actionCode,String actionSrNo, String quantity,
          String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId
          ) {
       */

      headers = NetworkConfig.jsonHeaders;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderApi.API_CHECK_SERIAL_NO;
      jsonBody = IssueByWorkOrderApi.checkSerialNo( companyCode,  partDetails.iPartNo,  value,    "1",
          "0",
          locationDetail.stockSrNo,
          "1",
          workOrder.iWONo,
          "0",
          "0",
          "txtSerialNo");
    } else if (type == PubBackApi.API_SAVE_PUT_BACK_FROM_WORK_ORDER) {


      print("location--->" + _locationController.text.toString());
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PubBackApi.API_SAVE_PUT_BACK_FROM_WORK_ORDER;
      jsonBody = PubBackApi.savePutBackFromWo(
          companyCode,
          partDetails,
          workOrder,
          _putBackPart,
          roomFacForLocation,
          _putBackQuantityController.text.toString(),
          userId,
          _locationController.text.toString());
      print("jsonBody---" + jsonBody);

    }
    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response =
        await post(uri, headers: headers, body: jsonBody, encoding: encoding)
            .then((resp) {
              isLoading = true;
              print(resp.statusCode);
              if (resp.statusCode == 200) {
                String responseBody = resp.body;
                print("----------" + resp.body);
                onSuccessResponse(type, responseBody);
              } else {
                isLoading = true;
                onErrorResponse(type, resp.body);
              }
            })
            .catchError((error) {
              isLoading = true;
              print(error.toString());
              onErrorResponse(type, error.toString());
            })
            .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
            .catchError((e) {
              isLoading = true;
              dismissLoader();
              Utility.showToastMsg("Error-->" + e.toString());
            });
  }

  onSuccessResponse(String type, String value) {
    isLoading = true;
    if (type == PubBackApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        Map map = jsonDecode(value);
        Map map1 = map["NewDataSet"];
        partDetails = IssueByWOPartModal.fromJson(map1["Table"]);
        print("Part Description--->" + partDetails.partDescription);
        isSerialPart = false;
        _locationController.text = "";
        _isLocation = false;
        _isPartNo = true;
        strPartDesc = partDetails.partDescription;
        if (partDetails != null && partDetails.iPartNo.length > 0) {
          if (partDetails.serialNumber.toString() == "true") {
            isSerialNumberExist = true;
            _putBackQuantityController.text = "1";
          } else {
            isQuantityEnable = true;
            _putBackQuantityController.text = "";
            isSerialNumberExist = false;
          }
        }
        strIPartNo = partDetails.iPartNo;
        setState(() {});
        print("IPart No--->" + strIPartNo);
        print("value--->" + value);
      } else {
        print("show Error");
        String error = errorMessage(type);
        onErrorResponse(type, error);
      }
      return;
    }
    if (type == PubBackApi.API_GET_WO_MASTER_FOR_HELP) {
      showMessage(false, "", Colors.white);
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        Map map = jsonDecode(value);
        Map map1 = map["NewDataSet"];
        workOrder = IssueByWorkOrderModal.fromJson(map1["Table"]);
        print("Work Order description---->" + workOrder.workOrderDescription);
        print(workOrder.iFacilityCode);
        print(iFacilityCode);
        if (workOrder.iFacilityCode != iFacilityCode)
        {
          String error = errorMessage(type);
          onErrorResponse(type, error);
          return;
        } else {
          _isWorkOrder = true;
          strWorkOrderDesc = workOrder.workOrderDescription;
          setState(() {
            getDetails("", PubBackApi.API_GET_PART_DATA_FOR_PUT_BACK_MOBILE);
          });
        }
      } else {
        if (value == "0") {
          print("show Error");
          String error = errorMessage(type);
          onErrorResponse(type, error);
        }
      }
      return;
    }
    if (type == PubBackApi.API_GET_PART_DATA_FOR_PUT_BACK_MOBILE)
    {
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      Map data2;
      value = parsedXml.findElements("string").first.text;
      if (value == "0") {
        reset();
        Utility.showToastMsg(
            "Part No does not exist or not issued for this WorkOrder!");
        return;
      }
      Map data1;
      try {
        data1 = jsonDecode(value);
      } catch (ex) {
        print(ex);
      }
      try {
        data2 = data1['NewDataSet'];
      } catch (ex) {
        print(ex);
      }
      List<dynamic> data;
      try {
        data = data2['Table'];
      } catch (ex) {
        print("data in single");
      }
      if (data != null && data.length > 0) {
        print(" multiple array  data  --");
        putBackPartList = new List();
        if (data.length > 0)
        {
          for (int i = 0; i < data.length; i++)
          {
            Map dat = data.elementAt(i);
            PutBackPart temp = PutBackPart.fromJson(dat);
            putBackPartList.add(temp);
          }
          if(putBackPartList.length>1)
          {
            showSelectedLocationDialog(context);
          }
        }
      }
      else {
        print(" single data  --");
        putBackPartList = new List();
        _putBackPart = PutBackPart.fromJson(data2['Table']);
        putBackPartList.add(_putBackPart);
        _isWorkOrder = true;
        strWorkOrderDesc = workOrder.workOrderDescription;
        setState(() {
          _putBackPart = putBackPartList.elementAt(0);
          _lineItemController.text = _putBackPart.lineItemSrNo;
          _issueQuantityController.text = _putBackPart.actualQuantity;
          _locationController.text = _putBackPart.roomAreaCode;
          int putBackQuantity = 0;
          print("putback quauttiy---" + _putBackPart.putBackQuantity);
          if (StringUtil.isInt(_putBackPart.putBackQuantity)) {
            putBackQuantity = StringUtil.tryParse(_putBackPart.putBackQuantity);
          }
          else{
            print("putback quauttiy---" + putBackQuantity.toString());
            if (StringUtil.isDouble(_putBackPart.putBackQuantity))
            {
              print("true---") ;
              double d=    StringUtil.tryParse(_putBackPart.putBackQuantity);
              putBackQuantity =d.round();
            }
            else{
              print("false---") ;
            }
          }
          _putBackQuantityController.text = putBackQuantity.toString();
          strWorkOrderDesc = workOrder.workOrderDescription;
          print("before ---" + _putBackPart.serialNo);
          serialNumber = _putBackPart.serialNo;
          print("after ---" + _putBackPart.serialNo);
          getRoomFacForLocation(companyCode, userId,
              _locationController.text, iFacilityCode);
        });
      }
    }

    else if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO)
    {
      print("data for parsing" + value);
      Map map;
      try {
        map = jsonDecode(value);
      } catch (ex) {
        print(ex);
      }
      String jsonData = map["d"];
      List<dynamic> data = jsonDecode(jsonData);
      barcodeItem.clear();
      for (int i = 0; i < data.length; i++) {
        Map map = data.elementAt(i);
        SerialNumberStatus status = SerialNumberStatus.fromJson(map);
        print(status.serialNo);
        print(status.controlId);
        print(status.type);
        barcodeItem.add(status);
      }
      status = barcodeItem.elementAt(0);
      print(status.type);
      if (status.type == "success") {
        colorStatus = true;
        setState(() {
          colorStatus = true;
        });
      } else {
        colorStatus = false;
        _serialNoController.text = "";
        _serialNoController.text = "";
        onErrorResponse(type, status.message);
        return;
      }
      return;
    }
    else if (type == PubBackApi.API_SAVE_PUT_BACK_FROM_WORK_ORDER) {
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value == "Part putback successfully!")
      {
        Utility.showSuccessToastMsg(value);
        reset();
        return;
      }
      else {
        Utility.showToastMsg(value);
        return;
      }
    }
  }
  onErrorResponse(String type, String error) {
    dismissLoader();
    if (type == PubBackApi.API_GET_WO_MASTER_FOR_HELP) {
      Utility.showToastMsg(error.toString());
      reset();
      return;
    }
    if (type == PubBackApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      _partNoController.text = "";
      FocusScope.of(context).requestFocus(_partNoFocus);
      setState(() {});
      Utility.showToastMsg(error);
      hideLocationAndSerialNumber();
    }
    if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO) {
      Utility.showToastMsg(error);
      FocusScope.of(context).requestFocus(_serialNumberFocus);
      setState(() {});
    }

    if (type == PubBackApi.API_SAVE_PUT_BACK_FROM_WORK_ORDER) {
      Utility.showToastMsg(error);
      return;
    }
  }

  _onSave() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
      setState(() {
        _validate = true;
      });
    }


    if (_workOrderController.text.trim().length == 0)
    {
      FocusScope.of(context).requestFocus(_workOrderFocus);
      return;
    }
    if (_partNoController.text .trim().length == 0) {
      FocusScope.of(context).requestFocus(_partNoFocus);
      return;
    }

    if (_locationController.text.trim().length == 0) {
      FocusScope.of(context).requestFocus(_locationFocus);
      return;
    }

    if (roomFacForLocation == null) {
      FocusScope.of(context).requestFocus(_locationFocus);
      Utility.showToastMsg("Sync for location");
      return;
    }

    if (roomFacForLocation.roomAreaCode !=_locationController.text.trim().toString())
    {
      FocusScope.of(context).requestFocus(_locationFocus);
      Utility.showToastMsg("Location Does not Exist for Facility");
      return;
    }
    if (_putBackQuantityController.text.length == 0) {
      setFocus(_putbackQuantityFocus);
      return;
    }
    if (workOrder == null) {
      FocusScope.of(context).requestFocus(_workOrderFocus);
      Utility.showToastMsg("Sync for work order details ");
      return;
    }
    if (partDetails == null) {
      FocusScope.of(context).requestFocus(_partNoFocus);
      Utility.showToastMsg("Sync for Part Details ");
      return;
    }


    if (partDetails == null) {
      FocusScope.of(context).requestFocus(_partNoFocus);
      Utility.showToastMsg("Sync for Part Details ");
      return;
    }

    int putBackQuantity = 0;
    int issueQty = 0;
    if (StringUtil.isInt(_issueQuantityController.text))
    {
      issueQty =
          StringUtil.tryParse(_issueQuantityController.text);
    }
    else{
      if (StringUtil.isDouble(_issueQuantityController.text))
      {
        double d =
            StringUtil.tryParse(_issueQuantityController.text);
        issueQty  =d.round();
      }
    }

    if (StringUtil.isInt(_putBackQuantityController.text))
    {
      putBackQuantity =
          StringUtil.tryParse(_putBackQuantityController.text);
    }
    else{
      if (StringUtil.isDouble(_putBackQuantityController.text))
      {
        double d =
            StringUtil.tryParse(_putBackQuantityController.text);
        putBackQuantity =d.round();
      }
    }

    print(putBackQuantity);
    print(issueQty);


    if(putBackQuantity<=0 )
    {

      Utility.showToastMsg("Putback quantity cannot be less than equal to zero");
      setFocus(_putbackQuantityFocus);
      return;
    }

    if(putBackQuantity>issueQty)
    {

      Utility.showToastMsg("PutBack quantity cannot be greater than Issued quantity");
      return;
    }

    checkInternetConnection("", PubBackApi.API_SAVE_PUT_BACK_FROM_WORK_ORDER);
  }

  void sysError() {
    if (workOrder == null) {
      FocusScope.of(context).requestFocus(_workOrderFocus);
      Utility.showToastMsg("Sync for work order details ");
      return;
    }
    if (partDetails == null) {
      FocusScope.of(context).requestFocus(_partNoFocus);
      Utility.showToastMsg("Sync for Part Details ");
      return;
    }
    if (locationDetail == null) {
      FocusScope.of(context).requestFocus(_locationFocus);
      Utility.showToastMsg("Sync for location for stock room details ");
      return;
    }
    if (partDetails != null && partDetails.serialNumber == "true") {
      if (status == null && status.type == null) {
        FocusScope.of(context).requestFocus(_serialNumberFocus);
        Utility.showToastMsg("Sync for serial numberfor details ");
        return;
      }
    }
  }
  void testData(int type)
    {
    //    _partNoController.text = "Rk101";
    //  _workOrderController.text = "WO113780";
    //_partNoController.text = "RK05";
    //_workOrderController.text = "WO113777";


    if (type == 1) {
      _partNoController.text = "P01";
      _workOrderController.text = "WO113663";
    }
  }

  onCancel() {
    isLoading = true;
    String strWorkOrder = _workOrderController.text;
    String strPartNo = _partNoController.text;
    String strLocation = _locationController.text;
    String strQuantity = _putBackQuantityController.text;
    if (strWorkOrder.length == 0 &&
        strPartNo.length == 0 &&
        strQuantity.length == 0 &&
        strLocation.length == 0) {
      this
          .widget
          .callBack
          .viewType(DashboardView(this.widget.callBack), AppStrings.LABEL_HOME);
    } else {
      showMessage(false, "", Colors.white);
      reset();
    }
  }

  reset() {
    colorStatus = false;
    _workOrderController.text = "";
    _partNoController.text = "";
    _putBackQuantityController.text = "";
    _locationController.text = "";
    locationDetail = null;
    partDetails = null;
    workOrder = null;
    partDetails = null;
    _putBackPart =null;
    roomFacForLocation =null;
    //dymicList.clear();
    roomFacForLocationList.clear();
    _serialNoController.text = "";
    _validate = false;
    setState(() {
      _isWorkOrder = false;
      _isPartNo = false;
      isSerialPart = false;
      isQuantityEnable = true;
    });
  }

  hideLocationAndSerialNumber() {
    colorStatus = false;
    _partNoController.text = "";
    _putBackQuantityController.text = "";
    _locationController.text = "";
    _serialNoController.text = "";
    _isLocation = false;
    _isPartNo = false;
    isSerialPart = false;
    isQuantityEnable = true;
    if (partDetails != null) {
      partDetails = null;
    }
    if (locationDetail != null) {
      locationDetail = null;
    }
    setState(() {});
  }

  hideLocation() {
    colorStatus = false;
    _locationController.text = "";
    locationDetail = null;
    _serialNoController.text = "";
  /*  setState(() {
      _isLocation = false;
    });*/
  }

  void starLoader() {
    print("dialog started");
    if (!isLoaderRunning) {
      isLoaderRunning = true;
      Dialogs.showLoadingDialog(context, _formLocationKey);
    }
  }
  void dismissLoader()
  {
    if (isLoaderRunning) {
      isLoaderRunning = false;
      Navigator.of(_formLocationKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }
  String errorMessage(String type) {
    String error = "";
    if (type == PubBackApi.API_GET_WO_MASTER_FOR_HELP) {
      error =
          "WorkOrder does not exist or not in Open, Approved or Scheduled status !";
    }
    if (type == PubBackApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      error = "Part No does not exist or is inactive!";
    }
    return error;
  }
  Color getColorButton(bool status) {
    if (status)
      return AppColors.CYAN_BLUE_COLOR;
    else
      return Colors.grey;
  }

  void showMessage(bool status, String msg, Color color) {
    serverRespone = status;
    serverResponse = msg;
    serverResponeColor = color;
    setState(() {});
  }

  Future<void> getRoomFacForLocation(String companyCode, String userId,
      String roomAreaCode, String iFacilityCode) async {
    print(" get Room Fac  For Location --");
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION;
    String jsonBody = NetworkConfig.getRoomFacForLocation(
        companyCode, userId, roomAreaCode, iFacilityCode);

    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);

    if (statusCode == 200)
    {
      isLoading = true;
      dismissLoader();
      String responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
       Map data1 ,data2;
       try{
         data1 = jsonDecode(responseBody);
         print("data1 size  "+data1.length.toString());

       }catch(e ){
         print("data1 size  "+e.toString());
         Utility.showToastMsg(
             "Location Does not Exist for Facility");
         roomFacForLocationList = new List();
         roomFacForLocation =null;
         setState(() {

         });
         return;
       }
       try{
        data2 = data1['NewDataSet'];
      }catch(e ){
        print("data1 size  "+e.toString());
        Utility.showToastMsg(
            "Location Does not Exist for Facility");
        return;
       }

      List<dynamic> data = [];
      try {
        data = data2['Table'];
        roomFacForLocationList = new List();
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++)
          {
            Map dat = data.elementAt(i);
            RoomFacForLocation temp = RoomFacForLocation.fromJson(dat);
            roomFacForLocationList.add(temp);
            if (i == 0)
            {
              roomFacForLocation = temp;
              print(
                  "---------------" + roomFacForLocation.iLocationRoomFacility);
            }
          }
          setState(()
          {
            _isLocation = true;
          });
        } else {
          print("Wrong input");
          Utility.showToastMsg(
              "Please Select Faclility and  Enter Correct Bin No.");
        }
      } catch (e)
      {
        roomFacForLocationList = new List();
        isLoading = true;
        dismissLoader();
        _isLocation = true;
        roomFacForLocation = RoomFacForLocation.fromJson(data2['Table']);
        roomFacForLocationList.add(roomFacForLocation);
        setState(()
        {
          _isLocation = true;
          setFocus(_putbackQuantityFocus);
        });
      }
    }
  }
  Future setFocus(FocusNode focus) async
  {
    Future.delayed(Duration(microseconds: 25), ()
    {
      setState(()
      {
        FocusScope.of(context).requestFocus(focus);
      });
    });
  }

  void showSelectedLocationDialog(BuildContext context )
  {
    PutBackByWorkLocationDialog putAwayLocationDialog = new PutBackByWorkLocationDialog(callback: this,
        putBackPartList: putBackPartList,partNo:_partNoController.text,workOrderNo:_workOrderController.text,
        );
    showDialog(
        context: context,
        builder: (context) {
          return  putAwayLocationDialog;
        });
  }
  @override
  void onItemSelect(PutBackPart putBackPart)
    {
      _putBackPart=putBackPart;
      _locationController.text =_putBackPart.roomAreaCode;
      setDataOnField(putBackPart);
      print("room code selected from the lineitem number"+putBackPart.roomAreaCode);
      if (isLoading)
      {
      isLoading = false;
    //  starLoader();
    //  starLoader();
      getRoomFacForLocation(companyCode, userId,
          putBackPart.roomAreaCode, iFacilityCode);
    }
  }
  void setDataOnField(PutBackPart putBackPart)
  {
    setFocus(_putbackQuantityFocus);
    setState(()
     {
      _lineItemController.text = _putBackPart.lineItemSrNo;
      _issueQuantityController.text = _putBackPart.actualQuantity;
      int putBackQuantity = 0;
      if (StringUtil.isInt(_putBackPart.putBackQuantity))
      {
        putBackQuantity =
            StringUtil.tryParse(_putBackQuantityController.text);
      }
      if (StringUtil.isDouble(_putBackQuantityController.text))
      {
        putBackQuantity =
            StringUtil.tryParse(_putBackQuantityController.text);
      }
      _putBackQuantityController.text = putBackQuantity.toString();
      print(_putBackPart.serialNo);
      serialNumber = _putBackPart.serialNo;
      print(serialNumber);
    });
  }
}
