import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:im3_warehouse/models/dashboard_modal.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/facility/facility_dialog.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/issue_by_pick_list/issue_by_pick_list.dart';
import 'package:im3_warehouse/views/issue_by_wo_or_task/issue_by_wo_or_task.dart';
import 'package:im3_warehouse/views/issue_by_work_order/issue_by_work_order.dart';
import 'package:im3_warehouse/views/issue_for_equipement/issue_for_equipement.dart';
import 'package:im3_warehouse/views/issue_to_employee/issue_to_employee.dart';
import 'package:im3_warehouse/views/pick_my_order/pick_my_order.dart';
import 'package:im3_warehouse/views/putaway/putaway_items.dart';
import 'package:im3_warehouse/views/putback/putback.dart';
import 'package:im3_warehouse/views/receivebypart/receivebypart.dart';
import 'package:im3_warehouse/views/reversereceive/reverse_receive.dart';
import 'package:im3_warehouse/views/transfer/tranfer.dart';
import 'package:im3_warehouse/views/physical_count/physicalCount.dart';
import 'package:im3_warehouse/views/receive_by_po/receive_by_po.dart';
import 'package:im3_warehouse/views/transfer_pallet/tranfer_pallet.dart';
import 'package:im3_warehouse/views/view_inventory/view_inventory.dart';



class DashboardView extends StatefulWidget {
  ViewTypeCallBack callBack;
  DashboardView(this.callBack);
  @override
  State<StatefulWidget> createState() => DashboardState();
}

class DashboardState extends State<DashboardView> {
  double deviceWidth;
  double deviceHeight;
  DashBoardModal choice;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    deviceWidth = MediaQuery.of(context).size.width;
    deviceHeight = MediaQuery.of(context).size.height;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(
          child: Card(
            color: Colors.white,
            elevation: 0,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Image(
                      image: AssetImage("images/im3-big.png",),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    color: Colors.green,
                    child: Center(child: Text("Layout for data")),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: GridView.count(
                shrinkWrap: true,
                mainAxisSpacing: 10,
                physics: ScrollPhysics(),
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                crossAxisCount: 4,
                children: List.generate(choices.length, (index) {
                  return Center(
                    child: InkWell(
                      child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Expanded(
                              child: Image(
                                image: AssetImage(choices[index].imageName),
                                color: choices[index].colorName,
                                height: 30,
                                width: 30,
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 70,
                                child: Text(
                                  choices[index].title,
                                  style: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      fontSize: 12),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ]),
                      onTap: () {
                        String strTitle = choices[index].title;
                        print(strTitle + " clicked...");
                        if (strTitle == AppStrings.LABEL_FACILITY)
                        {
                          showDialog(
                              context: context,
                              builder: (context) => FacilityDialog(viewTypeCallBack:this.widget.callBack,));
                        } else if (strTitle ==
                            AppStrings.LABEL_PHYSICAL_COUNT) {
                          this.widget.callBack.viewType(PhysicalCountApp(this.widget.callBack),
                              AppStrings.LABEL_PHYSICAL_COUNT);
                        }
                        else if (strTitle ==
                            AppStrings.LABEL_ISSUE_BY_PICKLIST) {
                          this.widget.callBack.viewType(
                              IssueByPickListPage(this.widget.callBack),
                              AppStrings.LABEL_ISSUE_BY_PICKLIST);
                        }
                        else if (strTitle ==
                            AppStrings.LABEL_PICK_MY_ORDER)
                        {
                          this.widget.callBack.viewType(
                              PickMyOrder(this.widget.callBack),
                            //  MyApp(),
                              AppStrings.LABEL_PICK_MY_ORDER);
                        }
                        else if (strTitle == AppStrings.LABEL_PUTAWAY_ITEMS) {
                          this.widget.callBack.viewType(
                              PutAwayItems(this.widget.callBack),
                              AppStrings.LABEL_PUTAWAY_ITEMS);
                        } else if (strTitle ==
                            AppStrings.LABEL_ISSUE_BY_WORK_ORDER) {
                          this.widget.callBack.viewType(
                              IssueByWorkOrder(this.widget.callBack),
                              AppStrings.LABEL_ISSUE_BY_WORK_ORDER);
                        } else if (strTitle ==
                            AppStrings.LABEL_ISSUE_BY_WO_OR_TASK) {
                          this.widget.callBack.viewType(
                              IssueByWoOrTask(this.widget.callBack),
                              AppStrings.LABEL_ISSUE_BY_WO_OR_TASK);
                        } else if
                        (strTitle ==
                            AppStrings.LABEL_REVERSE_RECEIVE) {
                          this.widget.callBack.viewType(ReverseReceive(callBack:this.widget.callBack),
                              AppStrings.LABEL_REVERSE_RECEIVE);
                        }
                        else if (strTitle == AppStrings.LABEL_TRANSFER) {
                          this.widget.callBack.viewType(
                              Transfer(this.widget.callBack),
                              AppStrings.LABEL_TRANSFER);
                        } else if (strTitle ==
                            AppStrings.LABEL_RECEIVE_BY_PART) {
                          this.widget.callBack.viewType(ReceiveByPart(callBack:this.widget.callBack),
                              AppStrings.LABEL_RECEIVE_BY_PART);
                        } else if (strTitle ==
                            AppStrings.LABEL_ISSUE_FOR_EQUIPEMENT) {
                          this.widget.callBack.viewType(
                              IssueForEquipment(this.widget.callBack),
                              AppStrings.LABEL_ISSUE_FOR_EQUIPEMENT);
                        } else if (strTitle == AppStrings.LABEL_RECEIVE_BY_PO)
                        {
                          this.widget.callBack.viewType(
                              ReceiveByPo(callBack:this.widget.callBack),
                              AppStrings.LABEL_RECEIVE_BY_PO);
                        }

                        else if (strTitle == AppStrings.LABEL_PUTBACK)
                        {
                          this
                              .widget
                              .callBack
                              .viewType(PutBack(this.widget.callBack), AppStrings.LABEL_PUTBACK);
                        }




                        else if (strTitle == AppStrings.LABEL_ISSUE_TO_EMPLOYEE)
                        {
                          this
                              .widget
                              .callBack
                              .viewType(IssueToEmployee(this.widget.callBack), AppStrings.LABEL_ISSUE_TO_EMPLOYEE);
                        }

                        else if (strTitle == AppStrings.LABEL_TRANSFER_PALLET)
                        {
                          this
                              .widget
                              .callBack
                              .viewType(TransferPallet(this.widget.callBack), AppStrings.LABEL_TRANSFER_PALLET);
                        }


                        else if (strTitle == AppStrings.LABEL_VIEW_INVENTORY)
                        {
                          this
                              .widget
                              .callBack
                              .viewType(ViewInventory(callBack:this.widget.callBack,context: context, ), AppStrings.LABEL_VIEW_INVENTORY);
                        }

                      },
                    ),
                  );
                })),
          ),
        ),
      ],
    );
  }
}
const List<DashBoardModal> choices = const <DashBoardModal>[
  const DashBoardModal(
      title: AppStrings.LABEL_FACILITY,
      imageName: AppStrings.IMAGE_FACILITY,
      colorName: Colors.teal),
  const DashBoardModal(
    title: AppStrings.LABEL_PHYSICAL_COUNT,
    imageName: "images/cloud_computing.png",
    colorName: Colors.purple,
  ),
  const DashBoardModal(
      title: AppStrings.LABEL_TRANSFER,
      imageName: AppStrings.IMAGE_TRANSFER,
      colorName: Colors.teal),
  const DashBoardModal(
    title: AppStrings.LABEL_ISSUE_BY_PICKLIST,
    imageName: "images/settingsnote.png",
    colorName: Colors.amberAccent,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_PICK_MY_ORDER,
    imageName: "images/settingsnote.png",
    colorName: Colors.amberAccent,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_ISSUE_BY_WORK_ORDER,
    imageName: "images/settingsnote.png",
    colorName: Colors.brown,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_ISSUE_BY_WO_OR_TASK,
    imageName: "images/settingsnote.png",
    colorName: Colors.grey,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_ISSUE_FOR_EQUIPEMENT,
    imageName: "images/settingsnote.png",
    colorName: Colors.green,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_PUTBACK
    ,
    imageName: "images/network.png",
    colorName: Colors.cyan,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_RECEIVE_BY_PO,
    imageName: "images/document.png",
    colorName: Colors.red,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_RECEIVE_BY_PART,
    imageName: "images/document.png",
    colorName: Colors.orange,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_REVERSE_RECEIVE,
    imageName: "images/transfer.png",
    colorName: Colors.blue,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_PUTAWAY_ITEMS,
    imageName: "images/network.png",
    colorName: Colors.indigo,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_ISSUE_TO_EMPLOYEE,
    imageName:  "images/settingsnote.png",
    colorName: AppColors.CYAN_BLUE_COLOR,
  ),
  const DashBoardModal(
    title: AppStrings.LABEL_TRANSFER_PALLET,
    imageName: AppStrings.IMAGE_TRANSFER,
    colorName: Colors.indigo,
  ),

  const DashBoardModal(
    title: AppStrings.LABEL_VIEW_INVENTORY,
    imageName:  "images/settingsnote.png",
    colorName: Colors.green,
  ),



];
