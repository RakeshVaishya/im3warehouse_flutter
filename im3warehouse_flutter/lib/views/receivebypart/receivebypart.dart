import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_item_detail.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_list.dart';
import 'package:im3_warehouse/network/api/recieve_by_po_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/repair_request/Dialogs.dart';
import 'package:xml/xml.dart' as xml;
import 'dart:core';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'dart:async';
import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/models/reserves_part_location.dart';

import 'reciecve_by_part_activity.dart';


class ReceiveByPart extends StatefulWidget {

  ViewTypeCallBack callBack;
  ReceiveByPart({this.callBack});
  static  List< PoDetail > partList;

  @override
  _ReceiveByPartState createState() => _ReceiveByPartState();
}



class _ReceiveByPartState extends State<ReceiveByPart> {
  var _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool isRecordVisible = false;
  bool isData = false;
  FocusNode searchFocus = FocusNode();
  bool isAvailable = false;
  TextEditingController _partNumberController = TextEditingController();
  Color wordOrderColor;
  Color partColor;
  Color locationColor;
  Color quantityColor;
  bool isLoaderRunning = false;
  bool isSerialPart = false;
  bool colorStatus = true;
  Facility facility;
  var _formLocationKey = GlobalKey<FormState>();
  List<RoomFacForLocation> roomFacForLocationList = [];
  RoomFacForLocation roomFacForLocation;

  String userId = "";
  String companyCode = "";
  String iFacilityCode = "";
  String strWorkOrderDesc = "";
  String strPartDesc = "part Desc";
  String strIPartNo = "";
  String strOnHandQuantity = "";
  String strStockRoom = "";
  String employeeName = "";
  List< PoDetail > partList = [];
  POItemDetails  partItemDetail;
  DynamicHelp selectedDynamicHelp;
  ReservesPartLocation reservePartLocation;
  BuildContext context;
  bool isLoading = true;
  bool serverRespone = false;
  Color serverResponeColor;
  String serverResponse = "";
  String serialNumber = "";
  PoDetail partDetail;

  @override
  void initState() {
    super.initState();


    if(ReceiveByPart.partList !=null)
    {
      print("not null"+ReceiveByPart.partList.length.toString());
      if (ReceiveByPart.partList.length != 0)
      {
        partList = ReceiveByPart.partList;
        _partNumberController.text = partList
            .elementAt(0)
            .poNo;
        isRecordVisible = true;
      }
    }
    getData();
    _partNumberController.addListener(() {
      setState(() {
        isData = false;
      });
    });
  }
  Future<void> getData() async {
    String data =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    facility = Facility.fromJson(map);
    print("retrived from SH file--->" + facility.iFacilityCode);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);
    employeeName =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
  }

  @override
  Widget build(BuildContext context)
  {

    this.context =context;
    return fromWidget(context);

  }
  Widget fromWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: this._formKey,
        autovalidate: _validate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 5, right: 5),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                textBaseline: TextBaseline.ideographic,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child :  Padding(
                      padding: isAvailable
                          ? const EdgeInsets.only(right: 5, left: 5, bottom: 20)
                          : EdgeInsets.only(right: 5, left: 5, bottom: 0),
                      child: TextFormField(
                        focusNode: searchFocus,
                        style: TextStyle(color: Colors.black),
                        controller: _partNumberController,
                        textInputAction: TextInputAction.send,
                        onFieldSubmitted: (text) {
                          if (text.trim().length > 0)
                          {
                            if(isLoading)
                            {
                              isLoading =false;
                              checkInternetConnection(
                                  text, ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO);
                            }
                          }
                          else{
                            Utility.showToastMsg("Please enter part number!");
                            return;
                          }
                        },
                        decoration: InputDecoration(
                            labelText: "Part #:",
                            labelStyle: TextStyle(color: Colors.black87),
                            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0)),
                        onChanged: (value)
                        {
                        },
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: InkWell(
                          child: Image.asset(
                            "images/QRCode.png",
                          ),
                          onTap: ()
                          {
                            barCodeScan( ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO);
                          },
                        ),
                      ))

                  /*    Expanded(
                    child: Padding(
                      padding: isData
                          ? EdgeInsets.only(top: 9.0, bottom: 20)
                          : EdgeInsets.only(top: 9.0, bottom: 0),
                      child: Stack(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              right: 4.0,
                            ),

                            child: MaterialButton(
                              onPressed: () {
                                onSearch();
                              },
                              color: AppColors.CYAN_BLUE_COLOR,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Search".toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 17.0,
                                      color: Colors.white,
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 4.0, top: 38),
                            child: Divider(
                              thickness: 1,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
              */
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 15.0, bottom: 5.0, right: 8.0, left: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          if(_partNumberController.text.length==0){
                            Utility.showToastMsg("Please enter part number!");
                            return;
                          }
                          else {
                            onSearch(_partNumberController.text);
                          }
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppStrings.LABEL_SEARCH.toUpperCase(),
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Colors.white,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          print("on cancel called");
                          if (_partNumberController.text.length == 0)
                          {
                            print("on back  ");
                            this
                                .widget
                                .callBack
                                .viewType(DashboardView(this.widget.callBack), AppStrings.LABEL_HOME);
                          }
                          else {
                            _partNumberController.text ="";
                            partList.clear();
                            ReceiveByPart.partList =null;
                            isRecordVisible =false;
                          }
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppStrings.LABEL_CANCEL.toUpperCase(),
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Colors.white,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Visibility(
                visible: isRecordVisible,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding:
                      const EdgeInsets.only(left: 8.0, right: 8, top: 5),
                      child: Container(
                        color: Colors.grey[200],
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(
                                5.0,
                              ),
                              child: Text(
                                "Select One of Below",
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            )),
                      ),
                    ),
                    Padding(
                      padding:
                      const EdgeInsets.only(top: 0.0, left: 5, right: 5),
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        child: ListView.builder(
                          itemCount: partList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              child: Card(
                                  elevation: 2,
                                  child: Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 5,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "PO #: ",
                                                    style: TextStyle(
                                                        color: Colors.grey[600],
                                                        fontFamily: AppStrings
                                                            .SEGOEUI_FONT),
                                                  ),
                                                  Text(
                                                    partList.elementAt(index).poNo,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontFamily: AppStrings
                                                            .SEGOEUI_FONT),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Order Qty: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      partList.elementAt(index).issueUOMOrderQuantity,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Release No: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      partList.elementAt(index).releaseNo,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding:
                                          const EdgeInsets.only(top: 8.0),
                                          child: Container(
                                              height: 80,
                                              child: VerticalDivider(
                                                  color: Colors.grey)),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text("Line #: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      partList.elementAt(index).lineItemSrNo,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Received Qty: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      partList.elementAt(index).receivingQty,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Release Qty: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      partList.elementAt(index).releaseQuantity,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                              onTap: ()
                              {partDetail=partList.elementAt(index);
                              if(isLoading) {
                                isLoading =false;
                                checkInternetConnection(
                                    "", ReceiveByPoApi.API_GET_RECIEVE_PO_ITEM_DETAILS);
                              }



                                /*    ReceiveByPOModal receiveByPOModal =
                                new ReceiveByPOModal();
                                this.widget.callBack.viewType(
                                    ReceiveByPoActivity(
                                        receiveByPOModal, receiveByPOList),
                                    AppStrings.LABEL_RECEIVE_BY_PO_ACTIVITY);
                            */


                              },
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }

  onSearch(String partNumber)
  {
    if(isLoading) {
      isLoading =false;
      checkInternetConnection(
          partNumber, ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO);
    }

  }

  void checkInternetConnection(String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
                () {
              isLoading = true;
              dismissLoader();
            });
        print('check connection-------------->connected');
        if (type == ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO)
        {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
        if (type == ReceiveByPoApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
        {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
        if (type == ReceiveByPoApi.API_IS_PART_SERIALIZED)
        {
          //starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
        {
          // starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
      }

    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }
  Future barCodeScan(String type) async {
    try {
      var barcode = await BarcodeScanner.scan();
      if(barcode.rawContent.length==0){
        return;
      }
      if(isLoading)
      {
        isLoading =false;
        checkInternetConnection(
            barcode.rawContent, ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO);
      }
      setState(() {
        _partNumberController.text =barcode.rawContent;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
    SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  onCancel() {


  }

  void starLoader()
  {
    print("dialog started");
    if (!isLoaderRunning) {
      isLoaderRunning = true;
      Dialogs.showLoadingDialog(context, _formLocationKey);
    }
  }

  void dismissLoader()
  {
    if (isLoaderRunning)
    {
      isLoaderRunning = false;
      Navigator.of(_formLocationKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }

  Future getDetails(String value, String type) async {
    print("data -->" + value);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";
    Map headers = new Map();
    print(NetworkConfig.BASE_URL);

    if (type == ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO)
    {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO;

      /* Po number blank */
      jsonBody = ReceiveByPoApi.getAllReceiveAgainstPo(
          userId,  companyCode,  "",value,facility);
    }

    if (type == ReceiveByPoApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
    {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReceiveByPoApi.API_GET_RECIEVE_PO_ITEM_DETAILS;
      jsonBody = ReceiveByPoApi.getReceivePOItemDetail(
          companyCode, partDetail.iPONo, partDetail.lineItemSrNo);
    }
    if (type == ReceiveByPoApi.API_IS_PART_SERIALIZED)
    {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReceiveByPoApi.API_IS_PART_SERIALIZED;
      jsonBody = ReceiveByPoApi.isPartSerialized(
          companyCode,value);
    }
    if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
    {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION;
      jsonBody = NetworkConfig.getRoomFacForLocation(
          companyCode, userId, partItemDetail.roomAreaCode, partDetail.iFacilityCode);
    }
    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response =
    await post(uri, headers: headers, body: jsonBody, encoding: encoding)
        .then((resp) {
      isLoading = true;
      print(resp.statusCode);
      if (resp.statusCode == 200)
      {
        String responseBody = resp.body;
        print("----------" + resp.body);
        onSuccessResponse(type, responseBody);
      } else {
        isLoading = true;
        onErrorResponse(type, resp.body);
      }
    })
        .catchError((error) {
      isLoading = true;
      print(error.toString());
      onErrorResponse(type, error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
        .catchError((e) {
      isLoading = true;
      dismissLoader();
      Utility.showToastMsg("Error-->" + e.toString());
    });
  }
  void  onSuccessResponse(String type, String value) {
    isLoading = true;
    if (type == ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO) {
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml
          .findElements("string")
          .first
          .text;
      if (value != "0") {
        Map data1;
        try {
          data1 = jsonDecode(value);
        } catch (ex) {
          print(ex);
        }
        Map data2;
        try {
          data2 = data1['NewDataSet'];
        } catch (ex) {
          print(ex);
        }
        List<dynamic> data;
        try{
          data=   data2['Table'];
          if (data != null && data.length > 0)
          {
            partList.clear();
            if (data.length > 0)
            {
              for (int i = 0; i < data.length; i++) {
                Map dat = data.elementAt(i);
                PoDetail temp = PoDetail.fromJson(dat);
                partList.add(temp);
              }
              print(" size of roomfacrLoaction--");
              print(" end --");
              partList.sort((a, b) => a.lineItemSrNo.compareTo(b.lineItemSrNo));
              isRecordVisible = true;
              ReceiveByPart.partList =partList;

              setState(() {});
            }

            else {
              reset();
              Utility.showToastMsg("No Record Found");
              return;
            }
          }
        }catch(e)
        {
          partList.clear();
          PoDetail temp = PoDetail.fromJson(data2['Table']);
          partList.add(temp);
          print(" size of roomfacrLoaction--");
          print(" end --");
          ReceiveByPart.partList =partList;
          isRecordVisible = true;
          setState(() {});
        }
        return;
      }
      else {
        reset();
        Utility.showToastMsg("No Record Found");
        return;
      }
    }

    if (type == ReceiveByPoApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
    {
      isLoading = true;
      // dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml
          .findElements("string")
          .first
          .text;
      if (value != "0") {
        Map data1;
        try {
          data1 = jsonDecode(value);
        } catch (ex) {
          print(ex);
        }
        Map data2;
        try {
          data2 = data1['NewDataSet'];
        } catch (ex) {
          print(ex);
        }
        try {
          partItemDetail =POItemDetails.fromJson(data2['Table']);
          print("get part in "+partItemDetail.iPartNo.toString());
          setState(()
          {
            checkInternetConnection(
                partItemDetail.iPartNo.toString(), ReceiveByPoApi.API_IS_PART_SERIALIZED);

          });
        } catch (ex) {
          print("data in single");
        }
        return;
      }
      else {
        dismissLoader();
        reset();
        Utility.showToastMsg("No Record Found");
        return;
      }
    }

    if (type == ReceiveByPoApi.API_IS_PART_SERIALIZED)
    {
      isLoading = true;
      xml.XmlDocument parsedXml = xml.parse(value);
      value = parsedXml .findElements("boolean").first
          .text;
      print(value);

      if (value == "false" || value == "true")
      {
        if (value.toUpperCase() == "false".toUpperCase())
        {
          isSerialPart = false;
        }
        else {
          isSerialPart = true;
        }
        checkInternetConnection(
            "", NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION);
      }
      else{
        dismissLoader();
        Utility.showToastMsg("No Record Found ");
        return;
      }
    }

    if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
    {
      isLoading = true;
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if(value !="0") {
        Map data1 = jsonDecode(value);
        Map data2 = data1['NewDataSet'];
        List<dynamic> data = [];
        try {
          data = data2['Table'];
          roomFacForLocationList = new List();
          if (data.length > 0) {
            for (int i = 0; i < data.length; i++) {
              Map dat = data.elementAt(i);
              RoomFacForLocation temp = RoomFacForLocation.fromJson(dat);
              roomFacForLocationList.add(temp);
            }
            print("list size--" + roomFacForLocationList.length.toString());
          }
          else {
            print("Wrong input");
            Utility.showToastMsg(
                "Please Select Faclility and  Enter Correct Bin No.");
          }
        } catch (e) {
          roomFacForLocationList.clear();
          roomFacForLocation = RoomFacForLocation.fromJson(data2['Table']);
          roomFacForLocationList.add(roomFacForLocation);
          print(roomFacForLocationList.length.toString());
          Widget view= ReceiveByPartActivity(widget.callBack,
              partDetail, partItemDetail, roomFacForLocationList,
              isSerialPart);
          this.widget.callBack.viewType(
              view,
              AppStrings.LABEL_RECEIVE_BY_PART_ACTIVITY);
          }
      }
      else{
        Utility.showToastMsg("No Record Found");
        return;
      }
    }
  }

  onErrorResponse(String type, String error) {
    dismissLoader();
    if (type == ReceiveByPoApi.API_GET_ALL_RECIEVE_AGAINST_PO)
    {
      Utility.showToastMsg(error.toString());
      reset();
      return;
    }
    if (type == ReceiveByPoApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
    {
      Utility.showToastMsg(error.toString());
      reset();
    }
    if (type == ReceiveByPoApi.API_IS_PART_SERIALIZED)
    {

    }
  }
  void reset()
  {
    print("reset call");
    _partNumberController.text = "";
    partList.clear();
    isRecordVisible = false;
    setState(() {

    });
  }
}

