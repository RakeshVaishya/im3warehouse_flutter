import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/equipment/equipment.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_item_detail.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_list.dart';
import 'package:im3_warehouse/network/api/recieve_by_po_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/repair_request/Dialogs.dart';
import 'package:xml/xml.dart' as xml;
import 'receive_by_po.dart';

class ReceiveByPoActivity extends StatefulWidget {


  List<RoomFacForLocation> roomFacForLocationList = [];
  POItemDetails  poItemDetail;
  bool isSerialPart;
  PoDetail poDetail;
  ViewTypeCallBack callBack;
  ReceiveByPoActivity(this.callBack,this.poDetail,
      this.poItemDetail,this.roomFacForLocationList,this.isSerialPart);

  @override
  State<StatefulWidget> createState() {
    return ReceiveByPoActivityState();
  }
}

class ReceiveByPoActivityState extends State<ReceiveByPoActivity> {
  TextEditingController _poNumberController = TextEditingController();
  TextEditingController _lineNumberController = TextEditingController();
  TextEditingController _partNoController = TextEditingController();
  TextEditingController _orderQController = TextEditingController();
  TextEditingController _releaseNoController = TextEditingController();
  TextEditingController _binController = TextEditingController();
  TextEditingController serialNumberController = TextEditingController();
  TextEditingController _vendorLotNoController = TextEditingController();
  TextEditingController _expirationDateController = TextEditingController();
  TextEditingController _receiveQtyController = TextEditingController();
  TextEditingController _equipmentController = TextEditingController();
  TextEditingController _equipmentDescController = TextEditingController();
  TextEditingController _notesController = TextEditingController();


  FocusNode _binFocus = new FocusNode();
  FocusNode _receiveQtyFocus = new FocusNode();
  FocusNode _expirationDateFocus = new FocusNode();
  FocusNode _vendorLotNoFocus = new FocusNode();
  FocusNode _equipmentFocus = new FocusNode();
  FocusNode _noteFocus = new FocusNode();
  FocusNode _serialNumberFocus = new FocusNode();

  bool loading = true;
  bool isLoaderRunning = false;
  var _formkey = GlobalKey<FormState>();

  bool _validate = false;
  bool countVisible = false;
  var colorBlue = Colors.blue;
  String userId = "";
  String companyCode = "";
  bool serialNoBool = false;
  RoomFacForLocation selectedStockRoom ;
      Color partColor;
  Color binColor;
  bool isStockRoomExist = false;
  List<RoomFacForLocation> roomFacForLocationList=[];
  POItemDetails  poItemDetail;
  bool isSerialPart;
  var dialogKey = GlobalKey();
  Equipment equipment;
  PoDetail poDetail ;
  Facility facility;
  bool onPageLoad =false;
  @override
  void initState() {
    super.initState();
     onPageLoad =true;

     getData();
     roomFacForLocationList = widget.roomFacForLocationList;
     poItemDetail= widget.poItemDetail;

    poDetail =widget.poDetail;
     isSerialPart =widget.isSerialPart;
    _poNumberController.text =poItemDetail.pONo;
    selectedStockRoom = roomFacForLocationList.elementAt(0);
    //testData();
//  _equipmentController.text=  "EQ1235497";
//  _equipmentController.text=  "EQ1235497";


  if(isSerialPart)
  {
    _receiveQtyController.text = "1";
  }
  else{
     if(poDetail.releaseNo !=null && poDetail.releaseNo.length>0)
     {
       _receiveQtyController.text = "0";
     }
     else{
       print(poItemDetail.orderQuantity);
       print(poItemDetail.totalReceivedQty);
       int orderQuantity = 0;
       int totalReceivedQty = 0;
       if (StringUtil.isInt(poItemDetail.orderQuantity))
       {
         orderQuantity =
             StringUtil.tryParse(poItemDetail.orderQuantity);
       }
       else{
         if (StringUtil.isDouble(poItemDetail.orderQuantity))
         {
           double d =
           StringUtil.tryParse(poItemDetail.orderQuantity);
           orderQuantity  =d.round();
         }
       }
       if (StringUtil.isInt(poItemDetail.totalReceivedQty))
       {
         totalReceivedQty =
             StringUtil.tryParse(poItemDetail.totalReceivedQty);
       }
       else{
         if (StringUtil.isDouble(poItemDetail.totalReceivedQty))
         {
           double d =
           StringUtil.tryParse(poItemDetail.totalReceivedQty);
           totalReceivedQty =d.round();
         }
       }
       int receiveQty= orderQuantity-totalReceivedQty;
      _receiveQtyController.text ="0";
     }
  }
    print(" before--poDetail.releaseNo---"+poDetail.releaseNo);
    if(poDetail.releaseNo ==null )
    {
      poDetail.releaseNo ="0";
    }
    if(poDetail.releaseNo !=null &&poDetail.releaseNo.length==0)
    {
      {
        poDetail.releaseNo = "0";
      }
  }
    setData();
  }
  Future<void> getData() async {

    String data =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    facility = Facility.fromJson(map);
    print("retrived from SH file--->" + facility.iFacilityCode);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
  }
  @override
  Widget build(BuildContext context) {
    if(onPageLoad){
      FocusScope.of(this.context).requestFocus(_receiveQtyFocus);
      onPageLoad =false;
    }
    return Scaffold(
      body:   SafeArea(
       child: formWidget()
      ),
    );
    return formWidget();
  }
  Widget formWidget() {
    return Container(
      child:Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 5.0, left: 5, right: 0, bottom: 50),
            child: Scrollbar(
                child: SingleChildScrollView(
                    child: Form(
                        key: this._formkey,
                        autovalidate: _validate,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                // crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 0),
                                      child: TextFormField(
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        controller: _poNumberController,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            labelText: AppStrings.LABEL_PO_NUMBER,
                                            labelStyle: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: AppStrings.SEGOEUI_FONT),
                                            contentPadding:
                                            EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        left: 10.0,
                                      ),
                                      child: TextFormField(
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        controller: _partNoController,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            labelText: AppStrings.LABEL_PART_NUMBER,
                                            labelStyle: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: AppStrings.SEGOEUI_FONT),
                                            contentPadding: EdgeInsets.only(top: 3)),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Expanded(
                                    flex: 4,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 0),
                                      child: TextFormField(
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        controller: _orderQController,
                                        enabled: false,
                                        decoration: InputDecoration(
                                            labelText: AppStrings.LABEL_ORDER_Q,
                                            labelStyle: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: AppStrings.SEGOEUI_FONT),
                                            contentPadding:
                                            EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 4,
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 5.0),
                                        child: TextFormField(
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontFamily: AppStrings.SEGOEUI_FONT),
                                          controller: _releaseNoController,
                                          enabled: false,
                                          decoration: InputDecoration(
                                              labelText: AppStrings.LABEL_RELEASE_NO,
                                              labelStyle: TextStyle(
                                                  color: Colors.grey[600],
                                                  fontFamily:
                                                  AppStrings.SEGOEUI_FONT),
                                              contentPadding:
                                              EdgeInsets.only(top: 3)),
                                        ),
                                      )),
                                  Expanded(
                                      flex: 2,
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 5.0),
                                        child: TextFormField(
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontFamily: AppStrings.SEGOEUI_FONT),
                                          controller: _lineNumberController,
                                          enabled: false,
                                          decoration: InputDecoration(
                                              labelText: AppStrings.LABEL_LINE_NUMBER,
                                              labelStyle: TextStyle(
                                                  color: Colors.grey[600],
                                                  fontFamily:
                                                  AppStrings.SEGOEUI_FONT),
                                              contentPadding:
                                              EdgeInsets.only(top: 3)),
                                        ),
                                      )),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                    flex: 9,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 4.0),
                                      child: TextFormField(
                                        focusNode:_binFocus,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        controller: _binController,
                                        textInputAction: TextInputAction.send,
                                        onFieldSubmitted: (text) {
                                          if (text.trim().length > 0)
                                          {
                                            if(isLoading) {
                                              isLoading =false;
                                              checkInternetConnection(
                                                  text, NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION);
                                            }
                                          }
                                          else{
                                            Utility.showToastMsg("Please enter purchase order number!");
                                            return;
                                          }
                                        },

                                        onChanged: (text){
                                          if(text.length==0)
                                          {
                                            hideStockRoom();
                                          }
                                          else{
                                          }
                                        },
                                        decoration: InputDecoration(
                                            labelText: AppStrings.LABEL_BIN,
                                            labelStyle: TextStyle(
                                                fontFamily: AppStrings.SEGOEUI_FONT,
                                                color: _binFocus.hasFocus
                                                    ? binColor
                                                    : null),
                                            contentPadding:
                                            EdgeInsets.only(top: 10, bottom: 5)),
                                        validator: (text) {
                                          if (text.isEmpty) {
                                            return AppStrings.LABEL_BIN_ERROR;
                                          }
                                        },
                                      ),
                                    )),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 25),
                                      child: InkWell(
                                        child: Image.asset(
                                          "images/QRCode.png",
                                        ),
                                        onTap: () {
                                          barCodeScan(AppStrings.LABEL_BIN);
                                        },
                                      ),
                                    ))
                              ],
                            ),
                            Visibility(
                              visible:isStockRoomExist ,
                              maintainState: true,
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 1.0,
                                    ),
                                    child: Text(
                                      AppStrings.LABEL_STOCK_ROOM,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: AppStrings.FONT_SIZE_12,
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: Colors.grey[600],
                                          fontWeight: FontWeight.normal,
                                          decoration: TextDecoration
                                              .none), // removes yellow line
                                    ),
                                  ),
                                  Padding(
                                    padding: isStockRoomExist
                                        ? const EdgeInsets.only(top: 0.0, bottom: 0)
                                        : EdgeInsets.only(top: 0, bottom: 0),
                                    child: DropdownButton<RoomFacForLocation>(
                                      value: selectedStockRoom,
                                      icon: Icon(Icons.arrow_drop_down),
                                      iconSize: 24,
                                      isExpanded: true,
                                      elevation: 10,
                                      hint: Text(""),
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      underline: Container(
                                        height: 1,
                                        color: Colors.grey,
                                      ),
                                      onChanged: (RoomFacForLocation newValue) {
                                        setState(() {
                                          print("selectedCountTye--" + newValue.roomAreaCode);
                                          selectedStockRoom = newValue;
                                        });
                                      },
                                      items: roomFacForLocationList.map((dropdownValue) {
                                        return DropdownMenuItem<RoomFacForLocation>(
                                            value: dropdownValue,
                                            child: Text(
                                              dropdownValue.stockRoom,
                                              style: TextStyle(color: Colors.black),
                                            ));
                                      }).toList(),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 0),
                              child: TextFormField(
                                readOnly: isSerialPart,
                                focusNode:_receiveQtyFocus,
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                controller: _receiveQtyController,
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.send,
                                onFieldSubmitted: (text)
                                {
                                  FocusScope.of(this.context).requestFocus(_serialNumberFocus);
                                },
                                decoration: InputDecoration(
                                    labelText: "Receive Q",
                                    labelStyle: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    contentPadding: EdgeInsets.only(top: 3)),
                                validator: (value)
                                {
                                  if (value.isEmpty) {
                                    return "Please enter recieve Quantity";
                                  }
                                },
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 9,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 0),
                                    child: TextFormField(
                                      focusNode: _serialNumberFocus,
                                      style: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      controller: serialNumberController,
                                      textInputAction: TextInputAction.send,
                                      onFieldSubmitted: (text)
                                      {
                                        FocusScope.of(this.context).requestFocus(_vendorLotNoFocus);
                                      },

                                      decoration: InputDecoration(
                                          labelText: AppStrings.LABEL_SERIAL_NO,
                                          labelStyle: TextStyle(
                                              color: Colors.grey[600],
                                              fontFamily: AppStrings.SEGOEUI_FONT),
                                          contentPadding: EdgeInsets.only(top: 3)),

                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 10),
                                      child: InkWell(
                                        child: Image.asset(
                                          "images/QRCode.png",
                                        ),
                                        onTap: () {
                                          barCodeScan(AppStrings.LABEL_SERIAL_NO);
                                        },
                                      ),
                                    ))
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                    flex: 9,
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 4.0),
                                      child: TextFormField(
                                        focusNode: _vendorLotNoFocus,
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        controller: _vendorLotNoController,
                                        textInputAction: TextInputAction.next,
                                        onFieldSubmitted: (text)
                                        {
                                          selectDate(context);
                                        },

                                        decoration: InputDecoration(
                                            labelText: AppStrings.LABEL_VENDOR_LOT_NO,
                                            labelStyle: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: AppStrings.SEGOEUI_FONT),
                                            contentPadding: EdgeInsets.only(top: 0)),

                                      ),
                                    )),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 15),
                                      child: InkWell(
                                        child: Image.asset(
                                          "images/QRCode.png",
                                        ),
                                        onTap: () {
                                          barCodeScan(AppStrings.LABEL_VENDOR_LOT_NO);
                                        },
                                      ),
                                    ))
                              ],
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 0),
                              child: TextFormField(
                                onTap: (){
                                  selectDate(context);
                                },
                                readOnly: true,
                                focusNode: _expirationDateFocus,
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                controller: _expirationDateController,
                                decoration: InputDecoration(
                                    labelText: AppStrings.LABEL_EXPIRATION_DATE,
                                    labelStyle: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    contentPadding: EdgeInsets.only(top: 3)),

                              ),
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                    flex: 9,
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 5.0),
                                      child: TextFormField(
                                        focusNode: _equipmentFocus,
                                        style: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        controller: _equipmentController,
                                        textInputAction: TextInputAction.next,
                                        onFieldSubmitted: (text) {
                                          if (text.trim().length > 0)
                                          {
                                            if(isLoading) {
                                              isLoading =false;
                                              checkInternetConnection(
                                                  text, NetworkConfig.API_GET_DYNAMIC_HELP);
                                            }
                                          }
                                          else{
                                            FocusScope.of(this.context).requestFocus(_noteFocus);
                                          }
                                      },

                                        decoration: InputDecoration(
                                            labelText: AppStrings.LABEL_EQUIPMENT,
                                            labelStyle: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: AppStrings.SEGOEUI_FONT),
                                            contentPadding: EdgeInsets.only(top: 3)),

                                      ),
                                    )),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 25),
                                      child: InkWell(
                                        child: Image.asset(
                                          "images/QRCode.png",
                                        ),
                                        onTap: ()
                                        {
                                          //binScan();
                                          barCodeScan(AppStrings.LABEL_EQUIPMENT);
                                        },
                                      ),
                                    ))
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only( top: 1),
                              child: TextFormField(
                                readOnly: true,
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                controller: _equipmentDescController,
                                decoration: InputDecoration(
                                    labelText: AppStrings.LABEL_EQUIPMENT_DESC,
                                    labelStyle: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    contentPadding: EdgeInsets.only(top: 3)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 5.0, top: 1),
                              child: TextFormField(
                                focusNode: _noteFocus,
                                maxLines: 4,
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                controller: _notesController,
                                decoration: InputDecoration(
                                    labelText: AppStrings.LABEL_NOTES,
                                    labelStyle: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    contentPadding: EdgeInsets.only(top: 3)),
                                validator: (value)
                                {
                                },
                              ),
                            ),
                          ],
                        )))
            ),
          ),
          new Positioned(
            child: new Align(
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                padding: const EdgeInsets.only(top:20, left: 5, right: 5,),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 4.0),
                        child: MaterialButton(
                          onPressed: () {
                            onSave();
                          },
                          color: AppColors.CYAN_BLUE_COLOR,
                          minWidth: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "SAVE".toUpperCase(),
                              style: TextStyle(
                                  fontSize: 17.0,
                                  color: Colors.white,
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(0.0))),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4.0),
                        child: MaterialButton(
                          onPressed: ()
                          {
                            widget.callBack.viewType(ReceiveByPo(callBack:widget.callBack
                            ),
                                AppStrings.LABEL_RECEIVE_BY_PO);
                          },
                          color: AppColors.CYAN_BLUE_COLOR,
                          minWidth: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(
                              "Cancel".toUpperCase(),
                              style: TextStyle(
                                  fontSize: 17.0,
                                  color: Colors.white,
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(0.0))),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )

        ],
      ),
    );



    /*return Padding(
      padding: const EdgeInsets.only(top: 5.0, left: 5, right: 0, bottom: 0),
      child: Scrollbar(
          child: SingleChildScrollView(
              child: Form(
                  key: this._formkey,
                  autovalidate: _validate,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          // crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 0),
                                child: TextFormField(
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: _poNumberController,
                                  enabled: false,
                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_PO_NUMBER,
                                      labelStyle: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      contentPadding:
                                          EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  left: 10.0,
                                ),
                                child: TextFormField(
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: _partNoController,
                                  enabled: false,
                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_PART_NUMBER,
                                      labelStyle: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      contentPadding: EdgeInsets.only(top: 3)),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 0),
                                child: TextFormField(
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: _orderQController,
                                  enabled: false,
                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_ORDER_Q,
                                      labelStyle: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      contentPadding:
                                          EdgeInsets.fromLTRB(0, 0, 0, 0)),
                                ),
                              ),
                            ),
                            Expanded(
                                flex: 4,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: TextFormField(
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    controller: _releaseNoController,
                                    enabled: false,
                                    decoration: InputDecoration(
                                        labelText: AppStrings.LABEL_RELEASE_NO,
                                        labelStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily:
                                                AppStrings.SEGOEUI_FONT),
                                        contentPadding:
                                            EdgeInsets.only(top: 3)),
                                  ),
                                )),
                            Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: TextFormField(
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    controller: _lineNumberController,
                                    enabled: false,
                                    decoration: InputDecoration(
                                        labelText: AppStrings.LABEL_LINE_NUMBER,
                                        labelStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily:
                                                AppStrings.SEGOEUI_FONT),
                                        contentPadding:
                                            EdgeInsets.only(top: 3)),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                              flex: 9,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: TextFormField(
                                  focusNode:_binFocus,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                       controller: _binController,

                                    textInputAction: TextInputAction.send,
                                    onFieldSubmitted: (text) {
                                    if (text.length > 0)
                                    {
                                      if(isLoading) {
                                        isLoading =false;
                                        checkInternetConnection(
                                            text, NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION);
                                      }
                                    }
                                    else{
                                      Utility.showToastMsg("Please enter purchase order number!");
                                      return;
                                    }
                                  },

                                  onChanged: (text){
                                    if(text.length==0)
                                    {
                                      hideStockRoom();
                                    }
                                    else{
                                    }
                                  },
                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_BIN,
                                      labelStyle: TextStyle(
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: _binFocus.hasFocus
                                              ? binColor
                                              : null),
                                      contentPadding:
                                          EdgeInsets.only(top: 10, bottom: 5)),
                                  validator: (text) {
                                    if (text.isEmpty) {
                                      return AppStrings.LABEL_BIN_ERROR;
                                    }
                                  },
                                ),
                              )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: InkWell(
                              child: Image.asset(
                                "images/QRCode.png",
                              ),
                              onTap: () {
                                barCodeScan(AppStrings.LABEL_BIN);
                              },
                            ),
                          ))
                        ],
                      ),

            Visibility(
              visible:isStockRoomExist ,
              maintainState: true,
              child:
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 1.0,
                            ),
                            child: Text(
                              AppStrings.LABEL_STOCK_ROOM,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: AppStrings.FONT_SIZE_12,
                                  fontFamily: AppStrings.SEGOEUI_FONT,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.normal,
                                  decoration: TextDecoration
                                      .none), // removes yellow line
                            ),
                          ),
                       Padding(
                   padding: isStockRoomExist
                       ? const EdgeInsets.only(top: 0.0, bottom: 0)
                       : EdgeInsets.only(top: 0, bottom: 0),
                   child: DropdownButton<RoomFacForLocation>(
                     value: selectedStockRoom,
                     icon: Icon(Icons.arrow_drop_down),
                     iconSize: 24,
                     isExpanded: true,
                     elevation: 10,
                     hint: Text(""),
                     style: TextStyle(
                         color: Colors.grey,
                         fontFamily: AppStrings.SEGOEUI_FONT),
                     underline: Container(
                       height: 1,
                       color: Colors.grey,
                     ),
                     onChanged: (RoomFacForLocation newValue) {
                       setState(() {
                         print("selectedCountTye--" + newValue.roomAreaCode);
                         selectedStockRoom = newValue;
                       });
                     },
                     items: roomFacForLocationList.map((dropdownValue) {
                       return DropdownMenuItem<RoomFacForLocation>(
                           value: dropdownValue,
                           child: Text(
                             dropdownValue.stockRoom,
                             style: TextStyle(color: Colors.black),
                           ));
                     }).toList(),
                   ),
                 ),
               ],
                      ),
            ),

                      Padding(
                        padding: const EdgeInsets.only(top: 0),
                        child: TextFormField(
                          readOnly: isSerialPart,
                          focusNode:_receiveQtyFocus,
                          style: TextStyle(
                              color: Colors.grey[600],
                              fontFamily: AppStrings.SEGOEUI_FONT),
                          controller: _receiveQtyController,
                          keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.send,
                            onFieldSubmitted: (text)
                            {
                              FocusScope.of(this.context).requestFocus(_serialNumberFocus);
                            },
                              decoration: InputDecoration(
                              labelText: "Receive Q",
                              labelStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                              contentPadding: EdgeInsets.only(top: 3)),
                             validator: (value)
                             {
                            if (value.isEmpty) {
                              return "Please enter recieve Quantity";
                            }
                          },
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 0),
                              child: TextFormField(
                                focusNode: _serialNumberFocus,
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                controller: serialNumberController,
                                textInputAction: TextInputAction.send,
                                onFieldSubmitted: (text)
                                {
                                  FocusScope.of(this.context).requestFocus(_vendorLotNoFocus);
                                },

                                decoration: InputDecoration(
                                    labelText: AppStrings.LABEL_SERIAL_NO,
                                    labelStyle: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    contentPadding: EdgeInsets.only(top: 3)),

                              ),
                            ),
                          ),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: InkWell(
                              child: Image.asset(
                                "images/QRCode.png",
                              ),
                              onTap: () {
                                barCodeScan(AppStrings.LABEL_SERIAL_NO);
                              },
                            ),
                          ))
                        ],
                      ),
                       Row(
                        children: <Widget>[
                          Expanded(
                              flex: 9,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: TextFormField(
                                  focusNode: _vendorLotNoFocus,
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                     controller: _vendorLotNoController,
                                    textInputAction: TextInputAction.next,
                                    onFieldSubmitted: (text)
                                    {
                                      selectDate(context);
                                    },

                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_VENDOR_LOT_NO,
                                      labelStyle: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                         contentPadding: EdgeInsets.only(top: 0)),

                                ),
                              )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(top: 15),
                            child: InkWell(
                              child: Image.asset(
                                "images/QRCode.png",
                              ),
                              onTap: () {
                                barCodeScan(AppStrings.LABEL_VENDOR_LOT_NO);
                                },
                            ),
                          ))
                        ],
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 0),
                        child: TextFormField(
                          onTap: (){
                            selectDate(context);
                          },
                          readOnly: true,
                          focusNode: _expirationDateFocus,
                          style: TextStyle(
                              color: Colors.grey[600],
                              fontFamily: AppStrings.SEGOEUI_FONT),
                              controller: _expirationDateController,
                              decoration: InputDecoration(
                              labelText: AppStrings.LABEL_EXPIRATION_DATE,
                              labelStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                              contentPadding: EdgeInsets.only(top: 3)),

                        ),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(
                              flex: 9,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: TextFormField(
                                  focusNode: _equipmentFocus,
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: _equipmentController,
                                  textInputAction: TextInputAction.next,
                                  onFieldSubmitted: (text) {
                                    if (text.length > 0)
                                    {
                                      if(isLoading) {
                                        isLoading =false;
                                        checkInternetConnection(
                                            text, NetworkConfig.API_GET_DYNAMIC_HELP);
                                      }
                                    }
                                    else{
                                      Utility.showToastMsg("Please enter equipment number !");
                                      return;
                                    }
                                  },

                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_EQUIPMENT,
                                      labelStyle: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      contentPadding: EdgeInsets.only(top: 3)),

                                ),
                              )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: InkWell(
                              child: Image.asset(
                                "images/QRCode.png",
                              ),
                              onTap: () {
                                //binScan();
                                barCodeScan(AppStrings.LABEL_EQUIPMENT);

                              },
                            ),
                          ))
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only( top: 1),
                        child: TextFormField(
                          readOnly: true,
                          style: TextStyle(
                              color: Colors.grey[600],
                              fontFamily: AppStrings.SEGOEUI_FONT),
                          controller: _equipmentDescController,
                          decoration: InputDecoration(
                              labelText: AppStrings.LABEL_EQUIPMENT_DESC,
                              labelStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                              contentPadding: EdgeInsets.only(top: 3)),
                         ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0, top: 1),
                        child: TextFormField(
                          focusNode: _noteFocus,
                          maxLines: 4,
                          style: TextStyle(
                              color: Colors.grey[600],
                              fontFamily: AppStrings.SEGOEUI_FONT),
                          controller: _notesController,
                          decoration: InputDecoration(
                              labelText: AppStrings.LABEL_NOTES,
                              labelStyle: TextStyle(
                                  color: Colors.grey[600],
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                              contentPadding: EdgeInsets.only(top: 3)),
                             validator: (value)
                             {
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 0.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: MaterialButton(
                                  onPressed: ()
                                  {
                                    onSave();
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Save".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: MaterialButton(
                                  onPressed: ()
                                  {
                                      widget.callBack.viewType(ReceiveByPo(callBack:widget.callBack
                                      ),
                                          AppStrings.LABEL_PHYSICAL_COUNT);
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Cancel".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )))
      ),
    );
  */

  }

  onSave()
  {
    if (_formkey.currentState.validate())
    {
      _formkey.currentState.save();
      getDataFromField();
    } else {
      setState(()
      {
        _validate = true;
        isStockRoomExist = true;
      });
    }

  }

  @override
  void getList(List<String> data) {
    // TODO: implement getList
  }

  void getDataFromField() {
    _orderQController.text;
    _partNoController.text;
    _poNumberController.text;
    _lineNumberController.text;
    _releaseNoController.text;
    _vendorLotNoController.text;
    _expirationDateController.text;
    _equipmentController.text;
    _equipmentDescController.text;
    _binController.text;
    _notesController.text;
    serialNumberController.text;

    print(" order  Q   --" + _orderQController.text);
    print("part No --" + _partNoController.text);
    print("po Number  --" + _poNumberController.text);
    print("line number  --" + _lineNumberController.text);
    print("Release No  --" + _releaseNoController.text);
    print("vendor lot no  --" + _vendorLotNoController.text);
    print("expiration date  --" + _expirationDateController.text);
    print("equiment controller  --" + _equipmentController.text);
    print("equiment desc controller   --" + _equipmentDescController.text);
    print("_binController   --" + _binController.text);
    print("notes    --" + _notesController.text);
    print("serialNumberController   --" + serialNumberController.text);

    if(_binController.text.length ==0)
    {
      FocusScope.of(this.context).requestFocus(_binFocus);
      Utility.showToastMsg(
          "Please enter bin!");
      return;
    }

    if(selectedStockRoom ==null )
    {
      FocusScope.of(this.context).requestFocus(_binFocus);
      Utility.showToastMsg(
          "Please sync bin to load the stock room");
      return;
    }

    if(_receiveQtyController.text=="0")
    {
      Utility.showToastMsg(
          "Please enter receive quantity greater than zero !");
      FocusScope.of(this.context).requestFocus(_receiveQtyFocus);
       return;
    }

    if(!isSerialPart)
    {
      print(isSerialPart);

     int orderQuantity =0;
     int receiveQuantity=0;
      if (StringUtil.isInt(poItemDetail.orderQuantity))
      {
        orderQuantity =
            StringUtil.tryParse(poItemDetail.orderQuantity);
      }
      else{
        if (StringUtil.isDouble(poItemDetail.orderQuantity))
        {
          double d =
          StringUtil.tryParse(poItemDetail.orderQuantity);
          orderQuantity =d.round();
        }
      }

      if (StringUtil.isInt(_receiveQtyController.text))
     {
       receiveQuantity =
           StringUtil.tryParse(_receiveQtyController.text);
     }
     else{
       if (StringUtil.isDouble(_receiveQtyController.text))
       {
         double d =
         StringUtil.tryParse(_receiveQtyController.text);
         receiveQuantity =d.round();
       }
     }

      print(receiveQuantity);
      print(orderQuantity);

      if(receiveQuantity>orderQuantity)
     {
       Utility.showToastMsg(
           "Quantity receive is more then quantity ordered");
       FocusScope.of(this.context).requestFocus(_receiveQtyFocus);
       return;
     }
    }
    if(isSerialPart)
    {
      if(serialNumberController.text.length==0)
      {
        Utility.showToastMsg(
            "Please enter serial number");
        FocusScope.of(this.context).requestFocus(_serialNumberFocus);
        return;
      }
    }
    else{
      if (serialNumberController.text.length==0)
      {

        serialNumberController.text ="";
      }
    }

    if(_vendorLotNoController.text.length==0)
    {

      _vendorLotNoController.text ="";
    }
    if(_expirationDateController.text=="mm-dd-yyyy")
    {

      _expirationDateController.text ="";
    }
    if(_notesController.text.length==0)
    {
      _notesController.text ="";
    }
    if(isSerialPart)
    {
      checkInternetConnection("",NetworkConfig.API_CHECK_DUPLICATE_SERIAL_NO);
    }
    else{
      checkInternetConnection("",ReceiveByPoApi.API_SAVE_RECEIVE_BY_PO);
    }
  }

  void getClearFromField() {
    _orderQController.text = "";
    _partNoController.text = "";
    _poNumberController.text = "";
    _lineNumberController.text = "";
    _releaseNoController.text = "";
    _vendorLotNoController.text = "";
    _expirationDateController.text = "";
    _equipmentController.text = "";
    _equipmentDescController.text = "";
    _binController.text = "";
    _notesController.text = "";
    serialNumberController.text = "";
    print(" order  Q   --" + _orderQController.text);
    print("part No --" + _partNoController.text);
    print("po Number  --" + _poNumberController.text);
    print("line number  --" + _lineNumberController.text);
    print("Release No  --" + _releaseNoController.text);
    print("vendor lot no  --" + _vendorLotNoController.text);
    print("expiration date  --" + _expirationDateController.text);
    print("equiment controller  --" + _equipmentController.text);
    print("equiment desc controller   --" + _equipmentDescController.text);
    print("_binController   --" + _binController.text);
    print("notes    --" + _notesController.text);
    print("serialNumberController   --" + serialNumberController.text);
    selectedStockRoom = roomFacForLocationList.elementAt(0);
  }

  void onCancel()
  {
    getClearFromField();
  }

  void testData() {
    _poNumberController.text = "PO02353";
    _lineNumberController.text = " 1";
    _partNoController.text = "07-00397-11";
    _orderQController.text = "10.0000000";
    _releaseNoController.text = "0";
    _binController.text = "C1C";
    serialNumberController.text = "test Serial number";
    _vendorLotNoController.text = "test vendor lot no";
    _expirationDateController.text = "mm-dd-yyyy";
    _equipmentController.text = "test equipment";
    _equipmentDescController.text = "test Equiment Desc ";
    _notesController.text = "Notes";
  }


  void setData()
  {

    print("setData() method call---");
    _poNumberController.text = poItemDetail.pONo;
    _lineNumberController.text = poItemDetail.lineItemSrNo;
    _partNoController.text = poItemDetail.partNo;
    _orderQController.text = poItemDetail.orderQuantity;
    _releaseNoController.text =poDetail.releaseNo ;
    _binController.text = poItemDetail.roomAreaCode;
    serialNumberController.text = "";
    _vendorLotNoController.text = "";
    _expirationDateController.text = "mm-dd-yyyy";
    _equipmentController.text = "";
    _equipmentDescController.text = "";
    _notesController.text = "";
    if(roomFacForLocationList.length>0)
    {
      isStockRoomExist =true;
    }
  }

  Future barCodeScan(String type) async {
    try {
      var barcode = await BarcodeScanner.scan();
       if(barcode.rawContent.length==0){
         return;
       }
      if (type == AppStrings.LABEL_BIN
      ) {
        _binController.text = barcode.rawContent;
        FocusScope.of(this.context).requestFocus(_receiveQtyFocus);
        setState(()
        {

        });
        if(isLoading)
        {
          isLoading =false;
          checkInternetConnection(
              _binController.text, NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION);
        }

        return;
      }
      if (type == AppStrings.LABEL_SERIAL_NO)
      {
        serialNumberController.text = barcode.rawContent;
        FocusScope.of(this.context).requestFocus(_vendorLotNoFocus);
        setState(()
        {

        });
        return;
      }
      if (type == AppStrings.LABEL_VENDOR_LOT_NO) {
        _vendorLotNoController.text = barcode.rawContent;
        selectDate(context);
        setState(()
        {
        });

        return;
      }
      if (type == AppStrings.LABEL_EQUIPMENT)
      {

        _equipmentController.text = barcode.rawContent;
        setState(()
        {

        });

        if(isLoading)
        {
          isLoading =false;
          checkInternetConnection(
              _equipmentController.text, NetworkConfig.API_GET_DYNAMIC_HELP);
        }

        return;
      }
      } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    }
  }

  void checkInternetConnection(String value, String type)
  async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
         {
           Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
                () {
              isLoading = true;
              dismissLoader();
            });
        print('check connection-------------->connected');
        if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
        {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
        if (type == NetworkConfig.API_GET_DYNAMIC_HELP)
        {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
        if (type == NetworkConfig.API_CHECK_DUPLICATE_SERIAL_NO)
           {
             isLoading =false;
             starLoader();
             getData().then((va)
             {
               getDetails(value, type);
             });
           }
           if (type == ReceiveByPoApi.API_SAVE_RECEIVE_BY_PO)
          {
            if(!isSerialPart)
            {
              isLoading =false;
              starLoader();
            }
          getData().then((va) {
            getDetails(value, type);
          });
        }
         }

    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }
  void showSnackbarPage(BuildContext context)
  {
    final snackbar =
    SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

/*
  onCancel() {
    String strWorkOrder = _poNumberController.text;
    if (strWorkOrder.length == 0) {

  */
/*    this
          .widget
          .callBack
          .viewType(DashboardView(this.widget.callBack), AppStrings.LABEL_HOME);
      Home.listReceivePO.clear();
  *//*

    }
    else {
    }
  }
*/


  void starLoader() {
   print("dialog started");
    if (!isLoaderRunning) {
      isLoaderRunning = true;
      Dialogs.showLoadingDialog(context, dialogKey);
    }
  }

  void dismissLoader()
  {
    if (isLoaderRunning) {
      isLoaderRunning = false;
      Navigator.of(dialogKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }

  bool isLoading = true;
  Future getDetails(String value, String type) async {
    print("data -->" + value);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    //print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";
    Map headers = new Map();
    print(NetworkConfig.BASE_URL);

    if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
    {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION;
      jsonBody = NetworkConfig.getRoomFacForLocation(
          companyCode, userId, value,facility.iFacilityCode);
    }

    if (type == NetworkConfig.API_GET_DYNAMIC_HELP)
    {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_DYNAMIC_HELP;
      jsonBody = NetworkConfig.getDynamicHelp(
          companyCode,   "eqsearch",   "2",    "1",   "1",   _equipmentController.text,"" ,
          "0",  userId, facility.iFacilityCode);
    }

    if (type == NetworkConfig.API_CHECK_DUPLICATE_SERIAL_NO)
    {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      headers = NetworkConfig.headers;
         print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_CHECK_DUPLICATE_SERIAL_NO;
        jsonBody = NetworkConfig.chkDuplicateSerialNo(
        companyCode,
        poItemDetail.iPartNo,
        serialNumberController.text.trim(),
        "text_"+serialNumberController.text.trim(),
        "2",
      );
    }

    if (type == ReceiveByPoApi.API_SAVE_RECEIVE_BY_PO)
    {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReceiveByPoApi.API_SAVE_RECEIVE_BY_PO;
          jsonBody = ReceiveByPoApi.saveReceiveByPO(userId,
          companyCode, facility, widget.poDetail, poItemDetail,selectedStockRoom,equipment,
                     _receiveQtyController.text, serialNumberController.text,
                   _vendorLotNoController.text,_expirationDateController.text,_notesController.text
           );
    }
    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response =
    await post(uri, headers: headers, body: jsonBody, encoding: encoding)
        .then((resp) {
      isLoading = true;
      print(resp.statusCode);
      if (resp.statusCode == 200)
      {
        String responseBody = resp.body;
        print("----------" + resp.body);
        onSuccessResponse(type, responseBody);
      } else {
        isLoading = true;
        onErrorResponse(type, resp.body);
      }
    })
        .catchError((error) {
      isLoading = true;
      print(error.toString());
      onErrorResponse(type, error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
        .catchError((e) {
      isLoading = true;
      dismissLoader();
      Utility.showToastMsg("Error-->" + e.toString());
    });
  }
  void  onSuccessResponse(String type, String value) {
    isLoading = true;
    if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
    {
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      print("--->value --"+value);
      if(value !="0")
      {
        Map data1 = null;
        Map data2 =null;
        try {
           data1 = jsonDecode(value);
           data2 = data1['NewDataSet'];
        }
        catch(e){
          _binController.text ="";
          hideStockRoom();
          Utility.showToastMsg(
              "Please enter bin ! ");
            return;
        }
         List<dynamic> data = [];
        try {
          data = data2['Table'];
          roomFacForLocationList = new List();
          if (data.length > 0)
          {
            for (int i = 0; i < data.length; i++)
            {
              Map dat = data.elementAt(i);
              RoomFacForLocation temp = RoomFacForLocation.fromJson(dat);
              roomFacForLocationList.add(temp);
              if(i==0)
              {
                selectedStockRoom=  roomFacForLocationList.elementAt(0);
                isStockRoomExist =true;
                setState(()
                {

                });
              }
            }
            print("list size--"+roomFacForLocationList.length.toString());
          }
          else {
            Utility.showToastMsg(
                "Please Select Faclility and  Enter Correct Bin No.");
          }
        } catch (e)
        {
          try{
            roomFacForLocationList.clear();
            selectedStockRoom = RoomFacForLocation.fromJson(data2['Table']);
            roomFacForLocationList.add(selectedStockRoom);
            print(roomFacForLocationList.length.toString());
            isStockRoomExist =true;
            setState(()
            {
            });
          }
          catch(e)
          {
            Utility.showToastMsg(
                "Please  Enter Bin !");
          }
        }
      }
    }

    if (type == NetworkConfig.API_GET_DYNAMIC_HELP)
    {
      isLoading = true;
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if(value !="0")
      {
        List<dynamic> data = jsonDecode(value);
        print("Equipement --data size-->"+data.length.toString());
         try {
          for( int i=0;i<data.length;i++)
          {
            equipment =  Equipment.fromJson(data.elementAt(i));
          }
          _equipmentDescController.text=equipment.eQShortDescription;
          setState(()
          {
          });
        } catch (e)
        {
          _equipmentController.text ="";
          Utility.showToastMsg(
              "Equipment doesnot exist!");
              setState(()
              {

              });
              return;
          }
       }
    }
    if(type == NetworkConfig.API_CHECK_DUPLICATE_SERIAL_NO)
    {
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      String  responseBody = parsedXml.findElements("string").first.text;
      List<dynamic> data = jsonDecode(responseBody);
      bool isErrorExist = false;
      SerialNumberStatus status;
      for (int i = 0; i < data.length; i++)
      {
        Map map = data.elementAt(i);
        status = SerialNumberStatus.fromJson(map);
        print(status.serialNo);
        print(status.controlId);
        print(status.type);
        if (status.type =="error")
        {
        isErrorExist = true;
        }
       }
     if (!isErrorExist)
      {
        checkInternetConnection("",ReceiveByPoApi.API_SAVE_RECEIVE_BY_PO);
      }
      else {
      isLoading = true;
      dismissLoader();
      FocusScope.of(this.context).requestFocus(_serialNumberFocus);
        Utility.showToastMsg(status.message);
        return;
      }
    }
    if (type == ReceiveByPoApi.API_SAVE_RECEIVE_BY_PO)
    {

      isLoading = true;
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      print("--->value --"+value);
      if(value =="Receiving done successfully")
      {
        Utility.showSuccessToastMsg
        (
            "Receiving done successfully !");
        ReceiveByPo.poList =null;
        widget.callBack.viewType(ReceiveByPo(callBack:widget.callBack
        ),
            AppStrings.LABEL_RECEIVE_BY_PO);
        }
      else{
        Utility.showToastMsg(
            value);
        return;
        }
      }

  }
  onErrorResponse(String type, String error) {
    isLoading =true;
    dismissLoader();
    if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
    {
      Utility.showToastMsg(error.toString());
      return;
    }
    if (type == NetworkConfig.API_GET_DYNAMIC_HELP)
    {
      _equipmentController.text ="";
      Utility.showToastMsg(
          "Equipment doesnot exist!");
      setState(() {
        });

      return;
    }
    if (type == ReceiveByPoApi.API_SAVE_RECEIVE_BY_PO)
    {

    }
  }
  Future setFocus(FocusNode focus) async
  {
    Future.delayed(Duration(microseconds: 25), ()
    {
      setState(()
      {
        FocusScope.of(context).requestFocus(focus);
      });
    });
  }
  selectDate(BuildContext context)
  {
    Future<String> strdarte = Utility.selectDate(context);
    strdarte.then((strNewDate) {
      print("New Date---->" + strNewDate);
      setState(()
      {
        FocusScope.of(this.context).requestFocus(_equipmentFocus);
        _expirationDateController.text = strNewDate;
      });
    });
  }

 void hideStockRoom(){
    isStockRoomExist =false;
    selectedStockRoom =null;
    roomFacForLocationList.clear();
    setState(() {
    });
  }
}

