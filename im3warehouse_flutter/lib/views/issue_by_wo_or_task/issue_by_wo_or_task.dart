import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/issue_by_work_order_task/issubyworktask_location_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order_task/issuebytask_getallTaskmodal.dart';
import 'package:im3_warehouse/models/issue_by_work_order_task/issuebyworkTask_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order_task/issuebywotaskpart_modal.dart';
import 'package:im3_warehouse/network/api/issue_by_work_order_task_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;

class IssueByWoOrTask extends StatefulWidget {
  ViewTypeCallBack callBack;
  IssueByWoOrTask(this.callBack);
  @override
  State<StatefulWidget> createState() {
    return IssueByWoOrTaskState();
  }
}

class IssueByWoOrTaskState extends State<IssueByWoOrTask> {
  var _formKey = new GlobalKey<FormState>();
  var _validate = false;
  bool _isWorkOrder = false;
  bool _isWorkTask = false;
  bool _isPartNo = false;
  bool _isLocation = false;
  var _formLocationKey = GlobalKey<FormState>();
  String userId = "";
  String companyCode = "";
  String iFacilityCode = "";
  String strWorkOrderDesc = "";
  String strWorkTaskDesc = "";
  String strPartDesc = "";
  String strPrimaryLocation = "";
  String strIPartNo = "";
  String strOnHandQuantity = "";
  String strIWorkOrderNo = "";
  String strStockRoom = "";
  ProgressDialog pr;
  String employeeName = "";
  List<IssuByWorkTaskLocationModal> _elementExistInWareHouseList = [];
  IssuByWorkTaskLocationModal selectedLocation;

  IssueByWorkTaskModal workOder;
  IssueByGetAllTaskModal workOrderTask;
  IssueByWorkTaskPartModal partDetail;

  FocusNode _workOrderFocus = FocusNode();
  FocusNode _partNoFocus = FocusNode();
  FocusNode _locationFocus = FocusNode();

  FocusNode _quantityFocus = FocusNode();
  FocusNode _workTaskFocus = FocusNode();
  FocusNode _serialNumberFocus = FocusNode();

  Color wordOrderColor;
  Color partColor;
  Color locationColor;
  Color quantityColor;
  Color workTaskColor;

  TextEditingController _workOrderNoController = TextEditingController();
  TextEditingController _workTaskNoController = TextEditingController();
  TextEditingController _partNoController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _quantityController = TextEditingController();
  TextEditingController _serialNoController = TextEditingController();

  SerialNumberStatus status;
  List<SerialNumberStatus> barcodeItem = [];
  bool isLoaderRunning = false;
  Timer _timer;
  bool isSerialPart = false;
  //bool isQuantityEnable = true;
  bool colorStatus = false;

  bool serverRespone = false;
  Color serverResponeColor;
  String serverResponse = "";
  bool isLoading = true;
  BuildContext context;

  @override
  void initState() {
    super.initState();
    facility =AppStrings.facility;
    iFacilityCode= facility.iFacilityCode;
    print("iFacilityCode=---"+iFacilityCode);
    getData();
    _workOrderFocus.addListener(() {
      setState(() {
        wordOrderColor = Colors.grey[600];
      });
    });

    _workTaskFocus.addListener(() {});
    _partNoFocus.addListener(() {
      setState(() {
        partColor = Colors.grey[600];
      });
    });

    _locationFocus.addListener(() {});
    _quantityFocus.addListener(() {
      setState(() {
        quantityColor = Colors.grey[600];
      });
    });
    testData();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    if (_workOrderNoController.text.length == 0) {
      FocusScope.of(context).requestFocus(_workOrderFocus);
    }
    return issueByWOWidget();
  }

  Widget issueByWOWidget() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Scrollbar(
          child: Column(
            children: <Widget>[
              Container(
                child: Form(
                  key: _formKey,
                  autovalidate: _validate,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Visibility(
                        visible: serverRespone,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            serverResponse,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: serverResponeColor),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 5.0, left: 12, right: 8, bottom: 8),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.ideographic,
                          children: <Widget>[
                            Expanded(
                              flex: 9,
                              child: TextFormField(
                                focusNode: _workOrderFocus,
                                controller: _workOrderNoController,
                                textInputAction: TextInputAction.send,
                                onFieldSubmitted: (value) {
                                  if (value.length > 0) {
                                    if (isLoading) {
                                      isLoading = false;
                                      checkInternetConnection(
                                          value,
                                          IssueByWorkOrderTaskApi
                                              .API_GET_WO_MASTER_FOR_HELP);
                                    }
                                  } else {}
                                },
                                decoration: InputDecoration(
                                    labelText: "Work Order No",
                                    labelStyle: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: _workOrderFocus.hasFocus
                                            ? wordOrderColor
                                            : null),
                                    contentPadding:
                                    EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                onChanged: (value) {
                                  if (value.length == 0) {
                                    resetAll();
                                  }
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "WorkOrder No cannot be blank";
                                  }
                                },
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 15.0),
                                child: InkWell(
                                  child: Image(
                                    image: AssetImage("images/QRCode.png"),
                                  ),
                                  onTap: () {
                                    scanItems(IssueByWorkOrderTaskApi
                                        .API_GET_WO_MASTER_FOR_HELP);
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Visibility(
                        visible: _isWorkOrder,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: Text(
                            strWorkOrderDesc,
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 0.0, left: 12, right: 8, bottom: 8),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.ideographic,
                          children: <Widget>[
                            Expanded(
                              flex: 9,
                              child: TextFormField(
                                focusNode: _workTaskFocus,
                                controller: _workTaskNoController,
                                textInputAction: TextInputAction.send,
                                onFieldSubmitted: (value) {
                                  if (value.length > 0) {
                                    if (workOder == null) {
                                      Utility.showToastMsg("Sync work order");
                                      FocusScope.of(context)
                                          .requestFocus(_workOrderFocus);
                                      return;
                                    } else {
                                      if (isLoading) {
                                        isLoading = false;
                                        checkInternetConnection(
                                            value,
                                            IssueByWorkOrderTaskApi
                                                .API_GET_ALL_TASK);
                                      }
                                    }
                                  } else {}
                                },
                                decoration: InputDecoration(
                                    labelText: "Work Task No",
                                    labelStyle: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: _workTaskFocus.hasFocus
                                            ? workTaskColor
                                            : null),
                                    contentPadding:
                                    EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                onChanged: (value) {
                                  if (value.length == 0) {
                                    _isWorkTask = false;
                                    strWorkTaskDesc = "";
                                    setState(() {});
                                  }
                                },

                                /*  validator: (value) {
                                  if (value.isEmpty)
                                    return "Please enter Work Task No";
                                },
                              */
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 15.0),
                                child: InkWell(
                                  child: Image.asset("images/QRCode.png"),
                                  onTap: () {
                                    scanItems(IssueByWorkOrderTaskApi
                                        .API_GET_ALL_TASK);
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Visibility(
                        visible: _isWorkTask,
                        maintainState: true,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: Text(
                            strWorkTaskDesc,
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 0.0, left: 12, right: 8, bottom: 8),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.ideographic,
                          children: <Widget>[
                            Expanded(
                              flex: 9,
                              child: TextFormField(
                                focusNode: _partNoFocus,
                                controller: _partNoController,
                                textInputAction: TextInputAction.send,
                                onFieldSubmitted: (value) {
                                  if (value.length > 0) {
                                    if (isLoading) {
                                      isLoading = false;
                                      checkInternetConnection(
                                          value,
                                          IssueByWorkOrderTaskApi
                                              .API_GET_PART_DATA_FOR_ISSUE_MOBILE);
                                    }
                                  } else {}
                                },
                                decoration: InputDecoration(
                                    labelText: "Part No",
                                    labelStyle: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: _partNoFocus.hasFocus
                                            ? partColor
                                            : null),
                                    contentPadding:
                                    EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                onChanged: (value) {
                                  if (value.toString().length == 0) {
                                    hideLocationAndSerialNumber();
                                  }
                                },
                                validator: (value) {
                                  if (value.isEmpty)
                                    return "Part No cannot be blank";
                                },
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 15.0),
                                child: InkWell(
                                  child: Image.asset("images/QRCode.png"),
                                  onTap: () {
                                    scanItems(IssueByWorkOrderTaskApi
                                        .API_GET_PART_DATA_FOR_ISSUE_MOBILE);
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Visibility(
                        visible: _isPartNo,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(strPartDesc),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.only(left: 12.0, top: 10),
                              child: Text(
                                  "Primary Location: $strPrimaryLocation",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16)),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 0.0, left: 12, right: 8, bottom: 8),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.baseline,
                                textBaseline: TextBaseline.ideographic,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: TextFormField(
                                      focusNode: _locationFocus,
                                      textInputAction: TextInputAction.send,
                                      onFieldSubmitted: (value) {
                                        if (value.length > 0) {
                                          if (isLoading) {
                                            isLoading = false;
                                            checkInternetConnection(
                                                value,
                                                IssueByWorkOrderTaskApi
                                                    .API_GET_STOCK_FOR_MOBILE_PART);
                                          }
                                        } else {}
                                      },
                                      controller: _locationController,
                                      decoration: InputDecoration(
                                          labelText: "Location",
                                          labelStyle: TextStyle(
                                              fontFamily:
                                              AppStrings.SEGOEUI_FONT,
                                              color: _locationFocus.hasFocus
                                                  ? locationColor
                                                  : null),
                                          contentPadding:
                                          EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                      onChanged: (value) {
                                        if (value.length == 0) {
                                          hideLocation();
                                        }
                                      },
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Please enter Location";
                                        }
                                      },
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 15.0),
                                      child: InkWell(
                                        child: Image(
                                          image:
                                          AssetImage("images/QRCode.png"),
                                        ),
                                        onTap: () {
                                          scanItems(IssueByWorkOrderTaskApi
                                              .API_GET_STOCK_FOR_MOBILE_PART);
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            Visibility(
                              visible: _isLocation,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 12.0),
                                    child: Text(
                                      "StockRoom",
                                      style: TextStyle(
                                        color: Colors.grey[600],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.0, left: 12.0, right: 12.0),
                                      child: DropdownButton<
                                          IssuByWorkTaskLocationModal>(
                                        value: selectedLocation,
                                        icon: Icon(Icons.arrow_drop_down),
                                        iconSize: 24,
                                        isExpanded: true,
                                        elevation: 10,
                                        hint: Text("text"),
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontFamily:
                                            AppStrings.SEGOEUI_FONT),
                                        underline: Container(
                                          height: 1,
                                          color: Colors.grey,
                                        ),
                                        onChanged: (IssuByWorkTaskLocationModal
                                        newValue) {
                                          setState(() {
                                            selectedLocation = newValue;
                                          });
                                        },
                                        items: _elementExistInWareHouseList
                                            .map((dropdownValue) {
                                          return DropdownMenuItem<
                                              IssuByWorkTaskLocationModal>(
                                              value: dropdownValue,
                                              child: Text(
                                                dropdownValue.stockRoom,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 13.5),
                                              ));
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, top: 10),
                                        child: Text(
                                          "On Hand",
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontSize: 16),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10.0, top: 10),
                                        child: Text(
                                          strOnHandQuantity,
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.grey[600],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8.0, left: 12, right: 12),
                                    child: Divider(
                                      height: 5,
                                      color: Colors.grey,
                                      thickness: 0.5,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 0.0, left: 12, right: 8, bottom: 8),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.ideographic,
                          children: <Widget>[
                            Expanded(
                              flex: 9,
                              child: TextFormField(
                                enabled: !isSerialPart,
                                focusNode: _quantityFocus,
                                controller: _quantityController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  if (value.length > 0) {
                                    if (partDetail.serialNumber == "false" &&
                                        selectedLocation.stockRoom.length > 0) {
                                      colorStatus = true;
                                      setState(() {});
                                    } else {
                                      colorStatus = false;
                                      setState(() {});
                                    }
                                  }
                                  if (value.length == 0) {
                                    setState(() {
                                      colorStatus = false;
                                    });
                                  }
                                },
                                decoration: InputDecoration(
                                    labelText: "Quantity",
                                    labelStyle: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: _quantityFocus.hasFocus
                                            ? quantityColor
                                            : null),
                                    contentPadding:
                                    EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Quantity cannot be blank or less than equal to zero";
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: isSerialPart && _isLocation,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 0.0, left: 12, right: 8, bottom: 0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.baseline,
                            textBaseline: TextBaseline.ideographic,
                            children: <Widget>[
                              Expanded(
                                flex: 9,
                                child: TextFormField(
                                  focusNode: _serialNumberFocus,
                                  controller: _serialNoController,
                                  textInputAction: TextInputAction.send,
                                  onFieldSubmitted: (value) {
                                    if (value.length > 0) {
                                      if (isLoading) {
                                        isLoading = false;
                                        checkInternetConnection(
                                            value,
                                            IssueByWorkOrderTaskApi
                                                .API_CHECK_SERIAL_NO);
                                      }
                                    } else {}
                                  },
                                  decoration: InputDecoration(
                                      labelText: "Serial No ",
                                      labelStyle: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                      ),
                                      contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                  onChanged: (value) {
                                    if (value.length == 0) {
                                      status = null;
                                    }
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please enter serial number no";
                                    }
                                  },
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: InkWell(
                                    child: Image.asset("images/QRCode.png"),
                                    onTap: () {
                                      scanItems(IssueByWorkOrderTaskApi
                                          .API_CHECK_SERIAL_NO);
                                    },
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding:
                                const EdgeInsets.only(left: 12.0, right: 2),
                                child: FlatButton(
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Text(
                                        "Issue".toUpperCase(),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: AppStrings.SEGOEUI_FONT,
                                            fontSize: 17),
                                      ),
                                    ),
                                    color: getColorButton(colorStatus),
                                    onPressed: () {
                                      _onSave();
                                    }),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding:
                                const EdgeInsets.only(right: 12.0, left: 2),
                                child: FlatButton(
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Cancel".toUpperCase(),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          fontSize: 17),
                                    ),
                                  ),
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  onPressed: () {
                                    onCancel();
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future scanItems(String title) async {
    try {
      var barcode = await BarcodeScanner.scan();
      if(barcode.rawContent.length<=0)
      {
        return;
      }
      if (title == IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP) {


        setState(() {
          _workOrderNoController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, title);
        });
      } else if (title == IssueByWorkOrderTaskApi.API_GET_ALL_TASK) {
        setState(() {
          _workTaskNoController.text = barcode.rawContent;
          if (workOder == null) {
            Utility.showToastMsg("Sync work order");
            FocusScope.of(context).requestFocus(_workOrderFocus);
            return;
          }
          checkInternetConnection(barcode.rawContent, title);
        });
      } else if (title ==
          IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
        setState(() {
          _partNoController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, title);
        });
      } else if (title ==
          IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART) {
        setState(() {
          _locationController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, title);
        });
      } else if (title == IssueByWorkOrderTaskApi.API_CHECK_SERIAL_NO) {
        setState(() {
          _serialNoController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, title);
        });
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      Utility.showToastMsg(
          'null (user returned using the "back-button" before scanning)');
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  void checkInternetConnection(String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('check connection-------------->connected');
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
                () {
              isLoading = true;
              dismissLoader();
            });

        if (type == IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP) {
          starLoader();
          getData().then((v) {
            getDetails(
                value, IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP);
          });
        } else if (type == IssueByWorkOrderTaskApi.API_GET_ALL_TASK) {
          starLoader();
          getData().then((v) {
            getDetails(value, IssueByWorkOrderTaskApi.API_GET_ALL_TASK);
          });
        } else if (type ==
            IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
          starLoader();
          getData().then((v) {
            getDetails(value,
                IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE);
          });
        } else if (type ==
            IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART) {
          starLoader();
          getData().then((v) {
            getDetails(
                value, IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART);
          });
        } else if (type == IssueByWorkOrderTaskApi.API_CHECK_SERIAL_NO) {
          getDetails(value, IssueByWorkOrderTaskApi.API_CHECK_SERIAL_NO);
        } else if (type == IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO) {
          starLoader();
          getDetails(value, IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO);
        }
      }
    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
    SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  Facility facility ;
  Future<void> getData() async
  {
    /*  SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.FACILITY_DATA).then((value)
    {
      print("json data" + value);
      Map<String, dynamic> map = jsonDecode(value);
      facility = Facility.fromJson(map);
      print("retrived from SH file--->" + facility.iFacilityCode);
      iFacilityCode =facility.iFacilityCode;
      print("iFacilityCode--" + iFacilityCode);
    });
*/


    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);
    employeeName =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
  }

  Future getDetails(String value, String type) async {
    print("data -->" + value);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";

    Map headers = new Map();
    print(NetworkConfig.BASE_URL);
    if (type == IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP) {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP;
      jsonBody = IssueByWorkOrderTaskApi.getWOMasterForHelp(companyCode, value);
      headers = NetworkConfig.headers;
    } else if (type == IssueByWorkOrderTaskApi.API_GET_ALL_TASK) {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderTaskApi.API_GET_ALL_TASK;
      jsonBody = IssueByWorkOrderTaskApi.getAllTask(
          companyCode, workOder.iWONo, value);
      headers = NetworkConfig.headers;
    } else if (type ==
        IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE;
      jsonBody = IssueByWorkOrderTaskApi.getPartDataForIssueMobile(
          companyCode, value, iFacilityCode);
    } else if (type == IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART) {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART;
      jsonBody = IssueByWorkOrderTaskApi.getStockforMobilePart(companyCode,
          partDetail.iPartNo, iFacilityCode, userId, _locationController.text);
    } else if (type == IssueByWorkOrderTaskApi.API_CHECK_SERIAL_NO) {
/*
        checkSerialNo
        companyCode: "108"   iPartNo: "17809"    serialNo: "test"    actionCode: 1
        actionSrNo: 0   stockSrNo: "53906"    quantity: 1     iWoNo: 0    iSalesOrderNo: 0      lineItemSrNo: 0
        controlId: "txtSerialNo"

      static String checkSerialNo(
          String companyCode, String iPartNo, String serialNo, String actionCode,String actionSrNo, String quantity,
          String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId
          ) {
       */

      headers = NetworkConfig.jsonHeaders;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderTaskApi.API_CHECK_SERIAL_NO;

      jsonBody = IssueByWorkOrderTaskApi.checkSerialNo(
          companyCode,
          partDetail.iPartNo,
          value,
          "1",
          "0",
          selectedLocation.stockSrNo,
          "1",
          workOder.iWONo,
          "0",
          "0",
          "txtSerialNo");
    } else if (type == IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO) {
      /*

         static String addIssueByWO(
          String companyCode, String userId,String empId, String serialNumber,int quantity,
          IssueByWorkOrderModal issueForWorkOrderModal,
          IssueByWOPartModal issueByWOPartModal,
          IssueByWoLocationModel _issueByWoLocationModal ,
      )
*/
      headers = NetworkConfig.jsonHeaders;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO;

      jsonBody = IssueByWorkOrderTaskApi.addIssueByWO(
          companyCode,
          userId,
          "",
          _serialNoController.text,
          _quantityController.text,
          workOder,
          workOrderTask,
          partDetail,
          selectedLocation);
    } else if (type == IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO) {
      /*

         static String addIssueByWO(
          String companyCode, String userId,String empId, String serialNumber,int quantity,
          IssueByWorkOrderModal issueForWorkOrderModal,
          IssueByWOPartModal issueByWOPartModal,
          IssueByWoLocationModel _issueByWoLocationModal ,
      )
*/
      headers = NetworkConfig.jsonHeaders;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO;
      jsonBody = IssueByWorkOrderTaskApi.addIssueByWO(
          companyCode,
          userId,
          employeeName,
          _serialNoController.text,
          _quantityController.text,
          workOder,
          workOrderTask,
          partDetail,
          selectedLocation);
    }

    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response =
    await post(uri, headers: headers, body: jsonBody, encoding: encoding)
        .then((resp) {
      isLoading = true;
      print(resp.statusCode);
      if (resp.statusCode == 200)
      {
        showMessage(false, "", Colors.green);
        String responseBody = resp.body;
        print("----------" + resp.body);
        print("star processe");
        onSuccessResponse(type, responseBody);
      } else {
        onErrorResponse(type, "");
      }
    })
        .catchError((error) {
      isLoading = true;
      print(error.toString());
      dismissLoader();
      onErrorResponse(type, error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
        .catchError((e) {
      isLoading = true;
      dismissLoader();
      Utility.showToastMsg("Error-->" + e.toString());
    });
  }

  onSuccessResponse(String type, String value) {
    dismissLoader();
    setState(() {});
    if (type == IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP) {
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        Map map = jsonDecode(value);
        Map map1 = map["NewDataSet"];
        workOder = IssueByWorkTaskModal.fromJson(map1["Table"]);
        print("Work Order description---->" + workOder.workOrderDescription);
        print(workOder.iFacilityCode);
        print(iFacilityCode);
        if (workOder.iFacilityCode != iFacilityCode)
        {
          String error = errorMessage(type);
          onErrorResponse(type, error);
          return;
        }
        else{
          setState(() {
            _isWorkOrder = true;
            strWorkOrderDesc = workOder.workOrderDescription;
            FocusScope.of(context).requestFocus(_workTaskFocus);
          });
        }

      } else {
        if (value == "0") {
          String error = errorMessage(type);
          onErrorResponse(type, error);
        }
      }
      return;
    }
    if (type == IssueByWorkOrderTaskApi.API_GET_ALL_TASK) {
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        Map map = jsonDecode(value);
        Map map1 = map["NewDataSet"];
        workOrderTask = IssueByGetAllTaskModal.fromJson(map1["Table"]);
        if (workOrderTask != null && workOrderTask.taskDescription.length > 0) {
          _isWorkTask = true;
          strWorkTaskDesc = workOrderTask.taskDescription;
        }
        setState(() {
          FocusScope.of(context).requestFocus(_partNoFocus);
        });
      } else {
        if (value == "0") {
          _workTaskNoController.text = "";
          Utility.showToastMsg("WorkOrder Task does not exist !");
        }
      }
      return;
    } else if (type ==
        IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        Map map = jsonDecode(value);
        Map map1 = map["NewDataSet"];
        partDetail = IssueByWorkTaskPartModal.fromJson(map1["Table"]);
        print("Part Description--->" + partDetail.partDescription);
        _isPartNo = true;
        isSerialPart = false;
        strPartDesc = partDetail.partDescription;
        if (partDetail.primaryRoomArea == null ||
            partDetail.primaryRoomArea.length == 0) {
          strPrimaryLocation = "";
        } else {
          strPrimaryLocation = partDetail.primaryRoomArea;
        }
        _quantityController.text = "1";
        if (partDetail != null && partDetail.iPartNo.length > 0) {
          if (partDetail.serialNumber.toString() == "true") {
            //isQuantityEnable = false;
            _quantityController.text = "1";
          } else {
            // isQuantityEnable = true;
            _quantityController.text = "";
          }
        }
        strIPartNo = partDetail.iPartNo;
        print("IPart No--->" + strIPartNo);
        print("value--->" + value);
        setState(() {
          setFocus(_locationFocus);
        });
        return;
      } else {
        hideLocationAndSerialNumber();
        onErrorResponse(type, errorMessage(type));
      }
    } else if (type == IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART) {
      print("value- from server ---" + value);
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;

      if (value != "0") {
        Map data1;
        try {
          data1 = jsonDecode(value);
        } catch (ex) {
          print(ex);
        }
        Map data2;
        try {
          data2 = data1['NewDataSet'];
        } catch (ex) {
          print(ex);
        }
        List<dynamic> data;
        try {
          data = data2['Table'];
        } catch (ex) {
          print("data in single");
        }
        if (data != null && data.length > 0) {
          print(" multiple array  data  --");
          _elementExistInWareHouseList = new List();
          if (data.length > 0) {
            for (int i = 0; i < data.length; i++) {
              Map dat = data.elementAt(i);
              IssuByWorkTaskLocationModal temp =
              IssuByWorkTaskLocationModal.fromJson(dat);
              _elementExistInWareHouseList.add(temp);
              if (i == 0) {
                print(" addd --");
                selectedLocation = temp;
              }
            }
            if (partDetail != null && partDetail.iPartNo.length > 0) {
              if (partDetail.serialNumber.toString() == "true") {
                isSerialPart = true;
                setFocus(_serialNumberFocus);
              } else {
                isSerialPart = false;
                setFocus(_quantityFocus);
              }
            }
            _isLocation = true;
            selectedLocation = _elementExistInWareHouseList.elementAt(0);
            strOnHandQuantity = selectedLocation.onHandQuantity;
            setState(() {
              if (isSerialPart) {
                setFocus(_serialNumberFocus);
              } else {
                setFocus(_quantityFocus);
              }
            });
            print(" size of roomfacrLoaction--");
            print(" end --");
            return;
          }
        } else {
          print(" single data  --");
          _elementExistInWareHouseList = new List();
          selectedLocation =
              IssuByWorkTaskLocationModal.fromJson(data2['Table']);
          _elementExistInWareHouseList.add(selectedLocation);
          if (partDetail != null && partDetail.iPartNo.length > 0) {
            if (partDetail.serialNumber.toString() == "true") {
              isSerialPart = true;
            } else {
              isSerialPart = false;
            }
          }
          setState(() {
            _isLocation = true;
            selectedLocation = _elementExistInWareHouseList.elementAt(0);
            strOnHandQuantity = selectedLocation.onHandQuantity;
            if (isSerialPart) {
              setFocus(_serialNumberFocus);
            } else {
              setFocus(_quantityFocus);
            }
          });
        }
      } else {
        isLoading = true;
        Utility.showToastMsg(
            "Pickable/Breakpack Location did not found for this location!");
        _locationController.text = "";
        hideLocation();
        setState(() {
          FocusScope.of(context).requestFocus(_locationFocus);
        });

        return;
      }

      return;
    } else if (type == IssueByWorkOrderTaskApi.API_CHECK_SERIAL_NO) {
      print("data for parsing" + value);
      Map map;
      try {
        map = jsonDecode(value);
      } catch (ex) {
        print(ex);
      }
      String jsonData = map["d"];
      List<dynamic> data = jsonDecode(jsonData);
      barcodeItem.clear();
      for (int i = 0; i < data.length; i++) {
        Map map = data.elementAt(i);
        SerialNumberStatus status = SerialNumberStatus.fromJson(map);
        print(status.serialNo);
        print(status.controlId);
        print(status.type);
        barcodeItem.add(status);
      }
      status = barcodeItem.elementAt(0);
      print(status.type);
      if (status.type == "success") {
        colorStatus = true;
        setState(() {});
      } else {
        colorStatus = false;
        onErrorResponse(type, status.message);
        return;
      }
    } else if (type == IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO) {
      print("data for parsing" + value);
      Map map;
      try {
        map = jsonDecode(value);
      } catch (ex) {
        print(ex);
      }
      String jsonData = map["d"];
      print(jsonData);
      if (jsonData.toString() == "") {
        showMessage(
            true,
            "Part( " +
                _partNoController.text +
                ") added in WorkOrder.\nTransaction done Successfully.",
            Colors.green);
        resetAll();
      } else {
        showMessage(
            true,
            " There is not enough quantity to perform transaction for Part(" +
                _partNoController.text +
                ")",
            Colors.red);
      }
      print("responce IssueByWorkOrderApi.API_CHECK_SERIAL_NO " + value);
      return;
    }
  }

  onErrorResponse(String type, String error) {
    print("Type---" + type);
    print("Error --" + error);

    if (type == IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP) {
      resetAll();
      Utility.showToastMsg(error);
      FocusScope.of(context).requestFocus(_workOrderFocus);
    }
    if (type == IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      Utility.showToastMsg(error);
      FocusScope.of(context).requestFocus(_partNoFocus);
      hideLocationAndSerialNumber();
    }
    if (type == IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART) {
      Utility.showToastMsg(error);
      FocusScope.of(context).requestFocus(_locationFocus);
      hideLocation();
    }
    if (type == IssueByWorkOrderTaskApi.API_CHECK_SERIAL_NO) {
      Utility.showToastMsg(error);
      FocusScope.of(context).requestFocus(_serialNumberFocus);
      _serialNoController.text = "";
      status = null;
    }
    if (type == IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO) {
      Utility.showToastMsg(error);
      return;
    }
  }

  _onSave() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else {
      setState(() {
        _validate = true;
      });
    }

    if (_workOrderNoController.text.length == 0) {
      FocusScope.of(context).requestFocus(_workOrderFocus);
      return;
    }
    if (_partNoController.text.length == 0) {
      FocusScope.of(context).requestFocus(_partNoFocus);
      return;
    }

    if (_locationController.text.length == 0) {
      FocusScope.of(context).requestFocus(_locationFocus);
      return;
    }
    printLog("line 5");
     if (_quantityController.text.length == 0) {
      FocusScope.of(context).requestFocus(_quantityFocus);
      return;
    }

    if (_locationController.text.length > 0 && selectedLocation == null) {
      FocusScope.of(context).requestFocus(_locationFocus);
      Utility.showToastMsg("Sync for location for stock room details ");
      return;
    }

    if (partDetail.serialNumber == "true") {
      if (_serialNoController.text.length == 0) {
        FocusScope.of(context).requestFocus(_serialNumberFocus);
        return;
      }
    }
    if (partDetail == null) {
      FocusScope.of(context).requestFocus(_partNoFocus);
      Utility.showToastMsg("Sync for Part Details ");
      return;
    }
    if (_locationController.text.length > 0 && selectedLocation == null) {
      FocusScope.of(context).requestFocus(_locationFocus);
      Utility.showToastMsg("Sync for location for stock room details ");
      return;
    }
    printLog("line partDetail.serialNumber  --> " + partDetail.serialNumber);
    if (partDetail != null && partDetail.serialNumber == "true") {
      printLog("line 10 .1");
      if (status == null) {
        Utility.showToastMsg("Sync for serial numberfor details ");
        FocusScope.of(context).requestFocus(_serialNumberFocus);
        return;
      }
      if (status.type == "error") {
        printLog("line 10 .2");
        FocusScope.of(context).requestFocus(_serialNumberFocus);
        Utility.showToastMsg(status.type);
        return;
      }
    }
    printLog("line 11");
    checkInternetConnection("", IssueByWorkOrderTaskApi.API_ADD_ISSUE_BY_WO);
  }

  onCancel() {
    isLoading = true;
    String strWorkOrder = _workOrderNoController.text;
    String strWorkTask = _workTaskNoController.text;
    String strPartNo = _partNoController.text;
    String strLocation = _locationController.text;
    String strQuantity = _quantityController.text;
    if (strWorkOrder.length == 0 &&
        strWorkTask.length == 0 &&
        strPartNo.length == 0 &&
        strLocation.length == 0 &&
        strQuantity.length == 0) {
      this
          .widget
          .callBack
          .viewType(DashboardView(this.widget.callBack), AppStrings.LABEL_HOME);
    } else {
      showMessage(false, "", Colors.green);
      resetAll();
    }
  }

  resetAll() {
    isLoading = true;
    _validate = false;
    _workOrderNoController.text = "";
    _partNoController.text = "";
    _quantityController.text = "";
    _locationController.text = "";
    workOrderTask = null;
    partDetail = null;
    selectedLocation = null;
    workOder = null;
    status = null;
    _elementExistInWareHouseList.clear();
    _serialNoController.text = "";
    setState(() {
      _isWorkOrder = false;
      _isPartNo = false;
      isSerialPart = false;
      //isQuantityEnable = true;
    });
    _workOrderNoController.text = "";
    _workTaskNoController.text = "";
    _partNoController.text = "";
    _locationController.text = "";
    _quantityController.text = "";
    isSerialPart = false;
    setState(() {
      _isWorkOrder = false;
      _isPartNo = false;
      _isLocation = false;
      _isWorkTask = false;
    });
  }

  void starLoader() {
    print("dialog started");
    //  startTimer();
    isLoaderRunning = true;
    Dialogs.showLoadingDialog(context, _formLocationKey);
  }

  void dismissLoader() {
    isLoading = true;
    print("before --$isLoaderRunning");
    if (isLoaderRunning == true) {
      isLoaderRunning = false;
      print("after --$isLoaderRunning");
      Navigator.of(_formLocationKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }

  void cancelTimer() {
    _timer.cancel();
  }

  void startTimer() {
    _timer = new Timer(Duration(seconds: 20), () {
      print("Yeah, this line is printed after 20 seconds");
      if (isLoaderRunning) {
        print("start dismiss");
        dismissLoader();
        Utility.showToastMsg("No Responce from Server");
      }
    });
  }

  hideLocationAndSerialNumber() {
    colorStatus = false;
    _partNoController.text = "";
    _quantityController.text = "";
    _locationController.text = "";
    _serialNoController.text = "";
    _isLocation = false;
    _isPartNo = false;
    isSerialPart = false;
    //isQuantityEnable = true;
    if (partDetail != null) {
      partDetail = null;
    }
    if (selectedLocation != null) {
      selectedLocation = null;
    }
    _elementExistInWareHouseList = [];
    setState(() {});
  }

  hideLocation() {
    colorStatus = false;
    _locationController.text = "";
    selectedLocation = null;
    _serialNoController.text = "";
    setState(() {
      _isLocation = false;
    });
  }


  String errorMessage(String type) {
    String error = "";
    if (type == IssueByWorkOrderTaskApi.API_GET_WO_MASTER_FOR_HELP) {
      error =
      "WorkOrder does not exist or not in Open, Approved or Scheduled status !";
    }
    if (type == IssueByWorkOrderTaskApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      error = "Part No does not exist or is inactive!";
    }
    if (type == IssueByWorkOrderTaskApi.API_GET_STOCK_FOR_MOBILE_PART) {
      error = "Pickable/Breakpack Location did not found for this location!";
    }
    return error;
  }

  void testData() {
    //_workOrderNoController.text="WO12036";
    //_workTaskNoController.text ="INS-APX-TANK";
    //_partNoController.text="2345";
//    _locationController.text ="01A03";
  }

  Color getColorButton(bool status) {
    if (status)
      return AppColors.CYAN_BLUE_COLOR;
    else
      return Colors.grey;
  }

  void printLog(String msg) {
    print(msg);
  }

  void showMessage(bool status, String msg, Color color) {
    serverRespone = status;
    serverResponse = msg;
    serverResponeColor = color;
    setState(() {});
  }

  Future setFocus(FocusNode focus) async {
    Future.delayed(Duration(microseconds: 50), () {
      setState(() {
        FocusScope.of(context).requestFocus(focus);
      });
    });
  }
}
