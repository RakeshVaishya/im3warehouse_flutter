import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/issue_pick_list/Issue_by_pick_list_model.dart';
import 'package:im3_warehouse/models/pick_my_order/order_item.dart';
import 'package:im3_warehouse/models/pick_my_order/pick_order.dart';
import 'package:im3_warehouse/models/pick_my_order/so_line_Item_to_pick.dart';
import 'package:im3_warehouse/network/api/pick_order_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;

import 'view_callback.dart';


class OrderList extends StatefulWidget {
  ViewTypeCallBack callBack;
  Callback _callback;
  List<SOLineItemToPick> solineItemList = [];
  PickOrder pickOrder;
  Facility facility;
  String userId;
  String companyCode;
  OrderList(this._callback ,this.solineItemList,this.pickOrder , this.facility ,this.userId ,this.companyCode);

  @override
  State<StatefulWidget> createState() {
    return OrderListState();
  }
}
class OrderListState extends State<OrderList> {


  TextEditingController serialNumberController = new TextEditingController();
  int indexNumber;
  String stockSerialNumber = "";
  bool isVisible = false;
  List<SOLineItemToPick> solineItemList = [];
  bool isTabVisible = true;
  Callback _callback;
  PickOrder pickOrder;
  var scrollKey = GlobalKey();
  Facility facility;
  IssueByPickListModel issueByPickListModel;
  ProgressDialog pr;

  @override
  void initState() {
    super.initState();


    print("start dialog--->");
    _callback = widget._callback;
    solineItemList = widget.solineItemList;
    solineItemList =solineItemList.reversed.toList();
    pickOrder = widget.pickOrder;
    // testData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.all(0),
        padding: EdgeInsets.all(0),
        child: new Stack(
          //alignment:new Alignment(x, y)
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    color: Colors.black12,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //NOT WORKING!
                      children: <Widget>[
                    Padding(
                    padding: EdgeInsets.only(top:5 ,bottom: 5,left:15),
                    child:
                        Text("Order # "+pickOrder.salesOrderNo,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 15,
                            )),
                    )
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Expanded(
                    child: Scrollbar(
                      child: ListView.builder(
                        padding: EdgeInsets.only(bottom: 50),
                        scrollDirection: Axis.vertical,
                        itemCount: solineItemList.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            color: (index % 2 == 0) ? Colors.white : Color
                                .fromRGBO(220, 220, 220, 99.0),
                            child: InkWell(
                              hoverColor: Colors.black26,
                              onTap: () {
                                //onTapped(solineItemList.elementAt(index));
                              },
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 7, top: 10, bottom: 5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment
                                      .stretch,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(top:10,left: 10,right: 10),
                                      child:
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                  "Part #:" + solineItemList
                                                      .elementAt(index)
                                                      .partNo,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                  )),
                                            ),
                                          ),
                                          Expanded(
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                  "Order Qty # : " + solineItemList
                                                      .elementAt(index)
                                                      .orderQuantity
                                                      .toString(),

                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 15,
                                                  )),
                                            ),
                                          ),
                                        ],
                                      ),

                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top:5,left: 10,right: 10),

                                      child:Row(
                                        children: <Widget>[

                                          /*    Expanded(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(

                                    pickOrderList
                                        .elementAt(index)
                                        .notes
                                        .toString(),



                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14,
                                )),
                          ),
                        ),
                    */

                                          Expanded(
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Text("Back Order Qty #: " +backOrderQty(   solineItemList
                                                  .elementAt(index)).toString(),
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                  )),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                                      child:
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 8,
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                  "Ready to pick Qty # :"+ solineItemList
                                                      .elementAt(index)
                                                      .quantityLeftToIssue,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 15,
                                                  )),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                  ],
                                ),
                              )
                              ,),
                          );
                        },
                      ),
                    ),
                  ),
                )
              ],
            ),
            new Positioned(
              child: new Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 4.0),
                          child: MaterialButton(
                            onPressed: () {
                              _callback.doPicking(solineItemList, pickOrder);
                              /* Navigator.push(
                                  context, MaterialPageRoute(builder: (context) => MyApp()));*

                              */
                            },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "START PICKING".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: MaterialButton(
                            onPressed: () {
                              _callback.callBackScreen("TabView");
                            },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Cancel".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }


  double backOrderQty( SOLineItemToPick soLineItemToPick){
    double  orderQuantity =double.parse(soLineItemToPick.orderQuantity);
    double  issuedQuantity =double.parse(soLineItemToPick.issuedQuantity);
    double  quantityLeftToIssue =double.parse(soLineItemToPick.quantityLeftToIssue);

    return orderQuantity-issuedQuantity-quantityLeftToIssue;
  }
}


