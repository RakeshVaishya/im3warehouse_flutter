import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/issue_pick_list/Issue_by_pick_list_model.dart';
import 'package:im3_warehouse/models/issue_pick_list/issue_pick_list.dart';
import 'package:im3_warehouse/models/pick_my_order/so_line_Item_to_pick.dart';
import 'package:im3_warehouse/network/api/issue_by_picklist_api.dart';
import 'package:im3_warehouse/network/api/pick_order_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberModel.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_manager.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;
import 'dart:core';
import 'package:im3_warehouse/models/pick_my_order/pick_order.dart';
import 'order_item.dart';
import 'view_callback.dart';
import 'package:im3_warehouse/views/physical_count/dialogCallback.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_callback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';

class PickManager extends StatefulWidget {
  ViewTypeCallBack callBack;
  Callback _callback;
  List<SOLineItemToPick> lineItemList = [];
  PickOrder pickOrder;
  IssueByPickListModel issueByPickListModel;

  PickManager(this._callback, this.lineItemList, this.pickOrder,
      this.issueByPickListModel);

  @override
  State<StatefulWidget> createState() {
    return PickManagerState();
  }
}

class PickManagerState extends State<PickManager>
    implements DialogCallback, SerialNumberCallback {
  TextEditingController _pickTicketNoController = TextEditingController();
  TextEditingController _toBePickedQtyController = TextEditingController();
  TextEditingController _confirmPartNoController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  TextEditingController _partDescController = TextEditingController();
  TextEditingController _pickedLocationController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _qtyToIssueController = TextEditingController();
  TextEditingController _palletNoController = TextEditingController();
  TextEditingController _serialNumberNoController = TextEditingController();

  List<SOLineItemToPick> orderItemList = [];
  SOLineItemToPick orderItem;
  PickOrder pickOrder;
  bool serialNoBool = false;
  List<SerialNumberStatus> barcodeItem = new List();
  bool loading = true;
  var _formkey = GlobalKey<FormState>();
  bool _validate = false;
  bool isVisible = true;
  String userId = "";
  String companyCode = "";
  Facility facility;
  String barcodeValue = "";
  IssueByPickListModel issueByPickListModel;


  bool isLoading = true;
  bool isLoaderRuning = false;


  String strSerialNo = "";
  String controlId = "";
  bool isSerializedPart = false;
  bool isConfirmPick = false;
  bool isQuantityAllowToChange = true;
  FocusNode _pickTicketFocus = new FocusNode();
  FocusNode _locationFocus = new FocusNode();
  FocusNode _qtyToIssueFocus = new FocusNode();
  FocusNode _palletNoFocus = new FocusNode();
  FocusNode _confirmPartNo = new FocusNode();
  String PICK_TYPE = "";
  BuildContext context;
  IssueByPickList issueByPick;
  List<IssueByPickList> issueByPickList = [];
  String getPickListFromServer = "getPickListFromServer";

  String totalPickTicket = "0";
  String completedPickedTicket = "0";
  String inCompletedPickedTicket = "0";
  String skipPickTicket = "0";

  TextEditingController serialNumberController = new TextEditingController();
  int indexNumber;
  String stockSerialNumber = "";
  List<PickOrder> pickOrderList = [];
  bool isSavingData = false;
  bool isTabVisible = true;
  Callback _callback;
  String skipButtonName = "SKIP";
  String pickButtonName = "Pick & Next";
  int totalPickListSize = 0;
  int totalPicked = 0;
  int remaining = 0;
  //ProgressDialog pr;

  int pickListSelectedIndex = 0;
  int counter = 0;
  var _dialogKey = GlobalKey<FormState>();
  var _dialogKey2 = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    pickOrder = widget.pickOrder;
    orderItemList = widget.lineItemList;
    _callback = widget._callback;
    totalPickListSize = orderItemList.length;
    print("start dialog--->");

    if (orderItemList.length == 1) {
      skipButtonName = "Cancel".toUpperCase();
    }
    /*orderItem = orderItemList.elementAt(pickListSelectedIndex);
    pickListSelectedIndex++;
    // _pickTicketNoController.text =orderItem.product;
    // testData();
    */ /* _descriptionController.text ="test";
    _toBePickedQtyController.text ="1";*/ /*
    checkInternetConnection(
        orderItem.iInvPickingHeaderId, PickOrderApi.API_GET_PICK_TICKET_HELP);*/


    //orderItem = orderItemList.elementAt(0);
    //checkInternetConnection(orderItem.iInvPickingHeaderId, PickOrderApi.API_GET_PICK_TICKET_HELP);

    orderItem = orderItemList.elementAt(pickListSelectedIndex);
       checkInternetConnection(
        orderItem.iInvPickingHeaderId, PickOrderApi.API_GET_PICK_TICKET_HELP);
  }

  Future<void> getData() async
  {
    facility = AppStrings.facility;
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
  }

  @override
  Widget build(BuildContext context)
  {
    //pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: false, showLogs: false );
    this.context = context;
     return SafeArea(
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.all(0),
        padding: EdgeInsets.all(0),
        child: new Stack(
          //alignment:new Alignment(x, y)
          children: <Widget>[
        Container(
            margin: EdgeInsets.only(bottom: 60),
           child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    color: Color.fromRGBO(215, 220, 220, 99.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      //NOT WORKING!
                      children: <Widget>[
                        InkWell(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.black87,
                            size: 25.0,
                          ),
                          onTap: () {
                            _callback.callBackScreen("OrderList");
                          },
                        ),
                        Text(
                            "Pick Sr : $totalPickListSize of " +getPickSerialNumber().toString(),
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 17,
                            )),
                        InkWell(
                          child: Text(
                              "Picked :" +
                                  totalPickTicketProcessed().toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 15,
                              )),
                          onTap: () {
                            //    _callback.callBackScreen("OrderList");

                            print(" Picked Item called");
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        OrderItemDetail(orderItemList)));
                          },
                        ),
                        InkWell(
                          child: Text(
                              "Remaining : " +
                                  totalPickTicketRemainToProcess().toString(),
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                fontSize: 15,
                              )),
                          onTap: () {
                            print(" Remaing Item called");
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        OrderItemDetail(orderItemList)));
                          },
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Expanded(
                    child: SingleChildScrollView(
                        child: Scrollbar(
                            child: Form(
                                key: this._formkey,
                                autovalidate: _validate,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 9,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 0.0, right: 5),
                                            child: TextFormField(
                                                focusNode: _pickTicketFocus,
                                                style: TextStyle(
                                                    color: Colors.black),
                                                controller:
                                                    _pickTicketNoController,
                                                textInputAction:
                                                    TextInputAction.send,
                                                onFieldSubmitted: (value) {
                                                  if (value.toString().length >
                                                      0) {
                                                    if(orderItem.iInvPickingHeaderId==issueByPickListModel.iInvPickingHeaderId){
                                                    print("data exist---");
                                                      return;
                                                    }
                                                    if (isLoading) {
                                                      isLoading = false;
                                                      checkInternetConnection(
                                                          value,
                                                          PickOrderApi
                                                              .API_GET_PICK_TICKET_HELP);
                                                    }
                                                  }
                                                },
                                                decoration: InputDecoration(
                                                    labelText: ""
                                                        "Picked# #",
                                                    labelStyle: TextStyle(
                                                        color: Colors.black87)),
                                                onChanged: (text) {
                                                  if (text.length == 0) {
                                                    isVisible = false;
                                                    resetData();
                                                  }
                                                },
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return "Please Enter Ticket No";
                                                  }
                                                }),
                                          ),
                                        ),

                                        /*
                                        Expanded(
                                            child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20),
                                          child: InkWell(
                                            child: Image.asset(
                                              "images/QRCode.png",
                                            ),
                                            onTap: () {
                                              barCodeScan(IssueByPickListApi
                                                  .API_GET_PICK_TICKET_HELP);
                                            },
                                          ),
                                        ))
                                    */
                                      ],
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: TextFormField(
                                        style:
                                            TextStyle(color: Colors.grey[600]),
                                        enabled: false,
                                        controller: _descriptionController,
                                        decoration: InputDecoration(
                                            labelText: "SO #",
                                            labelStyle: TextStyle(
                                                color: Colors.grey[500])),
                                      ),
                                    ),
                                    Visibility(
                                      visible: isVisible,
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Expanded(
                                                flex: 65,
                                                child: TextFormField(
                                                  enabled: false,
                                                  style: TextStyle(
                                                      color: Colors.grey[600]),
                                                  controller:
                                                      _pickedLocationController,
                                                  decoration: InputDecoration(
                                                      labelText:
                                                          " Pick Location.",
                                                      labelStyle: TextStyle(
                                                          color: Colors
                                                              .grey[550])),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 35,
                                                child: TextFormField(
                                                  style: TextStyle(
                                                      color: Colors.grey[600]),
                                                  enabled: false,
                                                  controller:
                                                      _toBePickedQtyController,
                                                  decoration: InputDecoration(
                                                      labelText:
                                                          " To Be Picked  QTY ",
                                                      labelStyle: TextStyle(
                                                          color: Colors
                                                              .grey[550])),
                                                ),
                                              )
                                            ],
                                          ),
                                    Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 9,
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets.only(
                                                      bottom: 0.0,
                                                      right: 5),
                                                  child: TextFormField(
                                                      focusNode: _locationFocus,
                                                      style: TextStyle(
                                                          color: Colors.black),
                                                      controller: this
                                                          ._locationController,
                                                      textInputAction:
                                                      TextInputAction.next,
                                                      onFieldSubmitted:
                                                          (value) {
                                                        if (_locationController
                                                            .text
                                                            .toLowerCase() !=
                                                            issueByPickListModel
                                                                .roomAreaCode
                                                                .toLowerCase()) {
                                                          _locationController
                                                              .text = "";
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                              _locationFocus);
                                                          Utility.showToastMsg(
                                                              "Entered Location does not match with Picked Location");
                                                          return;
                                                        } else {
                                                          if(isSerializedPart)
                                                          {
                                                           showSerialNumberDialog();

                                                          }
                                                          else{
                                                            FocusScope.of(context)
                                                                .requestFocus(
                                                                _qtyToIssueFocus);
                                                          }

                                                        }
                                                      },
                                                      decoration: InputDecoration(
                                                          labelText:
                                                          "Location",
                                                          labelStyle: TextStyle(
                                                              color: Colors
                                                                  .black87)),
                                                      onChanged: (text) {},
                                                      validator: (value) {
                                                        if (value.isEmpty) {
                                                          return "Please Enter Location No";
                                                        }
                                                      }),
                                                ),
                                              ),
                                              Expanded(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(
                                                        top: 20),
                                                    child: InkWell(
                                                      child: Image.asset(
                                                        "images/QRCode.png",
                                                      ),
                                                      onTap: () {
                                                        barCodeScan("Location");
                                                      },
                                                    ),
                                                  ))
                                            ],
                                          ),
                                          Visibility(
                                            visible: true,
                                            maintainState: true,
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 9,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8.0, top: 0),
                                                    child: Material(
                                                      child: InkWell(
                                                        onTap: () {
                                                          print("data---");
                                                          showSerialNumberDialog();
                                                        },
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      bottom:
                                                                          0.0,
                                                                      top: 0),
                                                              child: Text(
                                                                "Serial No.",
                                                                style: TextStyle(color:
                                                                        Colors.grey[
                                                                            600],
                                                                    fontFamily:
                                                                        AppStrings
                                                                            .SEGOEUI_FONT,
                                                                    fontSize: serialNoBool
                                                                        ? AppStrings
                                                                            .FONT_SIZE_13
                                                                        : AppStrings
                                                                            .FONT_SIZE_16),
                                                              ),
                                                            ),
                                                            InkWell(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            8.0),
                                                                child: InkWell(
                                                                  child: Text(
                                                                      strSerialNo,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style: TextStyle(
                                                                          fontFamily: AppStrings
                                                                              .SEGOEUI_FONT,
                                                                          color: Colors.grey[
                                                                              600],
                                                                          fontSize:
                                                                              AppStrings.FONT_SIZE_15)),
                                                                  onTap: () {
                                                                    print(
                                                                        "tapped on serial no");
                                                                    showSerialNumberDialog();
                                                                  },
                                                                ),
                                                              ),
                                                              onTap: () {
                                                                print(
                                                                    "tapped on serial no");
                                                                showSerialNumberDialog();
                                                              },
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      top: 5),
                                                              child: Divider(
                                                                thickness: 1,
                                                                color:
                                                                    Colors.grey,
                                                                height: 10,
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                    child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 25),
                                                  child: InkWell(
                                                    child: Image.asset(
                                                      "images/QRCode.png",
                                                    ),
                                                    onTap: () {
                                                      showSerialNumberDialog();
                                                    },
                                                  ),
                                                ))
                                              ],
                                            ),
                                          ),


                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 10.0),
                                            child: TextFormField(
                                              focusNode: _qtyToIssueFocus,
                                              keyboardType:
                                              TextInputType.number,
                                              style: TextStyle(
                                                  color: Colors.grey[600]),
                                              readOnly:
                                              !isQuantityAllowToChange,
                                              controller: _qtyToIssueController,
                                              decoration: InputDecoration(
                                                  labelText: "Quantity Picked",
                                                  labelStyle: TextStyle(
                                                      color: Colors.grey[550])),
                                            ),
                                          ),



                                          Visibility(
                                            visible: false,
                                            maintainState: true,
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 9,
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8.0, top: 0),
                                                    child: Material(
                                                      child: InkWell(
                                                        onTap: () {
                                                          print("data---");
                                                          showSerialNumberDialog();
                                                        },
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      bottom:
                                                                          0.0,
                                                                      top: 0),
                                                              child: Text(
                                                                AppStrings
                                                                    .LABEL_SERIAL_NO,
                                                                style: TextStyle(
                                                                    color:
                                                                        Colors.grey[
                                                                            600],
                                                                    fontFamily:
                                                                        AppStrings
                                                                            .SEGOEUI_FONT,
                                                                    fontSize: serialNoBool
                                                                        ? AppStrings
                                                                            .FONT_SIZE_13
                                                                        : AppStrings
                                                                            .FONT_SIZE_16),
                                                              ),
                                                            ),
                                                            InkWell(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            8.0),
                                                                child: InkWell(
                                                                  child: Text(
                                                                      strSerialNo,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      style: TextStyle(
                                                                          fontFamily: AppStrings
                                                                              .SEGOEUI_FONT,
                                                                          color: Colors.grey[
                                                                              600],
                                                                          fontSize:
                                                                              AppStrings.FONT_SIZE_15)),
                                                                  onTap: () {
                                                                    print(
                                                                        "tapped on serial no");
                                                                    showSerialNumberDialog();
                                                                  },
                                                                ),
                                                              ),
                                                              onTap: () {
                                                                print(
                                                                    "tapped on serial no");
                                                                showSerialNumberDialog();
                                                              },
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      top: 5),
                                                              child: Divider(
                                                                thickness: 1,
                                                                color:
                                                                    Colors.grey,
                                                                height: 10,
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                    child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 25),
                                                  child: InkWell(
                                                    child: Image.asset(
                                                      "images/QRCode.png",
                                                    ),
                                                    onTap: () {},
                                                  ),
                                                ))
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    /*  Container(
                                                                color: Color
                                                                    .fromRGBO(
                                                                    220, 220,
                                                                    220, 99.0),
                                                                child: Row(
                                                                    mainAxisAlignment: MainAxisAlignment
                                                                        .spaceEvenly,
                                                                    children: <
                                                                        Widget>[
                                                                        Expanded(
                                                                            child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment
                                                                                    .spaceEvenly,
                                                                                children: <
                                                                                    Widget>[
                                                                                    Text(
                                                                                        "Schedule Cycle count",
                                                                                        style: TextStyle(
                                                                                            fontSize: 13
                                                                                        ),
                                                                                        softWrap: false,
                                                                                        overflow: TextOverflow
                                                                                            .ellipsis,
                                                                                    ),
                                                                                    Checkbox(
                                                                                        value: false,
                                                                                        onChanged: (
                                                                                            bool value) {
                                                                                            setState(() {});
                                                                                        },
                                                                                    ),
                                                                                ],
                                                                            ),
                                                                        ),
                                                                        Expanded(
                                                                            child: Text(
                                                                                "Find Another Location",
                                                                                textAlign: TextAlign
                                                                                    .center,
                                                                                style: TextStyle(
                                                                                    fontSize: 13
                                                                                )
                                                                            ),
                                                                        ),
                                                                    ],
                                                                ),
                                                            ),*/
                                  ],
                                )))),
                  ),
                )
              ],
            ),
        ),

            new Positioned(
              child: new Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.only(top: 0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 4.0, top: 0),
                          child: MaterialButton(
                            onPressed: () {
                              print("pickListSelectedIndex----" +
                                  pickListSelectedIndex.toString());
                              if (pickButtonName == "FINISH") {
                                _callback.callBackScreen("TabView");
                                return;
                              } else {
                                onSave();
                              }
                            },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                pickButtonName.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: MaterialButton(
                            onPressed: () {


                              if (skipButtonName == "FINISH" ||
                                  skipButtonName == "Cancel".toUpperCase()) {
                                _callback.callBackScreen("TabView");
                                return;
                              }
                              else{
                                pickTicketStatus("skip");
                                setNextItem();
                                print("  after --pickListSelectedIndex----" +
                                    pickListSelectedIndex.toString());
                              }
                              /*if (pickListSelectedIndex == totalPickListSize)
                              {
                                skipButtonName = "FINISH";
                                setState(() {});
                              }*/
                            },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                skipButtonName.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  Widget showCircularProgressbarWidget() {
    return Container(
        child: Center(
      child: new Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          new CircularProgressIndicator(
            strokeWidth: 5,
            backgroundColor: Colors.red,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5.0),
            child: new Text(
              "Loading",
              style:
                  TextStyle(fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
            ),
          ),
        ],
      ),
    ));
  }
  onSave() {
    getDataFromField();
  }

  @override
  void getList(List<String> data) {
    // TODO: implement getList
  }

  void getDataFromField() {

    if (_locationController.text.toLowerCase() !=
        issueByPickListModel.roomAreaCode.toLowerCase()) {
      FocusScope.of(context).requestFocus(_locationFocus);
      Utility.showToastMsg(
          "Entered Location does not match with Picked Location");
      return;
    }

   if(_qtyToIssueController.text.length ==0){

     Utility.showToastMsg(
         "Entered Issue Quantity");
     FocusScope.of(context).requestFocus(_qtyToIssueFocus);
     return;

   }



    int maxLimit = 0;
    double quantityLeftToIssue = double.parse(issueByPickListModel.quantityLeftToIssue);
    maxLimit = quantityLeftToIssue.toInt();


    int issueQty = int.parse(_qtyToIssueController.text);


   /* if (StringUtil.isDouble(issueByPickListModel.quantityLeftToIssue)) {
      double data =
          StringUtil.tryParse(issueByPickListModel.quantityLeftToIssue);
      maxLimit = data.toInt();
    }
    print(maxLimit);
    int quantityLeftToIssue = 0;
    if (StringUtil.isDouble(issueByPickListModel.quantityLeftToIssue)) {
      double data =
          StringUtil.tryParse(issueByPickListModel.quantityLeftToIssue);
      quantityLeftToIssue = data.toInt();
    }
*/
    /*if (StringUtil.isInt(_qtyToIssueController.text)) {
      quantityLeftToIssue = StringUtil.tryParse(_qtyToIssueController.text);
    }
*/
    print(issueQty);
    if (issueQty <= 0)
    {
      FocusScope.of(context).requestFocus(_qtyToIssueFocus);
      Utility.showToastMsg("Quantity to Issue cannot be zero");
      return;
    }

    if (issueQty > maxLimit) {
      FocusScope.of(context).requestFocus(_qtyToIssueFocus);
      Utility.showToastMsg(
          "Quantity to Issue cannot be greater than picked quantity");
      return;
    }

    issueByPickListModel.sOPalletNo = _palletNoController.text;
    print("pick ticket number --" + _pickTicketNoController.text);
    print("_documentNoController No --" + _descriptionController.text);
    print("_documentNoController No --" + _descriptionController.text);
    print("_partNoController No --" + _toBePickedQtyController.text);
    print("_partDescController No --" + _partDescController.text);
    print("_pickedLocationController No --" + _pickedLocationController.text);
    print("_locationController No --" + _locationController.text);
    print("_qtyToIssueController No --" + _qtyToIssueController.text);
    print("_palletNoController No --" + _palletNoController.text);

    if (isLoading) {
      isLoading = false;
      checkInternetConnection(
          "", IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET);
    }
  }

  void onCancel() {
    setState(() {
      isVisible = false;
      getClearFromField();
    });
  }

  void getClearFromField() {
    _toBePickedQtyController.text = "";
    _descriptionController.text = "";
    _partDescController.text = "";
    _pickTicketNoController.text = "";
    _pickedLocationController.text = "";
    _locationController.text = "";
    _qtyToIssueController.text = "";
    _palletNoController.text = "";
    print("part No --" + _toBePickedQtyController.text);
    print("part desc  --" + _partDescController.text);
  }

  void testData() {
    _toBePickedQtyController.text = "5010159A";
    _descriptionController.text = "SO09700";
    _partDescController.text = "GASKET SET AT 2000";
    _pickedLocationController.text = "EDM-A-J2G";
    _locationController.text = "DM-A-J2G";
    _qtyToIssueController.text = "1.00";
    _palletNoController.text = "";
    print("part No --" + _toBePickedQtyController.text);
    print("part desc  --" + _partDescController.text);
  }

  void setData() {
    isVisible = true;
    _toBePickedQtyController.text = issueByPickListModel.quantityLeftToIssue;
    _descriptionController.text = issueByPickListModel.documentNo;
    _partDescController.text = issueByPickListModel.partDescription;
    _pickedLocationController.text = issueByPickListModel.pickLocation;
    _qtyToIssueController.text = "";
    _pickTicketNoController.text = issueByPickListModel.iInvPickingHeaderId;
    print("part No --" + _toBePickedQtyController.text);
    print("part desc  --" + _partDescController.text);
  }

  Future barCodeScan(String type) async {
    try {
      var barcode = await BarcodeScanner.scan();

      if (type == PickOrderApi.API_GET_PICK_TICKET_HELP) {
        _pickTicketNoController.text = barcode.rawContent;
        if (barcode.rawContent.length == 0) {
          return;
        }
        print(_pickTicketNoController.text);
        setState(() {
          checkInternetConnection(_pickTicketNoController.text, type);
        });
      } else if (type == "Location") {
        if (barcode.rawContent.toLowerCase() ==
            issueByPickListModel.roomAreaCode.toLowerCase()) {
          _locationController.text = barcode.rawContent;
          FocusScope.of(context).requestFocus(_palletNoFocus);
          if (barcode.rawContent.length == 0) {
            return;
          }
        } else {
          _locationController.text = "";
          FocusScope.of(context).requestFocus(_locationFocus);
          Utility.showToastMsg(
              "Entered Location does not match with Picked Location");
          return;
        }
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    }
  }

  Future getDetails(String data, String type) async {
    print("part no -->" + data);
    print("user --Id  -->" + userId);
    print("companyCode --  -->" + companyCode);
    String jsonBody = "";
    String uri = "";
    print(NetworkConfig.BASE_URL);
    print("type --  -->" + type);

    if (type == PickOrderApi.API_GET_PICK_TICKET_HELP.toString())
    {
      print("--- Pickte ticket called  --");
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PickOrderApi.API_GET_PICK_TICKET_HELP;
      jsonBody = PickOrderApi.getPickTickethelp(
          companyCode, data, facility.iFacilityCode);

    }

    if (type == IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET) {
      print("---save  pick list --");
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET;

      /*static String saveIssueByPickTicket(IssueByPickListModel issueByPickListModel,
      String userId ,String empld,String iWorkOrderTask,String txtQuantity )
      */

      if (issueByPickListModel.partSrNo == null) {
        issueByPickListModel.partSrNo = "0";
      }

      if (issueByPickListModel.partSrNo != null &&
          issueByPickListModel.partSrNo.length == 0) {
        issueByPickListModel.partSrNo = "0";
      }

      if (issueByPickListModel.iWONo == null) {
        issueByPickListModel.iWONo = "0";
      }
      _palletNoController.text = issueByPickListModel.sOPalletNo;
      jsonBody = IssueByPickListApi.saveIssueByPickTicket(issueByPickListModel,
          userId, userId, "0", _qtyToIssueController.text);
      print("URI---" + uri);
      print("jsonBody---" + jsonBody);
    }


    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response = await post(uri,
            headers: NetworkConfig.headers, body: jsonBody, encoding: encoding)
        .then((resp) {
          print(resp.statusCode);
          isLoading = true;
          //print("----body ------" + resp.body);
          print(resp.statusCode);
          if (resp.statusCode == 200)
          {
            String responseBody = resp.body;
             print("----------" + "suceess responce ");
            onSuccessResponse(type, responseBody);
          }
          else {

            printLog("responce code is not 200 ");

            dismissLoader(" responce  not 200");
            print("  ");
            isLoading = true;
            if( isSavingData)
            {
              isSavingData = false;
            }
            if (type == PickOrderApi.API_GET_PICK_TICKET_HELP)
            {
              _pickTicketNoController.text = "";
              Utility.showToastMsg("Pick Ticket No does not exist ");
              return;
            }
            return;
          }
        })
        .catchError((error)
        {
          print(" catchError " + error.toString());
          //Utility.showToastMsg("Error-Responce from server Exception");
          isLoading = true;
         // dismissLoader("catchError");
          print("error ----"+error.toString());

          //  onErrorResponse(type ,  error.toString());
        })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))   .catchError((e)
    {
          print(" Time out error ");
          isLoading = true;
         // dismissLoader(" timeout catchError");
          Utility.showToastMsg("Error-->" + e.toString());
        });
  }

  void onSuccessResponse(String type, String responseBody) {


    if (type == PickOrderApi.API_GET_PICK_TICKET_HELP)
    {
      printLog("line 1  onSuccessResponse  PickOrderApi.API_GET_PICK_TICKET_HELP method ");
      isLoading = true;
      dismissLoader("onSuccessResponse");
      //print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      printLog("line 2    PickOrderApi.API_GET_PICK_TICKET_HELP method ");

      if (responseBody != "0")
      {         Map data1 = jsonDecode(responseBody);
        Map<String, dynamic> data2 = data1['NewDataSet'];
        issueByPickListModel = IssueByPickListModel.fromJson(data2['Table']);
      printLog("    PickOrderApi.API_GET_PICK_TICKET_HELP method  line 3");

        if (issueByPickListModel.canBeIssued.toLowerCase() =="FALSE".toLowerCase())
        {
          if (issueByPickListModel.iWONo != '0')
          {
            Utility.showToastMsg("WO Part not in Schedule Status");
          } else {
            Utility.showToastMsg("SO line Item not in Open Status");
          }
        }
        else {

          printLog("    PickOrderApi.API_GET_PICK_TICKET_HELP method  line 4 before increment  counter  "+counter.toString());
          printLog("    PickOrderApi.API_GET_PICK_TICKET_HELP method  line 5 before increment  pickListSelectedIndex  "+pickListSelectedIndex.toString());

          pickListSelectedIndex++;
          counter++;

          printLog("    PickOrderApi.API_GET_PICK_TICKET_HELP method  line 6 after increment  counter  "+counter.toString());
          printLog("    PickOrderApi.API_GET_PICK_TICKET_HELP method  line 7 after increment  pickListSelectedIndex  "+pickListSelectedIndex.toString());

          if (issueByPickListModel.iSalesOrderNo != null)
          {
            if (issueByPickListModel.iSalesOrderNo != "0") {
              print("Sale Order");
              PICK_TYPE = "SO";
              isQuantityAllowToChange = true;
            }
          }
          if (issueByPickListModel.iWONo != null) {
            if (issueByPickListModel.iWONo != "0") {
              PICK_TYPE = "WO";
              isQuantityAllowToChange = false;
              print("Work Order");
            }
          }
          if (issueByPickListModel.iWONo == null &&
              issueByPickListModel.iSalesOrderNo != null) {
            if (issueByPickListModel.iSalesOrderNo == "0") {
              PICK_TYPE = "TR";
              isQuantityAllowToChange = false;
              print("Transfer Order");
            }
          }
          if (issueByPickListModel.isConfirmPick.toLowerCase() ==
              "TRUE".toLowerCase()) {
            isConfirmPick = true;
          } else
            {
            isConfirmPick = false;
          }
          if (issueByPickListModel.isSerializedPart.toLowerCase() ==
              "TRUE".toLowerCase()) {
            isSerializedPart = true;
          } else {
            isSerializedPart = false;
          }
          if (isConfirmPick) {
            FocusScope.of(context).requestFocus(_confirmPartNo);
          } else {
            FocusScope.of(context).requestFocus(_locationFocus);
          }
        }


      printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 8  call totalPickTicketProcessed");

      totalPickTicketProcessed();

      printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 9  call totalPickTicketRemainToProcess");

      totalPickTicketRemainToProcess();

      printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 9  call set data");

      setData();

        setState(() {});
      }
      else {

        printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 10 else condition");

        printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 11  orderItemList size " +orderItemList.length.toString());

        if (orderItemList.length == 1)
        {

          printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 12 orderItemList.length == 1 " );

          totalPickTicketProcessed();

          printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 13  " );

          totalPickTicketRemainToProcess();

          printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 14  " );

          resetData();
          printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 15  " );

          _pickTicketNoController.text = "";
          Utility.showToastMsg("Pick Ticket No does not exist ");
          setState(() {});
          return;
        }
        else
          {
            printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 16  " );

            resetData();
            printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 17  orderItemList size  " +orderItemList.length.toString() +" counter--"+counter.toString());

            if(orderItemList.length == counter)
          {

            printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 1 8  FINISH call ");

            pickButtonName = "FINISH";
            _pickTicketNoController.text = "";
            totalPickTicketProcessed();
            totalPickTicketRemainToProcess();
            Utility.showToastMsg("Pick Ticket No does not exist ");
            setState(() {});
            return;
          }
          else{

              printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 1 8   call next item  pickListSelectedIndex-- before" +pickListSelectedIndex.toString());
              printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 1 9  call next item  counter-- before" +counter.toString());

              pickTicketStatus("0");
            totalPickTicketProcessed();
            totalPickTicketRemainToProcess();
            pickListSelectedIndex++;
            counter++;

              printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 20   call next item  pickListSelectedIndex-- after " +pickListSelectedIndex.toString());
              printLog("PickOrderApi.API_GET_PICK_TICKET_HELP method  line 21  call next item  counter-- after" +counter.toString());

              setNextItem();
            _pickTicketNoController.text = "";
            Utility.showToastMsg("Pick Ticket No does not exist ");
            setState(() {});
          }
        }
        return;
      }
    }

    if (type == IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET)
    {

      printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 1--" );

      dismissDataLoader();
      isLoading = true;
      isSavingData= false;

      printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 2--" );

      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      if (responseBody.toLowerCase() == "Issued successfully".toLowerCase())
      {
        printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 3-" );

        Utility.showSuccessToastMsg("Issued successfully");
        if (orderItemList.length == 1)
        { isLoading = true;
        _callback.callBackScreen("TabView");
        setState(() {});
        return;
        }
        else {

          printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 4--" );

          print("totalPickListSize -->" + totalPickListSize.toString());
          print(
              "pickListSelectedIndex --" + pickListSelectedIndex.toString());

          printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 5-totalPickListSize-"+totalPickListSize.toString() );
          printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 6-counter-"+counter.toString() );

          if (totalPickListSize == counter)
          { isLoading = true;
          printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 6.1-counter-");
          pickTicketStatus("issued");
          totalPickTicketProcessed();
          totalPickTicketRemainToProcess();
          pickButtonName = "FINISH";
          printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 7-counter-");

          resetData();
          _pickTicketNoController.text = "";
          setState(() {});
          return;
          }
          else
          {
            printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 8--");

            isLoading = true;

            setItemBalanceAndRemaining();
            printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 9--");

            totalPickTicketProcessed();
            printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 10-");

            totalPickTicketRemainToProcess();

            printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 11--");


            setState(()
            {
              _pickTicketNoController.text = "";
              resetData();
            });
            print(
                "API_SAVE_ISSUE_BY_PICK_TICKET --pickListSelectedIndex --" + pickListSelectedIndex.toString());
            print(
                "API_SAVE_ISSUE_BY_PICK_TICKET---counter --" + counter.toString());

            printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 12--");

            setNextItem();
            printLog("'IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET line 13--");

            setState(() {});
            return;
          }
        }
      } else {
        isLoading = true;
        Utility.showToastMsg(responseBody);
        setState(() {});
        return;
      }
    }
  }

  void onErrorResponse(String type) {
    if (type == PickOrderApi.API_GET_PICK_TICKET_HELP) {
      setState(() {});
    }
    if (type == IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET) {
      setState(() {});
    }
  }

  void getSerialNumberStatusList(List<SerialNumberStatus> list) {
    showSerialNumber(list);
    setState(() {});
  }

  void starLoader() {
    print("dialog started");

    print(" starLoader method called Loader running  state -before"+isLoaderRuning.toString());

    if(!isLoaderRuning) {

      print("Loader running  state -after"+isLoaderRuning.toString());
      isLoaderRuning =true;

      Dialogs.showLoadingDialog(context, _dialogKey); //invoking login

    }
  }


  void starDataLoader()
  {
    print("dialog  starDataLoader ");
   //  Dialogs.showLoadingDialog(context, _dialogKey2); //invoking login
  //  EasyLoading.show(status: 'loading...');

  }

  void dismissDataLoader() {
    print("dialog dismissDataLoader ");
    //pr.show();

//    EasyLoading.dismiss();

   // Navigator.of(context, rootNavigator: true).pop();

    //invoking login
  }


  void dismissLoader(String data)
    {
      print("dismissLoader---"+data);

      print(" dismissLoader method called Loader running  state -before"+isLoaderRuning.toString());

      if( isLoaderRuning) {
        print(" dismissLoader method called Loader running  state -before"+isLoaderRuning.toString());

        isLoaderRuning =false;
        Navigator.of(context, rootNavigator: true).pop();

      }
       // pr.hide();
    //  Navigator.pop(context);

    }

  void checkInternetConnection(String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        print('check connection-------------->connected');
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
            () {
             if(!isLoading)
             {
               dismissLoader("");
               isLoading =true;
             }
             if(isSavingData)
             {
               dismissDataLoader();
               isSavingData =false;
             }
        });
        if (type == PickOrderApi.API_GET_PICK_TICKET_HELP)
        {
          isLoading = false;
          starLoader();
          getData().then((v)
          {
           // pr.show();
            getDetails(value, PickOrderApi.API_GET_PICK_TICKET_HELP);
          });
        }
        if (type == PickOrderApi.API_SAVE_ISSUE_BY_PICK_TICKET) {
          print(issueByPickListModel.partSrNo);
          //starLoader();
          isLoading = false;
          isSavingData= true;
          starDataLoader();
          getData().then((v) {
            getDetails(value, PickOrderApi.API_SAVE_ISSUE_BY_PICK_TICKET);
          });
        }
      }
    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
        SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  void showSerialNumberDialog() {
    SerialNumberModel serialNumberModel = new SerialNumberModel();
    serialNumberModel.companyCode = companyCode;
    serialNumberModel.iPartNo = issueByPickListModel.iPartNo;
    serialNumberModel.actionCode = "1";
    serialNumberModel.actionSrNo = "0";
    serialNumberModel.stockSrNo = issueByPickListModel.stockSrNo;
    serialNumberModel.quantity = _qtyToIssueController.text;

    if (issueByPickListModel.iWONo == null) {
      serialNumberModel.iWoNo = "0";
    } else {
      serialNumberModel.iWoNo = issueByPickListModel.iWONo;
    }
    if (issueByPickListModel.iSalesOrderNo == null) {
      issueByPickListModel.iSalesOrderNo = "0";
    }
    serialNumberModel.iSalesOrderNo = issueByPickListModel.iSalesOrderNo;
    if (issueByPickListModel.iSalesOrderLineItem == null) {
      serialNumberModel.lineItemSrNo = "0";
    } else {
      serialNumberModel.lineItemSrNo = issueByPickListModel.iSalesOrderLineItem;
    }

    if (serialNumberModel.iWoNo != "0") {
      serialNumberModel.lineItemSrNo = issueByPickListModel.partSrNo;
    }

    serialNumberModel.iFacilityCode =issueByPickListModel.iFacilityCode;
    serialNumberModel.iRoomCode =issueByPickListModel.iRoomCode;
    serialNumberModel.controlId = controlId;
    serialNumberModel.list = barcodeItem;
    serialNumberModel.screenName = AppStrings.LABEL_ISSUE_BY_PICKLIST;
    int max = 0;
    if (StringUtil.isDouble(issueByPickListModel.quantityLeftToIssue))
    {
      double data =
          StringUtil.tryParse(issueByPickListModel.quantityLeftToIssue);
      max = data.toInt();
    }
    print(max);
    serialNumberModel.max = max;
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => SerialNumberManager(
                callback: this, serialNumberModel: serialNumberModel)));
  }

  void resetData() {
    _toBePickedQtyController.text = "";
    _descriptionController.text = "";
    _partDescController.text = "";
    _pickedLocationController.text = "";
    _locationController.text = "";
    _qtyToIssueController.text = "";
    _palletNoController.text = "";
    _serialNumberNoController.text = "";
    serialNoBool = false;
    barcodeItem = new List();
    loading = true;
    _validate = false;
    isVisible = false;
    barcodeValue = "";
    issueByPickListModel = null;
    isLoading = true;
    strSerialNo = "";
    controlId;
    isSerializedPart = false;
    isConfirmPick = false;
    isQuantityAllowToChange = false;

    setState(() {});
  }

  String showSerialNumber(List<SerialNumberStatus> list) {
    String data = "";
    print(list.length);
    controlId = "";
    if (list.length == 0) {
      data = "";
    } else {
      for (int i = 0; i < list.length; i++) {
        data += list.elementAt(i).serialNo + ",";
        int val = i + 1;
        if (list.length == 1) {
          int val = i;
        } else {
          val = i + 1;
        }
        controlId += "txtSerialNo_" + val.toString() + ",";
      }
      data = data.substring(0, data.length - 1);
      controlId = controlId.substring(0, controlId.length - 1);
    }
    setState(() {
      serialNoBool = true;
      strSerialNo = data;

      _qtyToIssueController.text = list.length.toString();
    });
    print("controlId---" + controlId);
    issueByPickListModel.serialNo = strSerialNo;
    return data;
  }

  void setNextItem()
  {

    printLog("'setNextItem  ---+pickListSelectedIndex --" +pickListSelectedIndex.toString());

    // print("setNextItem -pickListSelectedIndex- after increment- - >"+pickListSelectedIndex.toString());
    //print("counter ---> "+counter.toString());
    orderItem = orderItemList.elementAt(pickListSelectedIndex);
    checkInternetConnection(orderItem.iInvPickingHeaderId, PickOrderApi.API_GET_PICK_TICKET_HELP);

  }



  void setItemBalanceAndRemaining()
  {

    printLog("set Item Balance And Remaining --"+orderItemList.length.toString());

    for (int i = 0; i < orderItemList.length; i++)
    {
      if (orderItemList.elementAt(i).iInvPickingHeaderId ==
          orderItem.iInvPickingHeaderId)
      {
        int totalQuantityForIssue;
        if (StringUtil.isDouble( orderItemList.elementAt(i).quantityLeftToIssue))
        {
          double d = StringUtil.tryParse(
              orderItemList.elementAt(i).quantityLeftToIssue);
          totalQuantityForIssue = d.round();
          print("totalQuantityForIssue -- double " +
              totalQuantityForIssue.toString());
        } else {
          if (StringUtil.isInt(
              orderItemList.elementAt(i).quantityLeftToIssue)) {
            totalQuantityForIssue = StringUtil.tryParse(
                orderItemList.elementAt(i).quantityLeftToIssue);
            print("totalQuantityForIssue -- isInt " +
                totalQuantityForIssue.toString());
          }
        }
        int quantityIssued;
        if (StringUtil.isInt(_qtyToIssueController.text))
        {
          quantityIssued = StringUtil.tryParse(_qtyToIssueController.text);
        }
        orderItemList.elementAt(i).totalPicked = quantityIssued;
        print("totalQuantityForIssue -- isInt " +
            totalQuantityForIssue.toString());
        int balance = totalQuantityForIssue - quantityIssued;
        print("balance --  " + balance.toString());
        orderItemList.elementAt(i).itemRemainToPick = balance;
        orderItemList.elementAt(i).itemStatus ="issued";
      }
    }
  }

  void setButtonName() {}
  void onTapped(PickOrder pickOrder) {
    setState(() {
      isTabVisible = false;
    });
  }



  int totalPickTicketProcessed()
  {
    printLog("total PickTicket Processed --"+orderItemList.length.toString());

    int totalPickTicketProcessed = 0;
    for (int i = 0; i < orderItemList.length; i++)
    {
      print("totalPickTicketProcessed--"+orderItemList.elementAt(i).itemStatus);

        if (orderItemList.elementAt(i).itemStatus == "issued" || orderItemList.elementAt(i).itemStatus =="skip" ||
            orderItemList.elementAt(i).itemStatus =="0")
        {
          totalPickTicketProcessed++;
        }
    }
    print("totalPickTicketProcessed--${totalPickTicketProcessed}"          );
    return totalPickTicketProcessed;
  }

  int totalPickTicketRemainToProcess() {
    int totalPickTicketProcessed = 0;
    printLog("total PickTicket Remain To Process --"+orderItemList.length.toString());

    for (int i = 0; i < orderItemList.length; i++)
    {
       print(orderItemList
          .elementAt(i)
          .iInvPickingHeaderId+"-----"+orderItemList
          .elementAt(i)
          .itemStatus);
        if (orderItemList
            .elementAt(i)
            .itemStatus == "")
        {
          print(orderItemList
              .elementAt(i)
              .itemStatus+"");
          totalPickTicketProcessed++;
          print(orderItemList
              .elementAt(i)
              .iInvPickingHeaderId+"-----"+totalPickTicketProcessed.toString());
        }
    }
      return totalPickTicketProcessed;
  }

  int getPickSerialNumber() {

   if( orderItemList.length == counter)
    {
      skipButtonName ="CANCEL";
    }
    return counter;
  }
void pickTicketStatus(String status)
{

    print("pickTicketStatus---"+status);
  for (int i = 0; i < orderItemList.length; i++)
  {
    if (orderItemList.elementAt(i).iInvPickingHeaderId ==
        orderItem.iInvPickingHeaderId)
    {
      orderItemList.elementAt(i).itemStatus =status;
    }
  }
 }

 void printLog(String data){
   print(data);

 }
}
