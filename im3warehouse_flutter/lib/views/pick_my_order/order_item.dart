
import 'dart:convert';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/models/pick_my_order/order_item.dart';
import 'package:im3_warehouse/models/pick_my_order/pick_order.dart';
import 'package:im3_warehouse/models/pick_my_order/so_line_Item_to_pick.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberModel.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_callback.dart';
import 'package:im3_warehouse/views/pick_my_order/view_callback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;
import 'package:flutter/rendering.dart';


class OrderItemDetail extends StatelessWidget {
  String viewType;

  List<SOLineItemToPick> orderItemList = [];
  OrderItemDetail(this.orderItemList);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.withOpacity(0.70),
      // this is the main reason of transparency at next screen. I am ignoring rest implementation but what i have achieved is you can see.
      body: ItemDetailManager(this.orderItemList),
    );
  }
}
class ItemDetailManager extends StatefulWidget {
  SerialNumberCallback callback;
  SerialNumberModel serialNumberModel;
  String viewType;

  List<SOLineItemToPick> orderItemList = [];
  ItemDetailManager(this.orderItemList);

  @override
  ItemDetailState createState() => ItemDetailState();
}

class ItemDetailState extends State<ItemDetailManager> {

  TextEditingController serialNumberController = new TextEditingController();
  int indexNumber;
  String stockSerialNumber = "";
  bool isVisible = false;
  List<SOLineItemToPick> orderItemList = [];
  bool  isDataLoaded =true;
  bool  isTabVisible =true;
  Callback _callback;
  PickOrder pickOrder;

  @override
  void initState() {
    super.initState();
    print("start dialog--->");
    orderItemList =widget.orderItemList;
    print(orderItemList.length);
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.all(0),
        padding: EdgeInsets.all(0),
        child: new Stack(
          //alignment:new Alignment(x, y)
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    padding:EdgeInsets.all(10),
                    color: AppColors.CYAN_BLUE_COLOR,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //NOT WORKING!
                      children: <Widget>[
                        InkWell(
                          child: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 35.0,
                          ),
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                        Text("Item Detail",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                            )),
                        Text("",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 18,
                            )),
                        Text("",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 18,
                            )),
                      ],
                    ),
                  ),
                ),
                Container(
                  child:  Expanded(
                    child:  Scrollbar(
                      child: ListView.builder(
                        padding: EdgeInsets.only(bottom:50) ,
                        primary: false,
                        scrollDirection: Axis.vertical,
                        itemCount: orderItemList.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            color: (index % 2 == 0) ?  Color.fromRGBO(220,220,220,99.0): Colors.white,
                            child:InkWell(
                              hoverColor: Colors.black26,
                              onTap:(){
                                //  onTapped(orderItemList.elementAt(index));
                              },
                              child:Padding(
                                padding: EdgeInsets.only(left:7,top:10,bottom:10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    Row(
                                      children:<Widget>[
                                        Expanded(
                                          flex: 7,
                                          child:
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child:Text( "Item #:"+orderItemList.elementAt(index).iInvPickingHeaderId,textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 15,
                                                )
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 3,
                                          child:
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child:Text( "Item Picked #:"+orderItemList.elementAt(index).totalPicked.toString(),textAlign: TextAlign.left,


                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children:<Widget>[
                                        Expanded(
                                          child:
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child:Text( "Item Remaing #:"+orderItemList.elementAt(index).itemRemainToPick.toString(),textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                              ,),
                          );
                        },
                      ),
                    ),
                  ),
                )
              ],
            ),
            new Positioned(
              child: new Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                  child: Row(
                    children: <Widget>[


                      /*   Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 4.0),
                          child: MaterialButton(
                            onPressed: ()
                            {

                              },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "START PICKING".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      ),

                   */
                      /*          Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: MaterialButton(
                            onPressed: ()
                            {
                            },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "BACK".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      )
            */
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  int  getTotalQuantity(int index){
    return  0;
  }
  void onTapped(PickOrder pickOrder)
  {
    setState(() {
      isTabVisible =false;
    });
  }

}
