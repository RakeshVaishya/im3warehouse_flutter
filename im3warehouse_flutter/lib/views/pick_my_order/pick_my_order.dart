import 'dart:io';
import 'package:english_words/english_words.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:im3_warehouse/models/issue_pick_list/Issue_by_pick_list_model.dart';
import 'package:im3_warehouse/models/pick_my_order/order_item.dart';
import 'package:im3_warehouse/models/pick_my_order/pick_order.dart';
import 'package:im3_warehouse/models/pick_my_order/so_line_Item_to_pick.dart';
import 'package:im3_warehouse/network/api/pick_order_api.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/pick_my_order/pick_manager.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'order_list.dart';
import 'view_callback.dart';
import 'dart:convert';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:xml/xml.dart' as xml;

class PickMyOrder extends StatefulWidget {
  ViewTypeCallBack callBack;
  PickMyOrder(this.callBack);

  @override
  State<StatefulWidget> createState() {
    return _ListScreenState();
  }
}


class _ListScreenState extends State<PickMyOrder> with SingleTickerProviderStateMixin
    implements Callback  {

  bool isVisible = false;
  //List<PickOrder> mainList = [];

  List<PickOrder> toBePickOrderMainList = [];
  List<PickOrder> pickedOrderMainList = [];


  List<PickOrder> toBePickOrderSortedList = [];
  List<PickOrder> pickedOrderSortedList = [];


  List<SOLineItemToPick> soLineItemList = [];
  bool isDataLoaded = false;
  bool isTabVisible = true;
  String listType = "UnPicked";
  String viewType = "TabView";
  var scrollKey = GlobalKey();
  bool isPickOrderLoading = true;
  String userId = "";
  String companyCode = "";
  Facility facility;
  PickOrder _pickOrder;
  String requestedByUserID = "";
  TextEditingController _searchController = TextEditingController();
  IssueByPickListModel issueByPickListModel;
  bool _isSaleOrderLoading = true;
  bool _hasMore = true;
  ScrollController controller;
  int offset;
  ProgressDialog pr;

  ScrollController _scrollController;
  double _scrollPosition;
  int activeTab = 0;
  TabController tabController;

  @override
  void initState() {

    super.initState();

    tabController = TabController(vsync: this, length: 2);
    _isSaleOrderLoading = true;
    _hasMore = true;
    setOffSet(0);
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    tabController.addListener(_tabSelection);
    _loadMore();
  }

  void _loadMore()
  {
    print("_loadMore");
    _isSaleOrderLoading = true;
    checkInternetConnection("", PickOrderApi.API_GENERATE_AII_PICK_SO_PICK);
  }


  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print("didChangeDependencies");
  }

  @override
  void deactivate() {
    // TODO: implement deactivate
    print("deactivate");
    super.deactivate();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        print('appLifeCycleState inactive');
        break;
      case AppLifecycleState.resumed:
        print('appLifeCycleState resumed');
        break;
      case AppLifecycleState.paused:
        print('appLifeCycleState paused');
        break;

    }
  }
  @override
  Widget build(BuildContext context)
  {
    pr = new ProgressDialog(context,type: ProgressDialogType.Normal, isDismissible: false, showLogs: false );
    if (viewType == "TabView")
    {
      return tabView();
    }
    else if (viewType == "OrderList") {
      if (soLineItemList.length <= 0) {} else
        {
        return OrderList(this, soLineItemList, _pickOrder,facility,userId,companyCode);
      }
    } else if (viewType == "PickManager")
    {
      print("viewType--" + viewType);
      print("viewType--" + viewType);
      return PickManager(this, soLineItemList, _pickOrder,issueByPickListModel);
    }
  }

  Widget tabView() {
    return DefaultTabController(
      key: scrollKey,
      length: 2,
      child: SizedBox(
        child: Column(
          children: <Widget>[
            Visibility(
              maintainState: true,
              visible: isTabVisible,
              child: TabBar(
               controller: tabController,
                labelColor: Colors.black,
                tabs: <Widget>[
                  Tab(
                    text: "To Be Picked",
                  ),
                  Tab(
                    text: "Picked",
                  )
                ],
              ),
            ),

            searchBar(context),
            Expanded(
              child: TabBarView(
                controller: tabController,
                  children: <Widget>[
                  Container(
                    child: unPickedList(isDataLoaded),
                  ),
                  Container(
                    child: picked(isDataLoaded),

                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }


  void _tabSelection()
  {
      print("_tabSelection->   "+tabController.index.toString());
      if(tabController.index==0){
        listType ="UnPicked";

      }
      else{
        listType ="Picked";
      }
      setState(() {
           });
     if (tabController.indexIsChanging)
    {
       _searchController.text ="";
      // mainList  =

       if(listType =="UnPicked"){
         toBePickOrderSortedList=  toBePickOrderMainList.where((data) =>data.salesOrderNo.toLowerCase().contains(""))
             .toList();
       }
       else{
         pickedOrderSortedList=  pickedOrderMainList.where((data) =>data.salesOrderNo.toLowerCase().contains(""))
             .toList();
       }
    //   print("mainList---size  after--"+mainList.length.toString());
       setState(() {
       });
     }
  }



  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
           // print(_scrollPosition);
    });
  }


  Widget unPickedList(bool isDataLoaded) {

    if (listType == "UnPicked")
    {
        return ListView.builder(
        controller: _scrollController,
        itemCount: _hasMore ? toBePickOrderSortedList.length + 1 : toBePickOrderSortedList.length,
        itemBuilder: (BuildContext context, int index)
        {
          if (index >= toBePickOrderSortedList.length)
          {
            // Don't trigger if one async loading is already under way
            if (!_isSaleOrderLoading)
            {
                if(_searchController.text.length>0)
                {

                }
                else {
                  _loadMore();
                }
            }
            return Center(
              child: SizedBox(
                child: CircularProgressIndicator(),
                height: 24,
                width: 24,
              ),
            );
          }
          return Container(
            color: (index % 2 == 0)
                ? Color.fromRGBO(220, 220, 220, 99.0)
                : Colors.white,
            child: InkWell(
              hoverColor: Colors.black26,
              onTap: () {
                onTapped(toBePickOrderSortedList.elementAt(index));
              },
              child: Padding(
                padding: EdgeInsets.only(left: 2, top: 10, bottom: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top:10,left: 10,right: 10),
                      child:
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                  "Orders #: " +
                                      toBePickOrderSortedList
                                          .elementAt(index)
                                          .salesOrderNo,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 16,
                                  )),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                  "Order Qty #:" +
                                      getText(toBePickOrderSortedList
                                          .elementAt(index)
                                          .totalItemQty
                                          .toString()),

                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15,
                                  )),
                            ),
                          ),
                        ],
                      ),

                    ),
                    Padding(
                      padding: EdgeInsets.only(top:5,left: 10,right: 10),

                      child:Row(
                        children: <Widget>[

                          /*    Expanded(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(

                                    pickOrderList
                                        .elementAt(index)
                                        .notes
                                        .toString(),



                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14,
                                )),
                          ),
                        ),
                    */

                          Expanded(
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                  "BO Qty #:" +
                                      getText(toBePickOrderSortedList
                                          .elementAt(index)
                                          .backOrderQty
                                          .toString()),

                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 14,
                                  )),
                            ),
                          ),
                        ],
                      ), 
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                      child:
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 8,
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                    "To be Picked # :" +
                                        getText(toBePickOrderSortedList
                                            .elementAt(index)
                                            .pickedQuantity
                                            .toString()),

                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 15,
                                    )),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Icon(
                                  Icons.description,
                                  color: Colors.blueAccent,
                                  size: 15.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    }

    else {
      return Center(
        child: SizedBox(
          child: CircularProgressIndicator(),
          height: 24,
          width: 24,
        ),
      );;
    }
  }


  Widget picked(bool isDataLoaded) {

    if (listType == "Picked")
    {
      return ListView.builder(
        controller: _scrollController,
        itemCount: _hasMore ? pickedOrderSortedList.length + 1 : pickedOrderSortedList.length,
        itemBuilder: (BuildContext context, int index)
        {
          if (index >= pickedOrderSortedList.length)
          {
            // Don't trigger if one async loading is already under way
            if (!_isSaleOrderLoading)
            {
              if(_searchController.text.length>0)
              {

              }
              else {
                _loadMore();
              }
            }
            return Center(
              child: SizedBox(
                child: CircularProgressIndicator(),
                height: 24,
                width: 24,
              ),
            );
          }
          return Container(
            color: (index % 2 == 0)
                ? Color.fromRGBO(220, 220, 220, 99.0)
                : Colors.white,
            child: InkWell(
              hoverColor: Colors.black26,
              onTap: () {
               // onTapped(toBePickOrderSortedList.elementAt(index));
              },
              child: Padding(
                padding: EdgeInsets.only(left: 2, top: 10, bottom: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top:10,left: 10,right: 10),
                      child:
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                  "Orders #: " +
                                      pickedOrderSortedList
                                          .elementAt(index)
                                          .salesOrderNo,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 16,
                                  )),
                            ),
                          ),
                    /*      Expanded(
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                  "Order Qty #:" +
                                      getText(pickedOrderSortedList
                                          .elementAt(index)
                                          .totalItemQty
                                          .toString()),

                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15,
                                  )),
                            ),
                          ),*/
                        ],
                      ),

                    ),
                    Padding(
                      padding: EdgeInsets.only(top:5,left: 10,right: 10),

                      child:Row(
                        children: <Widget>[

                          /*    Expanded(
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(

                                    pickOrderList
                                        .elementAt(index)
                                        .notes
                                        .toString(),



                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14,
                                )),
                          ),
                        ),
                    */

                          Expanded(
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                      "Item Picked #:" +
                                      getText(pickedOrderSortedList
                                          .elementAt(index)
                                          .totalItemQty
                                          .toString()),

                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 14,
                                  )),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                      child:
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 8,
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                  "Item Remaining # :" +
                                      getText(pickedOrderSortedList
                                          .elementAt(index)
                                          .qtyleft
                                          .toString()),

                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15,
                                  )),
                            ),
                          ),
                        /*  Expanded(
                            flex: 2,
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Icon(
                                Icons.description,
                                color: Colors.blueAccent,
                                size: 15.0,
                              ),
                            ),
                          ),*/
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    }

    else {
      return Center(
        child: SizedBox(
          child: CircularProgressIndicator(),
          height: 24,
          width: 24,
        ),
      );;
    }
  }

  void setOffSet(int offset){
    this.offset =offset;
  }

  @override
  void callBackScreen(String screenName) {

     if(screenName =="OrderList")
     {
       viewType =screenName;
       setState(()
       {

       }); // TODO: implement callBackScreen
     }
     else {
       toBePickOrderMainList.clear();
       pickedOrderMainList.clear();
       toBePickOrderSortedList.clear();
       pickedOrderSortedList.clear();
       soLineItemList.clear();
       _isSaleOrderLoading = true;
       _hasMore = true;
       setOffSet(0);
       print(screenName);
       viewType = screenName;
       if (viewType == "TabView") {
         listType = "UnPicked";
         _loadMore();
         checkInternetConnection(
             "", PickOrderApi.API_GENERATE_AII_PICK_SO_PICK);
       }
       isTabVisible = true;
       setState(() {

       }); // TODO: implement callBackScreen
     }
  }
  @override
  Widget searchBar(BuildContext context) {
    return Container(
        height: 50,
        padding: EdgeInsets.all(8),
        child: TextField(
            controller:_searchController,
            style: TextStyle(color: Colors.black) ,
            onChanged:  onItemChanged,
            decoration: InputDecoration(
                fillColor: Colors.black.withOpacity(0.1),
                filled: true,
                prefixIcon: Icon(Icons.search,color: Colors.black, ),
                suffixIcon:  InkWell(child:Icon(Icons.clear,color: Colors.black,),
                  onTap: ()
                  {
                  _searchController.clear();
                  if(listType =="UnPicked")
                  {
                    toBePickOrderSortedList =
                        toBePickOrderMainList.where((data) =>data.salesOrderNo.toLowerCase().contains(""))
                            .toList();

                    toBePickOrderMainList =toBePickOrderMainList;
                    print("toBePickOrderList---size  after--"+toBePickOrderMainList.length.toString());
                    setState(() {
                    });
                  }
                  else{
                    pickedOrderSortedList =
                        pickedOrderMainList.where((data) =>data.salesOrderNo.toLowerCase().contains(""))
                            .toList();
                    pickedOrderMainList =pickedOrderSortedList;
                    print("toBePickOrderList---size  after--"+pickedOrderMainList.length.toString());
                    setState(() {
                    });
                  }
                  },
                ),
                hintText: 'Search Order No...',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2),
                    borderSide: BorderSide.none),
                contentPadding: EdgeInsets.zero
            )
        )
    );
  }

  void checkInternetConnection(String value, String type) async
  {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('check connection-------------->connected');
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
                () {
              isPickOrderLoading = true;
              dismissLoader();
            });
        if (type == PickOrderApi.API_GENERATE_AII_PICK_SO_PICK)
        {
          //starLoader();
          isPickOrderLoading = false;
          getData().then((v) {
            getDetails(value, type);
          });
        }

        else if (type == PickOrderApi.API_GENERATE_SO_PICK_TICKET) {
          starLoader();
          isPickOrderLoading = false;
          getData().then((v) {
            getDetails(value, type);
          });
        }

        else if (type == PickOrderApi.API_GENERATE_AII_SO_LINE_ITEM_TO_PICK)
        {
          //starLoader();
          //isLoading = false;
          getData().then((v) {
            getDetails(value, type);
          });
        }
        else if (type == PickOrderApi.API_GET_PICK_TICKET_HELP) {
          //starLoader();
          //isLoading = false;
          getData().then((v) {
            getDetails(value, type);
          });
        }
      }
    } on SocketException catch (_) {
      isPickOrderLoading = true;
      print('check connection-------------->not connected');
      // showSnackbarPage(context);
    }
  }

  Future<void> getData() async {
    facility = AppStrings.facility;
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);
  }

  Future getDetails(String data, String type) async {
    print("part no -->" + data);
    print("user --Id  -->" + userId);
    print("companyCode --  -->" + companyCode);
    String jsonBody = "";
    String uri = "";
    print(NetworkConfig.BASE_URL);
    print("type --  -->" + type);
    if (type == PickOrderApi.API_GENERATE_AII_PICK_SO_PICK) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PickOrderApi.API_GENERATE_AII_PICK_SO_PICK;

      jsonBody = PickOrderApi.getAllSOToPick(
          companyCode, "", facility.facilityCode, userId, requestedByUserID, this.offset,12,"");
    }

    if (type == PickOrderApi.API_GENERATE_SO_PICK_TICKET) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PickOrderApi.API_GENERATE_SO_PICK_TICKET;
      jsonBody = PickOrderApi.generateSOPickTicket(
          companyCode, data, userId,facility.iFacilityCode,  _pickOrder.iSalesOrderNo);
    }

    if (type == PickOrderApi.API_GENERATE_AII_SO_LINE_ITEM_TO_PICK) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PickOrderApi.API_GENERATE_AII_SO_LINE_ITEM_TO_PICK;


/*

      static String getAllSOLineItemToPick(
          String CompanyCode, String iSalesOrderNo , String iPartNo,
            String pickTicketNo , String iFacilityCode,
          String  userId) {

*/


      jsonBody = PickOrderApi.getAllSOLineItemToPick(
          companyCode, data, "0", "", facility.iFacilityCode, userId);
    }

    if (type == PickOrderApi.API_GET_PICK_TICKET_HELP
    ) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PickOrderApi.API_GENERATE_AII_SO_LINE_ITEM_TO_PICK;


/*

      static String getAllSOLineItemToPick(
          String CompanyCode, String iSalesOrderNo , String iPartNo,
          String pickTicketNo , String iFacilityCode,
          String  userId) {

*/


      jsonBody = PickOrderApi.getPickTickethelp(
          companyCode, data, facility.iFacilityCode);
    }


    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response = await post(uri,
        headers: NetworkConfig.headers,
        body: jsonBody,
        encoding: encoding)
        .then((resp) {
      print(resp.statusCode);
      isPickOrderLoading = true;
      print(resp.statusCode);
      if (resp.statusCode == 200) {
        String responseBody = resp.body;
        print("----------" + resp.body);
        _isSaleOrderLoading =false;
        onSuccessResponse(type, responseBody);
      } else {
        print(" responce not 200 ");
        isPickOrderLoading = true;
        //dismissLoader();
        //  onErrorResponse(type ,  resp.body);
      }
    })
        .catchError((error) {
      print(" catchError " + error.toString());
      isPickOrderLoading = true;
      // dismissLoader();
      print(error.toString());
      //  onErrorResponse(type ,  error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
        .catchError((e) {
      print(" Time out ");
      isPickOrderLoading = true;
      // dismissLoader();
      Utility.showToastMsg("Error-->" + e.toString());
    });
  }

  void onSuccessResponse(String type, String responseBody) {
    if (type == PickOrderApi.API_GENERATE_AII_PICK_SO_PICK) {
      //dismissLoader();

      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      String response = parsedXml
          .findElements("string")
          .first
          .text;
      if (response == "0")
      {
        return;
      }
      Map newDataSet = jsonDecode(response);
      List<dynamic> tableData = [];
      List<PickOrder> pickList;
      Map<String, dynamic> data2 = newDataSet['NewDataSet'];
      tableData = data2['Table'];
      if (data2['Table'] != null)
      {
        pickList = new List<PickOrder>();
        data2['Table'].forEach((v) {
          pickList.add(new PickOrder.fromJson(v));
        });
      }
      print(pickList.length);
      for( int i=0 ; i<pickList.length;i++)
      {
        double qtyleftDouble = double.parse(pickList.elementAt(i).qtyleft);
        int qtyleftInt= qtyleftDouble.toInt();

        if( qtyleftInt>0)
        {
          toBePickOrderMainList.add(pickList.elementAt(i));
        }
        else{
          pickedOrderMainList.add(pickList.elementAt(i));
        }
      }
      toBePickOrderSortedList =toBePickOrderMainList;
      pickedOrderSortedList =pickedOrderMainList;

      print('toBePickOrderList---'+toBePickOrderMainList.length.toString());
      print('picked---'+pickedOrderMainList.length.toString());

      this.offset += 12;
      print(  this.offset );
      isDataLoaded = true;
      setState(() {});
    }

    if (type == PickOrderApi.API_GENERATE_SO_PICK_TICKET) {
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      String response = parsedXml
          .findElements("string")
          .first
          .text;

      print(response);

      if (response == "0")
      {
        Utility.showToastMsg("Pick Ticket No does not exist ");
        isPickOrderLoading = true;
        dismissLoader();
        return;
      }
      else {

        if(response=="success")
        {
          checkInternetConnection(_pickOrder.iSalesOrderNo,
              PickOrderApi.API_GENERATE_AII_SO_LINE_ITEM_TO_PICK);
        }
        else {
          Utility.showToastMsg("ERROR--> " + response);
          isPickOrderLoading = true;
          dismissLoader();
        }
      }
    }

    if (type == PickOrderApi.API_GENERATE_AII_SO_LINE_ITEM_TO_PICK) {
      print("GENERATE_AII_PICK_SO_LINE_ITEM_TO_PICK");
      isPickOrderLoading = true;
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
     String response = parsedXml          .findElements("string")          .first          .text;
      if(response =="0")
      {
        Utility.showToastMsg("Pick Ticket No does not exist ");
        return;
      }
      Map data1;
      try {
        data1 = jsonDecode(response);
      } catch (ex) {
        print(ex);
      }
      Map data2;
      try {
        data2 = data1['NewDataSet'];
      } catch (ex) {
        print(ex);
      }
      soLineItemList.clear();
      List<dynamic> tableData = [];
      try {
        tableData = data2['Table'];
        if (tableData != null && tableData.length > 0) {
          data2['Table'].forEach((v) {
            soLineItemList.add(new SOLineItemToPick.fromJson(v));
          });
          print(" size of --" + soLineItemList.length.toString());
          soLineItemList =soLineItemList.reversed.toList();
          viewType = "OrderList";
          setState(() {});
        }
      } catch (e) {
        soLineItemList.clear();
        SOLineItemToPick temp = SOLineItemToPick.fromJson(data2['Table']);
        soLineItemList.add(temp);
        soLineItemList =soLineItemList.reversed.toList();
        print(" size of roomfacrLoaction--");
        print(" end --");
        viewType = "OrderList";
        setState(() {});
      }
    }
  }
  onItemChanged(String value) {
    _searchController.value =
        TextEditingValue(
            text: value.toUpperCase(),
            selection: _searchController.selection);
    print("------ ---");
    print("pickOrderList---size --"+toBePickOrderMainList.length.toString());
    print("value--- --"+value);
    if(listType =="UnPicked")
    {
      List<PickOrder> temp = [];
      temp  = toBePickOrderMainList
          .where((data) =>data.salesOrderNo.toLowerCase().contains(value.toLowerCase()))
          .toList();
      print("temp---size  after--"+temp.length.toString());
      toBePickOrderSortedList =temp;
      setState(() {

      });
      if(toBePickOrderSortedList.length ==0)
      {
         Future.delayed(Duration(seconds: 3),
              () {
                _isSaleOrderLoading = true;
                getSaleOrderFromSearchField( _searchController.text, PickOrderApi.API_GENERATE_AII_PICK_SO_PICK);
          });

      }
    }
else{

    List<PickOrder> temp = [];
    temp  = pickedOrderMainList
        .where((data) =>data.salesOrderNo.toLowerCase().contains(value.toLowerCase()))
        .toList();
    print("temp---size  after--"+temp.length.toString());
    pickedOrderSortedList =temp;
    setState(() {
    });

   if(pickedOrderSortedList.length ==0)
    {
      Future.delayed(Duration(seconds: 3),
              () {
            _isSaleOrderLoading = true;
            getSaleOrderFromSearchField( _searchController.text, PickOrderApi.API_GENERATE_AII_PICK_SO_PICK);
          });

    }
  }
    print("-- ++  ++++ ++  --");
  }

  @override
  void doPicking(List<SOLineItemToPick> orderItemList, PickOrder pickOrder) {
    this.soLineItemList = orderItemList;
    this._pickOrder = pickOrder;
    setState(() {
      viewType = "PickManager";
      isTabVisible = true;
    }); // TODO: implement callBackScreen
    // TODO: implement doPicking
  }
  @override
  void setPickList(List<SOLineItemToPick> orderItemList,
      PickOrder pickOrder,IssueByPickListModel issueByPickListModel) {
    this.soLineItemList = orderItemList;
    this._pickOrder = pickOrder;
    this.issueByPickListModel=issueByPickListModel;
    setState(() {
      viewType = "PickManager";
      isTabVisible = true;
    }); // TODO: implement callBackScreen
 }

  void onTapped(PickOrder pickOrder) {
    _pickOrder = pickOrder;
    checkInternetConnection(
        pickOrder.salesOrderNo, PickOrderApi.API_GENERATE_SO_PICK_TICKET);
  }

  void starLoader()
  {
    print(" dialog started");
      pr.show();
  }

  void dismissLoader() {
    pr.hide();
  }
  String getText(String value)
  {
    if(value ==null ||value =="null"||value.length ==0){
      value ="0";
    }
    else{

      double data= double.parse(value);
      int  dat = data.toInt();
      value =dat.toString();
    }
    return value;
  }


  Future getSaleOrderFromSearchField(String data, String type) async {
    print("part no -->" + data);
    print("user --Id  -->" + userId);
    print("companyCode --  -->" + companyCode);
    String jsonBody = "";
    String uri = "";
    print(NetworkConfig.BASE_URL);
    print("type --  -->" + type);
    if (type == PickOrderApi.API_GENERATE_AII_PICK_SO_PICK) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PickOrderApi.API_GENERATE_AII_PICK_SO_PICK;
      jsonBody = PickOrderApi.getAllSOToPick(
          companyCode, data, facility.facilityCode, userId, requestedByUserID, 0,0,"");
    }

    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response = await post(uri,
        headers: NetworkConfig.headers,
        body: jsonBody,
        encoding: encoding)
        .then((resp) {
      print(resp.statusCode);
      isPickOrderLoading = true;
      print(resp.statusCode);
      if (resp.statusCode == 200) {
        String responseBody = resp.body;
        print("----------" + resp.body);
        _isSaleOrderLoading =false;
        if (type == PickOrderApi.API_GENERATE_AII_PICK_SO_PICK)
        {
          xml.XmlDocument parsedXml = xml.parse(responseBody);
          print(parsedXml);
          String response = parsedXml
              .findElements("string")
              .first
              .text;
          if (response == "0")
          {
            return;
          }
          Map newDataSet = jsonDecode(response);
          List<dynamic> tableData = [];
          List<PickOrder> pickList;
          Map<String, dynamic> data2 = newDataSet['NewDataSet'];
                    try {
            tableData = data2['Table'];
            if (data2['Table'] != null) {
              pickList = new List<PickOrder>();
              data2['Table'].forEach((v) {
                pickList.add(new PickOrder.fromJson(v));
              });
            }
          }
            catch(e)
            {
              PickOrder pickOrder = PickOrder.fromJson(data2['Table']);

                double pickedQuantityInDouble = double.parse(pickOrder.pickedQuantity);
                int pickedQuantityInt= pickedQuantityInDouble.toInt();
                if(pickedQuantityInt>0)
                {
                   toBePickOrderMainList.add(pickOrder);
                   toBePickOrderSortedList.add(pickOrder);
                }
                else{

                  pickedOrderMainList.add(pickOrder);
                  pickedOrderSortedList.add(pickOrder);
                }
              }

              print('toBePickOrderList---'+toBePickOrderMainList.length.toString());
              print('picked---'+pickedOrderMainList.length.toString());


          isDataLoaded = true;
          setState(() {});
        }
      } else {
        print(" responce not 200 ");
        isPickOrderLoading = true;
        //dismissLoader();
        //  onErrorResponse(type ,  resp.body);
      }
    })
        .catchError((error) {
      print(" catchError " + error.toString());
      isPickOrderLoading = true;
      // dismissLoader();
      print(error.toString());
      //  onErrorResponse(type ,  error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
        .catchError((e) {
      print(" Time out ");
      isPickOrderLoading = true;
      // dismissLoader();
      Utility.showToastMsg("Error-->" + e.toString());
    });
  }


}




