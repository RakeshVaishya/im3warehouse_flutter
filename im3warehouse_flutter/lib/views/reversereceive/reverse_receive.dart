import 'dart:core';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/reverse_receive/reverse_recieve_item.dart';
import 'package:im3_warehouse/network/api/reverse_recieve_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/receive_by_po/receive_by_po.dart';
import 'package:im3_warehouse/views/reversereceive/reversereceive_activity.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_item_detail.dart';
import 'package:im3_warehouse/models/reverse_by_po/po_list.dart';
import 'package:im3_warehouse/views/receive_by_po/reciecve_po_activity.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/reserves_part_location.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:xml/xml.dart' as xml;
import 'package:xml/xml.dart';
class ReverseReceive extends StatefulWidget {
  ViewTypeCallBack callBack;
  ReverseReceive({this.callBack});
  static  List< ReverseReceiveItem > poList;

  @override
  State<StatefulWidget> createState() {
    return ReverseReceiveState();
  }
}

class ReverseReceiveState extends State<ReverseReceive> {


  var _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool isRecordVisible = false;

  ProgressDialog pr;
  bool isData = false;
  FocusNode _poFocus = FocusNode();
  FocusNode _partFocus = FocusNode();

  bool isAvailable = false;
  TextEditingController _poNumberController = TextEditingController();
  TextEditingController _partNumberController = TextEditingController();

  Color wordOrderColor;
  Color partColor;
  Color locationColor;
  Color quantityColor;
  bool isLoaderRunning = false;
  bool isSerialPart = false;
  bool colorStatus = true;
  Facility facility;
  var _formLocationKey = GlobalKey<FormState>();
  List<RoomFacForLocation> roomFacForLocationList = [];
  RoomFacForLocation roomFacForLocation;
  String userId = "";
  String companyCode = "";
  String iFacilityCode = "";
  String strWorkOrderDesc = "";
  String strPartDesc = "part Desc";
  String strIPartNo = "";
  String strOnHandQuantity = "";
  String strStockRoom = "";
  String employeeName = "";
  List< ReverseReceiveItem > poList = [];
  ReverseReceiveItem  reverseRecieveItemDetail;
  DynamicHelp selectedDynamicHelp;
  ReservesPartLocation reservePartLocation;
  BuildContext context;
  bool isLoading = true;
  bool serverRespone = false;
  Color serverResponeColor;
  String serverResponse = "";
  String serialNumber = "";
  ReverseReceiveItem poDetail;
  var receivQtyKey =UniqueKey();
  xml.XmlElement _xmlElement;
  POItemDetails     poItemDetail;
  @override
  void initState() {
    super.initState();

    getData();
    _poNumberController.addListener(() {
      setState(() {
        isData = false;
      });
    });

  }
  Future<void> getData() async
  {
    String data =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    facility = Facility.fromJson(map);
    print("retrived from SH file--->" + facility.iFacilityCode);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);
    employeeName =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
  }

  @override
  Widget build(BuildContext context)
  {

    this.context =context;
    return fromWidget(context);

  }
  Widget fromWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: this._formKey,
        autovalidate: _validate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 5, right: 5),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                textBaseline: TextBaseline.ideographic,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child :  Padding(
                      padding: isAvailable
                          ? const EdgeInsets.only(right: 5, left: 5, bottom: 20)
                          : EdgeInsets.only(right: 5, left: 5, bottom: 0),
                      child: TextFormField(
                        focusNode: _poFocus,
                        style: TextStyle(color: Colors.black),
                        controller: _poNumberController,
                        textInputAction: TextInputAction.send,
                        onFieldSubmitted: (text) {
                          if (text.length > 0)
                          {
                            /*if(isLoading)
                            {
                              isLoading =false;
                              checkInternetConnection(
                                  text, ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART);
                            }*/
                          }
                          else{
                            Utility.showToastMsg("Please enter purchase order number!");
                            return;
                          }
                        },
                        decoration: InputDecoration(
                            labelText: "PO#:",
                            labelStyle: TextStyle(color: Colors.black87),
                            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0)),
                        onChanged: (value)
                        {
                        },
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: InkWell(
                          child: Image.asset(
                            "images/QRCode.png",
                          ),
                          onTap: ()
                          {
                            barCodeScan("poNumber");
                          },
                        ),
                      ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 5, right: 5),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                textBaseline: TextBaseline.ideographic,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child :  Padding(
                      padding: isAvailable
                          ? const EdgeInsets.only(right: 5, left: 5, bottom: 20)
                          : EdgeInsets.only(right: 5, left: 5, bottom: 0),
                      child: TextFormField(
                        focusNode: _partFocus,
                        style: TextStyle(color: Colors.black),
                        controller: _partNumberController,
                        textInputAction: TextInputAction.send,
                        onFieldSubmitted: (text) {
                          if (text.length > 0)
                          {
                            /*if(isLoading)
                            {
                              isLoading =false;
                              checkInternetConnection(
                                  text, ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART);
                            }*/
                          }
                          else{
                            Utility.showToastMsg("Please enter purchase order number!");
                            return;
                          }
                        },
                        decoration: InputDecoration(
                            labelText: "Part#:",
                            labelStyle: TextStyle(color: Colors.black87),
                            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0)),
                        onChanged: (value)
                        {
                        },
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: InkWell(
                          child: Image.asset(
                            "images/QRCode.png",
                          ),
                          onTap: ()
                          {
                            barCodeScan("partNumber");
                          },
                        ),
                      ))

                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 15.0, bottom: 5.0, right: 8.0, left: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          if(_poNumberController.text.length==0){
                            FocusScope.of(context).requestFocus(_poFocus);
                            Utility.showToastMsg("Please enter purchase order number!");
                            return;
                          }
                         else if(_partNumberController.text.length==0){
                            FocusScope.of(context).requestFocus(_partFocus);
                            Utility.showToastMsg("Please enter part number!");
                            return;
                          }
                          else {
                            onSearch(_poNumberController.text);
                          }
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppStrings.LABEL_SEARCH.toUpperCase(),
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Colors.white,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          print("on cancel called");
                          if (_poNumberController.text.length == 0 || _partNumberController.text.length==0)
                          {
                            print("on back  ");
                            this
                                .widget
                                .callBack
                                .viewType(DashboardView(this.widget.callBack), AppStrings.LABEL_HOME);
                          }
                          else {
                            reset();
                          }
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppStrings.LABEL_CANCEL.toUpperCase(),
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Colors.white,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Visibility(
                visible: isRecordVisible,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding:
                      const EdgeInsets.only(left: 8.0, right: 8, top: 5),
                      child: Container(
                        color: Colors.grey[200],
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(
                                5.0,
                              ),
                              child: Text(
                                " ",
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                    fontSize: 4,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            )),
                      ),
                    ),
                    Padding(
                      padding:
                      const EdgeInsets.only(top: 0.0, left: 5, right: 5),
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        child: ListView.builder(
                          itemCount: poList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              child: Card(
                                  elevation: 2,
                                  child: Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 5,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "Receiver #: ",
                                                    style: TextStyle(
                                                        color: Colors.grey[600],
                                                        fontFamily: AppStrings
                                                            .SEGOEUI_FONT),
                                                  ),
                                                  Text(
                                                    poList.elementAt(index).pOReceiverNo ,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontFamily: AppStrings
                                                            .SEGOEUI_FONT),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Receiver: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      poList.elementAt(index).displayDateReceived,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Release No: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      poList.elementAt(index).releaseNo,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding:
                                          const EdgeInsets.only(top: 8.0),
                                          child: Container(
                                              height: 80,
                                              child: VerticalDivider(
                                                  color: Colors.grey)),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text("Rate #: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      poList.elementAt(index).itemRate,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Received Qty: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      poList.elementAt(index).quantityReceived,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Release Qty: ",
                                                      style: TextStyle(
                                                          color:
                                                          Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      poList.elementAt(index).releaseQuantity,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                              onTap: ()
                              {poDetail=poList.elementAt(index);
                              if(isLoading) {
                                isLoading =false;

                             /*   checkInternetConnection(
                                    "", ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART_DETAILS);
*/
                                checkInternetConnection(
                                    "", ReverseReceiveApi.API_GET_RECIEVE_PO_ITEM_DETAILS);

                                }
                              },
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }

  onSearch(String poNumber)
  {

   if(isLoading) {
      isLoading =false;
      checkInternetConnection(
          poNumber, ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART);
    }

  }

  void checkInternetConnection(String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
                () {
              isLoading = true;
              dismissLoader();
            });
        print('check connection-------------->connected');
        if (type == ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART)
        {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == ReverseReceiveApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
        {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART_DETAILS)
        {
         // starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == ReverseReceiveApi.API_IS_PART_SERIALIZED)
        {
          //starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
        {
          // starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
      }

    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }
  Future barCodeScan(String type) async {
    try {
      var barcode = await BarcodeScanner.scan();
      print(barcode.rawContent);
      if(barcode.rawContent.length==0){
        return;
      }
      if(type =="poNumber")
      {
        _poNumberController.text =barcode.rawContent;
        FocusScope.of(context).requestFocus(_partFocus);
        setState(() {

        });
      }
      if(type =="partNumber")
      {
        _partNumberController.text =barcode.rawContent;
        setState(() {

        });
      }
    }
    on PlatformException catch (e)
    {
      if (e.code == BarcodeScanner.cameraAccessDenied)
      {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
    SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  onCancel() {
  }

  void starLoader()
  {
    print("dialog started");
    if (!isLoaderRunning) {
      isLoaderRunning = true;
      Dialogs.showLoadingDialog(context, _formLocationKey);
    }
  }

  void dismissLoader()
  {
    if (isLoaderRunning)
    {
      isLoaderRunning = false;
      Navigator.of(_formLocationKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }

  Future getDetails(String value, String type) async {
    print("data -->" + value);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";
    Map headers = new Map();
    print(NetworkConfig.BASE_URL);

    if (type == ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART)
    {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART;
      jsonBody = ReverseReceiveApi.getPORevReceiveByPart(
            companyCode, _poNumberController.text,_partNumberController.text);
    }
    if (type == ReverseReceiveApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
    {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReverseReceiveApi.API_GET_RECIEVE_PO_ITEM_DETAILS;
      jsonBody = ReverseReceiveApi.getReceivePOItemDetail(
          companyCode, poDetail.iPONo, poDetail.lineItemSrNo);
    }


    if (type == ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART_DETAILS)
    {

      print( poDetail.iPOReceiverNo);
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART_DETAILS;
      jsonBody = ReverseReceiveApi.getPORevReceiveByPartDetails(
          companyCode, poDetail.iPOReceiverNo,poDetail.iPONo,poDetail.lineItemSrNo);
    }

    if (type == ReverseReceiveApi.API_IS_PART_SERIALIZED)
    {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ReverseReceiveApi.API_IS_PART_SERIALIZED;
      jsonBody = ReverseReceiveApi.isPartSerialized(
          companyCode,value);
    }
    if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
    {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION;
      jsonBody = NetworkConfig.getRoomFacForLocation(
          companyCode, userId, _xmlElement.getElement("RoomAreaCode").text, facility.iFacilityCode);
    }
    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response =
    await post(uri, headers: headers, body: jsonBody, encoding: encoding)
        .then((resp) {
      isLoading = true;
      print(resp.statusCode);
      if (resp.statusCode == 200)
      {
        String responseBody = resp.body;
        print("----------" + resp.body);
        onSuccessResponse(type, responseBody);
      } else {
        isLoading = true;
        onErrorResponse(type, resp.body);
      }
    })
        .catchError((error) {
      isLoading = true;
      print(error.toString());
      onErrorResponse(type, error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
        .catchError((e) {
      isLoading = true;
      dismissLoader();
      Utility.showToastMsg("Error-->" + e.toString());
    });
  }
  void  onSuccessResponse(String type, String value) {
    isLoading = true;
    if (type == ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART) {
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml
          .findElements("string")
          .first
          .text;
      if (value != "0") {
        Map data1;
        try {
          data1 = jsonDecode(value);
        } catch (ex) {
          print(ex);
        }
        Map data2;
        try {
          data2 = data1['NewDataSet'];
        } catch (ex) {
          print(ex);
        }
        List<dynamic> data;
        try{
          data=   data2['Table'];
          if (data != null && data.length > 0)
          {
            poList.clear();
            if (data.length > 0)
            {
              for (int i = 0; i < data.length; i++) {
                Map dat = data.elementAt(i);
                ReverseReceiveItem temp = ReverseReceiveItem.fromJson(dat);
                poList.add(temp);
              }
              print(" size of roomfacrLoaction--");
              print(" end --");
              isRecordVisible = true;
              setState(() {});
            }

            else {
              reset();
              Utility.showToastMsg("No Record Found");
              return;
            }
          }
        }catch(e)
        {
          poList.clear();
          ReverseReceiveItem temp = ReverseReceiveItem.fromJson(data2['Table']);
          poList.add(temp);
          print(" size of --");
          print(" end --");
          isRecordVisible = true;
          setState(() {});
        }
        return;
      }
      else {
        reset();
        Utility.showToastMsg("No Record Found");
        return;
      }
    }


    if (type == ReverseReceiveApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
    {
      isLoading = true;
      // dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml
          .findElements("string")
          .first
          .text;
      if (value != "0") {
        Map data1;
        try {
          data1 = jsonDecode(value);
        } catch (ex) {
          print(ex);
        }
        Map data2;
        try {
          data2 = data1['NewDataSet'];
        } catch (ex) {
          print(ex);
        }
        try {
          poItemDetail =POItemDetails.fromJson(data2['Table']);
          print("get part in "+poItemDetail.iPartNo.toString());
          setState(()
          {


              checkInternetConnection(
                                    "", ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART_DETAILS);

          });
        } catch (ex) {
          print("data in single");
        }
        return;
      }
      else {
        dismissLoader();
        reset();
        Utility.showToastMsg("No Record Found");
        return;
      }
    }

    if (type == ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART_DETAILS)
    {
      isLoading = true;
      dismissLoader();
     xml.XmlDocument parsedXml  = xml.parse(value);
  String responce=      parsedXml.findElements('POReceiver').first.text;
    if(responce=="0")
    {

    }
    else{
      _xmlElement=   parsedXml.findElements('POReceiver').first;
      print("data--->"+_xmlElement.getElement("PONo").text);
      checkInternetConnection(
          "", NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION);
    }


      return;

    }

    if (type == ReverseReceiveApi.API_IS_PART_SERIALIZED)
    {
      isLoading = true;
      xml.XmlDocument parsedXml = xml.parse(value);
      value = parsedXml .findElements("boolean").first
          .text;
      print(value);

      if (value == "false" || value == "true")
      {
        if (value.toUpperCase() == "false".toUpperCase())
        {
          isSerialPart = false;
        }
        else {
          isSerialPart = true;
        }
        checkInternetConnection(
            "", NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION);
      }
      else{
        dismissLoader();
        Utility.showToastMsg("No Record Found ");
        return;
      }
    }

    if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
    {
      isLoading = true;
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;

      if(value !="0") {
        Map data1 = jsonDecode(value);
        Map data2 = data1['NewDataSet'];
        List<dynamic> data = [];
        try {
          data = data2['Table'];
          roomFacForLocationList = new List();
          if (data.length > 0) {
            for (int i = 0; i < data.length; i++) {
              Map dat = data.elementAt(i);
              RoomFacForLocation temp = RoomFacForLocation.fromJson(dat);
              roomFacForLocationList.add(temp);
            }
            print("list size--" + roomFacForLocationList.length.toString());
          }
          else {
            print("Wrong input");
            Utility.showToastMsg(
                "Please Select Faclility and  Enter Correct Bin No.");
          }
        } catch (e) {
          roomFacForLocationList.clear();
          roomFacForLocation = RoomFacForLocation.fromJson(data2['Table']);
          roomFacForLocationList.add(roomFacForLocation);
          print(roomFacForLocationList.length.toString());
          Widget view= ReverseReceiveActivity(callBack:widget.callBack,
            reverseReceiveItem:reverseRecieveItemDetail, roomFacForLocationList:roomFacForLocationList,
             isSerialPart:isSerialPart,xmlElement:_xmlElement,poItemDetail:poItemDetail);
          this.widget.callBack.viewType(
              view,
              AppStrings.LABEL_REVERSE_RECEIVE_ACTIVITY);

           }
      }
      else{
        Utility.showToastMsg("No Record Found");
        return;
      }
    }
  }
  onErrorResponse(String type, String error) {
    dismissLoader();
    if (type == ReverseReceiveApi.API_GET_PO_REV_RECEIVE_BY_PART)
    {
      Utility.showToastMsg(error.toString());
      reset();
      return;
    }
    if (type == ReverseReceiveApi.API_GET_RECIEVE_PO_ITEM_DETAILS)
    {
      Utility.showToastMsg(error.toString());
      reset();
    }
    if (type == ReverseReceiveApi.API_IS_PART_SERIALIZED)
    {

    }
  }

  void reset()
  {
    FocusScope.of(context).requestFocus(_poFocus);

    print("reset call");
    _poNumberController.text = "";
    _partNumberController.text ="";
    poList.clear();
    isRecordVisible = false;
    setState(() {

    });
  }
}
