import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/models/transfer/transfer_from_model.dart';
import 'package:im3_warehouse/models/transfer/transfer_to_model.dart';
import 'package:im3_warehouse/network/api/tranfer_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'dart:async';
import 'dart:convert';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:flutter/widgets.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:im3_warehouse/views/util_screen/add_part_and_location.dart';
import 'package:im3_warehouse/views/util_screen/part_manager_callback.dart';
import 'package:xml/xml.dart' as xml;
import 'package:flutter/services.dart';
import 'package:im3_warehouse/views/util_screen/add_part_dialog.dart';
import 'package:im3_warehouse/views/util_screen/add_part_dialog_callback.dart';
class TransferPallet extends StatefulWidget {
  ViewTypeCallBack callback;
  TransferPallet(this.callback);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    print("createState");
    return TransferState();
  }
}

class TransferState extends State<TransferPallet> {
  @override
  void dispose() {
    super.dispose();
    print("dispose");
  }
   int  activityType;
  TextEditingController _partNoController = TextEditingController();
  TextEditingController _partDescController = TextEditingController();
  TextEditingController _onHandQtyController = TextEditingController();
  TextEditingController _fromBinController = TextEditingController();
  TextEditingController _toTransferBinController = TextEditingController();
  TextEditingController _toTransferQuantityController = TextEditingController();
  String buttonName = "Submit";
  bool isSerializePart = false;
  var _formkey = GlobalKey<FormState>();
  bool _validate = false;
  String userId = "";
  String companyCode = "";
  String employeeName = "";
  Color partColor;
  FocusNode partFocus = new FocusNode();
  FocusNode toBinFocus = new FocusNode();
  FocusNode fromBinFocus = new FocusNode();
  FocusNode toTransferQuantityFocus = new FocusNode();
  bool _isPartExist = false;

  bool isLoading =true;
//  ProgressDialog pr;
  Facility facility;
  PartDetails _partDetails;
  TransferFromModel _transferFromModel;

  TransferToModel _transferToModel;
  List<TransferToModel> transferToModelList = [];
  List<TransferFromModel> _transferFromModelList = [];
  BuildContext context;

  bool isCalledByGetPart = false;
  var _dialogKey = GlobalKey<FormState>();
  bool isLoaderRunning = false;

  @override
  void initState() {
    super.initState();
    print("initState");


    // _partNoController.text = "25-35491-00";
    // _partNoController.text = "wheat";
    // _toTransferBinController.text ="stocklocroom2 ";
    getData();
    partFocus.addListener(() {
      setState(() {
        partColor = Colors.grey[600];
      });
    });
  }
  Future<void> getData() async {
    String data =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    facility = Facility.fromJson(map);
    print("retrived from SH file--->" + facility.iFacilityCode);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);
    employeeName =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
  }

  @override
  Widget build(BuildContext context) {
    print("build");

    if (_partNoController.text.length == 0) {
      FocusScope.of(context).requestFocus(partFocus);
    }
    this.context = context;
    return formWidget();
  }


  Widget formWidget() {
   return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 8, right: 8, bottom: 5),
      child: SingleChildScrollView(
          child: Scrollbar(
              child: Form(
                  key: this._formkey,
                  autovalidate: _validate,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: Padding(
                              padding:
                              const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: TextFormField(
                                  focusNode: partFocus,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: this._partNoController,
                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_PART_NO,
                                      labelStyle: TextStyle(
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: partFocus.hasFocus
                                              ? partColor
                                              : null)),
                                  textInputAction: TextInputAction.send,
                                  onFieldSubmitted: (text) {

                                  },
                                  onChanged: (text) {
                                    if (text
                                        .toString()
                                        .length == 0) {
                                      return;
                                    }
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Part No cannot be blank";
                                    }
                                  }
                              ),
                            ),
                          ),
                          Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: InkWell(
                                  child: Image.asset(
                                    "images/QRCode.png",
                                  ),
                                  onTap: () {

                                  },
                                ),
                              ))
                        ],
                      ),
                      Visibility(
                        visible: _isPartExist,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 0.0),
                              child: TextFormField(
                                style: TextStyle(color: Colors.grey[600]),
                                enabled: false,
                                controller: _partDescController,
                                decoration: InputDecoration(
                                    labelText: "Part Desc",
                                    labelStyle:
                                    TextStyle(color: Colors.grey[500])),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 9,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: TextFormField(
                                      style: TextStyle(color: Colors.grey[600]),
                                      controller: _fromBinController,
                                      focusNode: fromBinFocus,
                                      onFieldSubmitted: (val) {

                                      },
                                      textInputAction: TextInputAction.send,
                                      onChanged: (text) {

                                      },
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Bin No cannot be blank";
                                        }
                                      },
                                      decoration: InputDecoration(
                                          labelText: "From Bin#",
                                          contentPadding:
                                          EdgeInsets.only(top: 0),
                                          labelStyle: TextStyle(
                                              color: Colors.grey[550])),
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 20),
                                      child: InkWell(
                                        child: Image.asset(
                                          "images/QRCode.png",
                                        ),
                                        onTap: () {
                                           },
                                      ),
                                    )
                                )
                              ],
                            ),
                            Visibility
                              (
                              visible: _transferFromModelList.isNotEmpty,
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        "From WareHouse - Facility",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Colors.grey[500],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 0.0),
                                    child: DropdownButton<TransferFromModel>(
                                      value: _transferFromModel,
                                      icon: Icon(Icons.arrow_drop_down),
                                      iconSize: 24,
                                      isExpanded: true,
                                      elevation: 10,
                                      hint: Text(""),
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      underline: Container(
                                        height: 1,
                                        color: Colors.grey,
                                      ),
                                      onChanged: (TransferFromModel newValue) {
                                        setState(() {
                                          _transferFromModel = newValue;
                                        });
                                      },
                                      items: _transferFromModelList
                                          .map((dropdownValue) {
                                        return DropdownMenuItem<
                                            TransferFromModel>(
                                            value: dropdownValue,
                                            child: Text(
                                              dropdownValue.stockRoom
                                                  .toString(),
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13.5),
                                            ));
                                      }).toList(),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 9,
                                        child: TextFormField(
                                          focusNode: toBinFocus,
                                          onFieldSubmitted: (val)
                                          {
                                            if (_toTransferBinController.text.trim().
                                                length > 0)
                                            {
                                            }
                                            else{
                                              return;
                                            }
                                          },
                                          textInputAction: TextInputAction.send,
                                          style: TextStyle(
                                              color: Colors.grey[600]),
                                          controller: _toTransferBinController,
                                          onChanged: (text) {
                                            if (text.length == 0) {
                                            }
                                          },
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return "Bin No cannot be blank";
                                            }
                                          },
                                          decoration: InputDecoration(
                                              labelText: "To Bin#",
                                              labelStyle: TextStyle(
                                                  color: Colors.grey[550])),
                                        ),
                                      ),
                                      Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 20),
                                            child: InkWell(
                                              child: Image.asset(
                                                "images/QRCode.png",
                                              ),
                                              onTap: () {
                                              },
                                            ),
                                          ))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible:
                              transferToModelList.isNotEmpty,
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        "To WareHouse - Facility",
                                        textAlign: TextAlign.left,
                                        style:
                                        TextStyle(color: Colors.grey[500]),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 0.0),
                                    child: DropdownButton<TransferToModel>(
                                      value: _transferToModel,
                                      icon: Icon(Icons.arrow_drop_down),
                                      iconSize: 24,
                                      isExpanded: true,
                                      elevation: 10,
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      underline: Container(
                                        height: 1,
                                        color: Colors.grey,
                                      ),
                                      onChanged: (TransferToModel newValue) {
                                        setState(() {
                                          print("newWarehouseSelected--" +
                                              newValue.companyCode);
                                          _transferToModel = _transferToModel;
                                        });
                                      },
                                      items: transferToModelList
                                          .map((dropdownValue) {
                                        return DropdownMenuItem<
                                            TransferToModel>(
                                            value: dropdownValue,
                                            child: Text(

                                              "",

                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15),
                                            ));
                                      }).toList(),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Visibility(
                                  visible: _transferFromModelList.isNotEmpty,
                                  child:
                                  Expanded(
                                    child: TextFormField(
                                      style: TextStyle(color: Colors.grey[600]),
                                      enabled: false,
                                      controller: _onHandQtyController,
                                      decoration: InputDecoration(
                                          labelText: "On Hand Quantity",
                                          labelStyle:
                                          TextStyle(color: Colors.grey[550])),
                                    ),
                                  ),
                                ),
                                Visibility(
                                    visible: transferToModelList
                                        .isNotEmpty &&
                                        (_partDetails.isSerailNumber ==
                                            "false"),
                                    child: Expanded(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(left: 10.0),
                                        child: TextFormField(
                                          focusNode: toTransferQuantityFocus,
                                          onFieldSubmitted: (val) {},
                                          textInputAction: TextInputAction.next,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return "Quantity  cannot be blank";
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: <TextInputFormatter>[
                                            WhitelistingTextInputFormatter
                                                .digitsOnly
                                          ],
                                          style: TextStyle(
                                              color: Colors.grey[600]),
                                          controller: _toTransferQuantityController,
                                          decoration: InputDecoration(
                                              labelText: "Quantity to Transfer",
                                              labelStyle: TextStyle(
                                                  color: Colors.grey[500])),
                                        ),
                                      ),
                                    )),
                              ],
                            ),

                            /*         Visibility(
                              visible: isSerialError,
                              child: Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Text(serialNumberErrorMessge,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red
                                  ),),
                              ),
                            ),
                            Visibility(
                              visible: isPartSerialize,
                              maintainState: true,
                              child:  Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(right: 8.0, top: 0),
                                      child: Material(
                                        child:InkWell(
                                          onTap: (){
                                            print("data---");
                                            showSerialNumberList();
                                          },
                                          child: Column(

                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 0.0, top: 0),
                                                child: Text(
                                                  AppStrings.LABEL_SERIAL_NO,
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                                      fontSize: serialNoBool
                                                          ? AppStrings.FONT_SIZE_13
                                                          : AppStrings.FONT_SIZE_16),
                                                ),
                                              ),

                                              InkWell(
                                                child:Padding(
                                                  padding: const EdgeInsets.only(top: 8.0),
                                                  child: InkWell(
                                                    child: Text(strSerialNo,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                            fontFamily:
                                                            AppStrings.SEGOEUI_FONT,
                                                            color: Colors.grey[600],
                                                            fontSize:
                                                            AppStrings.FONT_SIZE_15)),
                                                    onTap: ()
                                                    {
                                                      print("tapped on serial no");
                                                      showSerialNumberList();
                                                    },

                                                  ),
                                                ),
                                                onTap: ()
                                                {
                                                  print("tapped on serial no");
                                                  showSerialNumberList();
                                                },
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 5),
                                                child: Divider(
                                                  thickness: 1,
                                                  color: Colors.grey,
                                                  height: 10,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                  Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 25),
                                        child: InkWell(
                                          child: Image.asset(
                                            "images/QRCode.png",
                                          ),
                                          onTap: () {
                                            serialNoBarcodeValue = "";
                                            serialNoScan();
                                          },
                                        ),
                                      ))
                                ],
                              ),
                            ),
                   */

                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: MaterialButton(
                                  onPressed: ()
                                  {

                                  },
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      buttonName.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: MaterialButton(
                                  onPressed: () {
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Cancel".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )))),
    );
  }
  Widget showCircularProgressbarWidget() {
    return Container(
        child: Center(
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                strokeWidth: 5,
                backgroundColor: Colors.red,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: new Text(
                  "Loading",
                  style:
                  TextStyle(fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
                ),
              ),
            ],
          ),
        ));
  }




}