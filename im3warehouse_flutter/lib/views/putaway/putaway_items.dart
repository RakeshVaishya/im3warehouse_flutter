import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/putaway_item/default_purchasing_room_location.dart';
import 'package:im3_warehouse/models/putaway_item/put_away_responce_model.dart';
import 'package:im3_warehouse/network/api/put_away_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/putaway/dialogCallback.dart';
import 'package:im3_warehouse/views/putaway/putaway_location_dialog.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:xml/xml.dart' as xml;
import 'package:flutter/widgets.dart';

class PutAwayItems extends StatefulWidget {
  ViewTypeCallBack callback;
  PutAwayItems(this.callback);

  @override
  State<StatefulWidget> createState() {
    return PutAwayItemsState();
  }
}

class PutAwayItemsState extends State<PutAwayItems> implements DialogCallback {
  @override
  void didUpdateWidget(PutAwayItems oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("didUpdateWidget");
  }

  @override
  void dispose() {
    super.dispose();
    print("dispose");
  }

  TextEditingController _labelIdOrPalletNoController = TextEditingController();
  TextEditingController _putAwayLocationController = TextEditingController();
  TextEditingController _confirmPutawayLocationController =  TextEditingController();
  TextEditingController _putAwayQtyLeftController = TextEditingController();
  TextEditingController _putAwayQtyController = TextEditingController();
  TextEditingController qtyController = TextEditingController();
  TextEditingController serialNumberController = TextEditingController();

  FocusNode _labelIdOrPalletNoFocus = new FocusNode();
  FocusNode locationFocus = new FocusNode();
  FocusNode confirmLocationFocus = new FocusNode();

  FocusNode binFocus = new FocusNode();
  PutAwayResponseModel putAwayResponseModel;
  bool loading = true;
  var _formkey = GlobalKey<FormState>();
  var _dialogKey = GlobalKey();
  bool isLoaderRunning =false;
  bool _validate = false;
  String labelOrPalletBarcodeValue = "";
  String serialNoBarcodeValue = "";
  String userId = "";
  String companyCode = "";
  Facility facility;
  bool colorbool = false;
  bool serialNoBool = false;
  String selectedCountTye = "";
  Color partColor;
  Color binColor;
  String locationData = "";
  //ProgressDialog pr;
  BuildContext context;
  Color serverResponeColor;
  String serverResponse="";
  bool serverRespone= false;
   int qty =0;
  RoomFacForLocation location;
  List<RoomFacForLocation> putAwayRoomFacForLocationList = [];
  RoomFacForLocation confirmLocation;
  List<RoomFacForLocation> confirmPutAwayRoomFacForLocationList = [];
  bool isLoading =true;

  @override
  void initState() {
    super.initState();
    getData();
    _labelIdOrPalletNoFocus.addListener(() {
      setState(() {
        partColor = Colors.grey[600];
      });
    });
    binFocus.addListener(() {
      setState(() {
        binColor = Colors.grey[600];
      });
    });
    //testData();
  }
  Future<void> getData() async {
    String data =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    facility = Facility.fromJson(map);
    print("retrived from SH file--->" + facility.iFacilityCode);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
  }

  @override
  Widget build(BuildContext context) {
    if(_labelIdOrPalletNoController.text.length==0){
      FocusScope.of(context).requestFocus(_labelIdOrPalletNoFocus);
    }
    this.context =context;
    return formWidget();
  }
  Widget formWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 8, right: 8, bottom: 5),
      child: SingleChildScrollView(
          child: Scrollbar(
              child: Form(
                  key: this._formkey,
                  autovalidate: _validate,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Visibility(
                        visible: serverRespone,
                        maintainState: true,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(serverResponse,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: serverResponeColor
                            ),),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: TextFormField(
                                  focusNode: _labelIdOrPalletNoFocus,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: this._labelIdOrPalletNoController,
                                  textInputAction: TextInputAction.send,
                                  onFieldSubmitted: (value)
                                  {
                                    if(value.trim().length==0)
                                    {

                                    }
                                    else{
                                      if(isLoading) {
                                        isLoading =false;
                                        checkInternetConnection(PutAwayApi
                                            .API_VALIDATE_PUT_AWAY_LABEl_ID,
                                            value.toString().trim());
                                      }
                                     }
                                    },
                                  decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.fromLTRB(0, 5, 0, 0),
                                      labelText:
                                          AppStrings.LABEL_ID_OR_PALLET_NO,
                                      labelStyle: TextStyle(
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: _labelIdOrPalletNoFocus.hasFocus
                                              ? partColor
                                              : null)),
                                     onChanged: (text) {
                                     if(text.length==0)
                                     {

                                       setState(() {
                                         getClearFromField();
                                       });
                                    }
                                     else{
                                       _validate =false;
                                       setState(() {
                                       });
                                     }
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return AppStrings.ERROR_PUT_AWAY;
                                    }
                                  }),
                            ),
                          ),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: InkWell(
                              child: Image.asset(
                                "images/QRCode.png",
                              ),
                              onTap: () {
                                labelOrPalletBarcodeValue = "";
                                labelIdOrPalletNoScan();
                              },
                            ),
                          ))
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                              flex: 9,
                              child: TextFormField(
                                focusNode: locationFocus,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                   controller: _putAwayLocationController,
                                  textInputAction: TextInputAction.send,
                                onFieldSubmitted: (value)
                                {
                                  if(value.trim().length==0){
                                    return;
                                  }
                                  if(isLoading) {
                                    isLoading =false;
                                    checkInternetConnection("putAwayLocation",
                                        value.toString().trim());
                                  }
                                },
                                onChanged: (text) {
                                },
                                decoration: InputDecoration(
                                    labelText:
                                        AppStrings.LABEL_PUT_AWAY_LOCATION,
                                    labelStyle: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: locationFocus.hasFocus
                                            ? binColor
                                            : null)),
                                validator: (text) {
                                  if (text.isEmpty) {
                                    return AppStrings.LABEL_BIN_ERROR;
                                  }
                                },
                              )),
                          Expanded(
                              child: Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: InkWell(
                              child: Image.asset(
                                "images/QRCode.png",
                              ),
                              onTap: () {
                                binScan();
                              },
                            ),
                          ))
                        ],
                      ),
                      TextFormField(
                        style: TextStyle(
                            color: Colors.grey[600],
                            fontFamily: AppStrings.SEGOEUI_FONT),
                        controller: _confirmPutawayLocationController,
                        focusNode: confirmLocationFocus,
                        decoration: InputDecoration(
                            labelText:
                                AppStrings.LABEL_CONFIRM_PUT_AWAY_LOCATION,
                            labelStyle: TextStyle(
                                color: Colors.grey[600],
                                fontFamily: AppStrings.SEGOEUI_FONT)),
                                textInputAction: TextInputAction.send,
                                 onFieldSubmitted: (value)
                                 {
                                  /* if(confirmPutAwayRoomFacForLocation ==null){
                                     Utility.showToastMsg(
                                         "Sync for confirm location");
                                     return;
                                   }*/

                                  if(value.trim()
                                  .length>0)
                                  {
                                    if(isLoading ) {
                                      isLoading =false;
                                      checkInternetConnection(
                                          "confirmPutawayLocation", value
                                          .toString().trim());
                                    }
                                  }
                                  else{
                                  }
                                 },
                         onChanged: (text){
                          if(text.length==0){

                          }
                         },
                          validator: (text) {
                            if (text.isEmpty) {
                            return "Confirm Putaway location cannot be blank !";
                          }
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15.0),
                        child: MaterialButton(
                          onPressed: () {
                            if(_labelIdOrPalletNoController.text.length==0 || putAwayResponseModel.iPartNo.length ==0)
                            {
                              return;
                            }
                            else{
                              starLoader();
                              getDetails("", PutAwayApi.API_GET_DEFAULT_PURCHASING_ROOM_LOCATION);
                            }

                          },
                          color: AppColors.CYAN_BLUE_COLOR,
                          minWidth: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              AppStrings.BTN_ADD_LOCATION.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 17.0,
                                  color: Colors.white,
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0.0))),
                        ),
                      ),
                      TextFormField(
                        style: TextStyle(
                            color: Colors.grey[600],
                            fontFamily: AppStrings.SEGOEUI_FONT),
                        controller: _putAwayQtyLeftController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: AppStrings.LABEL_PUT_AWAY_QTY_LEFT,
                            labelStyle: TextStyle(
                                color: Colors.grey[600],
                                fontFamily: AppStrings.SEGOEUI_FONT)),
                        validator: (text) {
                          if (text.isEmpty) {
                            return "Please Enter Putaway Left Quantity";
                          }
                        },
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            color: Colors.grey[600],
                            fontFamily: AppStrings.SEGOEUI_FONT),
                        controller: _putAwayQtyController,
                        decoration: InputDecoration(
                            labelText: AppStrings.LABEL_PUT_AWAY_QTY,
                            labelStyle: TextStyle(
                                color: Colors.grey[600],
                                fontFamily: AppStrings.SEGOEUI_FONT)),
                        validator: (text) {
                          if (text.isEmpty) {
                            return "Please Enter Putaway Quantity";
                          }
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: MaterialButton(
                                  onPressed: () {
                                    onSave();
                                    /*if(_labelIdOrPalletNoController.text.length==0)
                                     {
                                       return;
                                     }
                                    if(_putAwayLocationController.text.trim() ==
                                        _confirmPutawayLocationController.text.trim())
                                    {

                                    }
                                    else{
                                      Utility.showToastMsg(
                                          "Putaway location/Confirm Putaway location is invalid");
                                        }
                                    },*/

                                  },
                                  color: getColorButton(colorbool),
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Putaway".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: MaterialButton(
                                  onPressed: () {
                                    onCancel();
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Cancel".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),)
                          ],
                        ),
                      ),
                    ],
                  )))),
    );
  }
  Widget showCircularProgressbarWidget() {
    return Container(
        child: Center(
      child: new Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          new CircularProgressIndicator(
            strokeWidth: 5,
            backgroundColor: Colors.red,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5.0),
            child: new Text(
              "Loading",
              style:
                  TextStyle(fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
            ),
          ),
        ],
      ),
    ));
  }

  onSave() {
    getDataFromField();
    if (_formkey.currentState.validate()) {
      _formkey.currentState.save();
    } else {
      setState(() {
        _validate = true;
      });
    }
    if(_putAwayLocationController.text.length==0 ||
        _confirmPutawayLocationController.text.length==0 ||
        _labelIdOrPalletNoController.text.length==0){
      return;
     }

    if(location==null )
    {
      Utility.showToastMsg(
          "Sync for location ");
      FocusScope.of(context).requestFocus(locationFocus);
      return;
    }
    if(confirmLocation==null )
    {
      Utility.showToastMsg(
          "Sync for confirm location ");
      FocusScope.of(context).requestFocus(confirmLocationFocus);
      return;
    }


    if(confirmLocation.ifacilityCode != location.ifacilityCode)
    {
      Utility.showToastMsg(
          "Put away and Confirm location not same ");
      FocusScope.of(context).requestFocus(confirmLocationFocus);
      return;
    }
    if(
    _confirmPutawayLocationController.text !=
        _putAwayLocationController.text)
    {
      Utility.showToastMsg(
          "Put away and Confirm location not same ");
      FocusScope.of(context).requestFocus(confirmLocationFocus);
      return;
    }

      starLoader();
        getDetails("", PutAwayApi.API_ADD_PUT_AWAY);

  }

  Future labelIdOrPalletNoScan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      if(barcode.rawContent.length<=0){
        Utility.showToastMsg("No Data ");
      }
      else{
        setState(() {
          _labelIdOrPalletNoController.text = barcode.rawContent;
          checkInternetConnection(PutAwayApi.API_VALIDATE_PUT_AWAY_LABEl_ID, _labelIdOrPalletNoController.toString().trim());
        });
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      setState(() {
        Utility.showToastMsg(
              'null (user returned using the "back-button" before scanning)');
      });
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  Future binScan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      if(barcode.rawContent.length<=0)
      {
        return;
      }
      setState(()
      {
        _putAwayLocationController.text = barcode.rawContent;
        if(barcode.rawContent.length>0) {
              //_handleSubmit(context);
             starLoader();
              getData().then((res) {
             getDetails(barcode.toString().trim(), "putAwayLocation");
          });
        }
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      setState(() {
        Utility.showToastMsg(
            'null (user returned using the "back-button" before scanning)');
      });
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }
  /*
  Future serialNoScan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      serialNoScan();
      setState(() {
        this.serialNoBarcodeValue = barcode;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      setState(() {
        Utility.showToastMsg(
            'null (user returned using the "back-button" before scanning)');
      });
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }
*/
  @override
  void getList(List<String> data) {
    // TODO: implement getList
  }
  void getDataFromField() {
    _labelIdOrPalletNoController.text;
    _putAwayLocationController.text;
    _confirmPutawayLocationController.text;
    _putAwayQtyLeftController.text;
    _putAwayQtyController.text;

    print(
        "_labelIdOrPalletNoController --" + _labelIdOrPalletNoController.text);
    print("_putAwayLocationController  --" + _putAwayLocationController.text);
    print("_confirmPutawayLocationController   --" +  _putAwayLocationController.text);
    print("_putAwayQtyLeftController   --" + _putAwayQtyLeftController.text);
    print("_putAwayQtyController   --" + _putAwayQtyController.text);

    if(_putAwayQtyController.text.length>0)
    {
      if( StringUtil.isNumeric(_putAwayQtyController.text))
      {
        if( StringUtil.isDouble(_putAwayQtyController.text))
        {
          double d = double.parse(_putAwayQtyController.text);
          qty =d.toInt();
        }
        else{
          if( StringUtil.isInt(_putAwayQtyController.text))
          {
            qty =int.parse(_putAwayQtyController.text);
          }
        }

      }
    }else{
      return;
    }
  }

  void getClearFromField() {
    _labelIdOrPalletNoController.text = "";
    _putAwayLocationController.text = "";
    _confirmPutawayLocationController.text = "";
    _putAwayQtyLeftController.text = "";
    _putAwayQtyController.text = "";
     location =null;
     putAwayRoomFacForLocationList = [];
     confirmLocation =null;
     confirmPutAwayRoomFacForLocationList = [];
    colorbool =false;
    print(
        "_labelIdOrPalletNoController --" + _labelIdOrPalletNoController.text);
    print("_putAwayLocationController  --" + _putAwayLocationController.text);
    print("_confirmPutawayLocationController   --" +
        _putAwayLocationController.text);
    print("_putAwayQtyLeftController   --" + _putAwayQtyLeftController.text);
    print("_putAwayQtyController   --" + _putAwayQtyController.text);
  }

  void showAddLocationDialog(DefaultPurchasingRoomLocation defaultPurchasingRoomLocation ,BuildContext context ) {

    PutAwayLocationDialog putAwayLocationDialog = new PutAwayLocationDialog(callback: this , defaultPurchasingRoomLocation:defaultPurchasingRoomLocation);
    showDialog(
        context: context,
        builder: (context) {
        return  putAwayLocationDialog;
        });
   }

   Future getDetails(String data, String type) async {
    print("part no -->" + data);
    print("user --Id  -->" + userId);
    print("companyCode --  -->" + companyCode);
    String jsonBody = "";
    String uri = "";
    print(NetworkConfig.BASE_URL);
    if  (StringUtil.isEquals(type, PutAwayApi.API_VALIDATE_PUT_AWAY_LABEl_ID))
    {
      uri = NetworkConfig.BASE_URL +  NetworkConfig.WEB_URL_PATH + PutAwayApi.API_VALIDATE_PUT_AWAY_LABEl_ID;
      jsonBody =  PutAwayApi.validatePutAwayLabelID( companyCode, facility,_labelIdOrPalletNoController.text);

    }
    if  (type == "putAwayLocation")
    {
      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION;
      jsonBody = NetworkConfig.getRoomFacForLocation(
          companyCode, userId, data,
          facility.iFacilityCode);
    }

    if  (type == "confirmPutawayLocation")
    {
      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION;
         jsonBody = NetworkConfig.getRoomFacForLocation(
          companyCode, userId, data,
          facility.iFacilityCode);
    }

    if  (StringUtil.isEquals(type, PutAwayApi.API_GET_DEFAULT_PURCHASING_ROOM_LOCATION))
    {
      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          PutAwayApi.API_GET_DEFAULT_PURCHASING_ROOM_LOCATION;
      jsonBody = PutAwayApi.getDefaultPurchasingRoomLocation(companyCode,facility.iFacilityCode,"0");
    }

    if  (type == PutAwayApi.API_ADD_PUT_AWAY)
    {
/*
    Public Function AddPutaway(ByVal PutawayLabelID As String,
        ByVal IPutawayLabelID As Integer,
        ByVal PutAwayqty As Integer,
        ByVal PutawayLocation As String,
        ByVal ConfirmPutawayLocation As String,
        ByVal ILocationRoomFacility As String) As String
*/





      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          PutAwayApi.API_ADD_PUT_AWAY;
            print(putAwayResponseModel.putawayLabelID);
            print(putAwayResponseModel.iPutawayLabelID);
            print(_putAwayQtyController.text);
            print(_putAwayLocationController.text);
            print(_confirmPutawayLocationController.text);
            print(location.iLocationRoomFacility);
            jsonBody = PutAwayApi.addPutaway(putAwayResponseModel.putawayLabelID,
            putAwayResponseModel.iPutawayLabelID,
            qty.toString(), _putAwayLocationController.text,
            _confirmPutawayLocationController.text,
                getILocationRoomFacility(confirmLocation),
                companyCode
            ,userId);
    }
    print("data---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    ).then((response)
    {
      isLoading =true;
      int statusCode = response.statusCode;
      print("Status code  $statusCode" );
      if (statusCode == 200)
      {
        String responseBody = response.body;
        print("responce body recived from server--->"+responseBody);
        onSuccessResponse(type, responseBody);
        return;
      } else
        {
          onErrorResponse(type,statusCode.toString());
          print("Wrong input");
         return;
      }
    }).catchError((error)
    {      isLoading =true;

    onErrorResponse(type,error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)).catchError((e)
    {
      isLoading =true;
      dismissLoader();
      Utility.showToastMsg("Error-->"+e.toString());
    } );
   }
  void onSuccessResponse(String type, String responseBody)
  {
    isLoading =true;
    if (StringUtil.isEquals(type, PutAwayApi.API_VALIDATE_PUT_AWAY_LABEl_ID))
    {
      print("onSuccessResponse --" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      responseBody = parsedXml.findElements("string").first.text;
      print("xml parse done--" + responseBody);
      if  (responseBody != "0")
      {
        dismissLoader();
        print(" do transaction--");
        Map data1 ;//= jsonDecode(responseBody);
        try{
           data1 = jsonDecode(responseBody);
        }
        catch(ex){

          print(ex);
          _labelIdOrPalletNoController.text="";
          setState(() {

          });
          Utility.showToastMsg("Please enter a valid Putaway Label ID/Pallet No");
          return;
        }
        Map data2 ;//
        try {
          data2 = data1['NewDataSet'];
        }
        catch(ex)
        {
          print(ex);
          Utility.showToastMsg("Please enter a valid Putaway Label ID/Pallet No");
          return;
        }
        if (data2 != null && data2.length > 0)
        {
          print(" data exist -");
          Map table = data2['Table'];
          putAwayResponseModel = PutAwayResponseModel.fromJson(table);
          print("putAwayResponseModel-->" + putAwayResponseModel.putawayLocation);
            setState(()
            {
            _putAwayQtyLeftController.text =putAwayResponseModel.putawayQty;
            _putAwayQtyController.text ="1.00";
            _putAwayLocationController.text = putAwayResponseModel.putawayLocation;
            _confirmPutawayLocationController.text = putAwayResponseModel.putawayLocation;
            FocusScope.of(context).requestFocus(locationFocus);
            colorbool =true;
            });
          return;
        }
        else {
          print(" show erro transaction--");
          serverRespone = true;
          serverResponse = "Please enter a valid Putaway Label ID/Pallet No";
          serverResponeColor = Colors.red;
          setState(() {});
          return;
        }
      }
      else{
         print(" show erro transaction--");
        serverRespone = true;
        serverResponse = "Please enter a valid Putaway Label ID/Pallet No";
        serverResponeColor = Colors.red;
        setState(() {});
      }
      return;
    }
    if (type == "putAwayLocation")
    {
         //dismiss();
         dismissLoader();
         print("----------" + responseBody);
         xml.XmlDocument parsedXml = xml.parse(responseBody);
         print(parsedXml);
         responseBody = parsedXml.findElements("string").first.text;
         Map data1 ;
         try {
            data1 = jsonDecode(responseBody);
         }
         catch(ex)
         {
           Utility.showToastMsg(
               "Putaway location/Confirm Putaway location is invalid");
           FocusScope.of(context).requestFocus(locationFocus);
           setState(()
           {

           });
           return;
         }
         Map data2 ;
         try {
         data2 = data1['NewDataSet'];
         }
         catch(ex)
        {
        Utility.showToastMsg(
            "Putaway location/Confirm Putaway location is invalid");
        FocusScope.of(context).requestFocus(locationFocus);
        setState(()
        {

        });
        print("");
        return;
        }
       List<dynamic> data;
        try {
          data = data2['Table'];
        }catch(ex)
        {
        print ("data in single");
        }
        if(data !=null && data.length>0)
        {
          print(" multiple array  data  --");
          putAwayRoomFacForLocationList = new List();
          if (data.length > 0) {
            for (int i = 0; i < data.length; i++) {
              Map dat = data.elementAt(i);
              RoomFacForLocation temp = RoomFacForLocation.fromJson(dat);
              putAwayRoomFacForLocationList.add(temp);
              if (i == 0)
              {
                print(" addd --"  );
                location = temp;
                print("---------------"+location.iLocationRoomFacility);
                FocusScope.of(context).requestFocus(confirmLocationFocus);
              }
            }
            print(" size of roomfacrLoaction--");
            print(" end --");
          }
        }
      else{
          print(" single data  --");
          location = RoomFacForLocation.fromJson(data2['Table']);
          putAwayRoomFacForLocationList.add(location);
          FocusScope.of(context).requestFocus(confirmLocationFocus);
          return;
         }
      }
    if (type == "confirmPutawayLocation") {
      dismissLoader();
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      Map data1 ;
      try {
        data1 = jsonDecode(responseBody);
      }
      catch(ex)
      {
        Utility.showToastMsg(
            "Putaway location/Confirm Putaway location is invalid");
        FocusScope.of(context).requestFocus(confirmLocationFocus);
        setState(() {

        });

        return;
      }
      Map data2 ;
      try {
        data2 = data1['NewDataSet'];
      }
      catch(ex)
      {
        Utility.showToastMsg(
            "Putaway location/Confirm Putaway location is invalid");
        FocusScope.of(context).requestFocus(confirmLocationFocus);
        setState(() {

        });
        print("");
        return;
      }
      List<dynamic> data ;
      try {
        data = data2['Table'];
      }catch(ex)
      {

      }
      if(data !=null && data.length>0)
      {
        confirmPutAwayRoomFacForLocationList = new List();
        if (data.length > 0)
        {
          for (int i = 0; i < data.length; i++) {
            Map dat = data.elementAt(i);
            RoomFacForLocation temp = RoomFacForLocation.fromJson(dat);
            confirmPutAwayRoomFacForLocationList.add(temp);
            if (i == 0)
            {
              print(" addd --"  );
              confirmLocation = temp;
              print("---------------"+confirmLocation.iLocationRoomFacility);
            }
          }
          print(" size of roomfacrLoaction--");
          print(" end --");
        }
      }
      else{
        confirmLocation = RoomFacForLocation.fromJson(data2['Table']);
        confirmPutAwayRoomFacForLocationList.add(confirmLocation);
      }
      setState(() {
        _putAwayQtyLeftController.text =putAwayResponseModel.putawayQty;
        _putAwayQtyController.text ="1.00";
      });
      return;
    }
    if (type == PutAwayApi.API_GET_DEFAULT_PURCHASING_ROOM_LOCATION )
      {dismissLoader();
         //dismiss ();
        print("----------" + responseBody);
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        print(parsedXml);
        responseBody = parsedXml.findElements("string").first.text;
        Map data = jsonDecode(responseBody);
        Map newDataSet = data['NewDataSet'];
        Map table = newDataSet['Table'];
        DefaultPurchasingRoomLocation defaultPurchasingRoomLocation =DefaultPurchasingRoomLocation.fromJson(table);
        defaultPurchasingRoomLocation.companyCode =companyCode;
        showAddLocationDialog(defaultPurchasingRoomLocation,context);
    }
    if  (type == PutAwayApi.API_ADD_PUT_AWAY)
    {dismissLoader();
      //dismiss ();
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
     if(responseBody=="Putaway Transaction Added Successfully")
      {
        serverRespone = true;
        serverResponse = "Putaway Transaction Added Successfully";
        serverResponeColor = Colors.green;
        setState(() {
          getClearFromField();
        });
        return;
       }
     else{
       List <String>data =responseBody.split("|");
       if(data !=null &&data.elementAt(0).toString().length>0)
       {
         serverResponse =data.elementAt(0).toString() ;
       }
       else{
         serverResponse =responseBody ;
       }
       serverRespone = true;
       serverResponeColor = Colors.red;
       setState(() {
       });
       return;
     }
    }
 }
  void onErrorResponse(String type ,String errorMessage )
  {
    isLoading =true;
    dismissLoader();
     if (type== PutAwayApi.API_VALIDATE_PUT_AWAY_LABEl_ID)
     {
      setState(()
      {
      });
      return;
    }
    if (type == "putAwayLocation")
    {
      setState(() {
        setState(() {
          Utility.showToastMsg(
              "Error ! Server Responce ->"+errorMessage);

        });
      });
      return;
    }
    if (type == "confirmPutawayLocation") {
      setState(() {
        Utility.showToastMsg(
            "Error ! Server Responce ->" + errorMessage);
      });
      return;
    }
    if (type == PutAwayApi.API_ADD_PUT_AWAY)
    {
      print("called");
      setState(() {
        Utility.showToastMsg(
            "Error ! Server Responce ->"+errorMessage);
      });
    }
    return;
  }

  @override
  void getServerRespForLocation(DefaultPurchasingRoomLocation data) {
    setState(()
     {
        print("---- getServerRespForLocation ---");
       _putAwayLocationController.text=data.roomAreaCode;
       _confirmPutawayLocationController.text=data.roomAreaCode;
        getData().then((value)
        {
        starLoader();
        getDetails(_putAwayLocationController.text, "putAwayLocation");
        });
     }
    );
  }
  void onCancel()
  {
    this
        .widget
        .callback
        .viewType(DashboardView(this.widget.callback), AppStrings.LABEL_HOME);
  }

  void testData()
  {
 // _labelIdOrPalletNoController.text="ZZ013602";

  }
  void showProgressDialog(BuildContext context)
  {

  }
  void dismissDialog()
  {
  }

  Future<bool> checkInternetConnection(String requestType, String input) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        isLoading =true;
        Future.delayed(
            Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION), ()
          {
          dismissLoader();
          });

        if (requestType == PutAwayApi.API_VALIDATE_PUT_AWAY_LABEl_ID)
        {
          serverRespone = false;
          serverResponeColor = Colors.white;
          setState(()
          {
          });
          starLoader();
          getDetails(input.toString().trim(),
              requestType.trim());
        }

        if (requestType == "putAwayLocation") {
          starLoader();
          getDetails(input, requestType);
        }
        if (requestType == "confirmPutawayLocation")
        {
          starLoader();
          getDetails(input, requestType);
        }
      }
    }
    on SocketException catch (e) {
      isLoading =true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar = SnackBar(
        content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }


  void starLoader()
  {
    print("dialog started");
    if(!isLoaderRunning)
    isLoaderRunning =true;
    Dialogs.showLoadingDialog(context, _dialogKey);
  }
  void dismissLoader() {
    print("before --$isLoaderRunning");
    if (isLoaderRunning == true)
    {
      isLoaderRunning = false;
      print("after --$isLoaderRunning");
      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }







/*
  bool isLoaderRunning = false;
  void starLoader() {
    print("dialog started");
    //  startTimer();
    isLoaderRunning = true;
    Dialogs.showLoadingDialog(context, _dialogKey);
  }

  void dismissLoader() {
    print("before --$isLoaderRunning");
    if (isLoaderRunning == true) {
      isLoaderRunning = false;
      print("after --$isLoaderRunning");
      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }
*/
/*

  Future<void> _handleSubmit(BuildContext context) async
  {
      Dialogs.showLoadingDialog(context, _dialogKey);//invoking login
  }
  void dismiss ()
  {try {
    Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
  }catch(ex)
  {
  }
  }
*/

 Color  getColorButton(bool status )
 {
    if(status)
      return  AppColors.CYAN_BLUE_COLOR;
     else
     return  Colors.grey;
 }


String  getILocationRoomFacility(RoomFacForLocation confirmPutAwayRoomFacForLocation)
{
  String data=confirmPutAwayRoomFacForLocation.iRoomAreaCode+"|"+
      confirmPutAwayRoomFacForLocation.iRoomCode+"|"+ confirmPutAwayRoomFacForLocation.ifacilityCode;
print("----->  "+data);
  return data;
}
}
