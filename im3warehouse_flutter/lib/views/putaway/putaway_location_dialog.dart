import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/putaway_item/default_purchasing_room_location.dart';
import 'package:im3_warehouse/network/api/put_away_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/putaway/dialogCallback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;
import 'package:flutter/widgets.dart';

class PutAwayLocationDialog extends StatefulWidget {
  DialogCallback callback;
  DefaultPurchasingRoomLocation defaultPurchasingRoomLocation;
  PutAwayLocationDialog({this.callback, this.defaultPurchasingRoomLocation});

  @override
  PutAwayLocationDialogState createState() => PutAwayLocationDialogState();
}

class PutAwayLocationDialogState extends State<PutAwayLocationDialog>
    with WidgetsBindingObserver {
  DefaultPurchasingRoomLocation defaultPurchasingRoomLocation;
  var _dialogKey = GlobalKey<FormState>();
  TextEditingController _locationCodeNoController = TextEditingController();
  TextEditingController _locationDescController = TextEditingController();
  var _formKey = GlobalKey<FormState>();

  FocusNode _locationCodeFocus = new FocusNode();
  FocusNode _locationDescFocus = new FocusNode();

  ProgressDialog pr;
  DialogCallback _callback;
  Color serverResponeColor;
  String serverResponse = "";
  bool serverRespone = false;

  String createdUserId = "";

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    defaultPurchasingRoomLocation = widget.defaultPurchasingRoomLocation;
    _callback = widget.callback;
    //getTest();
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID)
        .then((value) {
      createdUserId = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).requestFocus(_locationCodeFocus);
    return AlertDialog(
      title: Text("Putaway Items".toUpperCase(),
          style: TextStyle(
            fontSize: 16,
          )),
      content: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Visibility(
              visible: serverRespone,
              maintainState: true,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  serverResponse,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: serverResponeColor),
                ),
              ),
            ),
            TextFormField(
              focusNode: _locationCodeFocus,
              style: TextStyle(
                  color: Colors.black, fontFamily: AppStrings.SEGOEUI_FONT),
              controller: this._locationCodeNoController,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                  labelText: AppStrings.LABEL_LOCATION_CODE,
                  labelStyle: TextStyle(
                    fontFamily: AppStrings.SEGOEUI_FONT,
                  )),
              validator: (text) {
                if (text.isEmpty) {
                  return "Please Enter Location code";
                }
              },
            ),
            TextFormField(
                focusNode: _locationDescFocus,
                style: TextStyle(
                    color: Colors.black, fontFamily: AppStrings.SEGOEUI_FONT),
                controller: this._locationDescController,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                    labelText: AppStrings.LABEL_LOCATION_DESC,
                    labelStyle: TextStyle(
                      fontFamily: AppStrings.SEGOEUI_FONT,
                    )),
                validator: (value) {
                  if (value.isEmpty) {
                    return "Please Enter Location Description";
                  }
                }),
            Padding(
              padding: const EdgeInsets.only(top: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          if(_locationCodeNoController.text.trim().length==0)
                          {
                            Utility.showToastMsg(  "Location code cannot be blank");
                            FocusScope.of(context).requestFocus(_locationCodeFocus);
                            return;
                          }
                        else  if(_locationDescController.text.trim().length==0)
                          {
                            Utility.showToastMsg("Location description cannot be blank");
                            FocusScope.of(context).requestFocus(_locationDescFocus);
                            return;
                          }
                        else{
                          print("load location");
                          onSave();
                          }
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Save".toUpperCase(),
                            style:
                                TextStyle(fontSize: 14.0, color: Colors.white),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          onCancel();
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Cancel".toUpperCase(),
                            style:
                                TextStyle(fontSize: 14.0, color: Colors.white),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onSave() {
/*
    Location code cannot be blank
    Location description cannot be blank
*/


    if (_formKey.currentState.validate())
    {
      _formKey.currentState.save();


    }
    //showProgressDialog(context);
    _handleSubmit(context);
    Future.delayed(Duration(seconds: 50));
    getDataFromField();
  }

  void getDataFromField() {
    _locationCodeNoController.text;
    _locationDescController.text;
    defaultPurchasingRoomLocation.roomAreaCode = _locationCodeNoController.text;
    getDetails("", PutAwayApi.API_CHECK_DUPLICATE_ROOM_AREA_CODE);
  }

  void reset() {
    _locationCodeNoController.text = "";
    _locationDescController.text = "";
  }

  void getTest() {
    _locationCodeNoController.text = "test";
    _locationDescController.text = "test";
  }

  void onCancel() {
    dismissDialog();
    Navigator.pop(context);
  }

  @override
  void deactivate() {
    super.deactivate();
    //this method not called when user press android back button or quit
    print('deactivate');
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    //this method not called when user press android back button or quit
    print('dispose');
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    //print inactive and paused when quit
    print(state);
  }

  Future getDetails(String data, String type) async {
    print("  data  -->" + "");
    print("companyCode --  -->" + defaultPurchasingRoomLocation.companyCode);
    String jsonBody = "";
    String uri = "";
    print(NetworkConfig.BASE_URL);

    Map headers = new Map();
    if (StringUtil.isEquals(
        type, PutAwayApi.API_CHECK_DUPLICATE_ROOM_AREA_CODE)) {
      headers = NetworkConfig.headers;

      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PutAwayApi.API_CHECK_DUPLICATE_ROOM_AREA_CODE;
      jsonBody = PutAwayApi.isDuplicateRoomAreaCode(
          defaultPurchasingRoomLocation.companyCode,
          defaultPurchasingRoomLocation.iFacilityCode,
          defaultPurchasingRoomLocation.iRoomCode,
          defaultPurchasingRoomLocation.roomAreaCode);
    }
    if (StringUtil.isEquals(type, PutAwayApi.API_ADD_NEW_LOCATION)) {
      headers = NetworkConfig.jsonHeaders;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          PutAwayApi.API_ADD_NEW_LOCATION;
      defaultPurchasingRoomLocation.roomAreaName = _locationDescController.text;
      jsonBody = jsonBody = PutAwayApi.addNewLocation(
          defaultPurchasingRoomLocation, createdUserId);
    }
    print("data---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    Response response = await post(
      uri,
      headers: headers,
      body: jsonBody,
      encoding: encoding,
    ).then((response) {
      int statusCode = response.statusCode;
      print("Status code  $statusCode");
      if (statusCode == 200) {
        String responseBody = response.body;
        print("responce body recived from server--->" + responseBody);
        print("type--->" + type);
        onSuccessResponse(type, responseBody);
      } else {
 }
    }).catchError((error) {
        onErrorResponse(type,error.toString());

    }).timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION));
  }

  void onSuccessResponse(String type, String responseBody) {

    if (StringUtil.isEquals(
        type, PutAwayApi.API_CHECK_DUPLICATE_ROOM_AREA_CODE)) {
      print("onSuccessResponse --" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      responseBody = parsedXml.findElements("boolean").first.text;
      print("xml parse done--" + responseBody);
      if (StringUtil.isEquals(responseBody, "false")) {
        dismiss();
        serverRespone = true;
        serverResponse = "Location Code already exists for the room";
        serverResponeColor = Colors.red;
        setState(()
        {
        });
        print("duplicate Location");
        return;
       } else
        {
        getDetails("", PutAwayApi.API_ADD_NEW_LOCATION);
        return;
      }
    }
    if (StringUtil.isEquals(type, PutAwayApi.API_ADD_NEW_LOCATION))
    {

      print("----------" + responseBody);
      Map data1 = jsonDecode(responseBody);
      String response = data1['d'];

      if (response == "success")
      {  dismiss();

      print ("inside  ---");
        _callback.getServerRespForLocation(defaultPurchasingRoomLocation);
        setState(() {
          Navigator.pop(context);

        });
      } else {}
    } else {}
  }

  void onErrorResponse(String type ,String msg) {
    if (StringUtil.isEquals(
        type, PutAwayApi.API_CHECK_DUPLICATE_ROOM_AREA_CODE)) {
      serverRespone = true;
      serverResponse = msg;
      serverResponeColor = Colors.red;
      setState(() {});
    }

    if (StringUtil.isEquals(type, PutAwayApi.API_ADD_NEW_LOCATION)) {
      serverRespone = true;
      serverResponse = msg;
      serverResponeColor = Colors.red;
      setState(() {

      });
    }
  }
  void showProgressDialog(BuildContext context) {
    dismissDialog();
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
  }

  void dismissDialog() {
    if (pr != null) {
    //  pr.dismiss();
      pr.hide();


      pr = null;
    }
  }
  Future<void> _handleSubmit(BuildContext context) async {
    Dialogs.showLoadingDialog(context, _dialogKey); //invoking login
  }

  void dismiss()
  {
    Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
  }
}
