import 'dart:convert';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberModel.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_callback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';

import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:xml/xml.dart' as xml;


class SerialNumberManager extends StatelessWidget {
  SerialNumberCallback callback;
  SerialNumberModel serialNumberModel;
  String viewType;
  SerialNumberManager({this.callback, this.serialNumberModel});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.withOpacity(0.70),
      // this is the main reason of transparency at next screen. I am ignoring rest implementation but what i have achieved is you can see.
      body: SerialNumberManage(this.callback, this.serialNumberModel),
    );
  }
}

class SerialNumberManage extends StatefulWidget {
  SerialNumberCallback callback;
  SerialNumberModel serialNumberModel;
  String viewType;
  SerialNumberManage(this.callback, this.serialNumberModel );

  @override
  SerlizePartDialogState createState() => SerlizePartDialogState();
}

class SerlizePartDialogState extends State<SerialNumberManage> {
  List<SerialNumberStatus> barcodeItem = [];
  TextEditingController serialNumberController = new TextEditingController();
  int indexNumber;
  String buttonName = "Add";
  String label = "Add Serial Number";
  String strSerialNo = "";
  String controlId = "";
  String stockSerialNumber = "";

  SerialNumberCallback callback;
  bool isSerialError = false;
  ProgressDialog pr;
  var _dialogKey = new  GlobalKey();

  @override
  void initState() {
    super.initState();
    print("start dialog--->");
    print(barcodeItem.length);
    callback = widget.callback;
    barcodeItem = widget.serialNumberModel.list;

    for (int i = 0; i < barcodeItem.length; i++)
    {
      if (barcodeItem.elementAt(i).type.toString().contains("error")) {
        isSerialError = true;
        break;
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    String strTotalCount = barcodeItem.length.toString();
    return SafeArea(
      child: Container(
        color: Colors.white,
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(5),
        child: new Stack(
          //alignment:new Alignment(x, y)
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Align(
                  alignment:  Alignment.topCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            //flex: 4,
                            flex: 8,
                            child: Text("Serial Number->Total " + strTotalCount,
                                style: TextStyle(
                                  fontSize: 16,
                                )),
                          ),
                          Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 10, bottom: 10),
                                child: Container(
                                  child: InkWell(
                                    child: Image.asset(
                                      "images/QRCode.png",
                                    ),
                                    onTap: () {
                                      if( widget.serialNumberModel.screenName
                                          ==AppStrings.LABEL_ISSUE_BY_PICKLIST)
                                      {
                                        if(widget.serialNumberModel.max!=barcodeItem.length)
                                        {
                                          serialNoScan();
                                        }
                                      }
                                      else {
                                        serialNoScan();
                                      }
                                      return;
                                    },
                                  ),
                                ),
                              )),

                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 8,
                            child: TextField(
                              controller: serialNumberController,
                              onChanged:(value){
                                if(value.length>0)
                                  serialNumberController.value =
                                      TextEditingValue(
                                          text: value.toUpperCase(),
                                          selection: serialNumberController.selection);
                              },
                              decoration: InputDecoration(
                                labelText: label,
                                labelStyle:
                                TextStyle(fontFamily: AppStrings.SEGOEUI_FONT),

                              ),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: FlatButton(
                              onPressed: ()
                              {
                                if( widget.serialNumberModel.screenName ==AppStrings.LABEL_ISSUE_BY_PICKLIST)
                                {
                                  if(widget.serialNumberModel.max==barcodeItem.length)
                                  {
                                    return;
                                  }
                                }
                                print(indexNumber);
                                print(buttonName);
                                if (label == "Edit Serial Number")
                                {
                                  print(indexNumber);
                                  print(buttonName);
                                  setState(() {
                                    if (serialNumberController.text.length > 0) {
                                      barcodeItem.removeAt(indexNumber);
                                      SerialNumberStatus status =
                                      SerialNumberStatus();
                                      status.serialNo = serialNumberController.text;
                                      barcodeItem.insert(indexNumber, status);
                                      buttonName = "ADD";
                                      indexNumber = 0;
                                      serialNumberController.text = "";
                                      label = "Add Serial Number";
                                    } else {
                                      barcodeItem.removeAt(indexNumber);
                                      buttonName = "ADD";
                                      indexNumber = 0;
                                      serialNumberController.text = "";
                                      label = "Add Serial Number";
                                    }
                                  });
                                  return;
                                } else
                                {
                                  add();
                                  return;
                                }
                              },
                              color: AppColors.CYAN_BLUE_COLOR,
                              child: Padding(
                                padding: const EdgeInsets.all(0),
                                child: Text(
                                  buttonName.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 8.0, color: Colors.white),
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(0.0))),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  child:  Expanded(
                    child:  Scrollbar(
                      child: ListView.builder(
                        padding: EdgeInsets.only(bottom:50) ,
                        primary: false,
                        scrollDirection: Axis.vertical,
                        itemCount: barcodeItem.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                              margin: const EdgeInsets.all(1.5),
                              padding: const EdgeInsets.all(3.0),
                              decoration: getBoxDecoration(barcodeItem.elementAt(index)),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      Expanded(
                                        flex:60,
                                        child:Align(
                                          alignment: Alignment.topLeft,
                                          child: InkWell(
                                            child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 2.0, top: 5, bottom: 5),
                                                child: Text(
                                                  barcodeItem[index]
                                                      .serialNo
                                                      .toString(),
                                                  style: TextStyle(fontSize: 15),
                                                )),

                                            onTap: () {
                                              setState(() {
                                                if (barcodeItem[index]
                                                    .type
                                                    .toString()
                                                    .contains("suceess")) {
                                                  return;
                                                } else {
                                                  serialNumberController.text =
                                                      barcodeItem[index].serialNo;
                                                  buttonName = "UPDATE";
                                                  indexNumber = index;
                                                  label = "Edit Serial Number";
                                                  print(indexNumber);
                                                }
                                              });
                                            },
                                          ),
                                        ) ,
                                      ),
                                      Expanded(
                                        flex :30,
                                        child:
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: InkWell(
                                            child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20.0, top: 0, bottom: 0),
                                                child: textView(
                                                    barcodeItem.elementAt(index))),
                                            onTap: () {
                                              setState(() {
                                                SerialNumberStatus status =
                                                barcodeItem[index];
                                                serialNumberController.text =
                                                    status.serialNo.toString();
                                                buttonName = "UPDATE";
                                                indexNumber = index;
                                                label = "Edit Serial Number";
                                                print(indexNumber);
                                              });
                                            },
                                          ),
                                        ),

                                      ),
                                      Visibility(
                                        visible: getDeleteIconStatus(
                                            barcodeItem.elementAt(index)),
                                        child: Expanded(
                                          flex: 10,
                                          child: Align(
                                            alignment: Alignment.topRight,
                                            child: IconButton(
                                              icon: new Icon(
                                                const    IconData(0xe872,
                                                    fontFamily: 'MaterialIcons'),
                                              ),
                                              onPressed: () {
                                                setState(() {
                                                  if (barcodeItem
                                                      .elementAt(index)
                                                      .type
                                                      .toString()
                                                      .contains("success")) {
                                                    return;
                                                  } else {
                                                    barcodeItem.removeAt(index);
                                                    buttonName = "ADD";
                                                    indexNumber = 0;
                                                    print(indexNumber);
                                                  }
                                                });
                                              },
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ));
                        },
                      ),
                    ),
                  ),
                )
              ],
            ),
            new Positioned(
              child: new Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 4.0),
                          child: MaterialButton(
                            onPressed: () {
                              for (int i = 0; i < barcodeItem.length; i++) {
                                if (barcodeItem
                                    .elementAt(i)
                                    .type
                                    .toString()
                                    .contains("error")) {
                                  isSerialError = true;
                                  break;
                                } else {
                                  isSerialError = false;
                                }
                              }
                              if (isSerialError) {
                                setState(() {
                                  Utility.showToastMsg(
                                      "Please Remove Serial number with error");
                                  return;
                                });
                              } else {
                                if (barcodeItem.length == 0) {
                                  Utility.showToastMsg("No Data For Save");
                                } else {
                                  onSave();
                                }
                              }
                            },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "SAVE".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 4.0),
                          child: MaterialButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            color: AppColors.CYAN_BLUE_COLOR,
                            minWidth: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Cancel".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.white,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void add()
  {
    String d1 ="srtest1002";
    String d2 ="100";
    if(d1 == d2){
      print("matching ");
    }
    else{
      print(" not matching ");
    }

    print("Add method called");
    if (serialNumberController.text.length > 0) {
      SerialNumberStatus status = SerialNumberStatus();
      status.serialNo = serialNumberController.text.trim();
      status.type = "";
      status.message = "";
      status.cost = "";
      status.controlId = "";
      bool isExist = false;
      print(status.serialNo);
      print(barcodeItem.length);
      for (int i = 0; i < barcodeItem.length; i++) {
        print(" data  in the  list---->" +    barcodeItem.elementAt(i).serialNo);
        if(StringUtil.isEquals(barcodeItem.elementAt(i).serialNo, status.serialNo))
        {
          print(" Matching in the  barcode -");
          isExist = true;
          break;
        }
      }
      if (!isExist) {
        print("Duplicate serial not Exist");
        barcodeItem.add(status);
        setState(() {


        });
      } else {
        Utility.showToastMsg("Duplicate bar code ");
      }
      setState(() {
        serialNumberController.text = "";
      });
    }
  }

  void onSave() {
    /* (String companyCode, String iPartNo, String serialNo,
    String actionCode,String actionSrNo,
    String stockSrNo,String quantity,String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId)
*/
    /*
  SerialNumberModel  serialNumberModel;
    serialNumberModel.companyCode =companyCode;
    serialNumberModel.iPartNo=partDetails.iPartNo;
    serialNumberModel.actionCode ="18";
    serialNumberModel.actionSrNo ="0";
    serialNumberModel.stockSrNo =reservePartLocation.stockSrNo;
    serialNumberModel.quantity=_physicalCountController.text;
    serialNumberModel.iWoNo ="0";
    serialNumberModel.iSalesOrderNo ="0";
    serialNumberModel.lineItemSrNo ="0";
    serialNumberModel.controlId =controlId;

*/

    /* _physicalCountController.text = barcodeitem.length.toString();
    checkSerialNo(companyCode, partDetails.iPartNo, strSerialNo, "18", "0", reservePartLocation.stockSrNo,
        _physicalCountController.text, "0",
        "0", "0", controlId);
   */
    showSerialNumber(barcodeItem);
    print(
        "serialNumberModel.companyCode" + widget.serialNumberModel.companyCode);
    print("serialNumberModel.iPartNo" + widget.serialNumberModel.iPartNo);
    print("serialNumberModel strSerialNo --" + strSerialNo);
    print(
        "serialNumberModel.companyCode" + widget.serialNumberModel.companyCode);
    print("controlId --" + controlId);
    print("onSave");
    widget.serialNumberModel.quantity = barcodeItem.length.toString();
    print("list.length.toString()--" + barcodeItem.length.toString());
    print(widget.serialNumberModel.quantity);
    stockSerialNumber ="";

    if(widget.serialNumberModel.screenName ==AppStrings.LABEL_ISSUE_BY_PICKLIST)
    {
      for( int i =0;i<barcodeItem.length;i++)
      {
        stockSerialNumber +=  widget.serialNumberModel.stockSrNo +",";
      }
      stockSerialNumber =stockSerialNumber.substring(0,stockSerialNumber.length-1);
    }
    else{
      stockSerialNumber =  widget.serialNumberModel.stockSrNo ;
    }
    starLoader();
    print("stockSerialNumber--"+stockSerialNumber);

    if(widget.serialNumberModel.actionCode =="7")
    {

      print("checkDuplicateSerialNo api call--");
      checkDuplicateSerialNo(widget.serialNumberModel.companyCode, widget.serialNumberModel.
      iPartNo,strSerialNo,controlId,
          widget.serialNumberModel.actionCode
      );
    }
    else{
      if(widget.serialNumberModel.screenName ==AppStrings.LABEL_ISSUE_BY_PICKLIST)
      {
           if(widget.serialNumberModel.iWoNo !="0")
        {
          checkSerialNo(
              widget.serialNumberModel.companyCode,
              widget.serialNumberModel.iPartNo,
              strSerialNo,
              widget.serialNumberModel.actionCode,
              widget.serialNumberModel.actionSrNo,
              stockSerialNumber,
              widget.serialNumberModel.quantity,
              widget.serialNumberModel.iWoNo,
              widget.serialNumberModel.iSalesOrderNo,
              widget.serialNumberModel.lineItemSrNo,
              controlId);

        }
        else{

          chkExistsSerialNoRoomLevel( widget.serialNumberModel.companyCode,  widget.serialNumberModel.actionCode, controlId,
              widget.serialNumberModel.iPartNo, strSerialNo,  widget.serialNumberModel.lineItemSrNo,   widget.serialNumberModel.iWoNo,
              widget.serialNumberModel.iSalesOrderNo, widget.serialNumberModel.iRoomCode, widget.serialNumberModel.iFacilityCode);
        }

      }
      else{
        checkSerialNo(
            widget.serialNumberModel.companyCode,
            widget.serialNumberModel.iPartNo,
            strSerialNo,
            widget.serialNumberModel.actionCode,
            widget.serialNumberModel.actionSrNo,
            stockSerialNumber,
            widget.serialNumberModel.quantity,
            widget.serialNumberModel.iWoNo,
            widget.serialNumberModel.iSalesOrderNo,
            widget.serialNumberModel.lineItemSrNo,
            controlId);
      }
      print("checkSerialNo api call--");
    }
  }

  Future<void> checkSerialNo(
      String companyCode,
      String iPartNo,
      String serialNo,
      String actionCode,
      String actionSrNo,
      String stockSrNo,
      String quantity,
      String iWoNo,
      String iSalesOrderNo,
      String lineItemSrNo,
      String controlId) async {


    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_CHECK_SERIAL_NO;

    String jsonBody = NetworkConfig.checkSerialNo(
        companyCode,
        iPartNo,
        serialNo,
        actionCode,
        actionSrNo,
        stockSrNo,
        quantity,
        iWoNo,
        iSalesOrderNo,
        lineItemSrNo,
        controlId);
    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    ).then((response)
    {
      print("dialog dismissed");
      dismissLoader();
      int statusCode = response.statusCode;
      print("Status code");
      print(statusCode);
      if (statusCode == 200)
      {
        String responseBody = response.body;
        print("----------" + responseBody);
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        print(parsedXml);
        responseBody = parsedXml.findElements("string").first.text;
        List<dynamic> data = jsonDecode(responseBody);
        bool isExist = false;
        for (int i = 0; i < data.length; i++)
        {
          Map map = data.elementAt(i);
          SerialNumberStatus status = SerialNumberStatus.fromJson(map);
          print(status.serialNo);
          print(status.controlId);
          print(status.type);
          barcodeItem.elementAt(i).type = status.type.toString();
          barcodeItem.elementAt(i).message = status.message.toString();
          barcodeItem.elementAt(i).cost = status.cost.toString();
          if (status.type.contains("error"))
          {
            isExist = true;
          }
        }
        if (!isExist)
        {
          callback.getSerialNumberStatusList(barcodeItem);
          Navigator.pop(context);
        }
        else {
          setState(() {});
        }
      }


    }).catchError( (exception) {
      dismissLoader();
    }
    ).timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)).catchError((e)
    {
      dismissLoader();
      Utility.showToastMsg("Error-->"+e.toString());
    } );
  }



  Future<void> checkDuplicateSerialNo(
      String companyCode,
      String iPartNo,
      String serialNo,
      String controlId,
      String actionCode,

      ) async {


    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_CHECK_DUPLICATE_SERIAL_NO;

    /* yVal companyCode As Integer,
        ByVal iPartNo As Integer,
    ByVal serialNumber As String,
    ByVal controlId As String,
    ByVal actionCode As Integer
   */
    String jsonBody = NetworkConfig.chkDuplicateSerialNo(
      companyCode,
      iPartNo,
      serialNo,
      controlId,
      actionCode,
    );
    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    ).then((response)
    {
      print("dialog dismissed");
      dismissLoader();
      int statusCode = response.statusCode;
      print("Status code");
      print(statusCode);
      if (statusCode == 200)
      {
        String responseBody = response.body;
        print("----------" + responseBody);
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        print(parsedXml);
        responseBody = parsedXml.findElements("string").first.text;
        List<dynamic> data = jsonDecode(responseBody);
        bool isExist = false;
        for (int i = 0; i < data.length; i++) {
          Map map = data.elementAt(i);
          SerialNumberStatus status = SerialNumberStatus.fromJson(map);
          print(status.serialNo);
          print(status.controlId);
          print(status.type);
          barcodeItem.elementAt(i).type = status.type.toString();
          barcodeItem.elementAt(i).message = status.message.toString();
          barcodeItem.elementAt(i).cost = status.cost.toString();
          if (status.type.contains("error"))
          {
            isExist = true;
          }
        }
        if (!isExist)
        {
          callback.getSerialNumberStatusList(barcodeItem);
          Navigator.pop(context);
        }
        else {
          setState(() {});
        }
      }

    }).catchError( (exception) {
      dismissLoader();
    }
    ).timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)).catchError((e)
    {
      dismissLoader();
      Utility.showToastMsg("Error-->"+e.toString());
    } );
  }


  Future<void> chkExistsSerialNoRoomLevel(String companyCode,
      String actionCode,
      String controlId,
      String iPartNo ,
      String serialNo ,
      String lineItemSrNo,
      String iWoNo,
      String iSalesOrderNo,
      String iRoomCode,
      String iFacilityCode ,
      ) async {


    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_CHECK_EXIST_SERIAL_NO_ROOM_LEVEL;

    String jsonBody = NetworkConfig.chkExistsSerialNoRoomLevel( companyCode,
       actionCode,
       controlId,
       iPartNo ,
       serialNo ,
       lineItemSrNo,
       iWoNo,
       iSalesOrderNo,
       iRoomCode,
       iFacilityCode
    );


    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.jsonHeaders,
      body: jsonBody,
      encoding: encoding,
    ).then((response)
    {
      print("dialog dismissed");
      dismissLoader();
      int statusCode = response.statusCode;
      print("Status code");
      print(statusCode);
      if (statusCode == 200)
      {
        String responseBody = response.body;
        print("----------" + responseBody);
        Map map = jsonDecode(responseBody);
        List<dynamic> data = jsonDecode( map["d"]);
        print("line1"+data.length.toString());
        bool isExist = false;
        for (int i = 0; i < data.length; i++)
        {
          print("line2");
          Map map = data.elementAt(i);
          SerialNumberStatus status = SerialNumberStatus.fromJson(map);
          print(status.serialNo);
          print(status.controlId);
          print(status.type);
          barcodeItem.elementAt(i).type = status.type.toString();
          barcodeItem.elementAt(i).message = status.message.toString();
          barcodeItem.elementAt(i).cost = status.cost.toString();
          if (status.type == "error")
          {
            isExist = true;
          }
        }
        if (!isExist)
        {
          callback.getSerialNumberStatusList(barcodeItem);
          Navigator.pop(context);
        }
        else {
          setState(() {});
        }
      }

    }).catchError( (exception) {
      dismissLoader();
    }
    ).timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)).catchError((e)
    {
      dismissLoader();
      Utility.showToastMsg("Error-->"+e.toString());
    } );
  }
  SerialNumberStatus status = SerialNumberStatus();
  Future<void> serialNoScan() async {
    print("line 1");
    try {
      status = SerialNumberStatus();
      var barcode = await BarcodeScanner.scan();
      status.serialNo = barcode.rawContent;
      status.type = "";
      status.message = "";
      status.cost = "";
      status.controlId = "";
      print(status.serialNo);
      print(barcodeItem.length);
      print("line 2");
      bool isExist = false;
      if (barcode.rawContent.length > 0)
      {
        if (barcodeItem.length > 0)
        {
          for (int i = 0; i < barcodeItem.length; i++)
          {
            print(" data Exist in the  list---->" +
                barcodeItem.elementAt(i).serialNo);
            if( StringUtil.isEquals(barcodeItem.elementAt(i).serialNo, status.serialNo))
            {
              print(" Duplicate data Exist in the  barcode");
              isExist = true;
              break;
            }
          }
        }
        if (!isExist)
        {
          print("Duplicate serial not exist Exist");
          barcodeItem.add(status);
          setState(() {
            serialNoScan();

          });
        } else
        {
          serialNoScan();
          setState(() {
            Utility.showToastMsg("Duplicate bar code ");
          });
        }
      }
      else {
        print("line 3");
        // serialNoScan();
      }
    } on PlatformException catch (e)
    {

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      }
      else
      {
        Utility.showToastMsg("Unknown error: $e");
      }
      setState(() {
      });

    } on FormatException
    {
      setState(() {
        Utility.showToastMsg(
            'user returned using the "back-button" before scanning');
      });
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  String showSerialNumber(List<SerialNumberStatus> list) {
    String data = "";
    controlId ="";
    if (list.length == 0) {
      data = "";
    } else {
      for (int i = 0; i < list.length; i++) {
        data += list.elementAt(i).serialNo   + ",";
        int val = i + 1;
        if (list.length == 1) {
          int val = i;
        } else {
          val = i + 1;
        }
        controlId += "txtSerialNo_" + val.toString() + ",";
      }
      data = data.substring(0, data.length - 1);
      controlId = controlId.substring(0, controlId.length - 1);
    }
    setState(() {
      strSerialNo = data;
    });
    print("controlId---" + controlId);
    return data;
  }

  bool getDeleteIconStatus(SerialNumberStatus data) {
    bool status = false;
    if (data.type == null || data.type.length == 0) {
      status = true;
    }
    if (data.type != null && data.type.contains("error")) {
      status = true;
    }
    if (data.type != null && data.type.contains("success")) {
      status = false;
    }
    return status;
  }

  Widget textView(SerialNumberStatus data) {
    print(data.message);
    if (data.type == null || data.type.length == 0) {
      return Text(
        "Edit",
        softWrap: true,
        textAlign: TextAlign.left,
        style: TextStyle(fontSize: 13, color: Colors.green),
      );
    } else {
      if (data.type.contains("success")) {
        return Text(
          data.type,
          softWrap: true,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 12.5, color: Colors.green),
        );
      } else {
        return Text(
          data.message,
          softWrap: true,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 12.5, color: Colors.red),
        );
      }
    }
  }

  BoxDecoration getBoxDecoration(SerialNumberStatus data) {
    if (data != null && data.type != null && data.type.contains("error"))
      return BoxDecoration(border: Border.all(color: Colors.red));
    else if (data != null && data.type != null && data.type.contains("success"))
      return BoxDecoration(border: Border.all(color: Colors.green));
    else
      return BoxDecoration(
        border: Border.all(color: Colors.blueAccent),
      );
  }

  Color getBackGroundColors(SerialNumberStatus data) {
    if (data != null && data.type != null && data.type.contains("success"))
      return Colors.grey;
    else
      return Colors.white;
  }
  bool isLoaderRunning =false;
  void starLoader(){
    isLoaderRunning =true;
    Dialogs.showLoadingDialog(context, _dialogKey);//invoking login
  }
  void dismissLoader()
  {
    print("before --$isLoaderRunning");
    if (isLoaderRunning == true) {
      isLoaderRunning = false;
      print("after --$isLoaderRunning");
      // Navigator.of(context, rootNavigator: true).pop();
      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();

    }
  }


}
