class SerialNumberStatus{

  String serialNo ="";
  String message ="";
  String controlId ="";
  String cost ="";
  String type ="";


  SerialNumberStatus(
      {this.message, this.controlId, this.serialNo, this.cost, this.type});

  SerialNumberStatus.fromJson(Map<String, dynamic> json) {
    message = json['Message'];
    controlId = json['ControlId'];
    serialNo = json['SerialNo'];
    cost = json['Cost'];
    type = json['Type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Message'] = this.message;
    data['ControlId'] = this.controlId;
    data['SerialNo'] = this.serialNo;
    data['Cost'] = this.cost;
    data['Type'] = this.type;
    return data;
  }

}