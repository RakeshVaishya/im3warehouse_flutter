import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/issue_pick_list/Issue_by_pick_list_model.dart';
import 'package:im3_warehouse/models/issue_pick_list/issue_pick_list.dart';
import 'package:im3_warehouse/network/api/issue_by_picklist_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberModel.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_callback.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_manager.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/physical_count/dialogCallback.dart';
import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:xml/xml.dart' as xml;

class IssueByPickListPage extends StatefulWidget {
  ViewTypeCallBack callBack;

  IssueByPickListPage(this.callBack);

  @override
  IssueByPickListPageState createState() => IssueByPickListPageState();
}

class IssueByPickListPageState extends State<IssueByPickListPage>
    implements DialogCallback, SerialNumberCallback {
  TextEditingController _pickTicketNoController = TextEditingController();
  TextEditingController _partNoController = TextEditingController();
  TextEditingController _confirmPartNoController = TextEditingController();
  TextEditingController _documentNoController = TextEditingController();
  TextEditingController _partDescController = TextEditingController();
  TextEditingController _pickedLocationController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _qtyToIssueController = TextEditingController();
  TextEditingController _palletNoController = TextEditingController();
  TextEditingController _serialNumberNoController = TextEditingController();

  bool serialNoBool = false;
  List<SerialNumberStatus> barcodeItem = new List();
  bool loading = true;
  var _formkey = GlobalKey<FormState>();
  bool _validate = false;
  bool isVisible = false;
  String userId = "";
  String companyCode = "";
  Facility facility;
  String barcodeValue = "";
  IssueByPickListModel issueByPickListModel;
  bool isLoaderRunning = true;
  var _dialogKey = GlobalKey();
  bool isLoading = true;
  String strSerialNo = "";
  String controlId = "";
  bool isSerializedPart = false;
  bool isConfirmPick = false;
  bool isQuantityAllowToChange = false;

  FocusNode _pickTicketFocus = new FocusNode();
  FocusNode _locationFocus = new FocusNode();
  FocusNode _qtyToIssueFocus = new FocusNode();
  FocusNode _palletNoFocus = new FocusNode();
  FocusNode _confirmPartNo = new FocusNode();
  String PICK_TYPE = "";
  BuildContext context;

  IssueByPickList issueByPick;
  List<IssueByPickList> issueByPickList = [];
  String getPickListFromServer = "getPickListFromServer";
  int pickListSelectedIndex = 0;
  String buttonNameCancelOrSkip = "Cancel";
  String totalPickTicket = "0";
  String completedPickedTicket = "0";
  String inCompletedPickedTicket = "0";
  String skipPickTicket = "0";

  @override
  void initState() {
    super.initState();
    getData();
    //checkInternetConnection("",getPickListFromServer);
    //testData();
  }

  Future<void> getData() async {
    facility = AppStrings.facility;
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    if (_pickTicketNoController.text.length == 0) {
      FocusScope.of(context).requestFocus(_pickTicketFocus);
    }
    return formWidget();
  }

  Widget formWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 8, right: 8, bottom: 5),
      child: SingleChildScrollView(
          child: Scrollbar(
              child: Form(
                  key: this._formkey,
                  autovalidate: _validate,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      /*
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 25,
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: Text("Total:"+totalPickTicket ,style: TextStyle(
                                  fontSize: 13
                              ),),
                            ),
                          ),
                          Expanded(
                            flex: 25,
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: Text("Completed:"+completedPickedTicket ,style: TextStyle(
                                  fontSize: 13
                              ),),
                            ),
                          ),
                          Expanded(
                            flex: 30,
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: Text("InCompleted:"+inCompletedPickedTicket ,style: TextStyle(
                                fontSize: 13
                              ),),
                            ),

                          ),
                          Expanded(
                            flex: 20,
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: Text("Skiped :"+skipPickTicket ,style: TextStyle(
                                  fontSize: 13
                              ),),
                            ),
                          )
                        ]
                      ),

                */
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: Padding(
                              padding:
                              const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: TextFormField(
                                  focusNode: _pickTicketFocus,
                                  style: TextStyle(color: Colors.black),
                                  controller: this._pickTicketNoController,
                                  textInputAction: TextInputAction.send,
                                  onFieldSubmitted: (value) {
                                    if (value.toString().trim().length > 0) {
                                      if (isLoading) {
                                        isLoading = false;
                                        checkInternetConnection(
                                            value,
                                            IssueByPickListApi
                                                .API_GET_PICK_TICKET_HELP);
                                      }
                                    }
                                    else{
                                      FocusScope.of(context).requestFocus(_pickTicketFocus);
                                      return;
                                    }
                                  },
                                  decoration: InputDecoration(
                                      labelText:
                                      AppStrings.LABEL_PICK_TICKET_LINE_NO,
                                      labelStyle:
                                      TextStyle(color: Colors.black87)),
                                  onChanged: (text) {
                                    if (text.length == 0) {
                                      isVisible = false;
                                      resetData();
                                    }
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please Enter Ticket No";
                                    }
                                  }),
                            ),
                          ),
                          Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: InkWell(
                                  child: Image.asset(
                                    "images/QRCode.png",
                                  ),
                                  onTap: () {
                                    barCodeScan(IssueByPickListApi
                                        .API_GET_PICK_TICKET_HELP);
                                  },
                                ),
                              ))
                        ],
                      ),
                      Visibility(
                        visible: isVisible,
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: TextFormField(
                                      style: TextStyle(color: Colors.grey[600]),
                                      enabled: false,
                                      controller: _documentNoController,
                                      decoration: InputDecoration(
                                          labelText: "Document No.",
                                          labelStyle: TextStyle(
                                              color: Colors.grey[500])),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: TextFormField(
                                    style: TextStyle(color: Colors.grey[600]),
                                    enabled: false,
                                    controller: _partNoController,
                                    decoration: InputDecoration(
                                        labelText: "Part No.",
                                        labelStyle:
                                        TextStyle(color: Colors.grey[550])),
                                  ),
                                )
                              ],
                            ),
                            Visibility(
                              visible: isConfirmPick,
                              maintainState: true,
                              child: TextFormField(
                                focusNode: _confirmPartNo,
                                style: TextStyle(color: Colors.black),
                                controller: _confirmPartNoController,
                                textInputAction: TextInputAction.next,
                                onFieldSubmitted: (value) {
                                  if (_confirmPartNoController.text !=
                                      _partNoController.text) {
                                    Utility.showToastMsg(
                                        " Confirm Password not matching !");
                                    return;
                                  } else {


                                    FocusScope.of(context)
                                        .requestFocus(_locationFocus);
                                  }
                                },
                                decoration: InputDecoration(
                                    labelText: "Confirm Part No ",
                                    labelStyle: TextStyle(color: Colors.black)),
                              ),
                            ),
                            TextFormField(
                              enabled: false,
                              style: TextStyle(color: Colors.grey[600]),
                              controller: _partDescController,
                              decoration: InputDecoration(
                                  labelText: "Part Desc.",
                                  labelStyle:
                                  TextStyle(color: Colors.grey[550])),
                            ),
                            TextFormField(
                              enabled: false,
                              style: TextStyle(color: Colors.grey[600]),
                              controller: _pickedLocationController,
                              decoration: InputDecoration(
                                  labelText: "Pick Location.",
                                  labelStyle:
                                  TextStyle(color: Colors.grey[550])),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 9,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        bottom: 0.0, right: 5),
                                    child: TextFormField(
                                        focusNode: _locationFocus,
                                        style: TextStyle(color: Colors.black),
                                        controller: this._locationController,
                                        textInputAction: TextInputAction.next,
                                        onFieldSubmitted: (value)
                                        {
                                          if (_locationController.text
                                              .toLowerCase() !=
                                              issueByPickListModel.roomAreaCode
                                                  .toLowerCase()) {
                                            _locationController.text = "";
                                            FocusScope.of(context)
                                                .requestFocus(_locationFocus);
                                            Utility.showToastMsg(
                                                "Entered Location does not match with Picked Location");
                                            return;
                                          }
                                          else {
                                            if (PICK_TYPE == "WO")
                                            {
                                              if (isSerializedPart) {
                                                showSerialNumberList();
                                              }
                                              else {
                                                setFocus(_palletNoFocus);
                                              }
                                            }
                                            else  if (PICK_TYPE == "SO")
                                            {
                                              if (isSerializedPart)
                                              {
                                                setFocusByType(
                                                    _qtyToIssueFocus);
                                              }
                                              else {
                                                setFocusByType(
                                                    _qtyToIssueFocus);
                                              }
                                            }
                                            else  if (PICK_TYPE == "TR")
                                            {
                                              if (isSerializedPart)
                                              {
                                                showSerialNumberList();
                                              }
                                              else {
                                                setFocusByType(
                                                    _palletNoFocus);
                                              }
                                            }
                                          }
                                        },
                                        decoration: InputDecoration(
                                            labelText: "Location",
                                            labelStyle: TextStyle(
                                                color: Colors.black87)),
                                        onChanged: (text) {},
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return "Please Enter Location No";
                                          }
                                        }),
                                  ),
                                ),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 20),
                                      child: InkWell(
                                        child: Image.asset(
                                          "images/QRCode.png",
                                        ),
                                        onTap: () {
                                          barCodeScan("Location");
                                        },
                                      ),
                                    ))
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: TextFormField(
                                focusNode: _qtyToIssueFocus,
                                keyboardType: TextInputType.number,
                                style: TextStyle(color: Colors.grey[600]),
                                readOnly: !isQuantityAllowToChange,
                                controller: _qtyToIssueController,
                                onFieldSubmitted: (value)
                                {
                                  if(value.length==0)
                                  {
                                    FocusScope.of(context).requestFocus(_qtyToIssueFocus);
                                    Utility.showToastMsg(
                                        "Quantity to Issue cannot be zero");
                                    return;
                                  }
                                  print("_qtyToIssueController"+_qtyToIssueController.text);
                                  int quantityLeftToIssue = 0;
                                  if (StringUtil.isInt(_qtyToIssueController.text))
                                  {
                                    quantityLeftToIssue =  StringUtil.tryParse(_qtyToIssueController.text);
                                    print("$quantityLeftToIssue= $quantityLeftToIssue");
                                    if (quantityLeftToIssue <=0)
                                    {
                                      FocusScope.of(context).requestFocus(_qtyToIssueFocus);
                                      Utility.showToastMsg(
                                          "Quantity to Issue cannot be zero");
                                      return;
                                    }
                                    else{
                                      if (isSerializedPart)
                                      {
                                        showSerialNumberList();
                                      }
                                      else{
                                        FocusScope.of(context).requestFocus(_palletNoFocus);
                                      }
                                    }
                                  }
                                  if (StringUtil.isDouble(_qtyToIssueController.text))
                                  {
                                    double data =
                                    StringUtil.tryParse(_qtyToIssueController.text);
                                    print("_qtyToIssueController data $data");
                                    quantityLeftToIssue = data.toInt();
                                    if (quantityLeftToIssue <=0)
                                    {
                                      FocusScope.of(context).requestFocus(_qtyToIssueFocus);
                                      Utility.showToastMsg(
                                          "Quantity to Issue cannot be zero");
                                      return;
                                    }
                                    else{
                                      if (isSerializedPart)
                                      {
                                        showSerialNumberList();
                                      }
                                      else{
                                        FocusScope.of(context).requestFocus(_palletNoFocus);
                                      }
                                    }
                                  }
                                  else{
                                    print("_qtyToIssueController- is not double---"+_qtyToIssueController.text);

                                  }
                                  print("_qtyToIssueController----"+_qtyToIssueController.text);

                                },
                                decoration: InputDecoration(
                                    labelText: "Quantity To Issue.",
                                    labelStyle:
                                    TextStyle(color: Colors.grey[550])),
                              ),
                            ),
                            TextFormField(
                              style: TextStyle(color: Colors.grey[600]),
                              controller: _palletNoController,
                              focusNode:  _palletNoFocus,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (value) {

                              },
                              decoration: InputDecoration(
                                  labelText: "Pallet No.",
                                  labelStyle:
                                  TextStyle(color: Colors.grey[550])),
                            ),
                            Visibility(
                              visible: isSerializedPart,
                              maintainState: true,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 8.0, top: 0),
                                      child: Material(
                                        child: InkWell(
                                          onTap: () {
                                            print("data---");
                                            showSerialNumberList();
                                          },
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 0.0, top: 0),
                                                child: Text(
                                                  AppStrings.LABEL_SERIAL_NO,
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontFamily: AppStrings
                                                          .SEGOEUI_FONT,
                                                      fontSize: serialNoBool
                                                          ? AppStrings
                                                          .FONT_SIZE_13
                                                          : AppStrings
                                                          .FONT_SIZE_16),
                                                ),
                                              ),
                                              InkWell(
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets.only(
                                                      top: 8.0),
                                                  child: InkWell(
                                                    child: Text(strSerialNo,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontFamily: AppStrings
                                                                .SEGOEUI_FONT,
                                                            color: Colors
                                                                .grey[600],
                                                            fontSize: AppStrings
                                                                .FONT_SIZE_15)),
                                                    onTap: () {
                                                      print(
                                                          "tapped on serial no");
                                                      showSerialNumberList();
                                                    },
                                                  ),
                                                ),
                                                onTap: () {
                                                  print("tapped on serial no");
                                                  showSerialNumberList();
                                                },
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 5),
                                                child: Divider(
                                                  thickness: 1,
                                                  color: Colors.grey,
                                                  height: 10,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 25),
                                        child: InkWell(
                                          child: Image.asset(
                                            "images/QRCode.png",
                                          ),
                                          onTap: () {
                                            // serialNoBarcodeValue = "";
                                            //serialNoScan();
                                          },
                                        ),
                                      ))
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(right: 4.0),
                                      child: MaterialButton(
                                        onPressed: () {
                                          onSave();
                                        },
                                        color: AppColors.CYAN_BLUE_COLOR,
                                        minWidth: double.infinity,
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(
                                            "Issue".toUpperCase(),
                                            style: TextStyle(
                                                fontSize: 17.0,
                                                color: Colors.white,
                                                fontFamily:
                                                AppStrings.SEGOEUI_FONT),
                                          ),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(0.0))),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 4.0),
                                      child: MaterialButton(
                                        onPressed: () {
                                          if (buttonNameCancelOrSkip ==
                                              "SKIP") {
                                            setNextPickListData("click");
                                          } else {
                                            this.widget.callBack.viewType(
                                                DashboardView(
                                                    this.widget.callBack),
                                                AppStrings.LABEL_HOME);
                                          }
                                        },
                                        color: AppColors.CYAN_BLUE_COLOR,
                                        minWidth: double.infinity,
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Text(
                                            buttonNameCancelOrSkip
                                                .toUpperCase(),
                                            style: TextStyle(
                                                fontSize: 17.0,
                                                color: Colors.white,
                                                fontFamily:
                                                AppStrings.SEGOEUI_FONT),
                                          ),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(0.0))),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )))),
    );
  }

  Widget showCircularProgressbarWidget() {
    return Container(
        child: Center(
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                strokeWidth: 5,
                backgroundColor: Colors.red,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: new Text(
                  "Loading",
                  style:
                  TextStyle(fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
                ),
              ),
            ],
          ),
        ));
  }

  onSave() {
    getDataFromField();
  }

  @override
  void getList(List<String> data) {
    // TODO: implement getList
  }

  void getDataFromField() {
    print(_locationController.text);
    print(issueByPickListModel.pickLocation);

    if (isConfirmPick) {
      if (_confirmPartNoController.text != _partNoController.text) {
        FocusScope.of(context).requestFocus(_confirmPartNo);
        Utility.showToastMsg(" Confirm Password not matching !");
        return;
      }
    }
    if (_locationController.text.toLowerCase() !=
        issueByPickListModel.roomAreaCode.toLowerCase()) {
      setFocus(_locationFocus);
      Utility.showToastMsg(
          "Entered Location does not match with Picked Location");
      return;
    }

    int maxLimit = 0;
    if (StringUtil.isDouble(issueByPickListModel.quantityLeftToIssue)) {
      double data =
      StringUtil.tryParse(issueByPickListModel.quantityLeftToIssue);
      maxLimit = data.toInt();
    }

    print(maxLimit);

    int quantityLeftToIssue = 0;
    if (StringUtil.isDouble(issueByPickListModel.quantityLeftToIssue)) {
      double data =
      StringUtil.tryParse(issueByPickListModel.quantityLeftToIssue);
      quantityLeftToIssue = data.toInt();
    }

    if (StringUtil.isInt(_qtyToIssueController.text))
    {
      quantityLeftToIssue =  StringUtil.tryParse(_qtyToIssueController.text);
    }

    print(quantityLeftToIssue);


    if (quantityLeftToIssue <=0)

    {
      FocusScope.of(context).requestFocus(_qtyToIssueFocus);
      Utility.showToastMsg(
          "Quantity to Issue cannot be zero");
      return;
    }

    if (quantityLeftToIssue > maxLimit)
    {
      FocusScope.of(context).requestFocus(_qtyToIssueFocus);
      Utility.showToastMsg(
          "Quantity to Issue cannot be greater than picked quantity");
      return;
    }

    issueByPickListModel.sOPalletNo = _palletNoController.text;
    print("pick ticket number --" + _pickTicketNoController.text);
    print("_documentNoController No --" + _documentNoController.text);
    print("_documentNoController No --" + _documentNoController.text);
    print("_partNoController No --" + _partNoController.text);
    print("_partDescController No --" + _partDescController.text);
    print("_pickedLocationController No --" + _pickedLocationController.text);
    print("_locationController No --" + _locationController.text);
    print("_qtyToIssueController No --" + _qtyToIssueController.text);
    print("_palletNoController No --" + _palletNoController.text);

    if (isLoading) {
      isLoading = false;
      checkInternetConnection(
          "", IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET);
    }
  }

  void onCancel() {
    setState(() {
      isVisible = false;
      getClearFromField();
    });
  }

  void getClearFromField() {
    _partNoController.text = "";
    _documentNoController.text = "";
    _partDescController.text = "";
    _pickTicketNoController.text = "";
    _pickedLocationController.text = "";
    _locationController.text = "";
    _qtyToIssueController.text = "";
    _palletNoController.text = "";
    print("part No --" + _partNoController.text);
    print("part desc  --" + _partDescController.text);
  }

  void testData() {
    _partNoController.text = "5010159A";
    _documentNoController.text = "SO09700";
    _partDescController.text = "GASKET SET AT 2000";
    _pickedLocationController.text = "EDM-A-J2G";
    _locationController.text = "DM-A-J2G";
    _qtyToIssueController.text = "1.00";
    _palletNoController.text = "";
    print("part No --" + _partNoController.text);
    print("part desc  --" + _partDescController.text);
  }

  void setData() {
    isVisible = true;
    _partNoController.text = issueByPickListModel.PartNo;
    _documentNoController.text = issueByPickListModel.documentNo;
    _partDescController.text = issueByPickListModel.partDescription;
    _pickedLocationController.text = issueByPickListModel.pickLocation;
    _qtyToIssueController.text = issueByPickListModel.quantityLeftToIssue;
    _pickTicketNoController.text = issueByPickListModel.pickTicketNo;
    //   _palletNoController.text = issueByPickListModel.sOPalletNo;
    print("part No --" + _partNoController.text);
    print("part desc  --" + _partDescController.text);
  }

  Future barCodeScan(String type) async {
    try {
      var barcode = await BarcodeScanner.scan();

      if(barcode.rawContent.length==0)
      {
        return;
      }
      if (type == IssueByPickListApi.API_GET_PICK_TICKET_HELP)
      {
        _pickTicketNoController.text = barcode.rawContent;

        if(barcode.rawContent.length==0)
        {
          return;
        }
        print(_pickTicketNoController.text);
        setState(() {
          checkInternetConnection(_pickTicketNoController.text, type);
        });
      } else if (type == "Location")
      {


        if (barcode.rawContent.toLowerCase() ==
            issueByPickListModel.roomAreaCode.toLowerCase()) {
          _locationController.text = barcode.rawContent;
          FocusScope.of(context).requestFocus(_qtyToIssueFocus);
          setState(() {

          });
        }
        else {
          _locationController.text = "";
          setFocus(_locationFocus);
          setState(() {

          });
          Utility.showToastMsg(
              "Entered Location does not match with Picked Location");
          return;
        }
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    }
  }

  Future getDetails(String data, String type) async {
    print("part no -->" + data);
    print("user --Id  -->" + userId);
    print("companyCode --  -->" + companyCode);
    String jsonBody = "";
    String uri = "";
    print(NetworkConfig.BASE_URL);
    print("type --  -->" + type);
    if (type == getPickListFromServer) {
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByPickListApi.API_GET_PICK_TICKET_HELP;
      jsonBody = IssueByPickListApi.getPickTickethelp(
          companyCode, "12345678", facility.iFacilityCode);
    }

    if (type == IssueByPickListApi.API_GET_PICK_TICKET_HELP.toString()) {
      print("--- Pickte ticket called  --");
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByPickListApi.API_GET_PICK_TICKET_HELP;
      jsonBody = IssueByPickListApi.getPickTickethelp(
          companyCode, data, facility.iFacilityCode);
    }

    if (type == IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET) {
      print("---save  pick list --");
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET;

      /*static String saveIssueByPickTicket(IssueByPickListModel issueByPickListModel,
      String userId ,String empld,String iWorkOrderTask,String txtQuantity )
      */

      if (issueByPickListModel.partSrNo == null) {
        issueByPickListModel.partSrNo = "0";
      }

      if (issueByPickListModel.partSrNo != null &&
          issueByPickListModel.partSrNo.length == 0) {
        issueByPickListModel.partSrNo = "0";
      }

      if (issueByPickListModel.iWONo == null) {
        issueByPickListModel.iWONo = "0";
      }

      _palletNoController.text = issueByPickListModel.sOPalletNo;

      jsonBody = IssueByPickListApi.saveIssueByPickTicket(issueByPickListModel,
          userId, userId, "0", _qtyToIssueController.text);
      print("URI---" + uri);
      print("jsonBody---" + jsonBody);
    }
    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response = await post(uri,
        headers: NetworkConfig.headers, body: jsonBody, encoding: encoding)
        .then((resp) {
      print(resp.statusCode);
      isLoading = true;
      print(resp.statusCode);
      if (resp.statusCode == 200) {
        dismissLoader();
        String responseBody = resp.body;
        print("----------" + resp.body);
        onSuccessResponse(type, responseBody);
      }
      else {
        print(" responce not 200 ");
        isLoading = true;
        dismissLoader();
        if (type == getPickListFromServer) {
          //  pickListData();
        }
        else
        {
          if(type ==IssueByPickListApi.API_GET_PICK_TICKET_HELP)
          {
            _pickTicketNoController.text ="";
            Utility.showToastMsg("Pick Ticket No does not exist ");
            return;
          }
          return;
        }
        //  onErrorResponse(type ,  resp.body);
      }
    })
        .catchError((error) {
      print(" catchError " + error.toString());
      //Utility.showToastMsg("Error-Responce from server Exception");
      isLoading = true;
      dismissLoader();
      print(error.toString());
      //  onErrorResponse(type ,  error.toString());
    })
        .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
        .catchError((e) {
      print(" Time out ");

      isLoading = true;
      dismissLoader();
      Utility.showToastMsg("Error-->" + e.toString());
    });
  }

  void onSuccessResponse(String type, String responseBody) {
    if (type == getPickListFromServer) {
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      if (responseBody != "0") {
        Utility.showToastMsg(
            "Pick List not exist,Please Scan or Enter Pick Ticket ?");
        return;
      } else {
        // pickListData();
      }
    } else if (type == IssueByPickListApi.API_GET_PICK_TICKET_HELP) {
      setState(() {
        print("----------" + responseBody);
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        print(parsedXml);
        responseBody = parsedXml.findElements("string").first.text;
        if (responseBody != "0") {
          Map data1 = jsonDecode(responseBody);
          Map<String, dynamic> data2 = data1['NewDataSet'];
          issueByPickListModel = IssueByPickListModel.fromJson(data2['Table']);
          if (issueByPickListModel.canBeIssued.toLowerCase() ==
              "FALSE".toLowerCase()) {
            if (issueByPickListModel.iWONo != '0') {
              Utility.showToastMsg("WO Part not in Schedule Status");
            } else {
              Utility.showToastMsg("SO line Item not in Open Status");
            }
          } else {
            if (issueByPickListModel.iSalesOrderNo != null) {
              if (issueByPickListModel.iSalesOrderNo != "0") {
                print("Sale Order");
                PICK_TYPE = "SO";
                isQuantityAllowToChange = true;
              }
            }
            if (issueByPickListModel.iWONo != null) {
              if (issueByPickListModel.iWONo != "0") {
                PICK_TYPE = "WO";
                isQuantityAllowToChange = false;
                print("Work Order");
              }
            }
            if(issueByPickListModel.iTransferRequestID != null)
            {
              if (issueByPickListModel.iTransferRequestID != "0")
              {
                PICK_TYPE = "TR";
                isQuantityAllowToChange = false;
                print("Transfer Order");
              }
            }
            if (issueByPickListModel.isConfirmPick.toLowerCase() ==
                "TRUE".toLowerCase()) {
              isConfirmPick = true;
            } else {
              isConfirmPick = false;
            }
            if (issueByPickListModel.isSerializedPart.toLowerCase() ==
                "TRUE".toLowerCase()) {
              isSerializedPart = true;
            } else {
              isSerializedPart = false;
            }
            if (isConfirmPick) {
              FocusScope.of(context).requestFocus(_confirmPartNo);
            } else {
              setFocus(_locationFocus);
            }
            setData();
            setState(() {});
          }
        } else {
          dismissLoader();
          resetData();
          //setNextPickListData("fail");
          _pickTicketNoController.text="";
          Utility.showToastMsg("Pick Ticket No does not exist ");
          return;
        }
      });
    }
    if (type == IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET) {
      setState(()
      {
        isLoading = true;
        print("----------" + responseBody);
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        print(parsedXml);
        responseBody = parsedXml.findElements("string").first.text;
        if (responseBody.toLowerCase() == "Issued successfully".toLowerCase())
        {
          Utility.showSuccessToastMsg("Issued successfully");
          setState(() {
            _pickTicketNoController.text ="";
            resetData();
          });
          return;
        }
        else {
          isLoading = true;
          dismissLoader();
          Utility.showToastMsg(responseBody);
          return;
        }
      });
    }
  }
  void onErrorResponse(String type) {
    if (type == IssueByPickListApi.API_GET_PICK_TICKET_HELP) {
      setState(() {});
    }
    if (type == IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET) {
      setState(() {});
    }
  }

  void getSerialNumberStatusList(List<SerialNumberStatus> list) {
    showSerialNumber(list);
    _qtyToIssueController.text =list.length.toString();
    FocusScope.of(context).requestFocus(_palletNoFocus);
    setState(()
    {

    });
  }
  void starLoader()
  {
    print("dialog started");
    isLoaderRunning = true;
    Dialogs.showLoadingDialog(context, _dialogKey);
  }
  void dismissLoader() {
    print("before --$isLoaderRunning");
    if (isLoaderRunning == true) {
      isLoaderRunning = false;
      print("after --$isLoaderRunning");

      //Navigator.of(, rootNavigator: true).pop(true);
      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }

  void checkInternetConnection(String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('check connection-------------->connected');
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
                () {
              isLoading = true;
              dismissLoader();
            });
        if (type == getPickListFromServer) {
          starLoader();
          isLoading = false;
          getData().then((v) {
            getDetails("", getPickListFromServer);
          });
        }
        if (type == IssueByPickListApi.API_GET_PICK_TICKET_HELP) {
          starLoader();
          getData().then((v) {
            getDetails(value, IssueByPickListApi.API_GET_PICK_TICKET_HELP);
          });
        }
        if (type == IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET) {
          print(issueByPickListModel.partSrNo);
          starLoader();
          getData().then((v) {
            getDetails(value, IssueByPickListApi.API_SAVE_ISSUE_BY_PICK_TICKET);
          });
        }
      }
    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
    SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  void showSerialNumberList() {
    SerialNumberModel serialNumberModel = new SerialNumberModel();
    serialNumberModel.companyCode = companyCode;
    serialNumberModel.iPartNo = issueByPickListModel.iPartNo;
    serialNumberModel.actionCode = "1";
    serialNumberModel.actionSrNo = "0";
    serialNumberModel.stockSrNo = issueByPickListModel.stockSrNo;
    serialNumberModel.quantity = _qtyToIssueController.text;

    if(issueByPickListModel.iWONo==null){
      serialNumberModel.iWoNo ="0";
    }
    else{
      serialNumberModel.iWoNo =issueByPickListModel.iWONo;
    }
    if (issueByPickListModel.iSalesOrderNo == null) {
      issueByPickListModel.iSalesOrderNo = "0";
    }
    serialNumberModel.iSalesOrderNo = issueByPickListModel.iSalesOrderNo;
    if(issueByPickListModel.iSalesOrderLineItem ==null)
    {
      serialNumberModel.lineItemSrNo = "0";
    }else{
      serialNumberModel.lineItemSrNo = issueByPickListModel.iSalesOrderLineItem ;
    }

    if(serialNumberModel.iWoNo !="0" )
    {
      serialNumberModel.lineItemSrNo = issueByPickListModel.partSrNo ;

    }

    serialNumberModel.iFacilityCode =issueByPickListModel.iFacilityCode;
    serialNumberModel.iRoomCode =issueByPickListModel.iRoomCode;
    serialNumberModel.controlId = controlId;
    serialNumberModel.list = barcodeItem;
    serialNumberModel.screenName = AppStrings.LABEL_ISSUE_BY_PICKLIST;

    int max = 0;
    if (StringUtil.isDouble(issueByPickListModel.quantityLeftToIssue))
    {
      double data =
      StringUtil.tryParse(issueByPickListModel.quantityLeftToIssue);
      max = data.toInt();
    }
    print(max);
    serialNumberModel.max = max;
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => SerialNumberManager(
                callback: this, serialNumberModel: serialNumberModel)));
  }

  void resetData() {
    _partNoController.text = "";
    _documentNoController.text = "";
    _partDescController.text = "";
    _pickedLocationController.text = "";
    _locationController.text = "";
    _qtyToIssueController.text = "";
    _palletNoController.text = "";
    _serialNumberNoController.text = "";
    serialNoBool = false;
    barcodeItem = new List();
    loading = true;
    _validate = false;
    isVisible = false;
    userId = "";
    companyCode = "";
    barcodeValue = "";
    issueByPickListModel = null;
    isLoaderRunning = true;
    isLoading = true;
    strSerialNo = "";
    controlId;
    isSerializedPart = false;
    isConfirmPick = false;
    isQuantityAllowToChange = false;
    setState(() {});
  }

  String showSerialNumber(List<SerialNumberStatus> list) {
    String data = "";
    print(list.length);
    controlId = "";
    if (list.length == 0) {
      data = "";
    } else {
      for (int i = 0; i < list.length; i++) {
        data += list.elementAt(i).serialNo + ",";
        int val = i + 1;
        if (list.length == 1) {
          int val = i;
        } else {
          val = i + 1;
        }
        controlId += "txtSerialNo_" + val.toString() + ",";
      }
      data = data.substring(0, data.length - 1);
      controlId = controlId.substring(0, controlId.length - 1);
    }
    setState(() {
      serialNoBool = true;
      strSerialNo = data;
    });
    print("controlId---" + controlId);
    issueByPickListModel.serialNo = strSerialNo;
    return data;
  }

/*
  void pickListData() {
    issueByPick = new IssueByPickList();
    issueByPick.pickListNumber = "10697";
    issueByPickList.add(issueByPick);

    issueByPick = new IssueByPickList();
    issueByPick.pickListNumber = "10696";
    issueByPickList.add(issueByPick);

    issueByPick = new IssueByPickList();
    issueByPick.pickListNumber = "10698";
    issueByPickList.add(issueByPick);

    issueByPick = new IssueByPickList();
    issueByPick.pickListNumber = "10695";
    issueByPickList.add(issueByPick);

    issueByPick = new IssueByPickList();
    issueByPick.pickListNumber = "10694";
    issueByPickList.add(issueByPick);

    issueByPick = new IssueByPickList();
    issueByPick.pickListNumber = "10693";
    issueByPickList.add(issueByPick);
    var list = issueByPickList;
    for (int i = 0; i < list.length / 2; i++) {
      var temp = list[i];
      list[i] = list[list.length - 1 - i];
      list[list.length - 1 - i] = temp;
    }
    issueByPickList = list;
    for (int i = 0; i < issueByPickList.length / 2; i++) {
      print(issueByPickList.elementAt(i).pickListNumber);
    }
    if (issueByPickList.length > 0) {
      setState(() {
        int totalList = issueByPickList.length;
        totalPickTicket = "$totalList";
        buttonNameCancelOrSkip = "SKIP";
        _pickTicketNoController.text =
            issueByPickList.elementAt(pickListSelectedIndex).pickListNumber;
        checkInternetConnection(_pickTicketNoController.text,
            IssueByPickListApi.API_GET_PICK_TICKET_HELP);
      });
    } else {
      buttonNameCancelOrSkip = "Cancel";
    }
  }
*/

  void setNextPickListData(String mode)
  {
    print("setNextPickListData");
    if (buttonNameCancelOrSkip.toUpperCase() == "SKIP".toUpperCase()) {
      print(" index number before  $pickListSelectedIndex");
      if (mode == "click") {
        issueByPickList.elementAt(pickListSelectedIndex).isSkip = true;
      } else if (mode == "success") {
        issueByPickList.elementAt(pickListSelectedIndex).isCompleted = true;
      } else {
        issueByPickList
            .elementAt(pickListSelectedIndex)
            .isNoResponseFromServer = true;
      }
      pickListSelectedIndex++;
      print(" index number after  $pickListSelectedIndex");
      issueByPick = issueByPickList.elementAt(pickListSelectedIndex);
      if (pickListSelectedIndex == issueByPickList.length - 1) {
        buttonNameCancelOrSkip = "CANCEL";
      }
      int totalSkiped = 0;
      int isNoResponseFromServer = 0;
      for (int i = 0; i < issueByPickList.length; i++) {
        print(issueByPickList.elementAt(i).isSkip);
        if (issueByPickList.elementAt(i).isSkip) {
          totalSkiped++;
        }
        if (issueByPickList.elementAt(i).isNoResponseFromServer) {
          isNoResponseFromServer++;
        }
      }
      print(" total skiped   $totalSkiped");
      print(" total isNoResponseFromServer   $isNoResponseFromServer");

      if (!issueByPick.isCompleted && !issueByPick.isSkip) {
        _pickTicketNoController.text = issueByPick.pickListNumber;
        setState(() {
          skipPickTicket = "$totalSkiped";
          inCompletedPickedTicket = "$isNoResponseFromServer";
          if (isLoading) {
            isLoading = false;
            checkInternetConnection(_pickTicketNoController.text,
                IssueByPickListApi.API_GET_PICK_TICKET_HELP);
          }
        });
      } else {}
    }
  }


  setFocusByType(FocusNode focus)
  {
    if(PICK_TYPE =="WO"){

      setFocus(focus);

    }

    if(PICK_TYPE =="SO"){
      setFocus(focus);
    }

    if(PICK_TYPE =="TR"){
      setFocus(focus);
    }
  }

  Future setFocus(FocusNode focus) async
  {
    Future.delayed(Duration(microseconds: 50), ()
    {
      setState(()
      {
        FocusScope.of(context).requestFocus(focus);
      });
    });
  }
  void setButtonName() {}

}
