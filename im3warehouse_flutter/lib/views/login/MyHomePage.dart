import 'dart:io';

import 'package:flutter/material.dart';
import 'package:im3_warehouse/utils/file_util.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';



class CameraTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Login',
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File _image;
  String path ;
  FileUtil fileUtil =FileUtil();


  Future getImage() async {
    fileUtil.getRootPath();
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    //fileUtil.createFile(fileUtil.rootPath+"/FlutterDemo1/background.jpg");

    setState(() {
      _image = image;
      print("image--.path--->"+image.path);
      _image.copy(fileUtil.rootPath+"/FlutterDemo1/background.jpg");
    });
  }

  Future getImageByGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Example'),
      ),
      body: Center(
        child: _image == null
            ? Text('No image selected.')
            : Image.file(_image),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}