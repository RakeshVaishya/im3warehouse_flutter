import 'dart:async';
import 'dart:convert';
import 'package:im3_warehouse/databases/database_constant.dart';
import 'package:im3_warehouse/databases/database_helper.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/app_table/user_data.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:io';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart';
import 'login_animation.dart';
import 'package:flutter/widgets.dart';
import 'package:xml/xml.dart' as xml;

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Login',
      home: new LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  var db;
  static DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  String deviceName;
  List<dynamic> facilityTableData =[];
  List<Facility> facilityListTable;
  List<Facility> faciltiyList;

  Future<String> initPlatformState() async {
    try {
      if (Platform.isAndroid) {
        deviceName = getDeviceName(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        // deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
      print(deviceName);
    } on Exception {}
    if (!mounted) return "";
    setState(() {
      // _deviceData = deviceData;
      deviceName = deviceName;
      print(deviceName);
    });
  }

  String getDeviceName(AndroidDeviceInfo build) {
    return build.device;
  }

  var statusClick = 0;
  String msg = '';
  String hintValue = "Email";
  String passhintValue = "Password";
  String userName = "";
  String password = "";

  FocusNode usernameFocus = new FocusNode();
  FocusNode passwordFocus = new FocusNode();
  bool fontstyleEmail = true;
  bool fontStylePassword = true;
  bool passwordVisible;
  bool validateText = false;

  Color txtlblColor = Color(0xffAAAAAA);
  Color txtColor = Color(0xff535353);
  Color txtlblfocusColor = Color(0xff3E3E3E);
  var colorVar;

  TextEditingController editingControllerUser;
  TextEditingController editingControllerPass;

  AnimationController animationControllerButton;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  double screenHeight(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).height / dividedBy;
  }

  double screenWidth(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).width / dividedBy;
  }
  /*local system*/
  //String url = "https://ah.im3cloud.com/LoginAuth.aspx?uid=tuaXx1PTnUA=&pwd=cObH0Gh7caq9OhgCN0C3fA==";

  bool visibilityTag = false;
  //String url="https://sandboxah.im3cloud.com/LoginAuth.aspx?uid=tuaXx1PTnUA=&pwd=cObH0Gh7caqCWJLempyqPg==";

  String url = "";
  String response = "";
  final Completer<WebViewController> _controller =
  Completer<WebViewController>();
  static const platform = const MethodChannel('flutter.native/helper');
  Future<void> responseFromNativeCode() async {
    try {
      print("url for session key -->" + url);
      final String result =
      await platform.invokeMethod('getCookieFromUrl', {"url": url});
      response = result;
      print("responce from native channel--->" + response);
      AppStrings.SESSION_ID = response;
      print("session id --" + AppStrings.SESSION_ID);
      setState(() {
        visibilityTag = false;
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
              (Route<dynamic> route) => false,
        );

      });
      print("session Id--->" + AppStrings.SESSION_ID);
    } on PlatformException catch (e) {
      response = "Failed to Invoke: '${e.message}'.";
    }
  }
  Future<void> getLoginRequest() async {
    print(NetworkConfig.BASE_URL);
    final uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.Login;
    final headers = {'Content-Type': 'application/x-www-form-urlencoded'};
    /* sand box*/
    String jsonBody = "UID=" + userName + "&PWD=" + password;
    print(jsonBody);
    var db = new DatabaseHelper();
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);

    // String respBody = TestData.getLogindata();
    // List<String> resData = respBody.split("|");
    // setDataInTable(resData);

    Response response;

    response = await   post(
      uri,
      headers: headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    print("Status message--->");
    print(response.body);
    String responseBody ="";
    if (statusCode == 200)
    {
       responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;

      print(responseBody);

      if( responseBody=="0")
      {
        Utility.showToastMsg( "Enter valid username or password");
        return;
      }
      if(responseBody!="-1")
      {
        List<String> dat = responseBody.split("|");
        NetworkConfig.loginData = dat;
        print("data  at 4  ---" + NetworkConfig.loginData.elementAt(4));
        setDataInTable(dat);
        print("url set for the data --" + url);
        this.setState(() {
          visibilityTag = true;
          print("url set for the data --$visibilityTag");
        });
        getFaciltyData();
      }
      else{
        Utility.showToastMsg( "Enter valid username or password");
        return;
      }
      }
    else
      {
        Utility.showToastMsg("Server Responce --> "+response.body);
      }
  }

  Future<bool> getData() async {
    bool companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .isDataExistToSF(JsonUtil.KEY_Company_Code);
    return companyCode;
  }

  Future<void> getFaciltyData() async {
    final uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.GET_FACILITY_LIST_FOR_USER;
    final headers = {'Content-Type': 'application/x-www-form-urlencoded'};
    String strcompnayCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);

    String userId =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);

    String jsonBody = "pintCompanyCode=" + strcompnayCode + "&UserId=" + userId;
    print(jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);

    //<--------------------------------------------------------------->
    // String responseBody = TestData.getFacilityData();
    // print("Facility response---------->" + responseBody);

    // xml.XmlDocument parsedXml = xml.parse(responseBody);
    // print(parsedXml);
    // responseBody = parsedXml.findElements("string").first.text;

    // Map data1 = jsonDecode(responseBody);

    // Map data2 = data1['NewDataSet'];

    // Facility fac;
    // facilityTableData = data2['Table'];

    // if (data2['Table'] != null) {
    //   faciltiyList = new List<Facility>();
    //   data2['Table'].forEach((v) {
    //     faciltiyList.add(new Facility.fromJson(v));
    //     // fac = Facility.fromJson(v);
    //     // print("Data inserting into the table----->");
    //     // db.createFacilty(DataBaseConstant.TABLE_FACILITY, fac);
    //   });
    // }

    // db.createFacilty(DataBaseConstant.TABLE_FACILITY, fac);
    // db.insertData(DataBaseConstant.TABLE_FACILITY, faciltiyList);

    // Future<List<dynamic>> data = db.getFacilityData();
    // if (data != null) {
    //   print("data exist in the Facility table");
    // } else {
    //   print("data--------->inserting into the facility table");
    //   db.insertFacilityData(DataBaseConstant.TABLE_FACILITY, facilityTableData);
    // }

// <-------------------------------------------------------------------->

    Response response = await post(
      uri,
      headers: headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    if (statusCode == 200)
    {
      String responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      Map data1 = jsonDecode(responseBody);
      Map data2 = data1['NewDataSet'];
      Map data3;
      try {
        facilityTableData = data2['Table'];
        if (data2['Table'] != null)
        {
          faciltiyList = new List<Facility>();
          data2['Table'].forEach((v) {
            faciltiyList.add(new Facility.fromJson(v));
          });
        }

      }
      catch(e)
      {
        data3 =  data2['Table'];
        print(data3);
        facilityTableData.add(data3) ;
        faciltiyList = new List<Facility>();
        faciltiyList.add(new Facility.fromJson(data2['Table']));

        print(facilityTableData.length);
        print("----------");
        print(faciltiyList.length);
      }


      print("facility size----->");
      print(faciltiyList.length);

      facilityListTable = new List();
      Future<List<Facility>> facLi = db.getFacilityData();
      facLi.then((facListData)
      {
        this.facilityListTable = facListData;
        print("table fac lenght---->");
        print(facilityListTable.length);
        if (facilityListTable.length > 0)
        {
          print("data exist in the Facility table");
          for (int i = 0; i < facilityListTable.length; i++) {
            Facility facd = facilityListTable.elementAt(i);
            print(facd.facilityCode);
          }
        } else {
          print("data--------->inserting into the facility table");
          db.insertFacilityData(
              DataBaseConstant.TABLE_FACILITY, facilityTableData);
        }
      });

      // db.insertFacilityData(DataBaseConstant.TABLE_FACILITY, facilityTableData);
      // db.getFacilityData();

      SharedPreferencesHelper.getSharedPreferencesHelperInstance()
          .getStringValuesSF(JsonUtil.DEFAULT_I_FACILITY_CODE)
          .then((code) {
        print(" i defaultFacilityCode---->" + code);
        for (int i = 0; i < faciltiyList.length; i++) {
          print("data from list " +
              faciltiyList.elementAt(i).iFacilityCode.toString());
          if (code
              .toString()
              .contains(faciltiyList.elementAt(i).iFacilityCode.toString())) {
            Facility facility = faciltiyList.elementAt(i);
            print("JsonValue----------->" + jsonEncode(facility.toJson()));
            SharedPreferencesHelper.getSharedPreferencesHelperInstance()
                .addStringToSF(
                JsonUtil.FACILITY_DATA, jsonEncode(facility.toJson()));
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
                  (Route<dynamic> route) => false,
            );
          } else {
            print("not matching");
          }
        }
      });
    } else {}
  }

  @override
  void initState() {
    // getFaciltyData();
    getData().then((value) {
      print("from the shared pref");
      print(value);
    }).catchError((error) {
      print(error);
    });
    db = new DatabaseHelper();
    editingControllerUser = new TextEditingController(text: '');
    editingControllerPass = new TextEditingController(text: '');
   // testData();
    passwordVisible = false;
    super.initState();
    animationControllerButton =
    AnimationController(duration: Duration(seconds: 3), vsync: this)
      ..addStatusListener((status) {
        if (status == AnimationStatus.dismissed) {
          setState(() {
            statusClick = 0;
            msg = "Username or Password incorrect";
          });
        }
      });
    usernameFocus.addListener(() {
      print("Has focus: ${usernameFocus.hasFocus}");
      setState(() {
        hintValue = " ";
        fontstyleEmail = !fontstyleEmail;
        if (editingControllerUser.text.length > 0) {
          validateText = false;
        }
      });
    });
    passwordFocus.addListener(() {
      print("Has focus: ${passwordFocus.hasFocus}");
      setState(() {
        passhintValue = "";
        fontStylePassword = !fontStylePassword;
      });
    });
    initPlatformState();
  }

  @override
  void dispose() {
    animationControllerButton.dispose();
    super.dispose();
  }

  Future<Null> _playAnimation() async {
    try {
      await animationControllerButton.forward();
      await animationControllerButton.reverse();
    } on TickerCanceled {}
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Container(
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Container(
                          color: Colors.white,
                          height: 1,
                          width: 150,
                          child: Visibility(
                            visible: visibilityTag,
                            child: WebView(
                              initialUrl: url,
                              onWebViewCreated:
                                  (WebViewController webViewController) {
                                _controller.complete(webViewController);
                              },
                              onPageFinished: (String url) {
                                print('Page finished loading: $url');

                                responseFromNativeCode();
                              },
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 0),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 0.0, left: 8, right: 8, bottom: 20),
                                child: Center(
                                  child: Image.asset(
                                    'images/im3-big.png',
                                    height:
                                    MediaQuery.of(context).size.height / 4,
                                    width:
                                    MediaQuery.of(context).size.height / 6,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 8.0, left: 8, top: 15),
                                child: TextFormField(
                                  style: TextStyle(color: txtColor),
                                  focusNode: usernameFocus,
                                  controller: editingControllerUser,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    labelText: usernameFocus.hasFocus
                                        ? AppStrings.LOGIN_USER_EMAIL_ID +
                                        AppStrings.ASTERIK_SYMBOL
                                        : AppStrings.LOGIN_USER_EMAIL_ID,
                                    labelStyle: TextStyle(
                                        fontSize: AppStrings.FONT_SIZE_18,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontStyle: FontStyle.normal,
                                        color: getColors()),
                                    hintText: hintValue,
                                    hintStyle: TextStyle(
                                        color: usernameFocus.hasFocus
                                            ? Colors.red
                                            : Colors.grey),
                                    contentPadding:
                                    EdgeInsets.fromLTRB(5.0, 13.0, 20.0, 5),
                                    // border: OutlineInputBorder(),
                                  ),
                                  validator: (userName) {
                                    if (userName.isEmpty) {
                                      return AppStrings.LOGIN_USER_NAME_HINT;
                                    } else if (userName.length < 3) {
                                      return AppStrings
                                          .LOGIN_USER_NAME_ERROR_MSG;
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                              ),
                              Padding(
                                padding:
                                const EdgeInsets.only(left: 8.0, right: 8),
                                child: TextFormField(
                                  style: TextStyle(color: txtColor),
                                  focusNode: passwordFocus,
                                  controller: editingControllerPass,
                                  autofocus: false,
                                  obscureText: !passwordVisible,
                                  decoration: InputDecoration(
                                    labelText: passwordFocus.hasFocus
                                        ? AppStrings.LOGIN_USER_PASSWORD_LABEL +
                                        AppStrings.ASTERIK_SYMBOL
                                        : AppStrings.LOGIN_USER_PASSWORD_LABEL,
                                    labelStyle: TextStyle(
                                        fontSize: AppStrings.FONT_SIZE_18,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontStyle: FontStyle.normal,
                                        color: getPwdColorsData()),
                                    hintText: passhintValue,
                                    hintStyle: TextStyle(
                                        color: passwordFocus.hasFocus
                                            ? Colors.red
                                            : Colors.grey),
                                    contentPadding: EdgeInsets.fromLTRB(
                                        5.0, 10.0, 20.0, 5.0),
                                    // border: OutlineInputBorder(),
                                    suffixIcon: IconButton(
                                      icon: Icon(
                                        passwordVisible
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          passwordVisible
                                              ? passwordVisible = false
                                              : passwordVisible = true;
                                        });
                                      },
                                    ),
                                  ),
                                  validator: (passwordValue) {
                                    if (passwordValue.isEmpty) {
                                      return AppStrings
                                          .LOGIN_USER_PASSWORD_HINT;
                                    } else if (passwordValue.length < 8) {
                                      return AppStrings
                                          .LOGIN_USER_PASSWORD_ERROR_MSG;
                                    }
                                    return null;
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text(
                                  "",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: statusClick == 0
                        ? new InkWell(
                        onTap: () {
                          if (_formKey.currentState.validate()) {
                            setState(() {
                              userName = editingControllerUser.text;
                              print(userName);
                              password = editingControllerPass.text;
                              print(password);
                              checkInternetConnection(context);
                              statusClick = 1;
                            });
                            _playAnimation();
                          }
                        },
                        child: new SignIn())
                        : new StartAnimation(
                      buttonControler: animationControllerButton.view,
                      user: editingControllerUser.text.trim(),
                      pass: editingControllerPass.text.trim(),
                    ),
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.only(left: 15.0, top: 20, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          AppStrings.LOGIN_FORGOT_PASSWORD,
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              letterSpacing: 0.3,
                              fontSize: 18.0,
                              color: Colors.blue[800],
                              fontFamily: "SegoeUI",
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                AppStrings.IM3_STATUS_MSG,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: "fonts/SegoeUI",
                                    color: Colors.blue[800]),
                              ),
                              Image.asset(
                                "images/Registered.png",
                                height: 40,
                                width: 40,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Color getColors() {
    if (editingControllerUser.text.length > 0 || usernameFocus.hasFocus) {
      colorVar = Colors.blue;
    } else {
      colorVar = Colors.black;
    }
    return colorVar;
  }
  Color getPwdColorsData() {
    if (editingControllerPass.text.length > 0 || passwordFocus.hasFocus) {
      colorVar = Colors.blue;
    } else {
      colorVar = Colors.black;
    }
    return colorVar;
  }
  testData(){
    editingControllerPass.text = AppStrings.PASSWORD;
    editingControllerUser.text = AppStrings.LOGIN;
  }
  void setDataInTable(List<String> responseBody) async {
    // Map data1 = jsonDecode(responseBody);
    // Map data2 = data1['NewDataSet'];
    // UserData.userData = UserData.fromJson(responseBody);
    UserData.userData = UserData.fromJsonwithList(responseBody);

    String strValue =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.DEFAULT_FACILITY_CODE_VALUE);
    print("Demo  data ----->" + strValue);

/*


    List<dynamic> invAction=data2["Table1"];
    print("Table 1 $invAction.length " );
    db.insertAllInvAction(DataBaseConstant.TABLE_CD_INV_ACTION,invAction);
    List<dynamic> invType=data2["Table2"];
    print("Table 2 $invType.length " );
    db.insertAllInvType(DataBaseConstant.TABLE_WO_CD_INV_TYPE,invType);

    List<dynamic> inVoiceTerms=data2["Table3"];
    print("Table 3 $invType.length ");
    db.insertAllInvoiceTerms (DataBaseConstant.TABLE_CD_INVOICE_TERMS, inVoiceTerms);
    List<dynamic> country=data2["Table5"];
    print("Table 5 $country.length ");
    db.insertAllCountry (DataBaseConstant.TABLE_CD_COUNTRY, country);
    List<dynamic> state=data2["Table6"];
    print("Table 6 $state.length ");
    db.insertAllState (DataBaseConstant.TABLE_CD_STATE, state);
    List<dynamic> biilingType=data2["Table7"];
    print("Table 7 $biilingType.length ");
    db.insertAllBillingType(DataBaseConstant.TABLE_CD_BILLABLE_TYPE, biilingType);
    List<dynamic> customerGroup =data2["Table8"];
    print("Table 8 $customerGroup.length ");
    db.insertAllCustomerGroup (DataBaseConstant.TABLE_CD_CUST_GROUP, customerGroup);
    Map classCodeMap =data2["Table9"];
    String classCodeMa=classCodeMap["ClassCodes"];
    print("classCodes"+classCodeMa);
    List<dynamic> classCodes = jsonDecode(classCodeMa);
    print(classCodes.length);
    db.insertAllClassCode (DataBaseConstant.TABLE_CD_CLASS_CODE, classCodes);

*/
  }

  void checkInternetConnection( BuildContext context
      ) async
     {
      try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
          print('check connection-------------->connected');
         getLoginRequest();
      }
    } on SocketException catch (_) {
        print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
    SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }
}

class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: new Container(
          alignment: FractionalOffset.center,
          width: MediaQuery.of(context).size.width,
          height: 35.0,
          decoration: BoxDecoration(
              color: AppColors.CYAN_BLUE_COLOR,
              borderRadius: BorderRadius.all(const Radius.circular(5.0))),
          child: Text(
            AppStrings.LOGIN_LOGIN_BUTTON_LABEL,
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w300,
                letterSpacing: 0.3),
          )),
    );
  }


}
