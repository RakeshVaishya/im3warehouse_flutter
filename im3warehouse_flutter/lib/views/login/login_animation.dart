import 'package:im3_warehouse/views/home/home.dart';
import 'package:flutter/material.dart';


class StartAnimation extends StatefulWidget {
  final String user;
  final String pass;

  final AnimationController buttonControler;
  final Animation shrinkButtonAnimation;
  final Animation zoomAnimation;
 Color btnBackgroundColor = Color(0xff0D68C4);

  StartAnimation({Key key, this.buttonControler, this.user, this.pass})
      : shrinkButtonAnimation = new Tween(begin: 320.0, end: 70.0).animate(
          CurvedAnimation(parent: buttonControler, curve: Interval(0.0, 0.150)),
        ),
        zoomAnimation =
            new Tween(begin: 70.0, end: 900.0).animate(CurvedAnimation(
                parent: buttonControler,
                curve: Interval(
                  0.550,
                  0.999,
                  curve: Curves.bounceInOut,
                ))),
        super(key: key);

  Widget _buildAnimation(BuildContext context, Widget child) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 0.0),
        child: zoomAnimation.value <= 300
            ? new Container(
                alignment: FractionalOffset.center,
                width: shrinkButtonAnimation.value,
                height: 35.0,
                decoration: BoxDecoration(
                    color: btnBackgroundColor,
                    borderRadius:
                        BorderRadius.all(const Radius.circular(30.0))),
                child: shrinkButtonAnimation.value > 75
                    ? Text(
                        "Log In",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w300,
                            letterSpacing: 0.3),
                      )
                    : CircularProgressIndicator(
                        strokeWidth: 1.0,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.white)),
              )
            : user == '' && pass == ''
                ? Container(
                    width: zoomAnimation.value,
                    height: zoomAnimation.value,
                    decoration: BoxDecoration(
                        shape: zoomAnimation.value < 600
                            ? BoxShape.circle
                            : BoxShape.rectangle,
                        color: btnBackgroundColor),
                  )
                : new Container(
                    alignment: FractionalOffset.center,
                    width: shrinkButtonAnimation.value,
                    height: 35.0,
                    decoration: BoxDecoration(
                        color: btnBackgroundColor,
                        borderRadius:
                            BorderRadius.all(const Radius.circular(30.0))),
                    child: shrinkButtonAnimation.value > 75
                        ? Text(
                            "Log In",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontWeight: FontWeight.w300,
                                letterSpacing: 0.3),
                          )
                        : CircularProgressIndicator(
                            strokeWidth: 1.0,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white)),
                  ));
  }

  @override
  _StartAnimationState createState() => new _StartAnimationState();
}

class _StartAnimationState extends State<StartAnimation>
{
  @override
  Widget build(BuildContext context)
  {
    widget.buttonControler.addListener(() {
      if (widget.zoomAnimation.isCompleted) {
        if (widget.user.isNotEmpty)
        {

         /* Navigator.of(context).push(
              MaterialPageRoute(builder: (BuildContext context) => HomeTest()));
      */  }
      }
    });
    return new AnimatedBuilder(
      builder: widget._buildAnimation,
      animation: widget.buttonControler,
    );
  }
}
