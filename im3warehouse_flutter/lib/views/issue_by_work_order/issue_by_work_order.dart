import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_location_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_wo_part_modal.dart';
import 'package:im3_warehouse/models/issue_by_work_order/issue_by_work_order_modal.dart';
import 'package:im3_warehouse/network/api/issue_by_work_order_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:xml/xml.dart' as xml;

class IssueByWorkOrder extends StatefulWidget {

  ViewTypeCallBack callBack;
  IssueByWorkOrder(this.callBack);
  @override
  State<StatefulWidget> createState() {
    return IssueByWorkOrderState();
  }
}
class IssueByWorkOrderState extends State<IssueByWorkOrder> {
  var _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool _isWorkOrder = false;
  bool _isPartNo = false;
  bool _isLocation = false;
  var _formLocationKey = GlobalKey<FormState>();
  String selectedCountTye = "";
  String userId = "";
  String companyCode = "";
  String iFacilityCode = "";
  String strWorkOrderDesc = "";
  String strPartDesc = "";
  String strPrimaryLocation = "";
  String strIPartNo = "";
  String strOnHandQuantity = "";
  String strStockRoom = "";
  ProgressDialog pr;
  String employeeName = "";

  FocusNode _workOrderFocus = FocusNode();
  FocusNode _partNoFocus = FocusNode();
  FocusNode _locationFocus = FocusNode();
  FocusNode _quantityFocus = FocusNode();
  FocusNode _serialNumberFocus = FocusNode();

  Color wordOrderColor;
  Color partColor;
  Color locationColor;
  Color quantityColor;
  bool isLoaderRunning =false;
  bool isSerialPart =false;
  bool isQuantityEnable =true;
  bool colorStatus =false;

  SerialNumberStatus status;
  List<SerialNumberStatus> barcodeItem =[];
  IssueByWorkOrderModal workOrder;
  IssueByWOPartModal partDetails;
  IssueByWoLocationModel locationDetail;
  List<IssueByWoLocationModel> _elementExistInWareHouseList = [];


  TextEditingController _workOrderController = TextEditingController();
  TextEditingController _partNoController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _quantityController = TextEditingController();
  TextEditingController _serialNoController = TextEditingController();
  Timer _timer;

  bool isLoading =true;
  bool serverRespone= false;
  Color serverResponeColor;
  String serverResponse="";
  @override
  void initState() {
    super.initState();
    facility =AppStrings.facility;
    iFacilityCode= facility.iFacilityCode;
   // Utility.showToastMsg( "Facii  "+iFacilityCode);
    print(iFacilityCode);
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
    _workOrderFocus.addListener(() {
      setState(() {
        wordOrderColor = Colors.grey[600];
      });
    });
    _partNoFocus.addListener(() {
      setState(() {
        partColor = Colors.grey[600];
      });
    });
    _locationFocus.addListener(() {
      setState(() {
        locationColor = Colors.grey[600];
      });
    });
    _quantityFocus.addListener(()
    {
      setState(() {
        quantityColor = Colors.grey[600];
      });
    });
    //testData();

  }

  void cancelTimer(){
    _timer.cancel();
  }
  void startTimer()
  {
    _timer = new Timer(Duration(seconds: 20), ()
    {
      print("Yeah, this line is printed after 20 seconds");
      if(isLoaderRunning && _timer.isActive)
      {
        print("start dismiss");
        dismissLoader();
        Utility.showToastMsg("No Responce from Server");
      }
    });
  }

  BuildContext context;
  @override
  Widget build(BuildContext context)
  {
    if(_workOrderController.text.length==0)
    {
      FocusScope.of(context).requestFocus(_workOrderFocus);
    }
    this.context =context;
    return issueByWOWidget();
  }
  Widget issueByWOWidget() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              child: Form(
                key: _formKey,
                autovalidate: _validate,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Visibility(
                      visible: serverRespone,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(serverResponse,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: serverResponeColor
                          ),),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 0.0, left: 12, right: 8, bottom: 8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        textBaseline: TextBaseline.ideographic,
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: TextFormField(
                              focusNode: _workOrderFocus,
                              controller: _workOrderController,
                                textInputAction: TextInputAction.send,
                                  onFieldSubmitted: (value)
                                  {
                                    if(value.trim().length>0)
                                    {
                                      if(isLoading) {
                                        isLoading =false;
                                        checkInternetConnection(
                                            value.trim(), IssueByWorkOrderApi
                                            .API_GET_WO_MASTER_FOR_HELP);
                                      }

                                    }
                                  else{

                                    }
                               },
                                  decoration: InputDecoration(
                                  labelText: "Work Order No",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _workOrderFocus.hasFocus
                                          ? wordOrderColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                 onChanged: (value) {
                                if(value.length==0){
                                 getClearFields();
                                }
                                },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "WorkOrder No cannot be blank";
                                }
                              },
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: InkWell(
                                child: Image.asset("images/QRCode.png"),
                                onTap: () {
                                  print("barcode clicked");
                                  scanItems(IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: _isWorkOrder,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          strWorkOrderDesc,
                          style: TextStyle(
                              fontSize: 16,
                              fontFamily: AppStrings.SEGOEUI_FONT),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 0.0, left: 12, right: 8, bottom: 0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.baseline,
                        textBaseline: TextBaseline.ideographic,
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: TextFormField(
                              focusNode: _partNoFocus,
                              controller: _partNoController,
                              textInputAction: TextInputAction.send,
                              onFieldSubmitted: (value){
                                if(value.length>0)
                                {
                                  if(workOrder ==null){
                                    Utility.showToastMsg("Sync work order");
                                    FocusScope.of(context).requestFocus(_workOrderFocus);
                                    return;
                                  }

                                  if(isLoading) {
                                    isLoading =false;
                                    checkInternetConnection(
                                        value, IssueByWorkOrderApi
                                        .API_GET_PART_DATA_FOR_ISSUE_MOBILE);
                                  }
                                 }
                                else{
                                }
                              },
                              decoration: InputDecoration(
                                  labelText: "Part No",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _partNoFocus.hasFocus
                                          ? partColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                              onChanged: (value)
                              {

                                if(value.length==0)
                                {
                                hideLocationAndSerialNumber();
                                }
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Part No cannot be blank";

                                }
                              },
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 15.0),
                              child: InkWell(
                                child: Image.asset("images/QRCode.png"),
                                onTap: () {
                                  scanItems(IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE);
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Visibility(
                        visible: _isPartNo,
                        maintainState: true,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                              const EdgeInsets.only(left: 20.0, top: 10),
                              child: Text(
                                strPartDesc,
                                style: TextStyle(
                                    fontFamily: AppStrings.SEGOEUI_FONT,
                                    fontSize: 16),
                              ),
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.only(left: 12.0, top: 10),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    "Primary Location: ",
                                    style: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),

                                  Text(
                                    strPrimaryLocation,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 0.0, left: 12, right: 8, bottom: 8),
                              child: Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.baseline,
                                textBaseline: TextBaseline.ideographic,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: TextFormField(
                                      focusNode: _locationFocus,
                                      controller: _locationController,
                                      textInputAction: TextInputAction.send,
                                      onFieldSubmitted: (value){
                                        if(value.length>0)
                                        {
                                            if(isLoading)
                                            {
                                              isLoading =false;
                                              checkInternetConnection(
                                                  value, IssueByWorkOrderApi
                                                  .API_GET_STOCK_FOR_MOBILE_PART);
                                            }
                                        }
                                        else{

                                        }
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Location",
                                          labelStyle: TextStyle(
                                              fontFamily:
                                              AppStrings.SEGOEUI_FONT,
                                              color: _locationFocus.hasFocus
                                                  ? locationColor
                                                  : null),
                                          contentPadding: EdgeInsets.fromLTRB(
                                              0, 5, 0, 0)),
                                         onChanged: (value)
                                         {
                                        if (value.length == 0)
                                        {
                                          hideLocation();
                                        }
                                      },
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Please enter Location";
                                        }
                                      },
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(top: 20.0),
                                      child: InkWell(
                                        child: Image(
                                          image:
                                          AssetImage("images/QRCode.png"),
                                        ),
                                        onTap: ()
                                        {
                                          scanItems(IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART);
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible: _isLocation,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(left: 12.0),
                                    child: Text(
                                      "Stockroom",
                                      style: TextStyle(
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: Colors.grey[600],
                                          fontSize: 15),
                                    ),
                                  ),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          top: 0.0, left: 12.0, right: 12.0),
                                      child: DropdownButton<IssueByWoLocationModel>(
                                        value: locationDetail,
                                        icon: Icon(Icons.arrow_drop_down),
                                        iconSize: 24,
                                        isExpanded: true,
                                        elevation: 10,
                                        hint: Text("text"),
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontFamily:
                                            AppStrings.SEGOEUI_FONT),
                                        underline: Container(
                                          height: 1,
                                          color: Colors.grey,
                                        ),
                                        onChanged: (IssueByWoLocationModel newValue) {
                                          setState(()
                                          {
                                            locationDetail = newValue;
                                          });
                                        },
                                        items:_elementExistInWareHouseList
                                            .map((dropdownValue) {
                                          return DropdownMenuItem<IssueByWoLocationModel>(
                                              value: dropdownValue,
                                              child: Text(
                                                dropdownValue.stockRoom,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: Colors.black,fontSize: 13.5),
                                              ));
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, top: 10),
                                        child: Text(
                                          "On Hand",
                                          style: TextStyle(
                                              fontFamily:
                                              AppStrings.SEGOEUI_FONT,
                                              color: Colors.grey[600],
                                              fontSize: 16),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10.0, top: 10),
                                        child: Text(
                                          strOnHandQuantity,
                                          style: TextStyle(fontSize: 16),
                                        ),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8.0, left: 12, right: 12),
                                    child: Divider(
                                      height: 5,
                                      color: Colors.grey,
                                      thickness: 0.5,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),


                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 10.0, left: 12, right: 8, bottom: 8),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                             child: TextFormField(
                              enabled:isQuantityEnable,
                               focusNode: _quantityFocus,
                                controller: _quantityController,
                                keyboardType: TextInputType.number,
                                onChanged: (value)
                                {
                                if(value.length >0)
                                {
                                  if(partDetails.serialNumber =="false" && locationDetail.stockRoom.length>0)
                                  {
                                    colorStatus =true;
                                    setState(()
                                    {
                                    });
                                  }
                                 else{
                                    colorStatus =false;
                                    setState(()
                                    {
                                    });
                                  }
                                }
                                if(value.length ==0){
                                  setState(() {
                                    colorStatus =false;
                                  });
                                }
                                },
                                decoration: InputDecoration(
                                  labelText: "Quantity",
                                  labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                      color: _quantityFocus.hasFocus
                                          ? quantityColor
                                          : null),
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                  validator: (value) {
                                if (value.isEmpty)
                                {
                                  return "Quantity cannot be blank or less than equal to zero";
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: isSerialPart && _isLocation,
                      maintainState: true,
                      child:  Padding(
                        padding: const EdgeInsets.only( top: 0.0, left: 12, right: 8, bottom: 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          textBaseline: TextBaseline.ideographic,
                          children: <Widget>[
                            Expanded(
                              flex: 9,
                              child: TextFormField(
                                focusNode: _serialNumberFocus,
                                controller: _serialNoController,
                                textInputAction: TextInputAction.send,
                                onFieldSubmitted: (value){
                                  if(value.length>0)
                                  {
                                    if(isLoading){
                                      isLoading =false;
                                      checkInternetConnection(
                                          value, IssueByWorkOrderApi.API_CHECK_SERIAL_NO);

                                       }
                                     }
                                  else{
                                  }
                                  },
                                decoration: InputDecoration(
                                    labelText: "Serial No ",
                                    labelStyle: TextStyle(
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        ),
                                    contentPadding:
                                    EdgeInsets.fromLTRB(0, 5, 0, 0)),
                                onChanged: (value)
                                {
                                 if(value.length ==0){
                                   status =null;
                                 }
                                if(value.length>0)
                                  _serialNoController.value =
                                TextEditingValue(
                                text: value.toUpperCase(),
                                selection: _serialNoController.selection);
                                },
                                validator: (value)
                                {
                                  if (value.isEmpty) {
                                    return "Please enter serial number no";
                                  }
                                },
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 15.0),
                                child: InkWell(
                                  child: Image.asset("images/QRCode.png"),
                                  onTap: () {
                                    scanItems(IssueByWorkOrderApi.API_CHECK_SERIAL_NO);
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 12.0, right: 2),
                              child: FlatButton(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    "Issue".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontSize: 17),
                                  ),
                                ),
                                color: getColorButton(colorStatus),
                                onPressed: ()
                                 {
                                      _onSave();
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 12.0, left: 2),
                              child: FlatButton(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    "Cancel".toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        fontSize: 17),
                                  ),
                                ),
                                color: AppColors.CYAN_BLUE_COLOR,
                                onPressed: () {
                                  onCancel();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        // ),
      ),
    );
  }

  Future scanItems(String title) async {
    try {
      var barcode = await BarcodeScanner.scan();

      if(barcode.rawContent.length<=0)
      {
        return;
      }

      if (title == IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP)
      {
        setState(()
        {
          _workOrderController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP);
        });
      } else if (title == IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE)
      {
        setState(() {
          _partNoController.text = barcode.rawContent;
         checkInternetConnection(barcode.rawContent, IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE);
        });
      }
      else if (title == IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART)
      {
        setState(() {
          _locationController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART);
        });
      }
      else if (title == IssueByWorkOrderApi.API_CHECK_SERIAL_NO)
      {
        setState(()
        {
          _serialNoController.text = barcode.rawContent;
          checkInternetConnection(barcode.rawContent, IssueByWorkOrderApi.API_CHECK_SERIAL_NO);
        });
      }
     }

    on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      Utility.showToastMsg(
          'null (user returned using the "back-button" before scanning)');
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  void checkInternetConnection(  String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION), ()
        {
          isLoading =true;
          dismissLoader();
        });
        print('check connection-------------->connected');
          if (type == IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP)
          {
            starLoader();
            getData().then((va) {
             getDetails(value, IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP);
               });

          }
          else
          if (type == IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
            starLoader();
            getData().then((va)
            {
              getDetails(
                value, IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE);
            });

          }
          else if (type == IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART)
          {
            starLoader();
            getData().then((va)
            {
              getDetails(
                  value, IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART);
            });
          }

          else if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO)
          {
            starLoader();
            getData().then((va)
            {
              getDetails(value, IssueByWorkOrderApi.API_CHECK_SERIAL_NO);
            });


          }
          else if (type == IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO)
          {
            starLoader();
            getDetails(value, IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO);
          }
        }
    } on SocketException catch (_) {
      isLoading =true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }
  void showSnackbarPage(BuildContext context) {
    final snackbar =
        SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }
  Facility facility ;
  Future<void> getData() async
  {


    print("iFacilityCode=---"+iFacilityCode);
    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
    employeeName = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
  }
  Future getDetails(String value, String type) async {
    print("data -->" + value);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";

    Map headers = new Map();
    print(NetworkConfig.BASE_URL);
    if (type == IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP)
    {
      headers =NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP;
      jsonBody=IssueByWorkOrderApi.getWOMasterForHelp(companyCode,value,"");

      headers =NetworkConfig.headers;
    }
  else  if (type == IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE) {
      headers =NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE;
         jsonBody =IssueByWorkOrderApi.getPartDataForIssueMobile(companyCode, value, iFacilityCode);


    }
    else if (type == IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART)
    {
      headers =NetworkConfig.headers;
        uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART;
      jsonBody =IssueByWorkOrderApi.getStockforMobilePart(companyCode, partDetails.iPartNo,iFacilityCode, userId,_locationController.text);
    }
    else if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO)
    {
      /*
        checkSerialNo
        companyCode: "108"   iPartNo: "17809"    serialNo: "test"    actionCode: 1
        actionSrNo: 0   stockSrNo: "53906"    quantity: 1     iWoNo: 0    iSalesOrderNo: 0      lineItemSrNo: 0
        controlId: "txtSerialNo"

         static String checkSerialNo(
          String companyCode, String iPartNo, String serialNo, String actionCode,String actionSrNo, String quantity,
          String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId
          ) {
       */



         headers =NetworkConfig.jsonHeaders;
          uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderApi.API_CHECK_SERIAL_NO;
           jsonBody =IssueByWorkOrderApi.checkSerialNo(companyCode, partDetails.iPartNo,value,
          "1","0",locationDetail.stockSrNo,"1",workOrder.iWONo,"0","0","txtSerialNo");


    }
    else if (type == IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO)
    {

         /*

         static String addIssueByWO(
          String companyCode, String userId,String empId, String serialNumber,int quantity,
          IssueByWorkOrderModal issueForWorkOrderModal,
          IssueByWOPartModal issueByWOPartModal,
          IssueByWoLocationModel _issueByWoLocationModal ,
      )
*/
          headers =NetworkConfig.jsonHeaders;
          uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO;
          jsonBody =IssueByWorkOrderApi.addIssueByWO(companyCode, userId,employeeName,
          _serialNoController.text,_quantityController.text,workOrder,
          partDetails,locationDetail);
    }


    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
       Response response = await post(uri,
        headers:headers, body: jsonBody, encoding: encoding)
        .then((resp)
        {
          isLoading =true;
       print(resp.statusCode)   ;
        if (resp.statusCode == 200)
        {
        String responseBody = resp.body;
        print("----------" + resp.body);
        onSuccessResponse(type, responseBody);
        }
       else {
          isLoading =true;
          onErrorResponse(type ,  resp.body);
        }
      }).catchError((error)
      {
        isLoading =true;
        print(error.toString());
        onErrorResponse(type ,  error.toString());
      }
    ).
       timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)).catchError((e)
       {
         isLoading =true;
         dismissLoader();
         Utility.showToastMsg("Error-->"+e.toString());
       } );
  }

  onSuccessResponse(String type, String value) {
    dismissLoader();
    isLoading =true;
    if (type == IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP )
    {
      showMessage(false,"",Colors.white);
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml .findElements("string") .first .text;
      if(value !="0")
      {
      Map map = jsonDecode(value);
      Map map1 = map["NewDataSet"];
      workOrder = IssueByWorkOrderModal.fromJson(map1["Table"]);
      print("Work Order description---->" +   workOrder.workOrderDescription);

      print(workOrder.iFacilityCode);
      print(iFacilityCode);
      if (workOrder.iFacilityCode != iFacilityCode)
      {
        String error = errorMessage(type);
        onErrorResponse(type, error);
        return;
      }
      else{
        setState(()
        {
          _isWorkOrder = true;
          strWorkOrderDesc = workOrder.workOrderDescription;
          FocusScope.of(context).requestFocus(_partNoFocus);
        }
        );

      }

   }
      else{
        if(value == "0")
        {
          print("show Error");
          String error=errorMessage(type);
          onErrorResponse(type , error);
        }
      }
      return;
    }
    else  if (type == IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE)
     {
       xml.XmlDocument parsedXml = xml.parse(value);
       print(parsedXml);
       value = parsedXml .findElements("string") .first .text;
       if(value !="0") {
         Map map = jsonDecode(value);
         Map map1 = map["NewDataSet"];
         partDetails = IssueByWOPartModal.fromJson(map1["Table"]);
         print("Part Description--->" + partDetails.partDescription);
         setState(()
         {

           isSerialPart =false;
           _locationController.text ="";
           _isLocation =false;
           _isPartNo = true;
           strPartDesc = partDetails.partDescription;
           if(partDetails.primaryRoomArea !=null &&partDetails.primaryRoomArea.length>0){
             strPrimaryLocation = partDetails.primaryRoomArea;
           }
           else{
             strPrimaryLocation ="";

           }
           if(partDetails !=null && partDetails.iPartNo.length>0)
           {
             FocusScope.of(context).requestFocus(_locationFocus);
             if(partDetails.serialNumber.toString() =="true")
             {
               isQuantityEnable =false;
               _quantityController.text = "1";
             }
             else{
               isQuantityEnable =true;
               _quantityController.text = "";
              }
           }
         });
         strIPartNo = partDetails.iPartNo;
         print("IPart No--->" + strIPartNo);
         print("value--->" + value);
       }
       else{
         print("show Error");
         String error=errorMessage(type);
         onErrorResponse(type , error);
       }

      return;
   }

else if (type == IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART)
{

  xml.XmlDocument parsedXml = xml.parse(value);
  print(parsedXml);
  value = parsedXml .findElements("string") .first .text;

  if(value=="0"){
    Utility.showToastMsg("Pickable/Breakpack Location did not found for this location!");
    _locationController.text="";
    hideLocation();
    setState(() {
      FocusScope.of(context).requestFocus(_locationFocus);
   });
    return;
  }
  Map data1 ;
  try {
    data1 = jsonDecode(value);
  }
  catch(ex)
  {
 print(ex);
  }
  Map data2 ;
  try {
    data2 = data1['NewDataSet'];
  }
  catch(ex)
  {
    print(ex);

  }
  List<dynamic> data;
  try {
    data = data2['Table'];
  }catch(ex)
  {
    print ("data in single");
  }
  if(data !=null && data.length>0)
  {
    print(" multiple array  data  --");
    _elementExistInWareHouseList = new List();
    if (data.length > 0)
    {
      for (int i = 0; i < data.length; i++)
      {
        Map dat = data.elementAt(i);
        IssueByWoLocationModel temp = IssueByWoLocationModel.fromJson(dat);
        _elementExistInWareHouseList.add(temp);
        if (i == 0)
        {
          print(" addd --"  );
          locationDetail = temp;
        }
      }
      _isLocation = true;
      locationDetail = _elementExistInWareHouseList.elementAt(0);
      strOnHandQuantity = locationDetail.onHandQuantity;
      if(partDetails !=null && partDetails.iPartNo.length>0)
      {
        if(partDetails.serialNumber.toString() =="true")
        {
          FocusScope.of(context).requestFocus(_serialNumberFocus);
          isSerialPart =true;
        }
        else{
          isSerialPart =false;
          FocusScope.of(context).requestFocus(_quantityFocus);

        }
      }
      setState(()
      {

      });
      print(" size of roomfacrLoaction--");
      print(" end --");
      return;
    }
  }
  else{
    print(" single data  --");
    _elementExistInWareHouseList = new List();
    locationDetail = IssueByWoLocationModel.fromJson(data2['Table']);
    _elementExistInWareHouseList.add(locationDetail);
    setState(()
    {
      if(partDetails !=null && partDetails.iPartNo.length>0)
      {
        if(partDetails.serialNumber.toString() =="true")
        {
          FocusScope.of(context).requestFocus(_serialNumberFocus);
          isSerialPart =true;
        }
        else{
          FocusScope.of(context).requestFocus(_quantityFocus);
          isSerialPart =false;
        }
      }
      _isLocation = true;
      locationDetail = _elementExistInWareHouseList.elementAt(0);
       strOnHandQuantity = locationDetail.onHandQuantity;
    });
      }
  return;
    }
    else  if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO)
    {
      print("data for parsing----"+value);
      Map map ;
      try {
        map = jsonDecode(value);
      }
      catch(ex)
      {
        print(ex);
      }
      String jsonData = map["d"];
      List<dynamic> data = jsonDecode(jsonData);
      barcodeItem.clear();
      for( int i =0;i<data.length;i++)
      {
        Map map = data.elementAt(i);
        SerialNumberStatus status = SerialNumberStatus.fromJson(map);
        print(status.serialNo);
        print(status.controlId);
        print(status.type);
        barcodeItem.add(status);
      }
      status= barcodeItem.elementAt(0);
      print(status.type);
      if(status.type =="success")
      {
        colorStatus =true;
      setState(() {
          colorStatus =true;
        });
     //   checkInternetConnection("", IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO);
      }
      else{
        colorStatus =false;
        _serialNoController.text="";

        print("status.message--"+status.message);
        print("type--"+type);

        onErrorResponse( type, status.message);
        return;
      }
      return;
    }
    else  if (type == IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO)
    {
      print("data for parsing"+value);
      Map map ;
      try {
        map = jsonDecode(value);
      }
      catch(ex)
      {
        print(ex);
      }
      String jsonData = map["d"];
      if(jsonData.toString() == "")
      {
        showMessage(true,"Part ("+_partNoController.text+")added in WorkOrder.\nTransaction done Successfully.",Colors.green);
        getClearFields();
      }
      else{
        showMessage(true," There is not enough quantity to perform transaction for Part("+_partNoController.text+")"
            ,Colors.red);
      }
      print("responce IssueByWorkOrderApi.API_CHECK_SERIAL_NO " +value);
     return;
    }
  }

     onErrorResponse(String type, String error)
     {
    dismissLoader();
    if (type == IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP )
    {
      Utility.showToastMsg(error.toString());
      getClearFields();
      return;
    }
    if (type == IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE )
    {
      _partNoController.text ="";
      FocusScope.of(context).requestFocus(_partNoFocus);
      setState(()
      {
      });
      Utility.showToastMsg(error);
      hideLocationAndSerialNumber();
    }
    if (type == IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART )
    {
      Utility.showToastMsg(error);
      hideLocation();
    }
    if (type == IssueByWorkOrderApi.API_CHECK_SERIAL_NO )
    {
      print("in error block --status.message--"+status.message);
      print("type--"+type);
      Utility.showToastMsg(error);
      FocusScope.of(context).requestFocus(_serialNumberFocus);
      setState(()
      {

      });
    }
    if (type == IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO )
    {
      Utility.showToastMsg(error);
    }
  }
  _onSave() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    } else
      {
      setState(()
      {
        _validate = true;
      });
    }
    if(_workOrderController.text.length==0){
      FocusScope.of(context).requestFocus(_workOrderFocus);
     return;
    }

    if(_partNoController.text.length==0){
      FocusScope.of(context).requestFocus(_partNoFocus);
      return;
    }

    if(_locationController.text.length==0){
      FocusScope.of(context).requestFocus(_locationFocus);
      return;
    }
    if(_quantityController.text.length==0){
      FocusScope.of(context).requestFocus(_quantityFocus);
      return;
    }
     if(partDetails.serialNumber =="true")
     {
       if(_serialNoController.text.length==0)
       {
         FocusScope.of(context).requestFocus(_serialNumberFocus);
         return;
       }
     }
    if( workOrder ==null)
    {
      FocusScope.of(context).requestFocus(_workOrderFocus);
      Utility.showToastMsg("Sync for work order details ");
      return;
    }
    if( partDetails ==null)
    {
      FocusScope.of(context).requestFocus(_partNoFocus);
      Utility.showToastMsg( "Sync for Part Details ");
      return;
    }
    if(locationDetail ==null)
    {
      FocusScope.of(context).requestFocus(_locationFocus);
      Utility.showToastMsg( "Sync for location for stock room details ");
      return;
    }
    if(partDetails !=null && partDetails.serialNumber =="true")
    {
      if (status == null && status.type == null) {
        FocusScope.of(context).requestFocus(_serialNumberFocus);
        Utility.showToastMsg("Sync for serial numberfor details ");
        return;
      }
      if (status.type =="error") {
        FocusScope.of(context).requestFocus(_serialNumberFocus);
        Utility.showToastMsg(status.type);
        return;
      }
    }
     checkInternetConnection("", IssueByWorkOrderApi.API_ADD_ISSUE_BY_WO);
  }

  void sysError(){

  if( workOrder ==null)
  {
  FocusScope.of(context).requestFocus(_workOrderFocus);
  Utility.showToastMsg("Sync for work order details ");
  return;
  }
  if( partDetails ==null)
  {
  FocusScope.of(context).requestFocus(_partNoFocus);
  Utility.showToastMsg( "Sync for Part Details ");
  return;
  }
  if(locationDetail ==null)
  {
  FocusScope.of(context).requestFocus(_locationFocus);
  Utility.showToastMsg( "Sync for location for stock room details ");
  return;
  }
  if(partDetails !=null && partDetails.serialNumber =="true")
  {
  if(status ==null && status.type ==null  )
  {
  FocusScope.of(context).requestFocus(_serialNumberFocus);
  Utility.showToastMsg( "Sync for serial numberfor details ");
  return;
  }
  }

  }
  void testData()
  {

    //_workOrderController.text ="WO234574";
/*
    _workOrderController.text ="WO12351054";
    _partNoController.text = "54-00650-00";
*/
    //_workOrderController.text ="WO12131";
    //_partNoController.text = "SR02";

    //EDM facility
    // _scanWorkOrderController.text = "WO11997";
    // _partNoController.text = "0017";
    // _locationController.text = "F6A";

    // VAN facility
    // _scanWorkOrderController.text = "WO11964";
    // _partNoController.text = "01-009-01";
    // _locationController.text = "V60";

    // CGY facility
    // _scanWorkOrderController.text = "WO11935";
    // _partNoController.text = "0017";
    // _locationController.text = "N34";

  }
  onCancel() {
    isLoading =true;
    String strWorkOrder = _workOrderController.text;
    String strPartNo = _partNoController.text;
    String strLocation = _locationController.text;
    String strQuantity = _quantityController.text;
    if (strWorkOrder.length == 0 &&
        strPartNo.length == 0 &&
        strQuantity.length == 0 &&
        strLocation.length == 0)
    {
      this .widget .callBack.viewType(DashboardView(this.widget.callBack), AppStrings.LABEL_HOME);
    } else {
      showMessage(false,"",Colors.white);
      getClearFields();
    }
  }

  getClearFields()
  {
    colorStatus =false;
    _workOrderController.text = "";
    _partNoController.text = "";
    _quantityController.text = "";
    _locationController.text = "";
    locationDetail =null;
     partDetails =null;
     workOrder =null;
    partDetails =null;
    locationDetail =null;
    _elementExistInWareHouseList.clear();
    _serialNoController.text ="";
    _validate =false;
    setState(() {
      _isWorkOrder = false;
      _isPartNo =false;
       isSerialPart =false;
       isQuantityEnable =true;
       });
  }

  hideLocationAndSerialNumber()
  {
    colorStatus =false;
    _partNoController.text = "";
    _quantityController.text = "";
    _locationController.text = "";
    _serialNoController.text ="";
    _isLocation =false;
    _isPartNo = false;
    isSerialPart =false;
    isQuantityEnable =true;
    if(partDetails !=null){
      partDetails =null;
    }
    if(locationDetail !=null){
      locationDetail =null;
    }
    _elementExistInWareHouseList  =[];
    setState(( )
    {
    });
  }
  hideLocation()
  {
    colorStatus =false;
    _locationController.text = "";
    locationDetail =null;
    _serialNoController.text ="";
    setState(() {
      _isLocation =false;
    });
  }

  void starLoader()
    {
    print("dialog started");

      if(!isLoaderRunning)
      {
      isLoaderRunning = true;
      Dialogs.showLoadingDialog(context, _formLocationKey);
    }
    }
  void dismissLoader()
   {
    if(isLoaderRunning)
    {
     isLoaderRunning =false;
     Navigator.of(_formLocationKey.currentContext, rootNavigator: true).pop();
     print("dialog dismissed ");
     }
   }
   String errorMessage(String type)
   {
    String error="";
     if(type == IssueByWorkOrderApi.API_GET_WO_MASTER_FOR_HELP ){
       error ="WorkOrder does not exist or not in Open, Approved or Scheduled status !";
     }
     if(type == IssueByWorkOrderApi.API_GET_PART_DATA_FOR_ISSUE_MOBILE ){
       error ="Part No does not exist or is inactive!";
     }
    if(type == IssueByWorkOrderApi.API_GET_STOCK_FOR_MOBILE_PART )
    {
      error ="Pickable/Breakpack Location did not found for this location!";
    }
    return error;
   }
   void loadData(){
     if(!_isWorkOrder)
     {
     }
   }
  Color  getColorButton(bool status ){
    if(status)
      return  AppColors.CYAN_BLUE_COLOR;
    else
      return  Colors.grey;
  }

  void showMessage(  bool status,String msg,Color color ){
    serverRespone =status;
    serverResponse =msg;
    serverResponeColor =color;
    setState(() {
    });
  }
}
