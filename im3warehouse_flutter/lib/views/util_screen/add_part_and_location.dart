import 'package:flutter/material.dart';
import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/models/reserves_part_location.dart';
import 'package:im3_warehouse/utils/date_time_util.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/physical_count/dialogCallback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:xml/xml.dart' as xml;
import 'add_part_dialog_callback.dart';



class AddPartNLocationManager extends StatelessWidget {
  //ViewTypeCallBack callBack;
  String partNo;
  int activityType ;//
  AddPartDialogCallBack callback;
  AddPartNLocationManager({this.callback,this.partNo, this.activityType});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // this is the main reason of transparency at next screen. I am ignoring rest implementation but what i have achieved is you can see.
      body: AddPartNLocation(this.callback,this.partNo, this.activityType)

    );
  }
}

class AddPartNLocation extends StatefulWidget {
  AddPartDialogCallBack callBack;
  String partNo;
  int activityType ;// " 0 = Add part , 1 =add part and location, 2 =add location only "
  AddPartNLocation(this.callBack,this.partNo, this.activityType);
  @override
  AddPartNLocationState createState() => AddPartNLocationState();
}

class AddPartNLocationState extends State<AddPartNLocation>
    implements DialogCallback {


  TextEditingController _partNoController = TextEditingController();
  TextEditingController _partDescController = TextEditingController();
  TextEditingController _priceController = TextEditingController();

  FocusNode partFocus = new FocusNode();
  FocusNode partDescFocus = new FocusNode();
  FocusNode priceFocus = new FocusNode();

  bool loading = true;
  var _formkey = GlobalKey<FormState>();
  bool _validate = false;

  bool isSerailzePartChecked = false;
  bool isVendorLotNumberChecked = false;
  bool isExpireDateChecked = false;

  ReservesPartLocation reservePartLocation;
  PartDetails partDetails;
  DynamicHelp selectedDynamicHelp;
  String strPartDesc = "";
  String userId = "";
  String companyCode = "";
  Facility facility;
  bool colorbool = false;
  bool serialNoBool = false;
  Color partColor;
  Color binColor;
  String iFacilityCode;
  var _dialogKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    print(widget.partNo);
    partDetails = new PartDetails();
    getData();
    _partNoController.text =widget.partNo;
    selectedDynamicHelp = new DynamicHelp();
    print(_partNoController.text);
    print(widget.activityType);
  }
  Future<void> getData() async
  {
    iFacilityCode   =AppStrings.facility.iFacilityCode;
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.DEFAULT_I_FACILITY_CODE);
    companyCode = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code).then((value) {
      print("value company code from shared pref -->:"+value);
      if(value.length>0)
      {
        getDynamicHelp(value,"uom").then((uom)
        {
          getDynamicHelp(value,"billingtypeall").then((billingtypeall)
          {
            companyCode  =value;
            print("company code--"+companyCode);
            getAllCustomerGroupSeparator(value);
          });
        }
        );
      }
    });


/*

    getDynamicHelp(value,"uom").then((uom)
    {
      print("uom responce--->");
      getDynamicHelp(value,"billingtypeall").then((billingtypeall)
      {
        getAllCustomerGroupSeparator(value);
      });
    }
    );
*/


  }

  @override
  Widget build(BuildContext context) {
    return  Material(
      color: Colors.white,
      child: Container(
        padding: const EdgeInsets.all(5),
        child: formWidget(),
      )
    ) ;
  }

  Widget formWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0, left: 0, right: 0, bottom: 20),
      child: SingleChildScrollView(
          child: Scrollbar(
              child: Form(
                  key: this._formkey,
                  autovalidate: _validate,
                  child: Column(

                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: Padding(
                              padding:
                              const EdgeInsets.only(top :30,bottom: 0.0, right: 5),
                              child: TextFormField(
                                  enabled: false,
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: this._partNoController,
                                  decoration: InputDecoration(
                                      contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0),
                                      labelText: AppStrings.LABEL_PART_NO,
                                      labelStyle: TextStyle(
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: partFocus.hasFocus
                                              ? partColor
                                              : null)),
                                  onChanged: (text) {
                                    if (text.toString().length > 3) {
                                      getData();
                                    }
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return AppStrings.LABEL_PART_NO_ERROR;
                                    }
                                  }),
                            ),
                          ),
                        ],
                      ),
                      TextFormField(
                        controller: _partDescController,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: AppStrings.SEGOEUI_FONT),
                        onChanged: (text) {
                        },
                        decoration: InputDecoration(
                            labelText: AppStrings.LABEL_PART_DESC,
                            labelStyle: TextStyle(
                              fontFamily: AppStrings.SEGOEUI_FONT,
                            )
                        ),
                        validator: (text) {
                          if (text.isEmpty) {
                            return AppStrings.LABEL_PART_DESC_ERROR;
                          }
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Checkbox(
                                    value: isSerailzePartChecked,
                                    onChanged: (value){onSerializeCheckboxSelect(value);},
                                    activeColor: Colors.blue,
                                    checkColor: Colors.white,
                                    tristate: false,
                                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                  ),
                                  Text('Serialize Part', style: TextStyle(fontSize: 15)),
                                ],
                              ),
                            ),
                            Expanded(
                                child:Row(
                                  children: <Widget>[
                                    Checkbox(
                                      value: isVendorLotNumberChecked,
                                      onChanged: (value){onVendorLotNumberSelect(value);},
                                      activeColor: Colors.blue,
                                      checkColor: Colors.white,
                                      tristate: false,
                                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                    ),
                                    Text('Vendor Lot Number', style: TextStyle(fontSize: 15)),
                                  ],
                                )
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                Checkbox(
                                  value: isExpireDateChecked,
                                  onChanged: (value){onExpireDateCheckboxSelect(value);},
                                  activeColor: Colors.blue,
                                  checkColor: Colors.white,
                                  tristate: false,
                                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                ),
                                Text('Expiration Date', style: TextStyle(fontSize: 15)),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  right: 8.0, top: 0),
                              child: TextFormField(
                                  focusNode: priceFocus,
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  controller: _priceController,
                                  decoration: InputDecoration(
                                      labelText:"Price",
                                      labelStyle: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily:
                                          AppStrings.SEGOEUI_FONT),
                                      contentPadding:
                                      EdgeInsets.only(top: 4)),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return AppStrings.LABEL_PART_NO_ERROR;
                                    }
                                  }),
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: MaterialButton(
                                  onPressed: () {
                                  onSave();

                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "SAVE".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: MaterialButton(
                                  onPressed: () {
                                    onCancel();
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Cancel".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )))),
    );
  }
  Widget showCircularProgressbarWidget() {
    return Container(
        child: Center(
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                strokeWidth: 5,
                backgroundColor: Colors.red,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: new Text(
                  "Loading",
                  style:
                  TextStyle(fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
                ),
              ),
            ],
          ),
        ));
  }

  void onVendorLotNumberSelect(bool value) {

    if(isVendorLotNumberChecked == false)
    {
      // Put your code here which you want to execute on CheckBox Checked event.
      setState(() {
        isVendorLotNumberChecked = true;
      });
    }
    else
    {
      // Put your code here which you want to execute on CheckBox Un-Checked event.
      setState(() {
        isVendorLotNumberChecked = false;
      });
    }
  }

  void onSerializeCheckboxSelect(bool value) {
    if(isSerailzePartChecked == false)
    {
      // Put your code here which you want to execute on CheckBox Checked event.
      setState(() {
        isSerailzePartChecked = true;
      });
    }
    else
    {
      // Put your code here which you want to execute on CheckBox Un-Checked event.
      setState(() {
        isSerailzePartChecked = false;
      });
    }
  }

  void onExpireDateCheckboxSelect(bool value) {
    if(isExpireDateChecked == false)
    {
      // Put your code here which you want to execute on CheckBox Checked event.
      setState(() {
        isExpireDateChecked = true;
      });
    }
    else
    {
      // Put your code here which you want to execute on CheckBox Un-Checked event.
      setState(() {
        isExpireDateChecked = false;
      });
    }
  }
  onSave() {
    DateTimeUtil.getCurrentDate("/");
    if (_formkey.currentState.validate()) {
      _formkey.currentState.save();
      starLoader();
      getDataFromField();
      addPartToServer(partDetails);
    } else {
      setState(() {
        _validate = true;
      });
    }
  }
  void addPart()
  {
    addPartToServer(partDetails);
  }
  Future<void> addPartToServer(PartDetails partDetails,
      ) async {
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_ADD_PART;
    String jsonBody ="";
    jsonBody  = NetworkConfig.addPart(userId, partDetails);
    print("  -----data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.jsonHeaders,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code ");
    print(statusCode);
    if (statusCode == 200) {
      String responseBody = response.body;
      print("----------" + responseBody);
      Map rep = jsonDecode(responseBody);
      String data= rep['d'];
      if(data.length>0)
      {
        onSuccessResponse();
      }
      else{
        Utility.showToastMsg(
            "Error in Adding Part");
        print("Error in Adding");
      }

    } else {
      Utility.showToastMsg(
          "Error in Adding Part");
      //getClearFromField();
      print("Error ---");
    }
  }

  Future getAllCustomerGroupSeparator(String companyCode)async{
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_GET_ALL_CUSTOMER_GROUP_WITH_SEPERATOR;
    String jsonBody = NetworkConfig.getAllCustomerGroupWithSeparator(
        companyCode,"|");

    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    if (statusCode == 200)
    {
      String responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      partDetails.notToBeSoldToCustomerGroup =responseBody;
    } else {
    }
  }

  @override
  void getList(List<String> data) {
    // TODO: implement getList
  }

  void getDataFromField() {


    print("part No --" + _partNoController.text);
    print("part desc  --" + _partDescController.text);
    print("price   --" + _priceController.text);
    print("companyCode   --" + companyCode);
    partDetails.companyCode  =companyCode;
    partDetails.PartNo =_partNoController.text;
    partDetails.partDescription =_partDescController.text.toString();
    partDetails.DateAdded=DateTimeUtil.getCurrentDate("/");
    partDetails.LastUpdateUserId =userId;
    partDetails.CreateUserID =userId;
    partDetails.Rate =_priceController.text;
    partDetails.IBillingTypeCode =selectedDynamicHelp.IBilling_Type_Code;
    partDetails.IUOMCode =selectedDynamicHelp.IUOM_Code;
    partDetails.iInvTypeCode ="1";
    partDetails.IPartStatusCode=1;
    partDetails.PartCriticalityCode =1;
    partDetails.manufacturerCode ="0";
    partDetails.AutoPO =true;
    partDetails.Tax_Required =1;

    /*
    partDetails.  IUOMCode: "1772"
    partDetails IBillingTypeCode: "1"
    DateAdded: "02/11/2020"
    IInvTypeCode: 1
    IPartStatusCode: 1

    PartCriticalityCode: 1
    Manufactured: 0
    AutoPO: true
    Tax_Required: "1"
    SmartFIFO: 4
    VendorLotNumber: true
    ExpirationDate: true
    NotToBeSoldToCustomerGroup: "1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17"
   */
    if(isSerailzePartChecked)
    {
      partDetails.isSerailNumber =isSerailzePartChecked.toString();
    }
    if(isVendorLotNumberChecked)
    {
      partDetails.isVendorLotNumber =isVendorLotNumberChecked;
    }

    partDetails.isSmartFIFO =isExpireDateChecked;
    partDetails. SmartFIFOValue ="1";
    if(isExpireDateChecked)
    {
      partDetails.isSmartFIFO =isExpireDateChecked;
      partDetails. SmartFIFOValue ="4";

    }
    partDetails.isExpirationDateExist =isExpireDateChecked;

/*


    CompanyCode: "108"
    CreateUserID: "test_coldfront"
    LastUpdateUserId: "test_coldfront"
    PartNo: "test1234"
    PartDescription: "test"
    SerailNumber: true
    Rate: "100"
*/

/*
    getDynamicHelp(ByVal CompanyCode As Integer,
        ByVal Mode As String,
        ByVal CheckCountOnly As Integer,
        ByVal Min As Integer,
        ByVal Max As Integer,
        ByVal SearchTerm As String,
        ByVal Condition As String,
        ByVal IWONo As Integer,
        ByVal userid As String,
        ByVal IFacilityCode As Integer)
       (im3.AdjustInventory.companyCode, 'uom', 2, 1, 1, 'EA', '', 0, im3.AdjustInventory.userId, im3.AdjustInventory.iFacilityCode,
       im3.AdjustInventory.companyCode, 'billingtypeall', 2, 1, 1, 'B', '', 0, im3.AdjustInventory.userId, im3.AdjustInventory.iFacilityCode,
       */

  }


  Future<String> getDynamicHelp(String companyCode,String mode) async {
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_GET_DYNAMIC_HELP;
    String jsonBody ="";

    if(mode == "billingtypeall")
    {
      jsonBody = NetworkConfig.getDynamicHelp(
          companyCode,   mode,   "2",    "1",   "1",   "B",  "",   "0",  userId, iFacilityCode);
    }
    if(mode == "uom")
    {
      jsonBody = NetworkConfig.getDynamicHelp(
          companyCode,   mode,   "2",    "1",   "1",   "EA",  "",   "0",  userId, iFacilityCode);
    }

    print(jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    if (statusCode == 200)
    {
      setState(() {
        String responseBody = response.body;
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        responseBody = parsedXml.findElements("string").first.text;
        print("----==  " + responseBody);

        try {
          List data = jsonDecode(responseBody);
          if (data.length > 0) {
            for (int i = 0; i < data.length; i++) {
              Map dat = data.elementAt(i);
              DynamicHelp dynamicHelp = DynamicHelp.fromJson(dat);
              if (i == 0)
              {
                if(identical(mode, "uom"))
                {

                  selectedDynamicHelp.UOM_Code =dynamicHelp.UOM_Code;
                  selectedDynamicHelp.ucmDescription =dynamicHelp.ucmDescription;
                  selectedDynamicHelp.IUOM_Code =dynamicHelp.IUOM_Code;

                  print("selectedDynamicHelp.UOM_Code--"+selectedDynamicHelp.UOM_Code);
                  print("selectedDynamicHelp.ucmDescription--"+selectedDynamicHelp.ucmDescription);
                  print("selectedDynamicHelp.IUOM_Code--"+selectedDynamicHelp.IUOM_Code);
                }

                if(identical(mode, "billingtypeall"))
                {
                  selectedDynamicHelp.IBilling_Type_Code =dynamicHelp.IBilling_Type_Code;
                  selectedDynamicHelp.Billing_Type_Code =dynamicHelp.Billing_Type_Code;
                  selectedDynamicHelp.billingTypeDescription =dynamicHelp.billingTypeDescription;
                  print("selectedDynamicHelp.IBilling_Type_Code--"+selectedDynamicHelp.IBilling_Type_Code);
                  print("selectedDynamicHelp.Billing_Type_Code--"+selectedDynamicHelp.Billing_Type_Code);
                  print("selectedDynamicHelp.billingTypeDescription--"+selectedDynamicHelp.billingTypeDescription);
                }
              }
            }
          } else {
            print("Wrong input");
            Utility.showToastMsg(
                "Please Select Faclility and  Enter Correct Bin No.");
          }
        } catch (e) {
          print("exception----" + e.toString());
          Utility.showToastMsg(
              "Error in getting data from server,Please Contact to Admin");
        }
      });
    } else {
      print("exception----");
      Utility.showToastMsg(
          "Error in getting data from server,Please Contact to Admin");
    }
  }

  void getClearFromField() {
    _partNoController.text = "";
    _partDescController.text = "";
    _priceController.text ="";
    print("part No --" + _partNoController.text);
    print("part desc  --" + _partDescController.text);
    print("price   --" + _priceController.text);
  }
  void onCancel()
  {
    Navigator.pop(context);
  }

  void onSuccessResponse()
  {
    dismissLoader();
    //AppStrings.newlyAddedPartNo =widget.partNo.toString();
     Navigator.pop(context);
     widget.callBack.showPartManager(partDetails);
  }
  void getSerialNumberStatusList(List<SerialNumberStatus> list){

  }
  void starLoader()
  {
    print("dialog started");
    Dialogs.showLoadingDialog(context, _dialogKey);//invoking login
    //pr.show();
  }
  void dismissLoader(){
    print("dialog dismissed");
    Navigator.of(_dialogKey.currentContext,rootNavigator: true).pop();
  }
}
