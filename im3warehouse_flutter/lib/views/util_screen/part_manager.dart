import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/models/physical_count/room_fac_for_location.dart';
import 'package:im3_warehouse/models/reserves_part_location.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/network/network_service_responce_handler.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/utils/string_util.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberModel.dart';
import 'package:im3_warehouse/views/Physical_Count/SerialNumberStatus.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_callback.dart';
import 'package:im3_warehouse/views/Physical_Count/serial_number_manager.dart';

import 'package:im3_warehouse/views/physical_count/dialogCallback.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:im3_warehouse/views/util_screen/part_manager_callback.dart';
import 'package:xml/xml.dart' as xml;
import 'package:intl/intl.dart';


class PartManager extends StatelessWidget {
  PartManagerCallback callBack;
  String partNo;
  int activityType ;//
  PartManager({this.callBack,this.partNo ,this.activityType});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        // this is the main reason of transparency at next screen. I am ignoring rest implementation but what i have achieved is you can see.
        body:  PartManagerScreen(this.callBack,this.partNo,this.activityType)
    );
  }
}

class PartManagerScreen extends StatefulWidget {
  PartManagerCallback callBack;
  String partNo;
  int activityType ;//
  PartManagerScreen(this.callBack,this.partNo,this.activityType);
  @override
  PartManagerScreenState createState() => PartManagerScreenState();
}
class PartManagerScreenState extends State<PartManagerScreen>
    implements
        DialogCallback,
        NetworkServiceResponseHandler,SerialNumberCallback {
  TextEditingController _onHandQtyController = TextEditingController();
  TextEditingController _partNoController = TextEditingController();
  TextEditingController _physicalCountController = TextEditingController();
  TextEditingController _caseSizeController = TextEditingController();
  TextEditingController _costPerUnitController = TextEditingController();
  TextEditingController _partDescController = TextEditingController();
  TextEditingController _uomController = TextEditingController();
  TextEditingController _facilityController = TextEditingController();
  TextEditingController _primaryWareHouseController = TextEditingController();
  TextEditingController _binController = TextEditingController();
  TextEditingController qtyController = TextEditingController();
  TextEditingController serialNumberController = TextEditingController();
  TextEditingController expireDateController = TextEditingController();
  TextEditingController vendorLotNoController = TextEditingController();
  FocusNode partFocus = new FocusNode();
  FocusNode partDescFocus = new FocusNode();
  FocusNode binFocus = new FocusNode();
  FocusNode serialNoFocus = new FocusNode();
  FocusNode onHandQntyFocus = new FocusNode();
  FocusNode physicalCountFocus = new FocusNode();
  FocusNode castSizeFocus = new FocusNode();
  FocusNode uomFocus = new FocusNode();
  FocusNode facilityFocus = new FocusNode();
  FocusNode primaryWHFocus = new FocusNode();
  FocusNode vendorLotFocus  = new FocusNode();
  FocusNode costPerUnitFocus = new FocusNode();
  FocusNode expireDateFocus = new FocusNode();
  String hint = "";
  int totalScan = 0;
  String showCountValue = "";
  bool loading = true;
  var _formkey = GlobalKey<FormState>();
  bool _validate = false;
  bool countVisible = false;
  bool isPartDescVisible = false;
  bool isPartDetailExist = false;
  bool isBinDataExist = false;
  bool isPartSerialize=false;
  bool isExpireDateExist=false;
  bool isVendorLotExist=false;
  String controlId;
  List<SerialNumberStatus> barcodeItem = new List();
  String partBarcodeValue = "";
  String serialNoBarcodeValue = "";
  ReservesPartLocation reservePartLocation;
  PartDetails partDetails;
  //DynamicHelp selectedDynamicHelp;
  RoomFacForLocation roomFacForLocation;
  var colorBlue = Colors.blue;
  var colorVar;
  var colorfacility;
  var colorCostPer;
  var caseSize;
  var primaryH;
  var coloronHand;
 // List<DynamicHelp> dymicList = [];
  List<RoomFacForLocation> roomFacForLocationList = [];
  String strPartDesc = "";
  String strSerialNo ="Add or Scan Serial Number";
  String userId = "";
  String companyCode = "";
  Facility facility;
  bool colorbool = false;
  bool serialNoBool = false;
  bool isSerialError = false;
  List<String> actionTypeList = ['CycleCount'];
  String selectedActionType = "";
  Color partColor;
  Color binColor;
  String iFacilityCode = "";
  bool serverRespone= false;
  Color serverResponeColor;
  String serverResponse="";
  bool isLoaderRunning =false;
  String serialNumberErrorMessge="";
  bool physicalQuantityEnable =false ;
  BuildContext context ;
  var _dialogKey = GlobalKey();
  bool isLoading =true;

  String employeeId="";
  String dateFormat="";

  @override
  void initState() {
    super.initState();
    expireDateController.text ="mm-dd-yyyy";
    _caseSizeController.text = AppStrings.strOne;
    hint = AppStrings.strRoomNameFacilityName;
    selectedActionType = actionTypeList.elementAt(0);
    facility =AppStrings.facility;
    iFacilityCode  =facility.iFacilityCode;
    _facilityController.text =facility.facilityCode;
    partFocus.addListener(() {
      setState(() {
        partColor = Colors.grey[600];
      });
    });
    binFocus.addListener(() {
      setState(() {
        binColor = Colors.grey[600];
      });
    });
    getData().then((value)
    {
     _partNoController.text=widget.partNo;
      starLoader();
      getPartDetails(_partNoController.text);

    });
    costPerUnitFocus.addListener(()
    {
      setState(() {
        if(_costPerUnitController.text =="0.00"){
          _costPerUnitController.text ="";
        }
      });
    });
  }

  Future<void> getData() async {

    SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);

    employeeId =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
    dateFormat =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.DATE_FORMAT);

  }

  @override
  Widget build(BuildContext context)
  {
    if(_partNoController.text.length==0){
      FocusScope.of(context).requestFocus(partFocus);
    }
    this.context =context;
    return formWidget();
  }
  Widget formWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 60.0, left: 8, right: 8, bottom: 5),
      child: Scrollbar(
          child:SingleChildScrollView (
              child: Form(
                  key: this._formkey,
                  autovalidate: _validate,
                  child: Column(
                    children: <Widget>[
                      Visibility(
                        visible: serverRespone,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(serverResponse,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: serverResponeColor
                            ),),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: Padding(
                              padding:
                              const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: TextFormField(
                                  focusNode: partFocus,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: this._partNoController,
                                  readOnly: true,
                                  textInputAction: TextInputAction.send,
                                  onFieldSubmitted: (value){
                                    if(value.length>0)
                                    { if(isLoading)
                                    {
                                      isLoading =false;
                                      checkInternetConnection(
                                          context, NetworkConfig.API_GET_PART,
                                          value);
                                       }
                                    }
                                    else{
                                    }
                                  },
                                  decoration: InputDecoration(
                                      contentPadding:
                                      EdgeInsets.fromLTRB(0, 5, 0, 0),
                                      labelText: AppStrings.LABEL_PART_NO,
                                      labelStyle: TextStyle(
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: partFocus.hasFocus
                                              ? partColor
                                              : null)),
                                  onChanged: (text) {
                                    if (text
                                        .toString()
                                        .length == 0)
                                    {_primaryWareHouseController.text = "";
                                    _uomController.text = "";
                                    }
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return AppStrings.LABEL_PART_NO_ERROR;
                                    };
                                  }
                              ),
                            ),
                          ),

                          /*
                          Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: InkWell(
                                  child: Image.asset(
                                    "images/QRCode.png",
                                  ),
                                  onTap: () {
                                    partBarcodeValue = "";
                                    partScan();
                                  },
                                ),
                              ))
                     */
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Expanded(
                            flex: 70,
                            child: Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Material(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                        const EdgeInsets.only(bottom: 2.0),
                                        child: Text(
                                          AppStrings.LABEL_PART_DESC,
                                          style: TextStyle(
                                              color: Colors.grey[600],
                                              fontFamily:
                                              AppStrings.SEGOEUI_FONT,
                                              fontSize: colorbool
                                                  ? AppStrings.FONT_SIZE_13
                                                  : AppStrings.FONT_SIZE_16),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                        const EdgeInsets.only(top: 10.0),
                                        child: Visibility(
                                          visible: colorbool ? true : false,
                                          maintainState: true,
                                          child: Text(strPartDesc,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  fontFamily:
                                                  AppStrings.SEGOEUI_FONT,
                                                  color: Colors.grey[600],
                                                  fontSize:
                                                  AppStrings.FONT_SIZE_15)),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 0, bottom: 2.3),
                                        child: Divider(
                                          thickness: 0.5,
                                          color: Colors.grey,
                                          height: 10,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              // TextFormField(
                              //   focusNode: partDescFocus,
                              //   style: TextStyle(color: Colors.grey[600]),
                              //   enabled: false,
                              //   controller: _partDescController,
                              //   decoration: InputDecoration(
                              //       labelText: "Part desc",
                              //       labelStyle:
                              //           TextStyle(color: getColorsData())),
                              // ),
                            ),
                          ),
                          Expanded(
                            flex: 30,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(top: 5.0),
                                  child: Text(
                                    AppStrings.LABEL_ACTION_TYPE,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontSize: AppStrings.FONT_SIZE_12,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: Colors.grey[600],
                                        fontWeight: FontWeight.normal,
                                        decoration: TextDecoration
                                            .none), // removes yellow line
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 0.0),
                                  child: DropdownButton<String>(
                                    value: selectedActionType,
                                    icon: Icon(Icons.arrow_drop_down),
                                    iconSize: 24,
                                    isExpanded: true,
                                    elevation: 10,
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    underline: Container(
                                      height: 1,
                                      color: Colors.grey,
                                    ),
                                    onChanged: (String newValue) {
                                      setState(() {
                                        print("selectedCountTye--" + newValue);
                                        selectedActionType = newValue;
                                      });
                                    },
                                    items: actionTypeList.map((dropdownValue) {
                                      return DropdownMenuItem<String>(
                                          value: dropdownValue,
                                          child: Text(
                                            dropdownValue,
                                            overflow: TextOverflow.ellipsis,
                                            style:
                                            TextStyle(color: Colors.black,fontSize: 13.5),
                                          ));
                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 8.0,
                                    ),
                                    child: TextFormField(
                                      focusNode: uomFocus,
                                      style: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      readOnly: true,
                                      controller: _uomController,
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.only(
                                          top: 3,
                                        ),
                                        labelText: AppStrings.LABEL_UOM,
                                        labelStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                      ),
                                    ),
                                  )),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 4.0),
                                  child: TextFormField(
                                    focusNode: facilityFocus,
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    enabled: false,
                                    controller: _facilityController,
                                    decoration: InputDecoration(
                                        labelText:
                                        AppStrings.LABEL_FACILITY_HASHTAG,
                                        labelStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily:
                                            AppStrings.SEGOEUI_FONT),
                                        contentPadding: EdgeInsets.only(
                                          top: 3,
                                        )),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                                flex: 70,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      right: 8.0, top: 12),
                                  child: TextField(
                                    focusNode: primaryWHFocus,
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    enabled: false,
                                    controller: _primaryWareHouseController,
                                    decoration: InputDecoration(
                                        labelText:
                                        AppStrings.LABEL_PRIMARY_WAREHOUSE,
                                        labelStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily:
                                            AppStrings.SEGOEUI_FONT),
                                        contentPadding:
                                        EdgeInsets.only(top: 4)),
                                  ),

                                )),
                          ],
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                              flex: 9,
                              child: TextFormField(
                                focusNode: binFocus,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                controller: _binController,
                                textInputAction: TextInputAction.send,
                                onFieldSubmitted: (value){
                                  if(value.length>0){
                                    if(isLoading){
                                      isLoading =false;
                                      serverRespone=false;
                                      checkInternetConnection(context,
                                          NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION, value);
                                    }
                                  }
                                },
                                onChanged: (text) {
                                  if (text.length == 0) {
                                    setState(()
                                    {
                                      isBinDataExist = false;
                                      roomFacForLocation =null;
                                      roomFacForLocationList.clear();
                                    });
                                  }
                                },
                                decoration: InputDecoration(
                                    labelText: AppStrings.LABEL_BIN,
                                    labelStyle: TextStyle(
                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                    )
                                ),
                                validator: (text) {
                                  if (text.isEmpty) {
                                    return AppStrings.LABEL_BIN_ERROR;
                                  }
                                },
                              )),
                          Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: InkWell(
                                  child: Image.asset(
                                    "images/QRCode.png",
                                  ),
                                  onTap: () {
                                    if (isPartDescVisible) {
                                      binScan();
                                    }
                                  },
                                ),
                              ))
                        ],
                      ),
                      Visibility(
                        visible: isBinDataExist,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 12.0),
                              child: Text(
                                hint,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: AppStrings.FONT_SIZE_12,
                                    fontFamily: AppStrings.SEGOEUI_FONT,
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.normal,
                                    decoration: TextDecoration
                                        .none), // removes yellow line
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: DropdownButton<RoomFacForLocation>(
                                value: roomFacForLocation,
                                icon: Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.grey[600],
                                ),
                                iconSize: 24,
                                isExpanded: true,
                                elevation: 2,
                                hint: Text(
                                  AppStrings.LABEL_DATA,
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                ),
                                style: TextStyle(color: Colors.black),
                                //Colors.deepPurple
                                underline: Container(
                                  height: 1,
                                  color: Colors.grey,
                                ),
                                onChanged: (RoomFacForLocation newValue)
                                {
                                  setState(() {
                                    print("xccczc");
                                    roomFacForLocation = newValue;
                                    getReserveFromLocation(newValue);
                                  });
                                },
                                items: roomFacForLocationList.map((dropdownValue) {
                                  return DropdownMenuItem<RoomFacForLocation>(
                                    value: dropdownValue,
                                    child: Text(
                                      dropdownValue.roomCode ,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 13.5),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: isSerialError,
                        child: Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Text(serialNumberErrorMessge,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.red
                            ),),
                        ),
                      ),
                      Visibility(
                        visible: isPartSerialize,
                        maintainState: true,
                        child:  Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Expanded(
                              flex: 9,
                              child: Padding(
                                padding:
                                const EdgeInsets.only(right: 8.0, top: 0),
                                child: Material(
                                  child:InkWell(
                                    onTap: (){
                                      print("data---");
                                      showSerialNumberList();
                                    },
                                    child: Column(

                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 0.0, top: 0),
                                          child: Text(
                                            AppStrings.LABEL_SERIAL_NO,
                                            style: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: AppStrings.SEGOEUI_FONT,
                                                fontSize: serialNoBool
                                                    ? AppStrings.FONT_SIZE_13
                                                    : AppStrings.FONT_SIZE_16),
                                          ),
                                        ),

                                        InkWell(
                                          child:Padding(
                                            padding: const EdgeInsets.only(top: 8.0),
                                            child: InkWell(
                                              child: Text(strSerialNo,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontFamily:
                                                      AppStrings.SEGOEUI_FONT,
                                                      color: Colors.grey[600],
                                                      fontSize:
                                                      AppStrings.FONT_SIZE_15)),
                                              onTap: ()
                                              {
                                                print("tapped on serial no");
                                                showSerialNumberList();
                                              },

                                            ),
                                          ),
                                          onTap: ()
                                          {
                                            print("tapped on serial no");
                                            showSerialNumberList();
                                          },
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 5),
                                          child: Divider(
                                            thickness: 1,
                                            color: Colors.grey,
                                            height: 10,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),

                            Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 25),
                                  child: InkWell(
                                    child: Image.asset(
                                      "images/QRCode.png",
                                    ),
                                    onTap: () {
                                      serialNoBarcodeValue = "";
                                      serialNoScan();
                                    },
                                  ),
                                ))
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                flex: 35,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 8.0),
                                  child: TextFormField(
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    enabled: false,
                                    controller: _onHandQtyController,
                                    decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_ON_HAND_QNTY,
                                      labelStyle: TextStyle(
                                          color: Colors.grey[600],
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      // disabledBorder: OutlineInputBorder(
                                      //     borderSide: BorderSide(
                                      //         color: Colors.grey),)
                                    ),
                                  ),
                                )),
                            Visibility(
                              visible: isBinDataExist,
                              child: Expanded(
                                flex: 65,
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 12.0),
                                  child: TextFormField(
                                    enabled: false,
                                    controller: qtyController,
                                    style: TextStyle(
                                        fontSize: AppStrings.FONT_SIZE_13,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: AppStrings.SEGOEUI_FONT,
                                        color: Colors.grey[600]),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 0),
                                child: TextFormField(
                                  enabled: physicalQuantityEnable,
                                  focusNode: physicalCountFocus,
                                  keyboardType: TextInputType.number,
                                  textInputAction: TextInputAction.next,
                                  controller: _physicalCountController,
                                  onFieldSubmitted: (value)
                                  {
                                    setState(() {
                                      if(isVendorLotExist)
                                      {
                                        FocusScope.of(this.context).requestFocus(vendorLotFocus);
                                      }
                                      if(! isVendorLotExist  && isExpireDateExist )
                                      {
                                        FocusScope.of(this.context).requestFocus(expireDateFocus);
                                      }
                                      if(! isVendorLotExist  &&  !isExpireDateExist )
                                      {
                                        FocusScope.of(this.context).requestFocus(costPerUnitFocus);
                                      }
                                    });
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Quantity cannot be blank";
                                    };
                                  },
                                  style: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  decoration: InputDecoration(
                                    labelText: AppStrings.LABEL_PHYSICAL_COUNT,
                                    labelStyle: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                  ),
                                ),
                              ),
                            ),


                            Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: TextFormField(
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    focusNode: castSizeFocus,
                                    controller: _caseSizeController,
                                    decoration: InputDecoration(
                                        labelText: AppStrings.LABEL_CASE_SIZE,
                                        labelStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        contentPadding: EdgeInsets.only(top: 3)),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      Visibility(
                          visible: isVendorLotExist  ? true :false,
                          maintainState: true,
                          child:Padding(
                            padding: EdgeInsets.only(top:12.0,
                              left: 0.0,
                            ),
                            child: TextFormField(
                              autovalidate: false ,
                              focusNode: vendorLotFocus,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (value)
                              {
                                if(isExpireDateExist )
                                {
                                  selectDate(context);
                                }
                                else
                                {
                                  FocusScope.of(this.context).requestFocus(costPerUnitFocus);
                                }
                              },
                              style: TextStyle(
                                  color: Colors.grey[600],
                                  fontFamily: AppStrings.SEGOEUI_FONT),
                              controller: vendorLotNoController,
                              decoration: InputDecoration(
                                  labelText: "Vendor Lot No",
                                  hintStyle: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  labelStyle: TextStyle(
                                      color: Colors.grey[600],
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  contentPadding: EdgeInsets.only(top: 3)),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Vendor lot no.cannot be blank";
                                }
                              },
                              onChanged: (value){
                              },
                            ),
                          )
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Visibility(
                            visible: isExpireDateExist,
                            child: Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  selectDate(context);
                                },
                                child: AbsorbPointer(
                                  child: TextFormField(
                                    style: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    controller: expireDateController,
                                    decoration: InputDecoration(
                                        labelText: AppStrings.LABEL_EXPIRATION_DATE,
                                        hintText: "mm-dd-yyyy",
                                        hintStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        labelStyle: TextStyle(
                                            color: Colors.grey[600],
                                            fontFamily: AppStrings.SEGOEUI_FONT),
                                        contentPadding: EdgeInsets.only(top: 3)),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Ex. date cannot be blank";
                                      }
                                      if (value.contains("mm-dd-yyyy")) {
                                        return "Ex. date cannot be blank";
                                      }
                                    },
                                    onChanged: (value) {
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top:10.0,
                                left: 0.0,
                              ),
                              child: TextFormField(
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                                focusNode: costPerUnitFocus,
                                controller: _costPerUnitController,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    labelText: AppStrings.LABEL_COST_PER_UNIT,
                                    labelStyle: TextStyle(
                                        color: Colors.grey[600],
                                        fontFamily: AppStrings.SEGOEUI_FONT),
                                    contentPadding: EdgeInsets.only(top: 3)),
                                validator: (v){

                                },
                              ),
                            ),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: MaterialButton(

                                  onPressed: () {
                                    onSave();
                                    return;
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Update".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: MaterialButton(
                                  onPressed: () {
                                    onCancel();
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Cancel".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )))),
    );
  }

  Widget showCircularProgressbarWidget() {
    return Container(
        child: Center(
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                strokeWidth: 5,
                backgroundColor: Colors.red,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: new Text(
                  "Loading",
                  style:
                  TextStyle(fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
                ),
              ),
            ],
          ),
        ));
  }

  onSave(){


    var now = new DateTime.now();


    final birthday = DateTime(1967, 10, 12);
    final date2 = DateTime.now();
    final difference = date2.difference(birthday).inDays;
    print(difference);
    DateFormat formatter = new DateFormat('MM-dd-yyyy');
    //formatter.formatDuration()
    String formattedDate = formatter.format(now);
    print(formattedDate); // 2016-01-25
    String data="0.00";
    if (StringUtil.isNumeric(data))
    {
      print("is number--"+data);
      if(StringUtil.isDouble(data))
      {
        double d =double.parse(data);
        int i = d.toInt();
        print("is double--"+data);
        print("is int--$i");

      }
      if(StringUtil.isInt(data))
      {
        print("is isInt--"+data);
      }
    }
    else{
      print("data is alph ");
    }
    int v=   StringUtil.tryParse( "1") ;
    print(">value--->  $v");
    final form = _formkey.currentState;
    print("data");
    if (_formkey.currentState.validate()) {
      _formkey.currentState.save();
    } else {
      setState(() {
        _validate = true;
      });
    }
    if(isSerialError)
    {
      Utility.showToastMsg("Please Remove Serial number with error");
      return ;
    }
    print("uploda data");
    getDataFromField();
  }

  Future partScan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() {
        _partNoController.text = barcode.rawContent;
      });
      checkInternetConnection(context,NetworkConfig.API_GET_PART , barcode.rawContent);
      //    starLoader();
      //  getPartDetails(barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      setState(() {
        Utility.showToastMsg(
            'null (user returned using the "back-button" before scanning)');
      });
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  Future binScan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      if(barcode.rawContent.length<=0)
      {
        return;
      }
      setState(() {
        _binController.text = barcode.rawContent;
      });
      checkInternetConnection(context, NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION, barcode.rawContent);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      setState(() {
        Utility.showToastMsg(
            'null (user returned using the "back-button" before scanning)');
      });
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  Future serialNoScan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      if(barcode ==null ||barcode.rawContent.length==0){
        serialNoScan();
      }
      else{
        addBarCode(barcode.rawContent.trim());
        serialNoScan();
        setState(() {
          this.serialNoBarcodeValue = barcode.rawContent;
          _physicalCountController.text = barcodeItem.length.toString();
          showSerialNumber(barcodeItem);
          print("Barcode list size $barcodeItem.length");
        });
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      setState(()
      {
        showSerialNumber(barcodeItem);
        print("Barcode list size $barcodeItem.length");
        _physicalCountController.text = barcodeItem.length.toString();
        String stockSrNo="";
        if( reservePartLocation == null || reservePartLocation.stockSrNo==null)
        {
          stockSrNo ="0";
        }
        else{
          stockSrNo=  reservePartLocation.stockSrNo;
        }

        checkSerialNo(companyCode, partDetails.iPartNo, strSerialNo, "18", "0", stockSrNo,
            _physicalCountController.text, "0",
            "0", "0", controlId);
      });
    } catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }
  setBarcodeCount() {
    setState(() {
      if (totalScan == 0) {
        countVisible = false;
      } else {
        countVisible = true;
      }
    });
  }
  addBarCode(String code) {
    setState(() {
      SerialNumberStatus status = new SerialNumberStatus();
      status.serialNo =code;
      if (barcodeItem.length == 0) {
        barcodeItem.add(status);
        totalScan++;
        countVisible = true;
      } else {
        if (barcodeItem.length > 0) {
          bool isExist = false;
          for (int i = 0; i < barcodeItem.length; i++) {
            print(" data Exist in the  list---->" + barcodeItem.elementAt(i).serialNo);
            if (barcodeItem.elementAt(i).serialNo.contains(code)) {
              print(" Duplicate data Exist in the  list");
              isExist = true;
              break;
            }
          }
          if (!isExist) {
            print("Duplicate  n Exist");
            barcodeItem.add(status);
            totalScan++;
            countVisible = true;
          } else {
            Utility.showToastMsg("Duplicate barCode");
          }
        }
        _physicalCountController.text = barcodeItem.length.toString();
      }
      if (countVisible) {
        showCountValue = 'Count-->' + totalScan.toString();
      }
    });
    // setBarcodeCount();
  }
  Future getPartDetails(String partNo) async {
    print("part no -->" + partNo);
    print("user --Id  -->" + userId);
    print("companyCode --  -->" + companyCode);
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_GET_PART;

    String jsonBody =
    NetworkConfig.getPartDetails(facility, userId, partNo, "-1");
    print("data---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    )   .then((response)
    {
      isLoading =true;
      dismissLoader();
      int statusCode = response.statusCode;
      print(response.statusCode);
      print("server responce "+response.body);
      setState(() {
        serverRespone =false;
      });
      if (statusCode == 200)
      {

        isLoading =true;
        String responseBody = response.body;
        print("----------" + responseBody);
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        print(parsedXml);
        responseBody = parsedXml.findElements("string").first.text;
        if (responseBody !="0")
        {
          Map data1 = jsonDecode(responseBody);
          Map data2 = data1['NewDataSet'];
          partDetails = PartDetails.fromJson(data2["Table"]);
          print("partDetails.facilityName--->" + partDetails.facilityName);
          print("partDetails.facilityName--->" + partDetails.isSerailNumber);
          if(partDetails.isSerailNumber.contains("true")){
            print("partDetails.isSerailNumber---"+partDetails.isSerailNumber);
            setState(() {
              isPartSerialize=true;
            });
          }
          else{
            physicalQuantityEnable =true;
          }
          if(partDetails.isExpireDateExist.contains("true"))
          {
            setState(() {
              isExpireDateExist=true;
              serverRespone =false;
            });
          }
          if(partDetails.isVendorLotNumberExist.contains("true"))
          {
            setState(() {
              isVendorLotExist =true;
            });
          }
          setState(() {
            isPartDescVisible = true;
            _partDescController.text = partDetails.partDescription;
            strPartDesc = partDetails.partDescription;
            colorbool = true;
            _uomController.text =     partDetails.uOMCode + " " + partDetails.uOMDescription;
            _primaryWareHouseController.text =
                partDetails.roomCode + "-" + partDetails.roomAreaCode;
            _costPerUnitController.text = partDetails.costPerUnit;
            testdata(true);
            FocusScope.of(context).requestFocus(binFocus);
          });
        } else {
          return;
        }
      }
      else {
        isLoading =true;

        print("Error in getting data ");
        print("Error in getting data ");
      }
    }
    ).catchError(
            (error)
        {
          isLoading =true;
          dismissLoader();
          Utility.showToastMsg(
              "Error in getting data from server,Please Contact to Admin");
        }
    ).timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)).catchError((e)
    {
      isLoading =true;
      dismissLoader();
      Utility.showToastMsg("Error-->"+e.toString());
    } );
  }


  Future<void> getReserveFromLocation(RoomFacForLocation dynamicHelp) async {
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_GET_RESERVES_FOR_PART_LOCATION;
    String jsonBody = NetworkConfig.getReservesForPartLocation(
        dynamicHelp, companyCode, partDetails.iPartNo, "0");
    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    ).timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)) ;
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    dismissLoader();
    if (statusCode == 200) {
      isLoading =true;
      String responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      if(responseBody =="0")
      {
        setState(()
        {
          _onHandQtyController.text = "0";
          qtyController.text = "PRQ : " +
              "0.00" +
              " SRQ : " +
              "0.00" +
              " HRQ :" +
              "0.00";
          if(isPartSerialize)
          {
            setState(() {
              print("physicalQuantityEnable =false");
              showSerialNumberList();
              physicalQuantityEnable =false;
            });
          }
          else{
            setState((){
              print("physicalQuantityEnable =true");
              physicalQuantityEnable =true;
              showPhysicalCountFocusView();
            });
          }
        });
      }
      else{
        Map data1 = jsonDecode(responseBody);
        Map data2 = data1['NewDataSet'];
        reservePartLocation = ReservesPartLocation.fromJson(data2["Table"]);
        print("partLocation- hardReserveQuantity->" +
            reservePartLocation.hardReserveQuantity);
        setState(() {
          _onHandQtyController.text = reservePartLocation.onHandQuantity;
          qtyController.text = "PRQ : " +
              reservePartLocation.probableReserveQuantity +
              " SRQ : " +
              reservePartLocation.softReserveQuantity +
              " HRQ :" +
              reservePartLocation.hardReserveQuantity;
          if(isPartSerialize)
          {
            setState(() {
              print("physicalQuantityEnable =false");
              showSerialNumberList();
              physicalQuantityEnable =false;
            });
          }
          else{
            isLoading= true;
            setState((){
              print("physicalQuantityEnable =true");
              physicalQuantityEnable =true;
              showPhysicalCountFocusView();
            });
          }
        });
      }
    } else {
      isLoading =true;
      print("Error ---");
    }
  }


  Future<void> addCycleCcount(
      PartDetails partDetails, RoomFacForLocation roomFacForLocation) async {
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_ADD_CYCLE_COUNT;
    String jsonBody =
    NetworkConfig.addCycleCount(userId, partDetails, roomFacForLocation,employeeId,dateFormat);
    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print( jsonBody);

    // uri="http://m.im3.com/web/service/Mobile.asmx/Addcycleccount?pintCompanyCode=1&pstrUserID=ShahbazS&pstrAddPartNo=PART689310&pstrAddLocation=LOC_30&pintAddPhysicalCount=1&pintOnHandQty=1.00000&pstrRoom=35|4|10|Room_30|30th%20Sep|1033|0|1&pstrRate=1.00000&pintIUOMCode=1&pintIBillTypeCode=1&pstrPartDesc=Serial%20Mobile%20Part&PboolIsSerialNumberMandatory=true&pstrSerialNumber=1233&pstritemCost=8&pstrcaseSize=1&pstrinvActionCode=7&pstrVendorLotNo=&pstrExpirationDate=";
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    );
    //  .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION));

    dismissLoader();
    int  statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    if (statusCode == 200) {
      String responseBody = response.body;
      print("server responce   " + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      print(responseBody);
      if (responseBody =="Sucess_Save_CycleCount")
      {

        Utility.showToastMsg(
            "Cycle Count Transaction Done Successfully !");
        Navigator.pop(context);
        widget.callBack.onSuccess(widget.activityType, _partNoController.text);
      }
      else if(responseBody =="error")
      {
        setState(() {
          getClearFromField();
          serverRespone =true;
          serverResponse =responseBody;
          serverResponeColor= Colors.red;
        });
        return;
      }
      else {
        setState(() {
          serverRespone =true;
          if(responseBody.contains("|"))
          {
            List <String>data= responseBody.split("|");
            responseBody =data.elementAt(0);
          }
          serverResponse =responseBody;
          serverResponeColor= Colors.red;
        });
        return;
      }
    } else {
      getClearFromField();
      print("Error ---");
    }
  }
  String  showSerialNumber(List<SerialNumberStatus> list) {
    String data = "";
    print(list.length);
    controlId="";
    if (list.length == 0) {
      data = "";
    } else {
      for (int i = 0; i < list.length; i++) {
        data += list.elementAt(i).serialNo + ",";
        int val=i+1;
        if(list.length==1)
        {
          int val=i;
        }
        else{
          val=i+1;
        }
        controlId +="txtSerialNo_"+val.toString()+",";
      }
      data = data.substring(0, data.length - 1);
      controlId =controlId.substring(0, controlId.length - 1);
    }
    setState(() {
      serialNoBool = true;
      strSerialNo = data;
    });
    print("controlId---"+controlId);
    return data;
  }

  void showSerialNumberList() {
    SerialNumberModel  serialNumberModel = new SerialNumberModel() ;
    serialNumberModel.companyCode =companyCode;
    serialNumberModel.iPartNo=partDetails.iPartNo;
    serialNumberModel.actionCode ="18";
    serialNumberModel.actionSrNo ="0";
    serialNumberModel.stockSrNo ="";
    if(  reservePartLocation != null &&  reservePartLocation.stockSrNo !=null && reservePartLocation.stockSrNo.length>0){
      serialNumberModel.stockSrNo =reservePartLocation.stockSrNo;
    }
    else{
      serialNumberModel.stockSrNo ="";
    }
    serialNumberModel.quantity=_physicalCountController.text;
    serialNumberModel.iWoNo ="0";
    serialNumberModel.iSalesOrderNo ="0";
    serialNumberModel.lineItemSrNo ="0";
    serialNumberModel.controlId =controlId;
    serialNumberModel.list =barcodeItem;
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) =>
                SerialNumberManager(callback: this,serialNumberModel: serialNumberModel)));
  }
  @override
  void getList(List<String> data) {

  }
  void getDataFromField()
  {

    print("on hand qty  --" );
    if(_partNoController.text.length==0){
      return;
    }
    if(_binController.text.length==0){
      return;
    }

    if(isPartSerialize  &&  strSerialNo.length==0){
      return;
    }
    if(isVendorLotExist  &&  vendorLotNoController.text.length==0){
      return;
    }
    if(isExpireDateExist  &&  expireDateController.text.length==0)
    {
      return;
    }
    if(isExpireDateExist  &&  expireDateController.text=="mm-dd-yyyy")
    {
      return;
    }

    partDetails.PartNo = _partNoController.text.trim();
    partDetails.onHandQuantity = _onHandQtyController.text.trim();
    if (_physicalCountController.text.trim().length > 0) {
      partDetails.physicalCount = int.parse(_physicalCountController.text.trim());
    } else {
      partDetails.physicalCount = 0;
    }
    partDetails.caseSize = _caseSizeController.text;
    partDetails.costPerUnit = _costPerUnitController.text.trim();
    partDetails.partDescription = _partDescController.text.trim();
    _uomController.text.trim();
    _facilityController.text.trim();
    _primaryWareHouseController.text.trim();
    partDetails.binLocation = _binController.text.trim();
    qtyController.text.trim();
    partDetails.serialNumber = strSerialNo;
    if(! isPartSerialize)
    {
      partDetails.serialNumber="";

    }
    print(" expireDateController  --" + expireDateController.text);
    print("vendorLotNoController.text  --" + vendorLotNoController.text);
    partDetails.expirationDate =expireDateController.text;
    if(partDetails.expirationDate ==null || partDetails.expirationDate.length==0)
    {
      partDetails.expirationDate ="";
    }
    if(partDetails.expirationDate =="mm-dd-yyyy")
    {
      partDetails.expirationDate ="";
    }
    partDetails.vendorLotNumber =vendorLotNoController.text;
    if(partDetails.vendorLotNumber ==null || partDetails.vendorLotNumber.length==0)
    {
      partDetails.vendorLotNumber ="";
    }

    /*
    print("on hand qty  --" + _onHandQtyController.text);
    print("part No --" + _partNoController.text);
    print("pysical count  --" + _physicalCountController.text);
    print("case size  --" + _caseSizeController.text);
    print("cost per unit  --" + _costPerUnitController.text);
    print("part desc  --" + _partDescController.text);
    print("uom  --" + _uomController.text);
    print("facility controller  --" + _facilityController.text);
    print("_primaryWareHouseController   --" + _primaryWareHouseController.text);
    print("_binController   --" + _binController.text);
    print("qtyController   --" + qtyController.text);
    print("serialNumberController   --" + serialNumberController.text);
    //print("roomFacForLocation.iLocationRoomFacility   --" + roomFacForLocation.iLocationRoomFacility);
    print("roomFacForLocation.locationRoomFacility   --" + roomFacForLocation.locationRoomFacility);
*/
/*
refer by web
    <option value="7">Adjustment</option>
    <option value="7">Adjustment</option>
    <option value="18">CycleCount</option>
  */

    partDetails.actionCode="18";

   /* if(selectedActionType.contains(actionTypeList.elementAt(0)))
    {

    }
    else{
      partDetails.actionCode="7";
    }
*/
    if(_costPerUnitController.text ==""||_costPerUnitController.text=="0.00")
    {
      return;
    }
    checkInternetConnection(context, NetworkConfig.API_ADD_CYCLE_COUNT, "");
  }
  void getClearFromField() {

    /*print("on hand qty  --" + _onHandQtyController.text);
    print("part No --" + _partNoController.text);
    print("pysical count  --" + _physicalCountController.text);
    print("case size  --" + _caseSizeController.text);
    print("cost per unit  --" + _costPerUnitController.text);
    print("part desc  --" + _partDescController.text);
    print("uom  --" + _uomController.text);
    print("facility controller  --" + _facilityController.text);
    print("_primaryWareHouseController   --" + _primaryWareHouseController.text);
    print("_binController   --" + _binController.text);
    print("qtyController   --" + qtyController.text);
    print("serialNumberController   --" + serialNumberController.text);
    */

    selectedActionType = actionTypeList.elementAt(0);
    //dymicList.clear();
    isPartSerialize =false;
    barcodeItem.clear();

    _uomController.text = "";
    print("set --");
    _onHandQtyController.text = "";
    _partNoController.text = "";
    _partDescController.text="";
    _physicalCountController.text = "";
    _caseSizeController.text = "1";
    _costPerUnitController.text = "";
    _partDescController.text = "";
    _primaryWareHouseController.text = "";
    _binController.text = "";
    qtyController.text = "";
    serialNumberController.text = "";
    vendorLotNoController.text ="";
    expireDateController.text ="mm-dd-yyyy";
    serialNoBool = false;
    isSerialError = false;
    strPartDesc ="";

     setState((){
      isBinDataExist =false;
      isPartDescVisible =false;
      roomFacForLocationList.clear();
      isPartDescVisible = false;
      isPartDetailExist = false;
      isBinDataExist = false;
      isPartSerialize=false;
      isExpireDateExist=false;
      isVendorLotExist=false;
      physicalQuantityEnable =false;

      isPartDescVisible = false;
      isPartDetailExist = false;
      isBinDataExist = false;
      isPartSerialize=false;
      isExpireDateExist=false;
      isVendorLotExist=false;
      serverRespone= false;
      serverResponse="";
      serialNumberErrorMessge="";
      strSerialNo = "Add or Scan Serial Number";
      roomFacForLocation =null;
      reservePartLocation =null;
      AppStrings.newlyAddedPartNo ="";
      // _formkey.currentState.reset();
      _validate =false;
      FocusScope.of(this.context).requestFocus(partFocus);
    });
  }
  void onCancel() {
    isLoading =true;
    Navigator.pop(context);
    widget.callBack.onBack(widget.activityType, _partNoController.text);
  }
  @override
  void onYesTap()
  {

  }
  Future<void> getRoomFacForLocation(String companyCode, String userId,
      String roomAreaCode, String iFacilityCode) async {
    print(" get Room Fac  For Location --"  );
    print(NetworkConfig.BASE_URL);
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION;
    String jsonBody = NetworkConfig.getRoomFacForLocation(
        companyCode, userId, roomAreaCode, iFacilityCode);
    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    if (statusCode == 200) {
      String responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml.findElements("string").first.text;
      Map data1 ;
      try{
        data1 = jsonDecode(responseBody);
        print("data1-----");
        print("data1-----"+data1.length.toString());

      }
      catch (e){
        isLoading =true;
        dismissLoader();
        print("Wrong input");
        Utility.showToastMsg(
            "Location Does not Exist for Facility !");
        _binController.text ="";
        roomFacForLocationList.clear();
        isBinDataExist = false;
        roomFacForLocation =null;
      }
     Map data2 ;
     data2 = data1['NewDataSet'];
      try{
        print("data2-----");
        print("data2-----"+data2.length.toString());

      }
      catch (e) {
        print(" eror -----");
      }
      List<dynamic> data =[];
      try{
        data = data2['Table'];
        roomFacForLocationList = new List();
        if (data.length > 0)
        {
          roomFacForLocationList.clear();
          for (int i = 0; i < data.length; i++)
          {
            Map dat = data.elementAt(i);
            RoomFacForLocation temp = RoomFacForLocation.fromJson(dat);
            roomFacForLocationList.add(temp);
            if (i == 0)
            {
              isBinDataExist = true;
              print(" addd --" );
              roomFacForLocation = temp;
              print("---------------"+roomFacForLocation.iLocationRoomFacility);
            }
          }
          print(" size of roomfacrLoaction--");
          print(" end --");
        } else {
          print("Wrong input");
          Utility.showToastMsg(
              "Please Select Faclility and  Enter Correct Bin No.");
        }
      } catch (e)
      {

        roomFacForLocationList.clear();
        roomFacForLocation = RoomFacForLocation.fromJson(data2['Table']);
        roomFacForLocationList.add(roomFacForLocation);
        isBinDataExist = true;
        print(""+roomFacForLocationList.length.toString());
      }
      getReserveFromLocation(roomFacForLocation);
    }
  }

  @override
  void onError(Response response) {
    // TODO: implement onError
  }
  @override
  void onSuccess(Response response) {
    // TODO: implement onSuccess
  }

  Future <void> checkSerialNo(String companyCode, String iPartNo,String serialNo,
      String actionCode,String actionSrNo,
      String stockSrNo,String quantity,String iWoNo,String iSalesOrderNo,String lineItemSrNo,String controlId) async {
    String uri = NetworkConfig.BASE_URL +
        NetworkConfig.WEB_URL_PATH +
        NetworkConfig.API_CHECK_SERIAL_NO;
    String jsonBody = NetworkConfig.checkSerialNo(
        companyCode,
        iPartNo,
        serialNo,
        actionCode,
        actionSrNo,
        stockSrNo,
        quantity,
        iWoNo,
        iSalesOrderNo,
        lineItemSrNo,
        controlId);
    print("data --" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    print(jsonBody);
    setState(() {
      Utility.showToastMsg("Body --->"+jsonBody);
    });
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    );
    int statusCode = response.statusCode;
    print("Status code");
    print(statusCode);
    if (statusCode == 200)
    {
      String responseBody = response.body;
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml
          .findElements("string")
          .first
          .text;
      List<dynamic> data = jsonDecode(responseBody);
      for( int i =0;i<data.length;i++)
      {
        Map map = data.elementAt(i);
        SerialNumberStatus status = SerialNumberStatus.fromJson(map);
        print(status.serialNo);
        print(status.controlId);
        print(status.type);
        barcodeItem.elementAt(i).type = status.type.toString();
        barcodeItem.elementAt(i).message = status.message.toString();
        barcodeItem.elementAt(i).cost = status.cost.toString();
        if(status.type.contains("error"))
        {
          isSerialError =true;
          serialNumberErrorMessge  =status.message;
        }
      }
    }
    setState(()
    {
      if(isSerialError)
      {
        serialNumberErrorMessge ="You have error in serial please check !";
      }
    });
  }
  selectDate(BuildContext context) {
    Future<String> strdarte = Utility.selectDate(context);
    strdarte.then((strNewDate) {
      print("New Date---->" + strNewDate);
      setState(() {
        FocusScope.of(this.context).requestFocus(costPerUnitFocus);
        expireDateController.text = strNewDate;
      });
    });
  }
  void getSerialNumberStatusList(List<SerialNumberStatus> list)
  {
    showSerialNumber(list);
    setState(() {
      _physicalCountController.text =list.length.toString();
      isSerialError =false;
      if(isVendorLotExist)
      {
        FocusScope.of(this.context).requestFocus(vendorLotFocus);
      }
      if(! isVendorLotExist  && isExpireDateExist )
      {
        FocusScope.of(this.context).requestFocus(expireDateFocus);
      }
      if(! isVendorLotExist  &&  !isExpireDateExist )
      {
        FocusScope.of(this.context).requestFocus(costPerUnitFocus);
      }
    });
  }

  Future<void>showPhysicalCountFocusView()async{
    setState(() {
      FocusScope.of(this.context).requestFocus(physicalCountFocus);
    });
  }
  void starLoader()
  {
    print("dialog started");
    //  startTimer();
    isLoaderRunning =true;
    Dialogs.showLoadingDialog(context, _dialogKey);
  }
  void dismissLoader() {
    print("before --$isLoaderRunning");
    if (isLoaderRunning == true) {
      isLoaderRunning = false;
      print("after --$isLoaderRunning");

      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }
  testdata(bool isPartExist)
  {
    SerialNumberStatus status = new SerialNumberStatus();
    status.serialNo =strSerialNo;
    if(isPartExist) {
    }
  }

  void checkInternetConnection( BuildContext context ,String requestType,String input
      ) async
  {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        Future.delayed(Duration(seconds: 30), ()
        {isLoading =true;
        dismissLoader();
        });

        if(requestType ==NetworkConfig.API_GET_PART)
        {
          starLoader();
          getData();
          getPartDetails(input.toString().trim());
        }

        if(requestType ==NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION)
        {
          starLoader();
          getData();
          //  getDynamicHelp(input.toString().trim());
          getRoomFacForLocation(companyCode, userId, input.toString().trim(), iFacilityCode);
        }
        if(requestType ==NetworkConfig.API_ADD_CYCLE_COUNT)
        {
          starLoader();
          addCycleCcount(partDetails, roomFacForLocation);
        }
        print('check connection-------------->connected');
      }
    } on SocketException catch (e)
    {
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }
  void showSnackbarPage(BuildContext context)
  {
    final snackbar =
    SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

}

