import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'add_part_dialog_callback.dart';

class AddPartAlertDialog extends StatefulWidget {
  String data;
  AddPartDialogCallBack callback;
  AddPartAlertDialog({this.data, this.callback});
  @override
  AddPartAlertDialogState createState() => AddPartAlertDialogState();


  void setCallback(AddPartDialogCallBack callback){
    this.callback =callback;

  }
}

class AddPartAlertDialogState extends State<AddPartAlertDialog> {
  var _formkey = GlobalKey<FormState>();
  ProgressDialog pr;
  AddPartDialogCallBack _callback;

  @override
  void initState() {
    super.initState();
    _callback =widget.callback;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(    title: Text(widget.data,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 16,
          )),
      content: Form(
        key: this._formkey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          onSave();
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Yes".toUpperCase(),
                            style:
                            TextStyle(fontSize: 14.0, color: Colors.white),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 4.0),
                      child: MaterialButton(
                        onPressed: ()
                        {
                          onCancel();
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "No".toUpperCase(),
                            style:
                            TextStyle(fontSize: 14.0, color: Colors.white),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  void onSave()
  {
    Navigator.pop(context);
    _callback.onYesTap();
  }
      void getDataFromField() {
      }
      void onCancel(){
        _callback.onNoTop();
        Navigator.pop(context);
      }

}
