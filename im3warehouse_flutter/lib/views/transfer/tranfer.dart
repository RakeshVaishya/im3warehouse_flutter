import 'dart:io';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/models/transfer/transfer_from_model.dart';
import 'package:im3_warehouse/models/transfer/transfer_to_model.dart';
import 'package:im3_warehouse/network/api/tranfer_api.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'dart:async';
import 'dart:convert';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'package:flutter/widgets.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:im3_warehouse/views/util_screen/add_part_and_location.dart';
import 'package:im3_warehouse/views/util_screen/part_manager.dart';
import 'package:im3_warehouse/views/util_screen/part_manager_callback.dart';
import 'package:xml/xml.dart' as xml;
import 'package:flutter/services.dart';
import 'package:im3_warehouse/views/util_screen/add_part_dialog.dart';
import 'package:im3_warehouse/views/util_screen/add_part_dialog_callback.dart';
class Transfer extends StatefulWidget {
  ViewTypeCallBack callback;
  Transfer(this.callback);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    print("createState");

    return TransferState();
  }
}

class TransferState extends State<Transfer> implements AddPartDialogCallBack,PartManagerCallback
{
  @override
  void dispose() {
    super.dispose();
    print("dispose");
  }
   int  activityType;

  TextEditingController _partNoController = TextEditingController();
  TextEditingController _partDescController = TextEditingController();
  TextEditingController _onHandQtyController = TextEditingController();
  TextEditingController _fromBinController = TextEditingController();
  TextEditingController _toTransferBinController = TextEditingController();
  TextEditingController _toTransferQuantityController = TextEditingController();
  String buttonName = "Submit";
  bool isSerializePart = false;
  var _formkey = GlobalKey<FormState>();
  bool _validate = false;
  String userId = "";
  String companyCode = "";
  String employeeName = "";
  Color partColor;
  FocusNode partFocus = new FocusNode();
  FocusNode toBinFocus = new FocusNode();
  FocusNode fromBinFocus = new FocusNode();
  FocusNode toTransferQuantityFocus = new FocusNode();
  bool _isPartExist = false;

  bool isLoading =true;
//  ProgressDialog pr;
  Facility facility;
  PartDetails _partDetails;
  TransferFromModel _transferFromModel;

  TransferToModel _transferToModel;
  List<TransferToModel> transferToModelList = [];
  List<TransferFromModel> _transferFromModelList = [];
  BuildContext context;

  bool isCalledByGetPart = false;
  var _dialogKey = GlobalKey<FormState>();
  bool isLoaderRunning = false;

  @override
  void initState() {
    super.initState();
    print("initState");


    // _partNoController.text = "25-35491-00";
    // _partNoController.text = "wheat";
    // _toTransferBinController.text ="stocklocroom2 ";
    getData();
    partFocus.addListener(() {
      setState(() {
        partColor = Colors.grey[600];
      });
    });
  }
  Future<void> getData() async {
    String data =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    facility = Facility.fromJson(map);
    print("retrived from SH file--->" + facility.iFacilityCode);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.KEY_Company_Code);
    employeeName =
    await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.EMPLOYEE_NAME);
  }

  @override
  Widget build(BuildContext context) {
    print("build");

    if (_partNoController.text.length == 0) {
      FocusScope.of(context).requestFocus(partFocus);
    }
    this.context = context;
    return formWidget();
  }


  Widget formWidget() {
   return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 8, right: 8, bottom: 5),
      child: SingleChildScrollView(
          child: Scrollbar(
              child: Form(
                  key: this._formkey,
                  autovalidate: _validate,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 9,
                            child: Padding(
                              padding:
                              const EdgeInsets.only(bottom: 0.0, right: 5),
                              child: TextFormField(
                                  focusNode: partFocus,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: AppStrings.SEGOEUI_FONT),
                                  controller: this._partNoController,
                                  decoration: InputDecoration(
                                      labelText: AppStrings.LABEL_PART_NO,
                                      labelStyle: TextStyle(
                                          fontFamily: AppStrings.SEGOEUI_FONT,
                                          color: partFocus.hasFocus
                                              ? partColor
                                              : null)),
                                  textInputAction: TextInputAction.send,
                                  onFieldSubmitted: (text) {
                                    print(isLoading);

                                    if (text.trim().length > 0) {

                                      if(isLoading) {
                                        isLoading =false;

                                        checkInternetConnection(
                                            NetworkConfig.API_GET_PART, text);
                                      }
                                    }
                                  },
                                  onChanged: (text) {
                                    if (text
                                        .toString()
                                        .length == 0) {
                                      getClearFromField();
                                      return;
                                    }
                                  },
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Part No cannot be blank";
                                    }
                                  }
                              ),
                            ),
                          ),
                          Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: InkWell(
                                  child: Image.asset(
                                    "images/QRCode.png",
                                  ),
                                  onTap: () {
                                    barCodeScan(NetworkConfig.API_GET_PART);
                                  },
                                ),
                              ))
                        ],
                      ),
                      Visibility(
                        visible: _isPartExist,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 0.0),
                              child: TextFormField(
                                style: TextStyle(color: Colors.grey[600]),
                                enabled: false,
                                controller: _partDescController,
                                decoration: InputDecoration(
                                    labelText: "Part Desc",
                                    labelStyle:
                                    TextStyle(color: Colors.grey[500])),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 9,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: TextFormField(
                                      style: TextStyle(color: Colors.grey[600]),
                                      controller: _fromBinController,
                                      focusNode: fromBinFocus,
                                      onFieldSubmitted: (val) {
                                        if (_fromBinController.text.trim().length >
                                            0) {
                                          checkInternetConnection(TransferApi
                                              .API_GET_STOCK_FOR_TRANSFER_PART,
                                              _fromBinController.text);
                                        }
                                      },
                                      textInputAction: TextInputAction.send,
                                      onChanged: (text) {
                                        if (text
                                            .toString()
                                            .length == 0) {
                                          _toTransferBinController.text = "";
                                          hideFromBinView();
                                          hideToBinView();
                                        }
                                      },
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Bin No cannot be blank";
                                        }
                                      },
                                      decoration: InputDecoration(
                                          labelText: "From Bin#",
                                          contentPadding:
                                          EdgeInsets.only(top: 0),
                                          labelStyle: TextStyle(
                                              color: Colors.grey[550])),
                                    ),
                                  ),
                                ),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 20),
                                      child: InkWell(
                                        child: Image.asset(
                                          "images/QRCode.png",
                                        ),
                                        onTap: () {
                                          barCodeScan(TransferApi
                                              .API_GET_STOCK_FOR_TRANSFER_PART);
                                        },
                                      ),
                                    )
                                )
                              ],
                            ),
                            Visibility
                              (
                              visible: _transferFromModelList.isNotEmpty,
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        "From WareHouse - Facility",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Colors.grey[500],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 0.0),
                                    child: DropdownButton<TransferFromModel>(
                                      value: _transferFromModel,
                                      icon: Icon(Icons.arrow_drop_down),
                                      iconSize: 24,
                                      isExpanded: true,
                                      elevation: 10,
                                      hint: Text(""),
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      underline: Container(
                                        height: 1,
                                        color: Colors.grey,
                                      ),
                                      onChanged: (TransferFromModel newValue) {
                                        setState(() {
                                          _transferFromModel = newValue;
                                        });
                                      },
                                      items: _transferFromModelList
                                          .map((dropdownValue) {
                                        return DropdownMenuItem<
                                            TransferFromModel>(
                                            value: dropdownValue,
                                            child: Text(
                                              dropdownValue.stockRoom
                                                  .toString(),
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 13.5),
                                            ));
                                      }).toList(),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 9,
                                        child: TextFormField(
                                          focusNode: toBinFocus,
                                          onFieldSubmitted: (val)
                                          {
                                            if (_toTransferBinController.text.trim().
                                                length > 0)
                                            {
                                              checkInternetConnection(
                                                  TransferApi
                                                      .API_GET_ROOM_TO_TRANSFER_PART,
                                                  _toTransferBinController
                                                      .text);
                                            }
                                            else{
                                              return;
                                            }
                                          },
                                          textInputAction: TextInputAction.send,
                                          style: TextStyle(
                                              color: Colors.grey[600]),
                                          controller: _toTransferBinController,
                                          onChanged: (text) {
                                            if (text.length == 0) {
                                              hideToBinView();
                                            }
                                          },
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return "Bin No cannot be blank";
                                            }
                                          },
                                          decoration: InputDecoration(
                                              labelText: "To Bin#",
                                              labelStyle: TextStyle(
                                                  color: Colors.grey[550])),
                                        ),
                                      ),
                                      Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                top: 20),
                                            child: InkWell(
                                              child: Image.asset(
                                                "images/QRCode.png",
                                              ),
                                              onTap: () {
                                                barCodeScan(TransferApi
                                                    .API_GET_ROOM_TO_TRANSFER_PART);
                                              },
                                            ),
                                          ))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Visibility(
                              visible:
                              transferToModelList.isNotEmpty,
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        "To WareHouse - Facility",
                                        textAlign: TextAlign.left,
                                        style:
                                        TextStyle(color: Colors.grey[500]),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 0.0),
                                    child: DropdownButton<TransferToModel>(
                                      value: _transferToModel,
                                      icon: Icon(Icons.arrow_drop_down),
                                      iconSize: 24,
                                      isExpanded: true,
                                      elevation: 10,
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                      underline: Container(
                                        height: 1,
                                        color: Colors.grey,
                                      ),
                                      onChanged: (TransferToModel newValue) {
                                        setState(() {
                                          print("newWarehouseSelected--" +
                                              newValue.companyCode);
                                          _transferToModel = _transferToModel;
                                        });
                                      },
                                      items: transferToModelList
                                          .map((dropdownValue) {
                                        return DropdownMenuItem<
                                            TransferToModel>(
                                            value: dropdownValue,
                                            child: Text(
                                              showTobinSelected(dropdownValue),
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15),
                                            ));
                                      }).toList(),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Visibility(
                                  visible: _transferFromModelList.isNotEmpty,
                                  child:
                                  Expanded(
                                    child: TextFormField(
                                      style: TextStyle(color: Colors.grey[600]),
                                      enabled: false,
                                      controller: _onHandQtyController,
                                      decoration: InputDecoration(
                                          labelText: "On Hand Quantity",
                                          labelStyle:
                                          TextStyle(color: Colors.grey[550])),
                                    ),
                                  ),
                                ),
                                Visibility(
                                    visible: transferToModelList
                                        .isNotEmpty &&
                                        (_partDetails.isSerailNumber ==
                                            "false"),
                                    child: Expanded(
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(left: 10.0),
                                        child: TextFormField(
                                          focusNode: toTransferQuantityFocus,
                                          onFieldSubmitted: (val) {},
                                          textInputAction: TextInputAction.next,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return "Quantity  cannot be blank";
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: <TextInputFormatter>[
                                            WhitelistingTextInputFormatter
                                                .digitsOnly
                                          ],
                                          style: TextStyle(
                                              color: Colors.grey[600]),
                                          controller: _toTransferQuantityController,
                                          decoration: InputDecoration(
                                              labelText: "Quantity to Transfer",
                                              labelStyle: TextStyle(
                                                  color: Colors.grey[500])),
                                        ),
                                      ),
                                    )),
                              ],
                            ),

                            /*         Visibility(
                              visible: isSerialError,
                              child: Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Text(serialNumberErrorMessge,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red
                                  ),),
                              ),
                            ),
                            Visibility(
                              visible: isPartSerialize,
                              maintainState: true,
                              child:  Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Expanded(
                                    flex: 9,
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(right: 8.0, top: 0),
                                      child: Material(
                                        child:InkWell(
                                          onTap: (){
                                            print("data---");
                                            showSerialNumberList();
                                          },
                                          child: Column(

                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    bottom: 0.0, top: 0),
                                                child: Text(
                                                  AppStrings.LABEL_SERIAL_NO,
                                                  style: TextStyle(
                                                      color: Colors.grey[600],
                                                      fontFamily: AppStrings.SEGOEUI_FONT,
                                                      fontSize: serialNoBool
                                                          ? AppStrings.FONT_SIZE_13
                                                          : AppStrings.FONT_SIZE_16),
                                                ),
                                              ),

                                              InkWell(
                                                child:Padding(
                                                  padding: const EdgeInsets.only(top: 8.0),
                                                  child: InkWell(
                                                    child: Text(strSerialNo,
                                                        overflow: TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                            fontFamily:
                                                            AppStrings.SEGOEUI_FONT,
                                                            color: Colors.grey[600],
                                                            fontSize:
                                                            AppStrings.FONT_SIZE_15)),
                                                    onTap: ()
                                                    {
                                                      print("tapped on serial no");
                                                      showSerialNumberList();
                                                    },

                                                  ),
                                                ),
                                                onTap: ()
                                                {
                                                  print("tapped on serial no");
                                                  showSerialNumberList();
                                                },
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 5),
                                                child: Divider(
                                                  thickness: 1,
                                                  color: Colors.grey,
                                                  height: 10,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                  Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(top: 25),
                                        child: InkWell(
                                          child: Image.asset(
                                            "images/QRCode.png",
                                          ),
                                          onTap: () {
                                            serialNoBarcodeValue = "";
                                            serialNoScan();
                                          },
                                        ),
                                      ))
                                ],
                              ),
                            ),
                   */

                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: MaterialButton(
                                  onPressed: ()
                                  {
                                    onSave(buttonName);
                                  },
                                  color: getColorButton(isSerializePart),
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      buttonName.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: MaterialButton(
                                  onPressed: () {
                                    onCancel();
                                  },
                                  color: AppColors.CYAN_BLUE_COLOR,
                                  minWidth: double.infinity,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Text(
                                      "Cancel".toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 17.0,
                                          color: Colors.white,
                                          fontFamily: AppStrings.SEGOEUI_FONT),
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(0.0))),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )))),
    );
  }
  Widget showCircularProgressbarWidget() {
    return Container(
        child: Center(
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                strokeWidth: 5,
                backgroundColor: Colors.red,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: new Text(
                  "Loading",
                  style:
                  TextStyle(fontSize: 15, fontFamily: AppStrings.SEGOEUI_FONT),
                ),
              ),
            ],
          ),
        ));
  }

  onSave(String type) {
    print("type--->" + type);
    print("onSave  $isLoading");

    if (type == "Submit")
    {
      if (_formkey.currentState.validate())
      {
        _formkey.currentState.save();
         print("start loading ");
          print(isLoading);
        if(_partNoController.text.trim().length==0)
        {
          Utility.showToastMsg(
              "Part can not be blank ");
          return;
        }
       else{
          if(isLoading)
          {
            print("do submit---");
            isLoading =false;
            checkInternetConnection(
                NetworkConfig.API_GET_PART, _partNoController.text);
          }

        }

      }
      else {
        setState(() {
          _validate = true;
        });
      }
    }
    if (type == "Transfer")
    {
      if (_formkey.currentState.validate()) {
        _formkey.currentState.save();
        if (_toTransferBinController.text.toLowerCase().toString().trim() ==
            _fromBinController.text.toLowerCase().toString().trim()) {
          Utility.showToastMsg(
              "Item cannot be transfered to same location !");
          return;
        }
        else {
          if (_partDetails.isSerailNumber == "false")
          {
            if(isLoading) {
              isLoading =false;
              checkInternetConnection(TransferApi.API_TRANSFER_QUANTITY, "");
            }
          }
        }
      }
      else {
        setState(() {
          _validate = true;
        });
      }
    }
  }
  onCancel()
  {
    isLoading =true;
    if (_partNoController.text.length == 0) {
      this.widget
          .callback
          .viewType(DashboardView(this.widget.callback), AppStrings.LABEL_HOME);
    } else {
      getClearFromField();
      print("on cancel");
    }
  }

  Future barCodeScan(String type) async {
    try {
      var barcode = await BarcodeScanner.scan();

      if(barcode.rawContent.length<=0){
        return;
      }
      setState(() {
        if (type == NetworkConfig.API_GET_PART) {
          _partNoController.text = barcode.rawContent;
          if (barcode.rawContent.length > 0) {
            checkInternetConnection(type, barcode.rawContent);
          }
          return;
        }
        if (type == TransferApi.API_GET_STOCK_FOR_TRANSFER_PART) {
          _fromBinController.text = barcode.rawContent;
          if (barcode.rawContent.length > 0) {
            checkInternetConnection(type, barcode.rawContent);
          }
          return;
        }
        if (type == TransferApi.API_GET_ROOM_TO_TRANSFER_PART) {
          _toTransferBinController.text = barcode.rawContent;
          if (barcode.rawContent.length > 0) {
            checkInternetConnection(type, barcode.rawContent);
          }
          return;
        }
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    } on FormatException {
      setState(() {

      });
    }
    catch (e) {
      Utility.showToastMsg("Unknown error: $e");
    }
  }

  Future getDetails(String data, String type) async {
    print("part no -->" + data);
    print("user --Id  -->" + userId);
    print("companyCode --  -->" + companyCode);
    String jsonBody = "";
    String uri = "";
    print(NetworkConfig.BASE_URL);
    if (type == NetworkConfig.API_GET_PART) {
      print("---Part No called --");
      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_PART;
      jsonBody = NetworkConfig.getPartDetails(facility, userId, data, "-1");
    }
    if (type == TransferApi.API_GET_STOCK_FOR_TRANSFER_PART) {
      print("---To from   --");
      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          TransferApi.API_GET_STOCK_FOR_TRANSFER_PART;
      jsonBody = TransferApi.getStockForTransferPart(
          userId, facility, _partDetails, data);
    }

    if (type == TransferApi.API_GET_ROOM_TO_TRANSFER_PART) {
      print("---To bin  --");
      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          TransferApi.API_GET_ROOM_TO_TRANSFER_PART;
      jsonBody =
          TransferApi.getRoomToTransferPart(data, facility, _partDetails);
    }
    if (type == TransferApi.API_TRANSFER_QUANTITY) {
      print("---To  Transfer --");
      uri = NetworkConfig.BASE_URL + NetworkConfig.WEB_URL_PATH +
          TransferApi.API_TRANSFER_QUANTITY;
      jsonBody = TransferApi.setTransferQuantityToLocation(
          getPartDetails(),
          getStockFromDetails(),
          getStockTodDetails(),
          _toTransferQuantityController.text,
          companyCode,
          userId,
          employeeName);
      print("---To  transfer   body --" + jsonBody);
    }
    print("data---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName('utf-8');
    Response response = await post(
      uri,
      headers: NetworkConfig.headers,
      body: jsonBody,
      encoding: encoding,
    ).then((response) {
      isLoading = true;
      int statusCode = response.statusCode;
      print("Status code");
      print(statusCode);
      if (statusCode == 200) {
        String responseBody = response.body;
        print("----------" + responseBody);
        onSuccessResponse(type, responseBody);
      } else {
        isLoading = true;
        dismissLoader();
        Utility.showToastMsg(
            "Error Responce From Server -> " + statusCode.toString());
      }
    }).catchError((error) {
      isLoading = true;
      dismissLoader();
      Utility.showToastMsg(
          "Error Responce From Server -> " + error.toString());
    }).timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION)).catchError((e)
    {
      isLoading =true;
      dismissLoader();
      Utility.showToastMsg("Error-->"+e.toString());
    } );

  }

  void hideFromBinView() {
    setState(() {
      _transferFromModelList = [];
      _fromBinController.text = "";
    });
  }

  void hideToBinView() {
    setState(() {
      transferToModelList = [];
      _toTransferQuantityController.text = "";
    });
  }

  void onSuccessResponse(String type, String responseBody) {
    if (type == NetworkConfig.API_GET_PART)
    {
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml
          .findElements("string")
          .first
          .text;
      if (responseBody != "0")
      {
        isLoading =true;
        Map data1 = jsonDecode(responseBody);
        Map data2 = data1['NewDataSet'];
        isCalledByGetPart = true;
        _partDetails = PartDetails.fromJson(data2["Table"]);
        print("partDetails.isSerailNumber--->" + _partDetails.isSerailNumber);
        print("partDetails.facilityName--->" + _partDetails.facilityName);
        if (_partDetails.isSerailNumber == "true") {
          isSerializePart = true;
          setState(() {
          });
        }
        if (_partDetails.isSerailNumber == "false")
        {
          isSerializePart = false;
          setState(() {
          });
        }
        getDetails(_partDetails.roomAreaCode,
            TransferApi.API_GET_STOCK_FOR_TRANSFER_PART); // Run extra code here
      }
      else {
        isLoading =true;
        dismissLoader();
        showAddPartDialog(1);
        return;

        /*Utility.showToastMsg(
            "Part No. does not exist or inactive !");
        */
/*

        setState(() {
          getClearFromField();
        });
*/



     }
    }
    if (type == TransferApi.API_GET_STOCK_FOR_TRANSFER_PART)
    {
      _transferFromModelList.clear();
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml
          .findElements("string")
          .first
          .text;
      if (responseBody != "0")
      {
        print("----to from data set in field------");
        dismissLoader();
        Map data1 = jsonDecode(responseBody);
        Map<String, dynamic> data2 = data1['NewDataSet'];
        List<dynamic> data = [];
        try {
          data = data2['Table'];
          if (data.length > 0)
          {
            for (int i = 0; i < data.length; i++)
            {
              Map dat = data.elementAt(i);
              TransferFromModel temp = TransferFromModel.fromJson(dat);
              _transferFromModelList.add(temp);
              if (i == 0) {
                _fromBinController.text = _transferFromModel.roomAreaCode;
                _transferFromModel = _transferFromModelList.elementAt(i);
                if (isCalledByGetPart)
                {
                  _partDescController.text = _partDetails.partDescription;
                  isCalledByGetPart = false;
                }
                _onHandQtyController.text = _transferFromModel.onHandQuantity;
                buttonName = "Transfer";
                _isPartExist = true;
                }
            }
            print(" size of roomfacrLoaction--");
            print(" end --");
            setState(() {
             setFocus(toBinFocus);
            });
          }
          else {
            dismissLoader();
            _fromBinController.text = "";
            FocusScope.of(context).requestFocus(fromBinFocus);
            if(_partDetails != null  && _partDetails.iPartNo  !=null)
             {
              print(" bin data not exist");
              _isPartExist =true;
              _partDescController.text = _partDetails.partDescription;
              setState(()
              {

              });
             }
          }
        }
        catch (e) {
          print( " single data data is block-->");
          if (data == null || data.length == 0) {
            _transferFromModel = TransferFromModel.fromJson(data2['Table']);
            _transferFromModelList.add(_transferFromModel);
            _fromBinController.text = _transferFromModel.roomAreaCode;
            _transferFromModel = _transferFromModelList.elementAt(0);
            _partDescController.text = _partDetails.partDescription;
            _onHandQtyController.text = _transferFromModel.onHandQuantity;
            buttonName = "Transfer";
            _isPartExist = true;
            setState(()
            {
              setFocus(toBinFocus);
            });
          }
          else {
            setState(()
            {
            });
          }
        }
      }
      else {
        dismissLoader();
        if(_partDetails !=null && _partDetails.iPartNo !=null)
        {
          _partDescController.text = _partDetails.partDescription;
          _isPartExist =true;
          _isPartExist =true;
          _fromBinController.text = "";
          FocusScope.of(context).requestFocus(fromBinFocus);
          setState(()
          {

            showAddPartDialog(2);

         /*   Utility.showToastMsg(
                "Part not available in the current facility !");
        */
          });
        }
        return;
      }
    }
    if (type == TransferApi.API_GET_ROOM_TO_TRANSFER_PART) {

      print(" line 1");
      transferToModelList.clear();
      print("----------" + responseBody);
      xml.XmlDocument parsedXml = xml.parse(responseBody);
      print(parsedXml);
      responseBody = parsedXml
          .findElements("string")
          .first
          .text;
      if (responseBody != "0")
      {
        setState(() {
          setFocus(toTransferQuantityFocus);

        });
        Map data1;
        print(" line 2");

        try {
          data1 = jsonDecode(responseBody);
        }
        catch (ex) {
          dismissLoader();
          setState(() {
            _toTransferQuantityController.text ="";
            Utility.showToastMsg(
               "Location not found or inactive !");
          });
          return;
        }
        Map data2;
        print(" line 3");
        try {
          data2 = data1['NewDataSet'];
        }
        catch (ex)
        {
          print("Error -"+ex.toString());
          dismissLoader();
          Utility.showToastMsg(
              "Location not found or inactive !");
          setState(() {
          });
          return;
        }
        List<dynamic> data;
        try {
          data = data2['Table'];
        } catch (ex)
        {
          print("Error -"+ex.toString());
        }
        if (data != null && data.length > 0) {
          dismissLoader();
          for (int i = 0; i < data.length; i++) {
            Map dat = data.elementAt(i);
            _transferToModel = TransferToModel.fromJson(dat);
            transferToModelList.add(_transferToModel);
            if (i == 0) {
              _transferToModel = transferToModelList.elementAt(i);
            }
          }
          print(" size of roomfacrLoaction--");
          print(" end --");
        }
        else {
          dismissLoader();
          _transferToModel = TransferToModel.fromJson(data2['Table']);
          transferToModelList.add(_transferToModel);
          _transferToModel = transferToModelList.elementAt(0);
        }
      }
      else
        {
        dismissLoader();
        Utility.showToastMsg(
            "No Responce from server,Please contact to technical team !");
        setState(() {
        });
        return;
      }
      setState(() {});
    }
    if (type == TransferApi.API_TRANSFER_QUANTITY) {
      setState(() {
        print("----------" + responseBody);
        xml.XmlDocument parsedXml = xml.parse(responseBody);
        print(parsedXml);
        responseBody = parsedXml
            .findElements("string")
            .first
            .text;
        if (responseBody == "Transfer successfully done") {
          Utility.showSuccessToastMsg(responseBody);
          getClearFromField();
          dismissLoader();
        }
        else {
          Utility.showToastMsg(
              "No Responce from server,Please contact to technical team !");
          return;
        }
        //pr.dismiss();
      });
    }
  }

  void onErrorResponse(String type)
  {
    if (type == "partNo")
    {
      setState(() {});
    }
    if (type == TransferApi.API_GET_STOCK_FOR_TRANSFER_PART) {
      setState(() {});
    }
    if (type == TransferApi.API_GET_ROOM_TO_TRANSFER_PART) {
      setState(() {});
    }
  }

  void getDataFromField() {
    _partNoController.text;
    print(" Part No Controller --" + _partNoController.text);
    print("  part Desc Controller --" + _partDescController.text);
    print(" from bin  Controller --" + _fromBinController.text);
    print(" on Hand Quantity Controller --" + _onHandQtyController.text);
    print(" to transfer Quantiy Controller --" +
        _toTransferQuantityController.text);
  }

  void getClearFromField() {
    isSerializePart = false;
    transferToModelList = [];
    _transferFromModelList = [];
    _partDetails = null;
    setState(() {
      buttonName = "Submit";
      _isPartExist = false;
      _partNoController.text = "";
      _partDescController.text = "";
      _fromBinController.text = "";
      _toTransferBinController.text = "";
      _onHandQtyController.text = "";
      _toTransferQuantityController.text = "";
      print("_labelIdOrPalletNoController --" + _partNoController.text);
    });
  }
  Future<bool> checkInternetConnection(String requestType, String input) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty)
      {
        Future.delayed(
            Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION), ()
        {
          isLoading =true;
          dismissLoader();
        });
        if (requestType == NetworkConfig.API_GET_PART) {
          starLoader();
          getData().then((value)
          {
            getDetails(_partNoController.text, requestType);
          });
        }
        if (requestType == TransferApi.API_GET_STOCK_FOR_TRANSFER_PART) {
          starLoader();
          getDetails(input, requestType);
        }
        if (requestType == TransferApi.API_GET_ROOM_TO_TRANSFER_PART) {
          starLoader();
          getData().then((value)
          {
            getDetails(input, requestType);
          });
        }
        if (requestType == TransferApi.API_TRANSFER_QUANTITY) {
         // showProgressDialog(context);
          starLoader();
          getData().then((value)
          {
            getDetails("", requestType); // Run extra code here
          });
        }
      }
    }
    on SocketException catch (e) {
      isLoading =true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar = SnackBar(
        content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  String getPartDetails() {
    print("_partDetails.iPartNo--" + _partDetails.iPartNo.toString());
    print("_partDetails.partDescription--" + _partDetails.partDescription);
    print("_partDetails.uOMCode--" + _partDetails.uOMCode);
    print("_partDetails.Rate--" + _partDetails.Rate);
    print("_partDetails.PartNo--" + _partDetails.PartNo);
    print("_partDetails.IBillingTypeCode--" + _partDetails.IBillingTypeCode);
    print("_partDetails.billingTypeCode--" + _partDetails.billingTypeCode);
    print("_transferToModel.facilityCode--" + _transferToModel.facilityCode);
    print("_transferToModel.IUOMCode--" + _partDetails.IUOMCode.toString());

    String data;
    data =
        _partDetails.iPartNo.toString() + "~" + _partDetails.partDescription +
            "~" +
            _partDetails.uOMCode + "~" + _partDetails.Rate + "~" +
            _partDetails.PartNo + "~" +
            _partDetails.IBillingTypeCode + "~" + _partDetails.billingTypeCode +
            "~" + _partDetails.IUOMCode.toString();
    return data;
  }

  String getStockFromDetails() {
/*

    pstrStockFrom -- opt.text = tbl[i].StockRoom;
    opt.value = tbl[i].IFacility_Code + '~' +
        tbl[i].Facility_Code + '~' +
        tbl[i].Facility_Name + '~' +
        tbl[i].Room_Code + '~' +
        tbl[i].Room_Area_Code + '~' +
        tbl[i].On_Hand_Quantity + '~' + tbl[i].Billing_Type_Code + '~' + tbl[i].Zone_Code;

*/

    print("_transferFromModel.iFactilityCode--" +
        _transferFromModel.iFacilityCode);
    print("_transferFromModel.iFactilityCode--" +
        _transferFromModel.iFacilityCode);
    print("_partDetails.facilityName--" + _partDetails.facilityName);
    print("_transferFromModel.roomCode--" + _transferFromModel.roomCode);
    print(
        "_transferFromModel.roomAreaCode--" + _transferFromModel.roomAreaCode);
    print("_transferFromModel.onHandQuantity--" +
        _transferFromModel.onHandQuantity);
    print("_partDetails.billingTypeCode--" + _partDetails.billingTypeCode);

    _transferFromModel.zoneCode = "";
    print("_transferFromModel.zoneCode --" + _transferFromModel.zoneCode);

    String data = "";
    data = _transferFromModel.iFacilityCode + "~" +
        _transferFromModel.facilityCode + "~"
        + _partDetails.facilityName + "~" + _transferFromModel.roomCode + "~" +
        _transferFromModel.roomAreaCode +
        "~" + _transferFromModel.onHandQuantity + "~" +
        _partDetails.billingTypeCode + "~" + _transferFromModel.zoneCode;
    return data;
  }

  String getStockTodDetails() {
//    pstrStockTo ---opt.text = tbl[i].Facility_Code + '-' + tbl[i].Room_Code + '-' + tbl[i].Room_Area_Code;
//    opt.value = tbl[i].IFacility_Code + '~' +
//        tbl[i].Facility_Code + '~' +
//        tbl[i].Facility_Name + '~' +
//        tbl[i].Room_Code + '~' +
//        tbl[i].Room_Area_Code + '~' +
//        tbl[i].On_Hand_Quantity + '~' + tbl[i].Billing_Type_Code + '~' + tbl[i].Zone_Code;
//

    print("_transferToModel.iFactilityCode--" + _transferToModel.iFacilityCode);
    print("_transferToModel.facilityCode--" + _transferToModel.facilityCode);
    print("_transferToModel.roomCode--" + _transferToModel.roomCode);
    print("_transferToModel.roomAreaCode--" + _transferToModel.roomAreaCode);
    print("_transferToModel.facilityName--" + _transferToModel.facilityName);
    print("_transferToModel.roomCode--" + _transferToModel.roomCode);
    print("_transferToModel.roomAreaCode--" + _transferToModel.roomAreaCode);
    print("_transferToModel.facilityCode--" + _transferToModel.facilityCode);
    print("_partDetails.facilityName--" + _partDetails.facilityName);
    print("_transferToModel.zoneCode --" + _transferToModel.zoneCode);
    _transferToModel.onHandQuantity = _transferFromModel.onHandQuantity;
    print(
        "_transferToModel.onHandQuantity --" + _transferToModel.onHandQuantity);
    print("_partDetails.billingTypeCode --" + _partDetails.billingTypeCode);

    String data = "";
    data =
        _transferToModel.iFacilityCode + "~" + _transferToModel.facilityCode +
            "~" + _transferToModel.facilityName + "~"
            + _transferToModel.roomCode + "~" + _transferToModel.roomAreaCode +
            "~" + _transferToModel.onHandQuantity +
            "~" + _partDetails.billingTypeCode + "~" +
            _transferToModel.zoneCode;
    return data;
  }

  String showTobinSelected(TransferToModel transferToModel) {
    return transferToModel.facilityCode + "-" + transferToModel.roomCode + "-" +
        transferToModel.roomAreaName;
  }
/*

  Future<void> showProgressDialog(BuildContext context) async
  {
    Dialogs.showLoadingDialog(context, _dialogKey); //invoking login
  }

  void dismissProgressDialog() {
    try {
      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
    } catch (ex) {}
  }
*/

  Color getColorButton(bool status) {
    if (!status)
      return AppColors.CYAN_BLUE_COLOR;
    else
      return Colors.grey;
  }


/*
  void showSerialNumberList() {
    SerialNumberModel  serialNumberModel = new SerialNumberModel() ;
    serialNumberModel.companyCode =companyCode;
    serialNumberModel.iPartNo=partDetails.iPartNo;
    serialNumberModel.actionCode ="18";
    serialNumberModel.actionSrNo ="0";
    serialNumberModel.stockSrNo ="";
    if(  reservePartLocation != null &&  reservePartLocation.stockSrNo !=null && reservePartLocation.stockSrNo.length>0){
      serialNumberModel.stockSrNo =reservePartLocation.stockSrNo;
    }
    else{
      serialNumberModel.stockSrNo ="";
    }
    serialNumberModel.quantity=_physicalCountController.text;
    serialNumberModel.iWoNo ="0";
    serialNumberModel.iSalesOrderNo ="0";
    serialNumberModel.lineItemSrNo ="0";
    serialNumberModel.controlId =controlId;
    serialNumberModel.list =barcodeItem;
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) =>
                SerialNumberManager(callback: this,serialNumberModel: serialNumberModel)));
  }
*/

  void starLoader() {
    print("dialog started");
    //  startTimer();
    isLoaderRunning = true;
    Dialogs.showLoadingDialog(context, _dialogKey);
  }
  void showAddPartDialog( int activityType)
  {
    AddPartAlertDialog  dialog =null;
   this.activityType =activityType;
    if(activityType ==1){
        dialog = AddPartAlertDialog(data: AppStrings.ERROR_ADD_PART ,callback: this,);

    }
    if(activityType ==2)
    {
        dialog = AddPartAlertDialog(data: "Do you want add Location ? " ,callback: this,);
    }
    showDialog(
        context: context,
        builder: (context)
        {
          return dialog;
        });
  }
  void dismissLoader() {
    print("before --$isLoaderRunning");
    if (isLoaderRunning == true)
    {
      isLoaderRunning = false;
      print("after --$isLoaderRunning");
      Navigator.of(_dialogKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }
  Future setFocus(FocusNode focus) async
  {
    Future.delayed(Duration(microseconds: 50), ()
    {
      setState(()
      {
        FocusScope.of(context).requestFocus(focus);
      });
    });
  }
  @override
  void onNoTop()
  {
    _partNoController.text = "";
  }
  @override
  void onYesTap()
  {
   if(activityType ==1)
   {
     Navigator.push(
         context,
         new MaterialPageRoute(
             builder: (BuildContext context) =>
                 AddPartNLocationManager(callback : this,partNo:_partNoController.text,activityType :activityType)));
   }
   else{
     Navigator.push(
         context,
         new MaterialPageRoute(
             builder: (BuildContext context) =>
                 PartManager(callBack:this , partNo:_partNoController.text,activityType: activityType)));
       }
     }

  @override
  void showPartManager(PartDetails partDetails)
  {

    print("partDetails.PartNo---->"+partDetails.PartNo);
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) =>
                PartManager(callBack:this,partNo:partDetails.PartNo ,activityType: activityType)));
    // TODO: implement showPartManager
  }

  @override
  void onBack(int activityType, String partNo)
  {
    if(partNo ==_partNoController.text)
    {
      if(isLoading)
      {
        print("do submit---");
        isLoading =false;
        checkInternetConnection(
            NetworkConfig.API_GET_PART, _partNoController.text);
      }
    }
  }

  @override
  void onError(int activityType, String partNo)
  {

  }

  @override
  void onSuccess(int activityType, String partNo) {
    // TODO: implement onSuccess
    if(partNo ==_partNoController.text)
    {
      if(isLoading)
      {
        print("do submit---");
        isLoading =false;
        checkInternetConnection(
            NetworkConfig.API_GET_PART, _partNoController.text);
      }
    }
  }
  @override
  void didUpdateWidget(Transfer oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);

    print("didUpdateWidget");

  }
  @override
  void deactivate() {
    // TODO: implement deactivate
    super.deactivate();
    print("deactivate");

  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print("didChangeDependencies");

  }

}