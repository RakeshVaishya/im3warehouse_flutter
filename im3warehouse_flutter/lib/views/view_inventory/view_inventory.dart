import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:im3_warehouse/models/part_details.dart';
import 'package:im3_warehouse/models/view_inventory/inventroy_detail.dart';
import 'package:im3_warehouse/network/api/view_invetory_api.dart';
import 'package:im3_warehouse/views/home/view_callback.dart';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:im3_warehouse/models/facility.dart';
import 'package:im3_warehouse/network/network_config.dart';
import 'package:im3_warehouse/values/app_colors.dart';
import 'package:im3_warehouse/values/app_strings.dart';
import 'package:im3_warehouse/views/dashboard/dashboard_view.dart';
import 'package:im3_warehouse/views/receive_by_po/receive_by_po.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:im3_warehouse/databases/shared_pref_helper.dart';
import 'package:im3_warehouse/models/dynamic_help.dart';
import 'package:im3_warehouse/utils/json_util.dart';
import 'package:im3_warehouse/utils/utility.dart';
import 'package:im3_warehouse/views/util_screen/Dialogs.dart';
import 'package:xml/xml.dart' as xml;

class ViewInventory extends StatefulWidget {
  ViewTypeCallBack callBack;
  BuildContext context;

  ViewInventory({this.callBack, this.context});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ViewInventoryState();
  }
}

class ViewInventoryState extends State<ViewInventory> {
  var _formKey = GlobalKey<FormState>();
  bool _validate = false;
  bool isRecordVisible = false;

  ProgressDialog pr;
  bool isData = false;
  FocusNode _binLocationFocus = FocusNode();
  FocusNode _partFocus = FocusNode();
  bool isAvailable = false;
  TextEditingController binLocationController = TextEditingController();
  TextEditingController _partNumberController = TextEditingController();

  Color wordOrderColor;
  Color partColor;
  Color locationColor;
  bool isLoaderRunning = false;
  bool colorStatus = true;
  Facility facility;
  var _formLocationKey = GlobalKey<FormState>();

  String userId = "";
  String companyCode = "";
  String iFacilityCode = "";
  List<PartDetails> partList = [];
  DynamicHelp selectedDynamicHelp;
  List<DynamicHelp> dynamicHelpList = [];
  BuildContext context;
  bool isLoading = true;
  bool serverRespone = false;
  Color serverResponeColor;
  List<InventoryDetail> inventoryList = [];

  @override
  void initState() {
    super.initState();
    new Future.delayed(Duration.zero, () {
      FocusScope.of(context).requestFocus(_partFocus);
    });

    getData();
    binLocationController.addListener(() {
      setState(() {
        isData = false;
      });
    });
  }

  Future<void> getData() async {
    String data =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.FACILITY_DATA);
    print("json data" + data);
    Map<String, dynamic> map = jsonDecode(data);
    facility = Facility.fromJson(map);
    print("retrived from SH file--->" + facility.iFacilityCode);
    userId = await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
        .getStringValuesSF(JsonUtil.USER_ID);
    companyCode =
        await SharedPreferencesHelper.getSharedPreferencesHelperInstance()
            .getStringValuesSF(JsonUtil.KEY_Company_Code);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return fromWidget(context);
  }

  Widget fromWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: this._formKey,
        autovalidate: _validate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 5, right: 5),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                textBaseline: TextBaseline.ideographic,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child: Padding(
                      padding: isAvailable
                          ? const EdgeInsets.only(right: 5, left: 5, bottom: 20)
                          : EdgeInsets.only(right: 5, left: 5, bottom: 0),
                      child: TextFormField(
                        focusNode: _partFocus,

                        style: TextStyle(color: Colors.black),
                        controller: _partNumberController,
                        textInputAction: TextInputAction.send,
                        onFieldSubmitted: (text) {
                          if (text.length > 0)
                          {
                            if (isLoading) {
                              isLoading = false;
                              checkInternetConnection(
                                  text,
                                  ViewInventoryApi
                                      .API_GET_PART_DATA_FOR_MOBILE);
                            }
                          } else {
                            Utility.showToastMsg("Please enter part number!");
                            return;
                          }
                        },
                        decoration: InputDecoration(
                            labelText: "Part #",
                            labelStyle: TextStyle(color: Colors.black87),
                            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0)),
                        onChanged: (value) {
                          if(value.length==0){
                            isRecordVisible=false;
                            setState(() {});
                          }

                        },
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: InkWell(
                      child: Image.asset(
                        "images/QRCode.png",
                      ),
                      onTap: () {
                        barCodeScan(
                            ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE);
                      },
                    ),
                  ))
                ],
              ),
            ),
            Visibility(
              maintainState: true,
              visible: isRecordVisible,
              child: Padding(
                  padding: const EdgeInsets.only(top: 2.0, left: 25, right: 5),
                  child: Text(getPartDescription())),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0, left: 5, right: 5),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                textBaseline: TextBaseline.ideographic,
                children: <Widget>[
                  Expanded(
                    flex: 9,
                    child: Padding(
                      padding: isAvailable
                          ? const EdgeInsets.only(right: 5, left: 5, bottom: 20)
                          : EdgeInsets.only(right: 5, left: 5, bottom: 0),
                      child: TextFormField(
                        focusNode: _binLocationFocus,
                        style: TextStyle(color: Colors.black),
                        controller: binLocationController,
                        textInputAction: TextInputAction.send,
                        onFieldSubmitted: (text) {
                          if (text.length > 0) {
                            if (isLoading) {
                              isLoading = false;
                              checkInternetConnection(
                                  text, NetworkConfig.API_GET_DYNAMIC_HELP);
                            }
                          } else {
                            Utility.showToastMsg("Enter bin!");
                            return;
                          }
                        },
                        decoration: InputDecoration(
                            labelText: "Bin Location:",
                            labelStyle: TextStyle(color: Colors.black87),
                            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0)),
                        onChanged: (value) {
                          if (value.length == 0) {
                            dynamicHelpList.clear();
                            selectedDynamicHelp = null;
                            setState(() {});
                          }
                        },
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: InkWell(
                      child: Image.asset(
                        "images/QRCode.png",
                      ),
                      onTap: () {
                        barCodeScan(NetworkConfig.API_GET_DYNAMIC_HELP);
                      },
                    ),
                  ))
                ],
              ),
            ),
            Visibility(
              maintainState: true,
              visible: dynamicHelpList.isNotEmpty,
              child: Padding(
                  padding: const EdgeInsets.only(top: 2.0, left: 25, right: 5),
                  child: Text(getStockRoom())),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 15.0, bottom: 5.0, right: 8.0, left: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          if (binLocationController.text.length == 0 &&
                              _partNumberController.text.length == 0) {
                            Utility.showToastMsg(
                                "Please select Part No or Location!");
                            return;
                          } else {
                            onSearch();
                          }
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppStrings.LABEL_SEARCH.toUpperCase(),
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Colors.white,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 4.0),
                      child: MaterialButton(
                        onPressed: () {
                          print("on cancel called");
                          if (binLocationController.text.length == 0 &&
                              _partNumberController.text.length == 0 &&
                              inventoryList.length == 0) {
                            print("on back  ");
                            this.widget.callBack.viewType(
                                DashboardView(this.widget.callBack),
                                AppStrings.LABEL_HOME);
                          } else {
                            binLocationController.text = "";
                            partList.clear();
                            ReceiveByPo.poList = null;
                            isRecordVisible = false;
                            _partNumberController.text = "";
                            inventoryList.clear();
                            dynamicHelpList.clear();
                            selectedDynamicHelp = null;
                          }
                        },
                        color: AppColors.CYAN_BLUE_COLOR,
                        minWidth: double.infinity,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            AppStrings.LABEL_CANCEL.toUpperCase(),
                            style: TextStyle(
                                fontSize: 17.0,
                                color: Colors.white,
                                fontFamily: AppStrings.SEGOEUI_FONT),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(0.0))),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Visibility(
                visible: inventoryList.isNotEmpty,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 8.0, right: 8, top: 5),
                      child: Container(
                        color: Colors.grey[200],
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(
                                0.0,
                              ),
                              child: Text(
                                "",
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                    fontSize: 5,
                                    fontFamily: AppStrings.SEGOEUI_FONT),
                              ),
                            )),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 0.0, left: 5, right: 5),
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        child: ListView.builder(
                          itemCount: inventoryList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              child: Card(
                                  elevation: 2,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.baseline,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 7,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text(
                                                    "Part #: ",
                                                    style: TextStyle(
                                                        color: Colors.grey[600],
                                                        fontFamily: AppStrings
                                                            .SEGOEUI_FONT),
                                                  ),
                                                  Text(
                                                    inventoryList
                                                        .elementAt(index)
                                                        .partNo,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontFamily: AppStrings
                                                            .SEGOEUI_FONT),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Bin: ",
                                                      style: TextStyle(
                                                          color:
                                                              Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      inventoryList
                                                          .elementAt(index)
                                                          .location,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Stock Room: ",
                                                      style: TextStyle(
                                                          color:
                                                              Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      inventoryList
                                                          .elementAt(index)
                                                          .stockRoom,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 8.0),
                                          child: Container(
                                              height: 00,
                                              child: VerticalDivider(
                                                  color: Colors.grey)),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 6,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Text("On Hand Qty: ",
                                                      style: TextStyle(
                                                          color:
                                                              Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      inventoryList
                                                          .elementAt(index)
                                                          .onHandQuantity,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  Text("Available Qty: ",
                                                      style: TextStyle(
                                                          color:
                                                              Colors.grey[600],
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                  Text(
                                                      inventoryList
                                                          .elementAt(index)
                                                          .availableQuantity,
                                                      style: TextStyle(
                                                          fontFamily: AppStrings
                                                              .SEGOEUI_FONT)),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                              onTap: () {},
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }

  onSearch() {
    if (isLoading) {
      isLoading = false;
      checkInternetConnection(_partNumberController.text,
          ViewInventoryApi.API_CHK_Inventory_Detail);
    }
  }

  void checkInternetConnection(String value, String type) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Future.delayed(Duration(seconds: AppStrings.DIALOG_TIMEOUT_DURATION),
            () {
          isLoading = true;
          dismissLoader();
        });
        print('check connection-------------->connected');
        if (type == ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE) {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == NetworkConfig.API_GET_DYNAMIC_HELP) {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == ViewInventoryApi.API_CHK_Inventory_Detail) {
          starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }

        if (type == NetworkConfig.API_GET_ROOM_FAC_FOR_LOCATION) {
          // starLoader();
          getData().then((va) {
            getDetails(value, type);
          });
        }
      }
    } on SocketException catch (_) {
      isLoading = true;
      print('check connection-------------->not connected');
      showSnackbarPage(context);
    }
  }

  Future barCodeScan(String type) async {
    try {
      var barcode = await BarcodeScanner.scan();

      if (barcode.rawContent.length == 0) {
        return;
      }
      if (type == ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE) {
        if (isLoading) {
          isLoading = false;
          checkInternetConnection(barcode.rawContent,
              ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE);
        }
        setState(() {
          _partNumberController.text = barcode.rawContent;
        });
      } else if (type == NetworkConfig.API_GET_DYNAMIC_HELP) {
        if (isLoading) {
          isLoading = false;
          checkInternetConnection(
              barcode.rawContent, NetworkConfig.API_GET_DYNAMIC_HELP);
        }
        setState(() {
          binLocationController.text = barcode.rawContent;
        });
      }
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        Utility.showToastMsg("Camera permission not granted");
      } else {
        Utility.showToastMsg("Unknown error: $e");
      }
    }
  }

  void showSnackbarPage(BuildContext context) {
    final snackbar =
        SnackBar(content: Text("Please check your Internet connection"));
    Scaffold.of(context).showSnackBar(snackbar);
  }

  onCancel() {}

  void starLoader() {
    print("dialog started");
    if (!isLoaderRunning) {
      isLoaderRunning = true;
      Dialogs.showLoadingDialog(context, _formLocationKey);
    }
  }

  void dismissLoader() {
    if (isLoaderRunning) {
      isLoaderRunning = false;
      Navigator.of(_formLocationKey.currentContext, rootNavigator: true).pop();
      print("dialog dismissed ");
    }
  }

  Future getDetails(String value, String type) async {
    print("data -->" + value);
    print("user --Id  -->" + userId);
    print("companyCode ---->" + companyCode);
    print("iFacilityCode--->" + iFacilityCode);
    String jsonBody = "";
    String uri = "";
    Map headers = new Map();
    print(NetworkConfig.BASE_URL);

    if (type == ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE) {
      headers = NetworkConfig.headers;
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE;
      jsonBody = ViewInventoryApi.getPartDataForIssueMobile(
          value, facility, companyCode);
    }

    if (type == NetworkConfig.API_GET_DYNAMIC_HELP) {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          NetworkConfig.API_GET_DYNAMIC_HELP;
      jsonBody = NetworkConfig.getDynamicHelp(
          companyCode,
          "roomarea",
          "0",
          "1",
          "1",
          binLocationController.text,
          "",
          "0",
          userId,
          facility.iFacilityCode);
    }

    if (type == ViewInventoryApi.API_CHK_Inventory_Detail) {
      headers = NetworkConfig.headers;
      print(NetworkConfig.BASE_URL);
      uri = NetworkConfig.BASE_URL +
          NetworkConfig.WEB_URL_PATH +
          ViewInventoryApi.API_CHK_Inventory_Detail;
      jsonBody = ViewInventoryApi.chkInventoryDetail(
          companyCode, _partNumberController.text, binLocationController.text);
    }

    print("jsonBody---" + jsonBody);
    print("URI---" + uri);
    final encoding = Encoding.getByName("utf-8");
    Response response =
        await post(uri, headers: headers, body: jsonBody, encoding: encoding)
            .then((resp) {
              isLoading = true;
              print(resp.statusCode);
              if (resp.statusCode == 200) {
                String responseBody = resp.body;
                print("----------" + resp.body);
                onSuccessResponse(type, responseBody);
              } else {
                isLoading = true;
                onErrorResponse(type, resp.body);
              }
            })
            .catchError((error) {
              isLoading = true;
              print(error.toString());
              onErrorResponse(type, error.toString());
            })
            .timeout(Duration(seconds: AppStrings.NETWORK_TIMEOUT_DURATION))
            .catchError((e) {
              isLoading = true;
              dismissLoader();
              Utility.showToastMsg("Error-->" + e.toString());
            });
  }

  void onSuccessResponse(String type, String value) {
    isLoading = true;
    if (type == ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE) {
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        Map data1;
        try {
          data1 = jsonDecode(value);
        } catch (ex) {
          print(ex);
        }
        Map data2;
        try {
          data2 = data1['NewDataSet'];
        } catch (ex) {
          print(ex);
        }
        List<dynamic> data;
        try {
          data = data2['Table'];
          if (data != null && data.length > 0) {
            partList.clear();
            if (data.length > 0) {
              for (int i = 0; i < data.length; i++) {
                Map dat = data.elementAt(i);
                PartDetails temp = PartDetails.fromJson(dat);
                partList.add(temp);
              }
              print(" size of roomfacrLoaction--");
              print(" end --");
              FocusScope.of(context).requestFocus(_binLocationFocus);
              setState(() {});
            } else {
              reset();
              Utility.showToastMsg("No Record Found");
              return;
            }
          }
        } catch (e) {
          FocusScope.of(context).requestFocus(_binLocationFocus);

          partList.clear();
          PartDetails temp = PartDetails.fromJson(data2['Table']);
          partList.add(temp);
          print(" size of partList--" + partList.length.toString());
          print(" end --");
          isRecordVisible = true;
          setState(() {});
        }
        return;
      } else {
        _partNumberController.text = "";
        reset();
        Utility.showToastMsg("Part does not exist");
        return;
      }
    }
    if (type == NetworkConfig.API_GET_DYNAMIC_HELP) {
      isLoading = true;
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        List<dynamic> data = jsonDecode(value);
        print(" --data size-->" + data.length.toString());
        if (data.isEmpty) {
          dynamicHelpList.clear();
          selectedDynamicHelp = null;
          binLocationController.text = "";
          Utility.showToastMsg("does not exist or is inactive!");
          return;
        }
        try {
          for (int i = 0; i < data.length; i++) {
            Map dat = data.elementAt(i);
            DynamicHelp temp = DynamicHelp.fromJson(dat);
            dynamicHelpList.add(temp);
          }
          print(" --dynamic Help List-->" + dynamicHelpList.length.toString());
          selectedDynamicHelp = dynamicHelpList.elementAt(0);
          setState(() {
            setFocus(_binLocationFocus);
          });
        } catch (e) {
          binLocationController.text = "";
          Utility.showToastMsg("Equipment doesnot exist!");
          setState(() {});
          return;
        }
      }
    }
    if (type == ViewInventoryApi.API_CHK_Inventory_Detail) {
      isLoading = true;
      dismissLoader();
      xml.XmlDocument parsedXml = xml.parse(value);
      print(parsedXml);
      value = parsedXml.findElements("string").first.text;
      if (value != "0") {
        Map newDataSet = jsonDecode(value);
        Map table = newDataSet["NewDataSet"];

        try {
          List<dynamic> data = table["Table"];
          print(" --data size-->" + data.length.toString());
          if (data.isEmpty) {
            Utility.showToastMsg("does not exist or is inactive!");
            return;
          } else {
            inventoryList.clear();
            print(" --data size-->" + data.length.toString());
            for (int i = 0; i < data.length; i++) {
              Map dat = data.elementAt(i);
              InventoryDetail temp = InventoryDetail.fromJson(dat);
              inventoryList.add(temp);
            }
            setState(() {});
          }
        } catch (e) {
          InventoryDetail inventoryDetail =
              InventoryDetail.fromJson(table["Table"]);
          inventoryList.clear();
          inventoryList.add(inventoryDetail);

          setState(() {});
          return;
        }
      } else {
        Utility.showToastMsg("No Records found");
        Utility.showToastMsg("No Records found");
      }
    }
  }

  onErrorResponse(String type, String error) {
    dismissLoader();
    if (type == ViewInventoryApi.API_GET_PART_DATA_FOR_MOBILE) {
      Utility.showToastMsg(error.toString());
      reset();
      return;
    }
  }

  String getPartDescription()
  {
    if(_partNumberController.text.length ==0){
      return " ";
    }
    if (partList.length > 0)
    {
      return partList.elementAt(0).partDescription;
    }

        else {
      return " ";
    }
  }

  String getStockRoom() {
    if (selectedDynamicHelp != null) {
      return selectedDynamicHelp.roomAreaName;
    } else {
      return " ";
    }
  }

  void reset() {
    print("reset call");
    binLocationController.text = "";
    partList.clear();
    isRecordVisible = false;
    setState(() {});
  }

  Future setFocus(FocusNode focus) async {
    Future.delayed(Duration(microseconds: 50), () {
      setState(() {
        FocusScope.of(context).requestFocus(focus);
      });
    });
  }
}
