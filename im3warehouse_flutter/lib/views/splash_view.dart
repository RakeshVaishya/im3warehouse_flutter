import 'dart:async';

import 'package:flutter/material.dart';
import 'package:im3_warehouse/values/app_styles.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: AppStyles.lightTheme(),
      home: SplashView(),
      onGenerateRoute: null,
    );
  }
}

class SplashView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SplashState();
}

class SplashState extends State<SplashView>
    with SingleTickerProviderStateMixin {
  var iconAnimationController;
  var iconAnimation;
/*

  startTimeout() async {
    return Timer(const Duration(seconds: 1), null);
  }
*/

  startTimeout() async {
   // return Timer(const Duration(seconds: 1),handleTimeout );
  }
/*
  void handleTimeout() async {
    await App().getAppPreferences().isPreferenceReady;

    App().getAppPreferences().getLoggedIn().then((isLoggedIn) {

      isLoggedIn
          ? Navigator.pushReplacementNamed(
              context, AppRoutes.APP_ROUTE_DASHBOARD)
          : Navigator.pushReplacementNamed(context, AppRoutes.APP_ROUTE_LOGIN);
    });
  }*/

  @override
  void initState() {
    super.initState();

    iconAnimationController = AnimationController(
        duration: Duration(milliseconds: 1000), vsync: this);
    iconAnimation = CurvedAnimation(
        parent: iconAnimationController, curve: Curves.fastOutSlowIn);
    iconAnimation.addListener(() => this.setState(() {}));
    iconAnimationController.forward();

    startTimeout();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorDark,
      body: Center(
          child: Image(
        image: AssetImage("images/logo.png"),
        width: iconAnimation.value * 180,
        height: iconAnimation.value * 180,
      )),
    );
  }
}
